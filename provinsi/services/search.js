const db = require("../../config/database");
const response = require("../../config/responses");

const service = async (query) => {
  try {
    let provinsi = await db("ref_provinsi")
      .select("*")
      .where("nama_provinsi", "like", `%${query}`);

    if (provinsi.length) {
      return response.success(provinsi, "provinsi");
    } else {
      return response.notFound("provinsi");
    }
  } catch (error) {
    return response.serverError(error);
  }
};

module.exports = service;
