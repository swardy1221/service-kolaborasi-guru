const db = require("../../config/database");
const response = require("../../config/responses");

const service = async () => {
  try {
    let provinsi = await db("ref_provinsi").select("*");

    if (provinsi.length) {
      return response.success(provinsi, "provinsi");
    } else {
      return response.notFound("provinsi");
    }
  } catch (error) {
    return response.serverError(error);
  }
};

module.exports = service;
