const express = require("express");
const router = express.Router();
const getAllController = require("./controllers/getAll");
const getKota = require("./controllers/getKota");

// Route untuk Model Ref-Provinsi
router.get("/", getAllController);
router.get("/:idProvinsi/kota", getKota);

module.exports = router;
