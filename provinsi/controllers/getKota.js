const getKota = require("../../kota/services/getByProvinsi");

let controller = async (req, res) => {
  try {
    let idProvinsi = req.params.idProvinsi || "";
    let hasil = await getKota(idProvinsi);
    res.status(hasil.code).send(hasil);
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = controller;
