require("dotenv").config();
// Update with your config settings.

module.exports = {
  development: {
    client: "mysql2",
    connection: {
      host: process.env.DB_HOST,
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      port: process.env.DB_PORT,
      // host: "localhost",
      // user: "root",
      // password: "",
      // database: "db_kolaborasi_2109",
      // port: 3306,
    },
  },

  staging: {
    client: "mysql2",
    connection: {
      host: "46.17.172.1",
      user: "u143577159_kolaborasi",
      password: "Kolaborasi123",
      database: "u143577159_kolaborasi",
      port: 3306,
    },
    // pool: {
    //   min: 2,
    //   max: 10,
    // },
    // migrations: {
    //   tableName: "knex_migrations",
    // },
  },

  production: {
    client: "mysql2",
    connection: {
      database: "my_db",
      user: "username",
      password: "password",
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "knex_migrations",
    },
  },
};
