const paginationBackup = async (data, options) => {
    const page = parseInt(options.page || 1);
    const limit = parseInt(options.limit || 4);
    const offset = (page - 1) * limit;

    const startIndex = (page - 1) * limit;
    const endIndex = page * limit;

    const result = {};

    if (endIndex < data.length) {
        result.next = {
            page: page + 1,
            limit: limit
        }
    }

    if (startIndex > 0) {
        result.previous = {
            page: page - 1,
            limit: limit
        }
    }

    result.data = data.slice(startIndex, endIndex);
    result.totalItems = data.length;
    return result;
}

module.exports = paginationBackup;