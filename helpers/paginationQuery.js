const db = require('../config/database');

const pagination = async (table, options) => {
    const page = parseInt(options.page);
    const limit = parseInt(options.limit);
    const offset = (page - 1) * limit;

    const endIndex = page * limit;
    const result = {};
    let totalItems = await db(table).count('*', { as: 'totalItems' }).first();
    if (!totalItems) return false
    result.totalItems = totalItems.totalItems;

    if (endIndex < totalItems.totalItems) {
        result.next = {
            page: page + 1,
            limit: limit
        }
    }

    if (offset > 0) {
        result.previous = {
            page: page - 1,
            limit: limit
        }
    }

    result.results = await db(table).select("*").limit(limit).offset(offset);
    // console.log(result)
    return result;
}

module.exports = pagination;