const db = require('../config/database');
const helper = async (idUser) => {
    try {
        //kode_user, nama_lengkap, foto_profile,
        let nama = await db('tb_user_guru')
            .join('tb_user', 'tb_user_guru.id_user', 'tb_user.id_user')
            .where('tb_user_guru.id_user', idUser)
            .select('tb_user.kode_user', 'tb_user_guru.nama_lengkap', 'tb_user_guru.foto_profile')
            .first();
        if (!nama) return false
        return nama;
    } catch (error) {
        return false;
    }
}

module.exports = helper;