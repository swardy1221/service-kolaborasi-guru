const helper = () => {
    const now = new Date();
    const year = now.getFullYear();
    // console.log(year);
    let month = now.getMonth();
    if (month.toString().length < 2) month = '0' + (month + 1)
    // console.log(month)
    let date = now.getDate();
    if (date.toString().length < 2) date = '0' + date
    // console.log(date)
    let hour = now.getHours();
    if (hour.toString().length < 2) hour = '0' + hour
    // console.log(hour)
    let minute = now.getMinutes();
    if (minute.toString().length < 2) minute = '0' + minute
    // console.log(minute)
    let second = now.getSeconds();
    if (second.toString().length < 2) second = '0' + second
    // console.log(second)
    let milisecond = now.getMilliseconds();
    if (milisecond.toString().length < 2) milisecond = '0' + milisecond
    // console.log(milisecond)
    console.log(year.toString() + month.toString() + date.toString() + hour.toString() + minute.toString() + second.toString());
    return year.toString() + month.toString() + date.toString() + hour.toString() + minute.toString() + second.toString() + milisecond.toString()
}

module.exports = helper;