//jumlahkan view, likes dan download
//urutkan model berdasarkan attribute itu
// @ts-check

const helper = async (data, kategori) => {
    let updatedData = await sumScore(data);
    let sortedData = await sortData(updatedData);
    if (kategori == 'gabungan') return sortedData.slice(0, 4);
    return sortedData;
}

const sumScore = async (data) => {
    data.map((item) => {
        item.score = item.dilihat_sebanyak + item.jumlah_like + item.diunduh_sebanyak;
    })
    return data;
}

const sortData = async (data) => {
    data.sort(compareScore)
    return data;
}

const compareScore = (a, b) => {
    return b.score - a.score;
}

module.exports = helper;