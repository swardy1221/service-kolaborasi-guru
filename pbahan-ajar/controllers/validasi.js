const response = require('../../config/responses');
const {
    getDetailById,
    validasi
} = require('../services/index');

const controller = async (req, res) => {
    try {
        let { idBahanAjar } = req.params || "";
        let { idUser } = req.user || "";
        let { catatanValidasi, statusValidasi } = req.body || "";

        let cekBahanAjar = await getDetailById(idBahanAjar);
        if (!cekBahanAjar) return res.status(404).send(response.notFound('bahan ajar'));
        if (cekBahanAjar.status_dokumen != 'revisi_final') return res.status(403).send(response.forbidden('status belum final'));
        let updateBahanAjar = await validasi(idBahanAjar, idUser, catatanValidasi, statusValidasi);
        if (!updateBahanAjar) return res.status(400).send(response.badRequest('gagal memvalidasi bahan ajar'));

        return res.status(200).send(response.updated(updateBahanAjar, 'bahan ajar'));
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;