const response = require('../../config/responses');
const { softDelete, getDetailById } = require('../services/index');

const controller = async (req, res) => {
    try {
        let { idBahanAjar } = req.params || "";
        let cekBahanAjar = await getDetailById(idBahanAjar);
        if (!cekBahanAjar) return res.status(404).send(response.notFound('bahan ajar'));
        let deletedBahanAjar = await softDelete(idBahanAjar);
        if (!deletedBahanAjar) return res.status(400).send(response.badRequest('gagal menghapus bahan ajar'));
        return res.status(200).send(response.customSuccess('berhasil menghapus bahan ajar'));
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;