const response = require('../../config/responses');
const { riwayatRevisi, getDetailById } = require('../services/index');

const controller = async (req, res) => {
    try {
        let { idBahanAjar } = req.params || "";
        let cekBahanAjar = await getDetailById(idBahanAjar);
        if (!cekBahanAjar) return res.status(404).send(response.notFound('bahan ajar'));

        let riwayat = await riwayatRevisi(idBahanAjar);
        if (!riwayat) return res.status(404).send(response.notFound('riwayat revisi bahan ajar'));
        return res.status(200).send(response.success(riwayat, 'riwayat revisi bahan ajar'));
    } catch (error) {
        return res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;