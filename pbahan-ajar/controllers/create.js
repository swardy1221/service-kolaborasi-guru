const response = require('../../config/responses');
const slugGenerator = require('../../helpers/slugGenerator');
const { create } = require('../services/index');
const crypto = require('crypto');

const controller = async (req, res) => {
    try {
        let now = new Date().getTime();
        let idUser = req.user.idUser;
        let statusDokumen = req.body.statusDokumen || "revisi1";
        let {
            tipeBahanAjar,
            idMataPelajaran,
            namaMataPelajaran,
            idKompetensiKeahlian,
            kompetensiKeahlian,
            judul,
            deskripsi,
            akses,
        } = req.body || "";
        let createdAt = now;
        let updatedAt = now;
        let pernahTerbit = akses == 'terbit' ? 1 : 0;
        let slug = await slugGenerator(judul, 5);
        let kodeRevisi = crypto.randomBytes(10).toString('hex');
        let dataBahan = {
            idUser,
            tipeBahanAjar,
            idMataPelajaran,
            namaMataPelajaran,
            idKompetensiKeahlian,
            kompetensiKeahlian,
            judul,
            slug,
            deskripsi,
            akses,
            statusDokumen,
            pernahTerbit,
            kodeRevisi,
            createdAt,
            updatedAt
        };
        if (!req.files['sampul'][0]) dataBahan.sampul = `/image/default/${tipeBahanAjar}.png`;
        dataBahan.sampul = `/bahan-ajar/sampul/${req.files['sampul'][0]['filename']}`;
        if (!req.files['file'][0]) dataBahan.file = null;
        dataBahan.file = `/bahan-ajar/file/${req.files['file'][0]['filename']}`;

        let createdBahan = await create(dataBahan);
        if (!createdBahan) return res.status(400).send(response.badRequest('gagal membuat bahan ajar'));
        return res.status(200).send(response.created(createdBahan, 'bahan ajar'));
    } catch (error) {
        return res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;