const { getByStatus } = require('../services/index');
const response = require('../../config/responses');

const controller = async (req, res) => {
    try {
        let { status } = req.params || "";
        let bahanAjar = await getByStatus(status);

        if (!bahanAjar) return res.status(404).send(response.notFound('bahan ajar'));
        return res.status(200).send(response.success(bahanAjar, 'bahan ajar'));
    } catch (error) {
        res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;