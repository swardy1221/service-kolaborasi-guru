const response = require('../../config/responses');
const { getDetailBySlug } = require('../services/index');
const slugGenerator = require('../../helpers/slugGenerator');

const controller = async (req, res) => {
    try {
        let { judul } = req.body || "";
        let slug = await slugGenerator(judul, 5);
        let cekJudul = await getDetailBySlug(slug);
        if (cekJudul) return res.status(409).send(response.badRequest('judul sudah digunakan'));
        return res.status(200).send(response.customSuccess('judul dapat digunakan'));
    } catch (error) {
        return res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;