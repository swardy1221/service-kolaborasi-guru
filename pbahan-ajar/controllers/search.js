const response = require('../../config/responses');
const { search } = require('../services/index')

const controller = async (req, res) => {
    try {
        let { query, jenis, idKompetensi, idMataPelajaran, limit, page } = req.query || "";
        //Preprocess Query String
        // encodeURI()
        query = query ? decodeURI(query) : "";
        // console.log(query);
        // if (query && jenis && idKompetensi && idMataPelajaran) {
        //     let perangkat = await search.byAllParams({ query, jenis, idKompetensi, idMataPelajaran }, { limit, page })
        //     if (!perangkat) return res.status(404).send(response.notFound('perangkat pembelajaran'));
        //     return res.status(200).send(response.success(perangkat, 'perangkat pembelajaran'));
        // } else if (query) {
        //     let perangkat = await search.byQuery(query, { limit, page });
        //     if (!perangkat) return res.status(404).send(response.notFound('perangkat pembelajaran'));
        //     return res.status(200).send(response.success(perangkat, 'perangkat pembelajaran'))
        // }
        if (jenis) {
            console.log('jenis')
            if (idKompetensi) {
                console.log('kompetensi')
                if (idMataPelajaran) {
                    console.log('mapel')
                    if (query) {
                        console.log('query')
                        //jenis,kompetensi,mapel,query
                        let perangkat = await search.byAllParams({ query, jenis, idKompetensi, idMataPelajaran }, { limit, page })
                        if (!perangkat) return res.status(404).send(response.notFound('bahan ajar'));
                        return res.status(200).send(response.success(perangkat, 'bahan ajar'));
                    } else {
                        //jenis,kompetensi,mapel
                        let perangkat = await search.byJenisKompetensiMapel({ jenis, idKompetensi, idMataPelajaran }, { limit, page })
                        if (!perangkat) return res.status(404).send(response.notFound('bahan ajar'));
                        return res.status(200).send(response.success(perangkat, 'bahan ajar'));
                    }
                } else {
                    if (query) {
                        //jenis,kompetensi,query
                        let perangkat = await search.byJenisKompetensiQuery({ query, jenis, idKompetensi }, { limit, page })
                        if (!perangkat) return res.status(404).send(response.notFound('bahan ajar'));
                        return res.status(200).send(response.success(perangkat, 'bahan ajar'));
                    } else {
                        //jenis,kompetensi
                        let perangkat = await search.byJenisKompetensi({ jenis, idKompetensi }, { limit, page })
                        if (!perangkat) return res.status(404).send(response.notFound('bahan ajar'));
                        return res.status(200).send(response.success(perangkat, 'bahan ajar'));
                    }
                }
            } else {
                if (idMataPelajaran) {
                    if (query) {
                        //jenis,mapel,query
                        let perangkat = await search.byJenisMapelQuery({ jenis, idMataPelajaran, query }, { limit, page })
                        if (!perangkat) return res.status(404).send(response.notFound('bahan ajar'));
                        return res.status(200).send(response.success(perangkat, 'bahan ajar'));
                    } else {
                        //jenis,mapel
                        let perangkat = await search.byJenisMapel({ jenis, idMataPelajaran }, { limit, page })
                        if (!perangkat) return res.status(404).send(response.notFound('bahan ajar'));
                        return res.status(200).send(response.success(perangkat, 'bahan ajar'));
                    }
                } else {
                    if (query) {
                        //jenis,query
                        let perangkat = await search.byJenisQuery({ jenis, query }, { limit, page })
                        if (!perangkat) return res.status(404).send(response.notFound('bahan ajar'));
                        return res.status(200).send(response.success(perangkat, 'bahan ajar'));
                    } else {
                        //jenis
                        let perangkat = await search.byJenis({ jenis }, { limit, page })
                        if (!perangkat) return res.status(404).send(response.notFound('bahan ajar'));
                        return res.status(200).send(response.success(perangkat, 'bahan ajar'));
                    }
                }
            }
        } else {
            if (idKompetensi) {
                if (idMataPelajaran) {
                    if (query) {
                        //kompetensi,mapel,query
                        let perangkat = await search.byKompetensiMapelQuery({ query, idKompetensi, idMataPelajaran }, { limit, page })
                        if (!perangkat) return res.status(404).send(response.notFound('bahan ajar'));
                        return res.status(200).send(response.success(perangkat, 'bahan ajar'));
                    } else {
                        //kompetensi,mapel
                        let perangkat = await search.byKompetensiMapel({ idKompetensi, idMataPelajaran }, { limit, page })
                        if (!perangkat) return res.status(404).send(response.notFound('bahan ajar'));
                        return res.status(200).send(response.success(perangkat, 'bahan ajar'));
                    }
                } else {
                    if (query) {
                        //kompetensi,query
                        let perangkat = await search.byKompetensiQuery({ idKompetensi, query }, { limit, page })
                        if (!perangkat) return res.status(404).send(response.notFound('bahan ajar'));
                        return res.status(200).send(response.success(perangkat, 'bahan ajar'));
                    } else {
                        //kompetensi
                        let perangkat = await search.byKompetensi({ idKompetensi }, { limit, page })
                        if (!perangkat) return res.status(404).send(response.notFound('bahan ajar'));
                        return res.status(200).send(response.success(perangkat, 'bahan ajar'));
                    }
                }
            } else {
                if (idMataPelajaran) {
                    if (query) {
                        //mapel,query
                        let perangkat = await search.byMapelQuery({ idMataPelajaran, query }, { limit, page })
                        if (!perangkat) return res.status(404).send(response.notFound('bahan ajar'));
                        return res.status(200).send(response.success(perangkat, 'bahan ajar'));
                    } else {
                        //mapel
                        let perangkat = await search.byMapel({ idMataPelajaran }, { limit, page })
                        if (!perangkat) return res.status(404).send(response.notFound('bahan ajar'));
                        return res.status(200).send(response.success(perangkat, 'bahan ajar'));
                    }
                } else {
                    if (query) {
                        //query
                        let perangkat = await search.byQuery(query, { limit, page });
                        if (!perangkat) return res.status(404).send(response.notFound('bahan ajar'));
                        return res.status(200).send(response.success(perangkat, 'bahan ajar'))
                    } else {
                        //no params
                        let perangkat = await search.byNoParams({ limit, page });
                        if (!perangkat) return res.status(404).send(response.notFound('bahan ajar'));
                        return res.status(200).send(response.success(perangkat, 'bahan ajar'))
                    }
                }
            }
        }
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;