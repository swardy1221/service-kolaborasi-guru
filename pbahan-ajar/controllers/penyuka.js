const response = require('../../config/responses');
const { getDetailById, penyuka } = require('../services/index');

const controller = async (req, res) => {
    try {
        let { idBahanAjar } = req.params || "";
        let cekBahanAjar = await getDetailById(idBahanAjar);
        if (!cekBahanAjar) return res.status(404).send(response.notFound('bahan ajar'));

        let listPenyuka = await penyuka(idBahanAjar);
        if (!listPenyuka) return res.status(404).send(response.notFound('penyuka bahan ajar'));
        return res.status(200).send(response.success(listPenyuka, 'penyuka bahan ajar'));
    } catch (error) {
        return res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;