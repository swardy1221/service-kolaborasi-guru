const { getByTipe } = require('../services/index');
const response = require('../../config/responses');

const controller = async (req, res) => {
    try {
        let idUser = req.user.idUser || "";
        let tipe = req.params.tipe || "";
        let bahanAjar = await getByTipe(idUser, tipe);
        if (!bahanAjar) return res.status(404).send(response.notFound('bahan ajar'));
        return res.status(200).send(response.success(bahanAjar, 'bahan ajar'));
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;