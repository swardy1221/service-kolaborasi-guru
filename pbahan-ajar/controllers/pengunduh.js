const response = require('../../config/responses');
const { getDetailById, pengunduh } = require('../services/index');

const controller = async (req, res) => {
    try {
        let { idBahanAjar } = req.params || "";
        let cekBahanAjar = await getDetailById(idBahanAjar);
        if (!cekBahanAjar) return res.status(404).send(response.notFound('bahan ajar'));
        console.log('controller');
        let listPengunduh = await pengunduh(idBahanAjar);
        if (!listPengunduh) return res.status(404).send(response.notFound('pengunduh bahan ajar'));
        return res.status(200).send(response.success(listPengunduh, 'pengunduh bahan ajar'));
    } catch (error) {
        return res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;