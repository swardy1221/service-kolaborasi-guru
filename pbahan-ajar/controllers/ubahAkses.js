const response = require('../../config/responses');
const {
    getDetailById,
    ubahAkses,
} = require('../services/index');

const controller = async (req, res) => {
    try {
        let { idBahanAjar } = req.params || "";

        let cekBahanAjar = await getDetailById(idBahanAjar);
        if (!cekBahanAjar) return res.status(404).send(response.notFound('bahan ajar'));
        let akses = cekBahanAjar.akses == 'draft' ? 'terbit' : 'draft';
        let ubahAksesBahanAjar = await ubahAkses(idBahanAjar, akses);
        if (!ubahAksesBahanAjar) return res.status(400).send(response.badRequest('gagal mengubah akses bahan ajar'));

        let updatedBahanAjar = await getDetailById(idBahanAjar);
        return res.status(200).send(response.updated(updatedBahanAjar, 'bahan ajar'));
    } catch (error) {
        return res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;