const { getDetailBySlug, getKomentarById, viewCounter } = require('../services/index');
const namaPenulis = require('../../helpers/creatorName');
const response = require('../../config/responses');

const controller = async (req, res) => {
    try {
        let { slug } = req.params || "";
        let counterView = await viewCounter(slug);
        // if (!counterView) return res.status(400).send(response.badRequest('gagal memuat data'));
        let bahan = await getDetailBySlug(slug);
        if (!bahan) return res.status(404).send(response.notFound('bahan ajar'));
        let pembuat = await namaPenulis(bahan.id_user)
        let komentar = await getKomentarById(bahan.id_bahan_ajar);
        let results = {
            ...bahan,
            pembuat,
            komentar,
        }
        return res.status(200).send({
            code: 200,
            message: 'data ditemukan',
            results
        })
    } catch (error) {
        return res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;