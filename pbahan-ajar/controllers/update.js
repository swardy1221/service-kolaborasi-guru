const {
    update,
    getDetailById
} = require('../services/index');
const slugGenerator = require('../../helpers/slugGenerator');
const response = require('../../config/responses');
const crypto = require('crypto');

const controller = async (req, res) => {
    try {
        let { idBahanAjar } = req.params || "";
        let cekBahan = await getDetailById(idBahanAjar);
        if (!cekBahan) return res.status(404).send(response.notFound('bahan ajar'));

        let now = new Date().getTime();
        let idUser = req.user.idUser;
        let {
            tipeBahanAjar,
            idMataPelajaran,
            namaMataPelajaran,
            idKompetensiKeahlian,
            kompetensiKeahlian,
            judul,
            deskripsi,
            akses,
            statusDokumen,
            catatanRevisi
        } = req.body || "";

        //Cek status_dokumen harus berubah/berbeda dengan sebelumnya
        if (cekBahan.pernah_terbit == 1 && akses == 'terbit' && cekBahan.status_dokumen <= statusDokumen) return res.status(400).send(response.badRequest('status dokumen harus berubah'));
        let createdAt = now;
        let updatedAt = now;
        let slug = await slugGenerator(judul, 5);
        let kodeRevisi = crypto.randomBytes(10).toString('hex');

        let dataBahan = {
            idUser,
            idBahanAjar,
            tipeBahanAjar,
            idMataPelajaran,
            namaMataPelajaran,
            idKompetensiKeahlian,
            kompetensiKeahlian,
            judul,
            slug,
            deskripsi,
            akses,
            statusDokumen,
            catatanRevisi,
            kodeRevisi,
            createdAt,
            updatedAt
        };

        //Jika belum pernah terbit, dan akses == 'terbit', maka di update agar pernah_terbit = 1
        if (cekBahan.pernah_terbit == 0 && akses == 'terbit') {
            dataBahan.pernahTerbit = 1;
        }

        if (!req.files['sampul'][0]) dataBahan.sampul = `/image/default/${tipeBahanAjar}.png`;
        dataBahan.sampul = `/bahan-ajar/sampul/${req.files['sampul'][0]['filename']}`;
        if (!req.files['file'][0]) dataBahan.file = null;
        dataBahan.file = `/bahan-ajar/file/${req.files['file'][0]['filename']}`;

        let updatedBahan = await update(dataBahan);
        if (!updatedBahan) return res.status(400).send(response.badRequest('gagal mengubah bahan ajar'));
        return res.status(200).send(response.updated(updatedBahan, 'bahan ajar'));
    } catch (error) {
        return res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;