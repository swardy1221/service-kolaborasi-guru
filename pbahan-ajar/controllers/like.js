const { like, getDetailById } = require('../services/index');
const response = require('../../config/responses');

const controller = async (req, res) => {
    try {
        let idUser = req.user.idUser;
        let { idBahanAjar } = req.params || "";

        let cekBahan = await getDetailById(idBahanAjar);
        if (!cekBahan) return res.status(404).send(response.notFound('bahan ajar'));

        let updateBahan = await like(idUser, idBahanAjar);
        if (!updateBahan) return res.status(400).send(response.badRequest('gagal like/unlike bahan ajar'));

        return res.status(200).send(response.customSuccess('berhasil like/unlike bahan ajar'));
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;