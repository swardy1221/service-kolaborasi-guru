const { getKomentarById, getDetailById } = require('../services/index');
const response = require('../../config/responses');

const controller = async (req, res) => {
    try {
        let { idBahanAjar } = req.params || "";
        let cekBahan = await getDetailById(idBahanAjar);
        if (!cekBahan) return res.status(404).send(response.notFound('bahan ajar'));
        let komentar = await getKomentarById(idBahanAjar);
        if (!komentar) return res.status(404).send(response.notFound('komentar'));
        return res.status(200).send(response.success(komentar, 'komentar'));
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;