const response = require('../../config/responses');
const { getDetailById, downloadCounter } = require('../services/index');
const path = require('path')

const controller = async (req, res) => {
    try {
        let { idBahanAjar } = req.params || "";
        let idUser = req.user.idUser;

        let cekBahan = await getDetailById(idBahanAjar);
        if (!cekBahan) return res.status(404).send(response.notFound('bahan ajar'));

        let fullPath = path.join(__baseDir, `public`, cekBahan.file);
        // console.log(fullPath);
        // let fileName = perangkat.file.split(`/`)[3];
        // console.log(fileName)
        return res.download(fullPath, async (error) => {
            if (error) return res.status(500).send(response.serverError(error))
            return await downloadCounter({ idBahanAjar, idUser });
        });
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;