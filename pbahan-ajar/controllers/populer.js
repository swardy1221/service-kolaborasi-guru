const response = require('../../config/responses');
const filterPopuler = require('../../helpers/populer');
const dataPaginator = require('../../helpers/paginationData');
const { populer } = require('../services/index')

const controller = async (req, res) => {
    try {
        let { limit, page } = req.query || "";
        let { kategori } = req.params || "";
        let bahanAjar = await populer(kategori);
        if (!bahanAjar.length) return res.status(404).send(response.notFound('bahan ajar'));
        let filteredBahan = await filterPopuler(bahanAjar, kategori);
        if (kategori == 'gabungan') {
            return res.status(200).send(response.success(filteredBahan, 'bahan ajar'));
        }
        let paginatedBahan = await dataPaginator(filteredBahan, { limit, page });
        return res.status(200).send(response.success(paginatedBahan, kategori));
        switch (req.params.kategori) {
            case 'gabungan':
                let perangkatPembelajaran = await populer('gabungan');
                if (!perangkatPembelajaran) return res.status(404).send(response.notFound('perangkat pembelajaran'));

                let filtered = await filterPopuler(perangkatPembelajaran, 'gabungan');
                return res.status(200).send(response.success(filtered, 'perangkat pembelajaran'));

            case 'rpp':
                let rpp = await populer('rpp');
                if (!rpp) return res.status(404).send(response.notFound('perangkat pembelajaran'));

                let filteredRpp = await filterPopuler(rpp, 'rpp');
                let paginatedRpp = await dataPaginator(filteredRpp, { limit, page });
                return res.status(200).send(response.success(paginatedRpp, 'rpp'));

            case 'silabus':
                let silabus = await populer('silabus');
                if (!silabus) return res.status(404).send(response.notFound('perangkat pembelajaran'));

                let filteredSilabus = await filterPopuler(silabus, 'silabus');
                let paginatedSilabus = await dataPaginator(filteredSilabus, { limit, page });
                return res.status(200).send(response.success(paginatedSilabus, 'silabus'));

            case 'prota':
                let prota = await populer('prota');
                if (!prota) return res.status(404).send(response.notFound('perangkat pembelajaran'));

                let filteredProta = await filterPopuler(prota, 'prota');
                let paginatedProta = await dataPaginator(filteredProta, { limit, page });
                return res.status(200).send(response.success(paginatedProta, 'prota'));

            case 'prosem':
                let prosem = await populer('prosem');
                if (!prosem) return res.status(404).send(response.notFound('perangkat pembelajaran'));

                let filteredProsem = await filterPopuler(prosem, 'prosem');
                let paginatedProsem = await dataPaginator(filteredProsem, { limit, page });
                return res.status(200).send(response.success(paginatedProsem, 'prosem'));

            case 'lainnya':
                let lainnya = await populer('lainnya');
                if (!lainnya) return res.status(404).send(response.notFound('perangkat pembelajaran'));

                let filteredLainnya = await filterPopuler(lainnya, 'lainnya');
                let paginatedLainnya = await dataPaginator(filteredLainnya, { limit, page });
                return res.status(200).send(response.success(paginatedLainnya, 'lainnya'));

            default:
                break;
        }
        return res.status(404).send(response.notFound('kategori'));
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;