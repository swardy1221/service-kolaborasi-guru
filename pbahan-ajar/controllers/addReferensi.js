const response = require('../../config/responses');
const {
    addReferensi,
    getDetailBySlug,
    getDetailById
} = require('../services/index');
const slugGenerator = require('../../helpers/slugGenerator');

const controller = async (req, res) => {
    try {
        let { idBahanAjar } = req.params || "";
        let { referensi } = req.body || "";

        let cekBahan = await getDetailById(idBahanAjar);
        if (!cekBahan) return res.status(404).send(response.notFound('bahan ajar'))

        //loop over referensi
        //hasilnya ada found, not found
        //cek apakah ada not_found, jika ada, return error not found
        //jika found semua, lempar ke service utk di query
        // console.log(typeof referensi)
        let result = {};
        result.found = [];
        result.notFound = [];
        let cekLoop = 0;
        // console.log(referensi)
        if (typeof referensi == 'object') {
            referensi = [...new Set(referensi)]
            // console.log(referensi)
            for (data of referensi) {
                // console.log(cekLoop);
                let slug = await slugGenerator(data, 5);
                // console.log(slug);
                let cek = await getDetailBySlug(await slugGenerator(data, 5));
                // console.log(cek)
                if (!cek) {
                    result.notFound.push(data);
                    break;
                }
                result.found.push(cek);
                // return result;
                cekLoop++;
            }
        } else {
            let slug = await slugGenerator(referensi, 5);
            // console.log(slug);
            let cek = await getDetailBySlug(await slugGenerator(referensi, 5));
            // console.log(cek)
            if (!cek) {
                result.notFound.push(referensi);
                // break;
            }
            result.found.push(cek);
        };
        // console.log(referensi)

        // console.log(result)
        if (result.notFound.length > 0) return res.status(404).send(response.notFound(`referensi dengan judul: "${result.notFound[0]}" tidak ditemukan`));
        //jika ditemukan semua, lempar ke service untuk di query
        for (dataReferensi of result.found) {
            let createReferensi = await addReferensi({
                entitas: 'bahan_ajar',
                idEntitas: idBahanAjar,
                idEntitasReferensi: dataReferensi.id_bahan_ajar,
                judulReferensi: dataReferensi.judul
            })
            if (!createReferensi) return res.status(400).send(response.badRequest('gagal menambahkan referensi'));
        }
        return res.status(201).send(response.customSuccess('berhasil menambahkan referensi'));
    } catch (error) {
        return res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;