const db = require('../../config/database');

const service = async (idBahan) => {
    try {
        let bahan = await db('tb_bahan_ajar').where('id_bahan_ajar', idBahan).where('deleted_at', null).first();
        if (!bahan) return false
        return bahan;
    } catch (error) {
        throw error;
    }
}

module.exports = service;