const db = require('../../config/database');

const service = async (slug) => {
    const trxProvider = db.transactionProvider();
    const trx = await trxProvider();
    try {
        let counterView = await trx('tb_bahan_ajar').where('slug', slug)
            .increment('dilihat_sebanyak', 1);
        if (!counterView) {
            trx.rollback();
            return false
        }
        await trx.commit();
        return true
    } catch (error) {
        throw error;
    }
}

module.exports = service;