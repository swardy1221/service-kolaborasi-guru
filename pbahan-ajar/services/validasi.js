const db = require('../../config/database');

const service = async (idBahanAjar, idUser, catatanValidasi, statusValidasi) => {
    const trxProvider = db.transactionProvider();
    const trx = await trxProvider();
    try {
        let updateBahanAjar = await trx('tb_bahan_ajar').where('id_bahan_ajar', idBahanAjar).update({
            divalidasi_oleh: idUser,
            status_dokumen: statusValidasi,
            catatan_validasi: catatanValidasi,
            updated_at: new Date().getTime(),
        })

        if (!updateBahanAjar) {
            await trx.rollback();
            return false
        }

        await trx.commit();
        let updatedBahanAjar = await db('tb_bahan_ajar').where('id_bahan_ajar', idBahanAjar).first();
        return updatedBahanAjar;
    } catch (error) {
        await trx.rollback();
        throw error;
    }
}

module.exports = service;