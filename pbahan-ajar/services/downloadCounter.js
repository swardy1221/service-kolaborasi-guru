const db = require('../../config/database');

const service = async (data) => {
    const trxProvider = db.transactionProvider();
    const trx = await trxProvider();
    try {
        let logUnduh = await trx('tb_user_unduh_bahan_ajar').insert({
            id_user: data.idUser,
            id_bahan_ajar: data.idBahanAjar,
            created_at: new Date().getTime()
        })

        if (!logUnduh) {
            await trx.rollback()
            return false
        }

        let counterUnduh = await db('tb_bahan_ajar').transacting(trx).where('id_bahan_ajar', data.idBahanAjar).increment('diunduh_sebanyak', 1);
        if (!counterUnduh) {
            await trx.rollback();
            return false
        }

        await trx.commit()
        return true;
    } catch (error) {
        throw error;
    }
}

module.exports = service;