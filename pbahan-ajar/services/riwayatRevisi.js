const db = require('../../config/database');

const service = async (idBahanAjar) => {
    try {
        let riwayat = await db('tb_riwayat_revisi_bahan_ajar').where({
            id_bahan_ajar: idBahanAjar
        });
        if (!riwayat) return false
        return riwayat
    } catch (error) {
        throw error
    }
}

module.exports = service;