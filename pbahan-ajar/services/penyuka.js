const db = require('../../config/database');

const service = async (idBahanAjar) => {
    try {
        let listIdQuery = db('tb_user_like_bahan_ajar').select('id_user').where('id_bahan_ajar', idBahanAjar);

        let penyuka = await db('tb_user')
            .join('tb_user_like_bahan_ajar', 'tb_user.id_user', 'tb_user_like_bahan_ajar.id_user')
            .join('tb_user_guru', 'tb_user.id_user', 'tb_user_guru.id_user')
            .select('tb_user_guru.id_user', 'tb_user_guru.nama_lengkap', 'tb_user_guru.nuptk', 'tb_user_guru.nama_sekolah', 'tb_user_guru.foto_profile', 'tb_user_like_bahan_ajar.created_at')
            .where('tb_user_like_bahan_ajar.id_bahan_ajar', idBahanAjar)
            .whereIn('tb_user.id_user', listIdQuery);
        if (!penyuka.length) return false;
        return penyuka;
    } catch (error) {
        throw error;
    }
}
module.exports = service;