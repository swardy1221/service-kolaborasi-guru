const db = require('../../config/database');

const service = async (idBahanAjar, akses) => {
    try {
        let ubahAkses = await db('tb_bahan_ajar').where('id_bahan_ajar', idBahanAjar).update({
            akses
        });

        if (!ubahAkses) return false;
        return true;
    } catch (error) {
        throw error;
    }
}

module.exports = service;