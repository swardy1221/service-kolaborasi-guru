const db = require('../../config/database');

const service = async (idBahanAjar) => {
    try {
        let deleteBahanAjar = await db('tb_bahan_ajar').where('id_bahan_ajar', idBahanAjar).update({
            deleted_at: new Date().getTime()
        });
        if (!deleteBahanAjar) return false
        return true
    } catch (error) {
        throw error;
    }
}

module.exports = service;