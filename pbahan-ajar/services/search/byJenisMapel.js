const db = require('../../../config/database');

const service = async (params, options) => {
    try {
        const page = parseInt(options.page || 1);
        const limit = parseInt(options.limit || 8);
        const offset = (page - 1) * limit;
        let bahanAjar = await db('tb_bahan_ajar').where({
            tipe_bahan_ajar: params.jenis,
            id_mata_pelajaran: params.idMataPelajaran,
        }).limit(limit)
            .offset(offset);
        if (!bahanAjar.length) return false;
        return bahanAjar;
    } catch (error) {
        throw error;
    }
}

module.exports = service;