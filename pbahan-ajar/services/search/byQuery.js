const db = require('../../../config/database');

const service = async (query, options) => {
    try {
        const page = parseInt(options.page || 1);
        const limit = parseInt(options.limit || 8);
        const offset = (page - 1) * limit;
        let bahanAjar = await db('tb_bahan_ajar')
            .where('judul', 'like', `%${query}%`)
            .orWhere('deskripsi', 'like', `%${query}%`)
            .limit(limit)
            .offset(offset);
        if (!bahanAjar.length) return false;
        return bahanAjar;
    } catch (error) {
        throw error;
    }
}

module.exports = service;