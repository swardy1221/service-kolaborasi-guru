const db = require('../../config/database');

const service = async (idUser, tipe) => {
    try {
        let bahanAjar = await db('tb_bahan_ajar').where({ tipe_bahan_ajar: tipe, id_user: idUser, deleted_at: null });
        if (!bahanAjar.length) return false;
        return bahanAjar;
    } catch (error) {
        throw error
    }
}

module.exports = service;