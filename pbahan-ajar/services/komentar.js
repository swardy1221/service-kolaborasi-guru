const db = require('../../config/database');

const service = async (idUser, idBahanAjar, komentar) => {
    const trxProvider = db.transactionProvider();
    const trx = await trxProvider();
    try {
        let createKomentar = await trx('tb_user_komentar_bahan_ajar').insert({
            id_user: idUser,
            id_bahan_ajar: idBahanAjar,
            komentar,
            created_at: new Date().getTime()
        });
        console.log(createKomentar)
        if (!createKomentar) {
            trx.rollback()
            return false;
        }

        let counterKomentar = await db('tb_bahan_ajar').transacting(trx).where('id_bahan_ajar', idBahanAjar).increment('jumlah_komentar', 1);
        console.log(counterKomentar)
        if (!counterKomentar) {
            trx.rollback();
            return false;
        }
        await trx.commit()
        let createdKomentar = await db('tb_user_komentar_bahan_ajar').where({
            id_user: idUser,
            id_bahan_ajar: idBahanAjar,
        }).orderBy('created_at', 'desc').first();
        console.log(createdKomentar)
        return createdKomentar;
    } catch (error) {
        await trx.rollback();
        throw error;
    }
}

module.exports = service;