const db = require('../../config/database');
const pembuatHelper = require('../../helpers/creatorName');

const service = async (status) => {
    try {
        let bahanAjar = await db('tb_bahan_ajar').where('status_dokumen', status);
        if (!bahanAjar.length) return false;
        for (bahan of bahanAjar) {
            bahan.pembuat = await pembuatHelper(bahan.id_user)
        }
        return bahanAjar;
    } catch (error) {
        throw error;
    }
}

module.exports = service;