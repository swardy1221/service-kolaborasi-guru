const db = require('../../config/database');

const service = async (dataReferensi) => {
    const trxProvider = db.transactionProvider();
    const trx = await trxProvider();
    try {
        let createReferensi = await trx('tb_referensi').insert({
            entitas: dataReferensi.entitas,
            id_entitas: dataReferensi.idEntitas,
            id_entitas_referensi: dataReferensi.idEntitasReferensi,
            judul_referensi: dataReferensi.judulReferensi,
            created_at: new Date().getTime()
        });

        //Query counter
        let counter = await db('tb_bahan_ajar').transacting(trx).where('id_bahan_ajar', dataReferensi.idEntitas).increment('dirujuk_sebanyak', 1);

        //Query create notification
        if (!createReferensi && !counter) {
            await trx.rollback()
            return false
        }
        await trx.commit();
        return createReferensi;
    } catch (error) {
        await trx.rollback();
        throw error;
    }
}

module.exports = service;