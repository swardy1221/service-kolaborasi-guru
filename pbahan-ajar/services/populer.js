const db = require('../../config/database');
const pembuatHelper = require('../../helpers/creatorName');
// console.log(db);

const service = async (kategori) => {
    try {
        let bahans = kategori == 'gabungan' ? await db('tb_bahan_ajar') :
            await db('tb_bahan_ajar').where('tb_bahan_ajar.tipe_bahan_ajar', '=', kategori);
        if (!bahans.length) return false
        for (bahan of bahans) {
            bahan.pembuat = await pembuatHelper(bahan.id_user)
        }
        return bahans;
        switch (kategori) {
            case 'gabungan':
                let gabungan = await db('tb_perangkat_pembelajaran')
                    .join('tb_user_guru', 'tb_perangkat_pembelajaran.id_user', '=', 'tb_user_guru.id_user');
                if (!gabungan.length) return false;
                return gabungan;

            case 'rpp':
                let rpp = await db('tb_perangkat_pembelajaran')
                    .join('tb_user_guru', 'tb_perangkat_pembelajaran.id_user', '=', 'tb_user_guru.id_user')
                    .where('tb_perangkat_pembelajaran.tipe_perangkat_pembelajaran', '=', 'rpp');
                if (!rpp.length) return false;
                return rpp;

            case 'silabus':
                let silabus = await db('tb_perangkat_pembelajaran')
                    .join('tb_user_guru', 'tb_perangkat_pembelajaran.id_user', '=', 'tb_user_guru.id_user')
                    .where('tb_perangkat_pembelajaran.tipe_perangkat_pembelajaran', '=', 'silabus');
                if (!silabus.length) return false;
                return silabus;

            case 'prota':
                let prota = await db('tb_perangkat_pembelajaran')
                    .join('tb_user_guru', 'tb_perangkat_pembelajaran.id_user', '=', 'tb_user_guru.id_user')
                    .where('tb_perangkat_pembelajaran.tipe_perangkat_pembelajaran', '=', 'prota');
                if (!prota.length) return false;
                return prota;

            case 'prosem':
                let prosem = await db('tb_perangkat_pembelajaran')
                    .join('tb_user_guru', 'tb_perangkat_pembelajaran.id_user', '=', 'tb_user_guru.id_user')
                    .where('tb_perangkat_pembelajaran.tipe_perangkat_pembelajaran', '=', 'prosem');
                if (!prosem.length) return false;
                return prosem;

            case 'lainnya':
                let lainnya = await db('tb_perangkat_pembelajaran')
                    .join('tb_user_guru', 'tb_perangkat_pembelajaran.id_user', '=', 'tb_user_guru.id_user')
                    .where('tb_perangkat_pembelajaran.tipe_perangkat_pembelajaran', '=', 'lainnya');
                if (!lainnya.length) return false;
                return lainnya;

            default:
                break;
        }
        return false
    } catch (error) {
        throw error;
    }
}

module.exports = service;