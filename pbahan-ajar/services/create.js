const db = require('../../config/database');

const service = async (dataBahan) => {
    const trxProvider = db.transactionProvider();
    const trx = await trxProvider();
    try {
        let createBahan = await trx('tb_bahan_ajar').insert({
            id_user: dataBahan.idUser,
            tipe_bahan_ajar: dataBahan.tipeBahanAjar,
            id_mata_pelajaran: dataBahan.idMataPelajaran,
            nama_mapel: dataBahan.namaMataPelajaran,
            id_kompetensi_keahlian: dataBahan.idKompetensiKeahlian,
            kompetensi_keahlian: dataBahan.kompetensiKeahlian,
            judul: dataBahan.judul,
            slug: dataBahan.slug,
            deskripsi: dataBahan.deskripsi,
            akses: dataBahan.akses,
            sampul: dataBahan.sampul,
            file: dataBahan.file,
            status_dokumen: dataBahan.statusDokumen,
            pernah_terbit: dataBahan.pernahTerbit,
            created_at: dataBahan.createdAt,
            updated_at: dataBahan.updatedAt
        });

        let createRiwayat = await db('tb_riwayat_revisi_bahan_ajar').transacting(trx).insert({
            id_bahan_ajar: createBahan,
            catatan_revisi: "Initial Revisi",
            kode_revisi: dataBahan.kodeRevisi,
            tipe_bahan_ajar: dataBahan.tipePerangkatPembelajaran,
            id_mata_pelajaran: dataBahan.idMataPelajaran,
            nama_mapel: dataBahan.namaMataPelajaran,
            id_kompetensi_keahlian: dataBahan.idKompetensiKeahlian,
            kompetensi_keahlian: dataBahan.kompetensiKeahlian,
            judul: dataBahan.judul,
            deskripsi: dataBahan.deskripsi,
            sampul: dataBahan.sampul,
            file: dataBahan.file,
            status_dokumen: dataBahan.statusDokumen,
            created_at: dataBahan.createdAt
        });

        if (!createBahan || !createRiwayat) {
            await trx.rollback();
            return false;
        }
        await trx.commit();
        let createdBahan = await db('tb_bahan_ajar').where('id_bahan_ajar', createBahan).first();
        return createdBahan;
    } catch (error) {
        await trx.rollback();
        throw error;
    }
}

module.exports = service;