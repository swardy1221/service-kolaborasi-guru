const db = require('../../config/database');

const service = async (idUser, idBahanAjar) => {
    try {
        const trxProvider = db.transactionProvider();
        const trx = await trxProvider();
        try {
            let statusLike = await db('tb_user_like_bahan_ajar').where({
                id_user: idUser,
                id_bahan_ajar: idBahanAjar
            }).first();

            if (statusLike) {
                //Jika ada, maka unline
                let unlikeBahan = await trx('tb_bahan_ajar').where('id_bahan_ajar', idBahanAjar).decrement('jumlah_like', 1);
                if (!unlikeBahan) {
                    await trx.rollback();
                    return false
                }
                let userUnlike = await db('tb_user_like_bahan_ajar').transacting(trx).where({
                    id_user: idUser,
                    id_bahan_ajar: idBahanAjar
                }).del();
                if (!userUnlike) {
                    await trx.rollback()
                    return false
                }
                await trx.commit()
                return true
            }
            //Jika tidak ada, maka like
            let updateBahan = await trx('tb_bahan_ajar').where('id_bahan_ajar', idBahanAjar).increment('jumlah_like', 1);
            if (!updateBahan) {
                await trx.rollback();
                return false
            }
            let userLike = await db('tb_user_like_bahan_ajar').transacting(trx).insert({
                id_user: idUser,
                id_bahan_ajar: idBahanAjar,
                created_at: new Date().getTime()
            });
            if (!userLike) {
                await trx.rollback()
                return false
            }
            await trx.commit()
            return true
        } catch (error) {
            await trx.rollback();
            throw error;
        }
    } catch (error) {
        throw error;
    }
}

module.exports = service;