const db = require('../../config/database');

const service = async (dataBahan) => {
    const trxProvider = db.transactionProvider();
    const trx = await trxProvider();
    try {
        let updateBahan = dataBahan.pernahTerbit ? await trx('tb_bahan_ajar').where('id_bahan_ajar', dataBahan.idBahanAjar).update({
            tipe_bahan_ajar: dataBahan.tipeBahanAjar,
            id_mata_pelajaran: dataBahan.idMataPelajaran,
            nama_mapel: dataBahan.namaMataPelajaran,
            id_kompetensi_keahlian: dataBahan.idKompetensiKeahlian,
            kompetensi_keahlian: dataBahan.kompetensiKeahlian,
            judul: dataBahan.judul,
            slug: dataBahan.slug,
            deskripsi: dataBahan.deskripsi,
            file: dataBahan.file,
            sampul: dataBahan.sampul,
            status_dokumen: dataBahan.statusDokumen,
            akses: dataBahan.akses,
            pernah_terbit: dataBahan.pernahTerbit
        }) : await trx('tb_bahan_ajar').where('id_bahan_ajar', dataBahan.idBahanAjar).update({
            tipe_bahan_ajar: dataBahan.tipeBahanAjar,
            id_mata_pelajaran: dataBahan.idMataPelajaran,
            nama_mapel: dataBahan.namaMataPelajaran,
            id_kompetensi_keahlian: dataBahan.idKompetensiKeahlian,
            kompetensi_keahlian: dataBahan.kompetensiKeahlian,
            judul: dataBahan.judul,
            slug: dataBahan.slug,
            deskripsi: dataBahan.deskripsi,
            file: dataBahan.file,
            sampul: dataBahan.sampul,
            status_dokumen: dataBahan.statusDokumen,
            akses: dataBahan.akses,
        });

        if (!updateBahan) {
            await trx.rollback();
            return false;
        }

        let logRevisi = await db('tb_riwayat_revisi_bahan_ajar').transacting(trx).insert({
            id_bahan_ajar: dataBahan.idBahanAjar,
            kode_revisi: dataBahan.kodeRevisi,
            catatan_revisi: dataBahan.catatanRevisi,
            tipe_bahan_ajar: dataBahan.tipeBahanAjar,
            id_mata_pelajaran: dataBahan.idMataPelajaran,
            nama_mapel: dataBahan.namaMataPelajaran,
            id_kompetensi_keahlian: dataBahan.idKompetensiKeahlian,
            kompetensi_keahlian: dataBahan.kompetensiKeahlian,
            judul: dataBahan.judul,
            deskripsi: dataBahan.deskripsi,
            file: dataBahan.file,
            sampul: dataBahan.sampul,
            status_dokumen: dataBahan.statusDokumen,
            created_at: dataBahan.createdAt,
        });

        if (!logRevisi) {
            await trx.rollback();
            return false;
        }

        await trx.commit()
        let updatedBahan = await db('tb_bahan_ajar').where('id_bahan_ajar', dataBahan.idBahanAjar).first();
        return updatedBahan;
    } catch (error) {
        throw error;
    }
}

module.exports = service;