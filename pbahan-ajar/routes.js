const express = require("express");
const router = express.Router();

//Import Controllers
const {
    like,
    search,
    update,
    create,
    populer,
    penyuka,
    validasi,
    komentar,
    cekJudul,
    getByTipe,
    unduhById,
    ubahAkses,
    pengunduh,
    softDelete,
    getByStatus,
    addReferensi,
    riwayatRevisi,
    getDetailBySlug,
    getKomentarById,
    riwayatRevisiByKode
} = require('./controllers/index');

//Import Middlewares
const {
    authUser,
    authRole,
    uploader
} = require('../middlewares/index');

/**
 * GET METHOD
 */
router.get('/', search)
router.get('/:slug', getDetailBySlug);
router.get('/:kategori/popular', populer);
router.get('/my/:tipe', authUser, getByTipe);
router.get('/status/:status', authUser, getByStatus);
router.get('/:idBahanAjar/unduh', authUser, unduhById);
router.get('/:idBahanAjar/penyuka', authUser, penyuka);
router.get('/:idBahanAjar/pengunduh', authUser, pengunduh);
router.get('/:idBahanAjar/komentar', authUser, getKomentarById);
router.get('/:idBahanAjar/riwayat-perubahan', authUser, riwayatRevisi);
router.get('/:idBahanAjar/riwayat-perubahan/:kodeRevisi', authUser, riwayatRevisiByKode);

/**
 * POST METHOD
 */
router.post('/', authUser, uploader.bahanAjar.fields([{ name: 'sampul', maxCount: 1 }, { name: 'file', maxCount: 1 }]), create);
router.post('/cek-judul', authUser, cekJudul);
router.post('/:idBahanAjar/like', authUser, like);
router.post('/:idBahanAjar/komentar', authUser, komentar);
router.post('/:idBahanAjar/referensi', authUser, addReferensi);

/**
 * PUT
 */
router.put('/:idBahanAjar', authUser, uploader.bahanAjar.fields([{ name: 'sampul', maxCount: 1 }, { name: 'file', maxCount: 1 }]), update)
router.put('/:idBahanAjar/validasi', authUser, authRole('admin'), validasi);
router.put('/:idBahanAjar/ubah-akses', authUser, ubahAkses);

/**
 * DELTE
 */
router.delete('/:idBahanAjar', authUser, softDelete)

module.exports = router;
