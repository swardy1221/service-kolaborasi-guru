var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var logger = require("morgan");
const helmet = require('helmet');
require("dotenv").config();
global.__baseDir = __dirname;

var router = express.Router();
var indexRouter = require("./routes/index");
var usersRouter = require("./user/routes");
var bidangKeahlianRouter = require("./bidang-keahlian/routes");
var programKeahlianRouter = require("./program-keahlian/routes");
var mataPelajaranRouter = require("./mata-pelajaran/routes");
var kompetensiIntiRouter = require("./kompetensi-inti/routes");
var kompetensiDasarRouter = require("./kompetensi-dasar/routes");
var kompetensiKeahlianRouter = require("./kompetensi-keahlian/routes");
var skDikdasmenRouter = require("./sk-dikdasmen/routes");
var captchaRouter = require("./captcha/routes");
var provinsiRouter = require("./provinsi/routes");
var kotaRouter = require("./kota/routes");
var perangkatPembelajaranRouter = require('./perangkat-pembelajaran/routes');
var bahanAjarRouter = require('./pbahan-ajar/routes');
var kisiKisiRouter = require('./kis-kisi/routes');
var soalRouter = require('./soal/routes');
var paketSOalRouter = require('./paket-soal/routes');
var kecamatanRouter = require('./kecamatan/routes');

var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "hbs");
app.use(helmet());
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/api/v1/", router);
router.use("/", indexRouter);
router.use("/sk", skDikdasmenRouter);
router.use("/user", usersRouter);
router.use("/captcha", captchaRouter);
router.use("/provinsi", provinsiRouter);
router.use("/kota", kotaRouter);
router.use("/kecamatan", kecamatanRouter);
router.use("/kompetensi-inti", kompetensiIntiRouter);
router.use("/kompetensi-dasar", kompetensiDasarRouter);
router.use("/bidang-keahlian", bidangKeahlianRouter);
router.use("/program-keahlian", programKeahlianRouter);
router.use("/mata-pelajaran", mataPelajaranRouter);
router.use("/kompetensi-keahlian", kompetensiKeahlianRouter);
router.use("/perangkat-pembelajaran", perangkatPembelajaranRouter);
router.use('/bahan-ajar', bahanAjarRouter);
router.use('/kisi-kisi', kisiKisiRouter);
router.use('/soal', soalRouter)
router.use('/paket-soal', paketSOalRouter)

app.use((err, req, res, next) => {
  console.log(err);
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
