-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 14, 2020 at 09:02 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kolaborasi_guru`
--

-- --------------------------------------------------------

--
-- Table structure for table `ref_kota`
--

CREATE TABLE `ref_kota` (
  `id_kota` varchar(100) NOT NULL,
  `id_provinsi` varchar(100) DEFAULT NULL,
  `nama_kota` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_kota`
--

INSERT INTO `ref_kota` (`id_kota`, `id_provinsi`, `nama_kota`, `created_at`, `updated_at`, `deleted_at`) VALUES
('016000  ', '010000  ', 'Kota Jakarta Pusat', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('016100  ', '010000  ', 'Kota Jakarta Utara', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('016200  ', '010000  ', 'Kota Jakarta Barat', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('016300  ', '010000  ', 'Kota Jakarta Selatan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('016400  ', '010000  ', 'Kota Jakarta Timur', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('060100  ', '060000  ', 'Kab. Aceh Besar', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('060300  ', '060000  ', 'Kab. Aceh Utara', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('060400  ', '060000  ', 'Kab. Aceh Timur', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('060500  ', '060000  ', 'Kab. Aceh Tengah', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('060600  ', '060000  ', 'Kab. Aceh Barat', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('060700  ', '060000  ', 'Kab. Aceh Selatan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('060800  ', '060000  ', 'Kab. Aceh Tenggara', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('061100  ', '060000  ', 'Kab. Simeulue', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('061200  ', '060000  ', 'Kab. Bireuen', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('061300  ', '060000  ', 'Kab. Aceh Singkil', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('061400  ', '060000  ', 'Kab. Aceh Tamiang', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('061500  ', '060000  ', 'Kab. Nagan Raya', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('061600  ', '060000  ', 'Kab. Aceh Jaya', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('061700  ', '060000  ', 'Kab. Aceh Barat Daya', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('061800  ', '060000  ', 'Kab. Gayo Lues', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('061900  ', '060000  ', 'Kab. Bener Meriah', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('062000  ', '060000  ', 'Kab. Pidie Jaya', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('066000  ', '060000  ', 'Kota Sabang', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('066100  ', '060000  ', 'Kota Banda Aceh', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('066200  ', '060000  ', 'Kota Lhokseumawe', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('066300  ', '060000  ', 'Kota Langsa', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('066400  ', '060000  ', 'Kota Subulussalam', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('220100  ', '220000  ', 'Kab. Buleleng', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('220200  ', '220000  ', 'Kab. Jembrana', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('220300  ', '220000  ', 'Kab. Tabanan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('220500  ', '220000  ', 'Kab. Gianyar', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('220600  ', '220000  ', 'Kab. Klungkung', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('220700  ', '220000  ', 'Kab. Bangli', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('220800  ', '220000  ', 'Kab. Karang Asem', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('226000  ', '220000  ', 'Kota Denpasar', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('﻿016500  ', '010000', 'Kab. Kepulauan Seribu', '0000-00-00 00:00:00', '2020-07-25 15:50:54', NULL),
('﻿060200  ', '060000  ', 'Kab. Pidie', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('﻿220400  ', '220000  ', 'Kab. Badung', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ref_kota`
--
ALTER TABLE `ref_kota`
  ADD PRIMARY KEY (`id_kota`),
  ADD KEY `ref_kota_id_provinsi_foreign` (`id_provinsi`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ref_kota`
--
ALTER TABLE `ref_kota`
  ADD CONSTRAINT `ref_kota_id_provinsi_foreign` FOREIGN KEY (`id_provinsi`) REFERENCES `ref_provinsi` (`id_provinsi`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
