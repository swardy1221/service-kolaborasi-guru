-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 10, 2021 at 09:09 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kolaborasi_guru`
--

-- --------------------------------------------------------

--
-- Table structure for table `knex_migrations`
--

CREATE TABLE `knex_migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `batch` int(11) DEFAULT NULL,
  `migration_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `knex_migrations`
--

INSERT INTO `knex_migrations` (`id`, `name`, `batch`, `migration_time`) VALUES
(1, '20200611202820_create_tb_users.js', 1, '2020-07-12 14:36:13'),
(2, '20200611222608_create_tb_siswa.js', 1, '2020-07-12 14:36:14'),
(3, '20200611224316_create_tb_guru.js', 1, '2020-07-12 14:36:15'),
(4, '20200707080736_create_ref_sk.js', 1, '2020-07-12 14:36:15'),
(5, '20200708084204_create_ref_bidang_keahlian.js', 1, '2020-07-12 14:36:16'),
(6, '20200710081804_create_ref_program_keahlian.js', 1, '2020-07-12 14:36:16'),
(7, '20200710082800_create_ref_kompetensi_keahlian.js', 1, '2020-07-12 14:36:18'),
(8, '20200710083948_create_tb_riwayat_pendidikan.js', 1, '2020-07-12 14:36:19'),
(9, '20200710084427_create_tb_mata_pelajaran.js', 1, '2020-07-12 14:36:21'),
(10, '20200710084512_create_ref_kompetensi_inti.js', 1, '2020-07-12 14:36:21'),
(11, '20200710091518_create_ref_kompetensi_dasar.js', 1, '2020-07-12 14:36:23'),
(12, '20200710092058_create_ref_captcha.js', 1, '2020-07-12 14:36:23'),
(13, '20200710092200_create_ref_provinsi.js', 1, '2020-07-12 14:36:23'),
(14, '20200710092440_create_ref_kota.js', 1, '2020-07-12 14:36:25'),
(15, '20200710111007_create_tb_sekolah.js', 1, '2020-07-12 14:36:26'),
(16, '20200710111847_create_tb_grup.js', 1, '2020-07-12 14:36:27'),
(17, '20200710173203_create_tb_post.js', 1, '2020-07-12 14:36:28'),
(18, '20200710174703_create_tb_kisi_kisi.js', 1, '2020-07-12 14:36:29'),
(19, '20200710175523_create_tb_soal.js', 1, '2020-07-12 14:36:32'),
(20, '20200710181058_create_tb_paket_soal.js', 1, '2020-07-12 14:36:34'),
(21, '20200710183059_create_tb_bahan_ajar.js', 1, '2020-07-12 14:36:36'),
(22, '20200710185217_create_tb_perangkat_pembelajaran.js', 1, '2020-07-12 14:36:38'),
(23, '20200710193610_create_tb_opsi_jawaban.js', 1, '2020-07-12 14:36:39'),
(24, '20200710204105_create_tb_quiz.js', 1, '2020-07-12 14:36:40'),
(25, '20200711173932_create_tb_riwayat_revisi_soal.js', 1, '2020-07-12 14:36:41'),
(26, '20200711174836_create_tb_riwayat_revisi_opsi_jawaban.js', 1, '2020-07-12 14:36:42'),
(27, '20200711180258_create_tb_riwayat_revisi_bahan_ajar.js', 1, '2020-07-12 14:36:42'),
(28, '20200711180358_create_tb_riwayat_revisi_perangkat_pembelajaran.js', 1, '2020-07-12 14:36:44'),
(29, '20200711181825_create_tb_folder.js', 1, '2020-07-12 14:36:45'),
(30, '20200711183245_create_tb_diskusi.js', 1, '2020-07-12 14:36:46'),
(31, '20200711183714_create_tb_tugas.js', 1, '2020-07-12 14:36:47'),
(32, '20200711183844_create_tb_link.js', 1, '2020-07-12 14:36:49'),
(33, '20200711183947_create_tb_file.js', 1, '2020-07-12 14:36:50'),
(34, '20200711184155_create_tb_lampiran.js', 1, '2020-07-12 14:36:51'),
(35, '20200711184330_create_tb_aktivitas_siswa.js', 1, '2020-07-12 14:36:52'),
(36, '20200711185849_create_tb_guru_mengampu_mata_pelajaran.js', 1, '2020-07-12 14:36:54'),
(37, '20200711191942_create_tb_user_follow_user.js', 1, '2020-07-12 14:36:56'),
(38, '20200711193742_create_tb_anggota_grup.js', 1, '2020-07-12 14:36:57'),
(39, '20200711194353_create_tb_user_komentar_post.js', 1, '2020-07-12 14:36:59'),
(40, '20200712095945_create_tb_user_komentar_soal.js', 1, '2020-07-12 14:37:00'),
(41, '20200712100126_create_tb_user_komentar_bahan_ajar.js', 1, '2020-07-12 14:37:01'),
(42, '20200712100226_create_tb_user_komentar_perangkat_pembelajaran.js', 1, '2020-07-12 14:37:03'),
(43, '20200712100316_create_tb_user_komentar_quiz.js', 1, '2020-07-12 14:37:05'),
(44, '20200712100358_create_tb_user_komentar_diskusi.js', 1, '2020-07-12 14:37:06'),
(45, '20200712100437_create_tb_user_komentar_tugas.js', 1, '2020-07-12 14:37:08'),
(46, '20200712100621_create_tb_user_like_post.js', 1, '2020-07-12 14:37:10'),
(47, '20200712100708_create_tb_user_like_soal.js', 1, '2020-07-12 14:37:11'),
(48, '20200712100755_create_tb_user_like_bahan_ajar.js', 1, '2020-07-12 14:37:13'),
(49, '20200712100829_create_tb_user_like_perangkat_pembelajaran.js', 1, '2020-07-12 14:37:15'),
(50, '20200712100921_create_tb_user_dislike_soal.js', 1, '2020-07-12 14:37:17'),
(51, '20200712101303_create_tb_user_dislike_bahan_ajar.js', 1, '2020-07-12 14:37:19'),
(52, '20200712101359_create_tb_user_dislike_perangkat_pembelajaran.js', 1, '2020-07-12 14:37:20'),
(53, '20200712101443_create_tb_user_jawab_soal.js', 1, '2020-07-12 14:37:22'),
(54, '20200712101931_create_tb_paket_soal_berisi_soal.js', 1, '2020-07-12 14:37:24'),
(55, '20200712102201_create_tb_user_mengerjakan_tugas.js', 1, '2020-07-12 14:37:26'),
(56, '20200712102649_create_tb_user_mengerjakan_quiz.js', 1, '2020-07-12 14:37:28'),
(57, '20200712103652_create_tb_user_unduh_paket_soal.js', 1, '2020-07-12 14:37:29'),
(58, '20200712103738_create_tb_user_unduh_bahan_ajar.js', 1, '2020-07-12 14:37:31'),
(59, '20200712103814_create_tb_user_unduh_perangkat_pembelajaran.js', 1, '2020-07-12 14:37:33'),
(60, '20200712103950_create_tb_user_message_user.js', 1, '2020-07-12 14:37:34'),
(61, '20200712104410_create_tb_tipe_entitas_pemberitahuan.js', 1, '2020-07-12 14:37:34'),
(62, '20200712104558_create_tb_objek_pemberitahuan.js', 1, '2020-07-12 14:37:35'),
(63, '20200712105111_create_tb_pelaku_pemberitahuan.js', 1, '2020-07-12 14:37:37'),
(64, '20200712105321_create_tb_penerima_pemberitahuan.js', 1, '2020-07-12 14:37:38'),
(65, '20200712212503_create_tb_quiz_berisi_soal.js', 1, '2020-07-12 14:37:39'),
(66, '20200713104331_create_ref_kecamatan.js', 2, '2020-07-13 03:46:44'),
(67, '20200817000522_add_refresh_token_to_user_table.js', 3, '2020-08-16 17:11:08'),
(68, '20200817155441_add_last_login_to_tb_user.js', 4, '2020-08-17 08:57:35');

-- --------------------------------------------------------

--
-- Table structure for table `knex_migrations_lock`
--

CREATE TABLE `knex_migrations_lock` (
  `index` int(10) UNSIGNED NOT NULL,
  `is_locked` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `knex_migrations_lock`
--

INSERT INTO `knex_migrations_lock` (`index`, `is_locked`) VALUES
(1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ref_bidang_keahlian`
--

CREATE TABLE `ref_bidang_keahlian` (
  `id_bidang_keahlian` int(10) UNSIGNED NOT NULL,
  `id_sk_dikdasmen` int(11) UNSIGNED DEFAULT NULL,
  `kode_bidang_keahlian` varchar(10) NOT NULL,
  `nama_bidang_keahlian` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_bidang_keahlian`
--

INSERT INTO `ref_bidang_keahlian` (`id_bidang_keahlian`, `id_sk_dikdasmen`, `kode_bidang_keahlian`, `nama_bidang_keahlian`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '1', 'Teknologi dan Rekayasa', '2020-07-13 14:42:01', '2020-07-13 14:42:01', NULL),
(2, 1, '2', 'Energi dan Pertambangan', '2020-07-13 14:42:01', '2020-07-13 14:42:01', NULL),
(3, 1, '3', 'Teknologi Informasi dan Komunikasi', '2020-07-13 14:42:01', '2020-07-13 14:42:01', NULL),
(4, 1, '4', 'Kesehatan dan Pekerjaan Sosial', '2020-07-13 14:42:01', '2020-07-13 14:42:01', NULL),
(5, 1, '5', 'Agribisnis dan Agroteknologi', '2020-07-13 14:42:01', '2020-07-13 14:42:01', NULL),
(6, 1, '6', 'Kemaritiman', '2020-07-13 14:42:01', '2020-07-13 14:42:01', NULL),
(7, 1, '7', 'Bisnis dan Manajemen', '2020-07-13 14:42:01', '2020-07-13 14:42:01', NULL),
(8, 1, '8', 'Pariwisata', '2020-07-13 14:42:01', '2020-07-13 14:42:01', NULL),
(9, 1, '9', 'Seni dan Industri Kreatif', '2020-07-13 14:42:01', '2020-07-13 14:42:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ref_captcha`
--

CREATE TABLE `ref_captcha` (
  `id_captcha` int(10) UNSIGNED NOT NULL,
  `pertanyaan` varchar(100) NOT NULL,
  `jawaban` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_captcha`
--

INSERT INTO `ref_captcha` (`id_captcha`, `pertanyaan`, `jawaban`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Berapakah hasil 6 dibagi 3', '2', '2020-07-16 14:40:34', '2020-07-16 14:40:34', NULL),
(2, 'Hasil dari 7 ditambah 4', '11', '2020-07-16 14:40:34', '2020-07-16 14:40:34', NULL),
(3, 'Hasil dari 3 faktorial', '6', '2020-07-16 14:41:49', '2020-07-16 14:41:49', NULL),
(4, 'Hasil dari 5 ditambah 4', '9', '2020-07-16 14:41:49', '2020-07-16 14:41:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ref_kecamatan`
--

CREATE TABLE `ref_kecamatan` (
  `id_kecamatan` varchar(100) NOT NULL,
  `id_kota` varchar(100) DEFAULT NULL,
  `id_provinsi` varchar(100) DEFAULT NULL,
  `nama_kecamatan` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_kecamatan`
--

INSERT INTO `ref_kecamatan` (`id_kecamatan`, `id_kota`, `id_provinsi`, `nama_kecamatan`, `created_at`, `updated_at`, `deleted_at`) VALUES
('010101', '﻿016500  ', '010000', 'Kec. Kepulauan Seribu Selatan', '2020-07-13 10:49:15', '2020-07-13 10:49:15', NULL),
('010102', '﻿016500  ', '010000', 'Kec. Kepulauan Seribu Utara', '2020-07-13 10:49:15', '2020-07-13 10:49:15', NULL),
('016001', '016000  ', '010000', 'Kec. Tanah Abang', '2020-07-13 10:50:34', '2020-07-13 10:50:34', NULL),
('016002', '016000  ', '010000', 'Kec. Menteng', '2020-07-13 10:51:04', '2020-07-13 10:51:04', NULL),
('016003', '016000  ', '010000', 'Kec. Senen', '2020-07-13 10:51:31', '2020-07-13 10:51:31', NULL),
('016004', '016000  ', '010000', 'Kec. Johar Baru', '2020-07-13 10:51:51', '2020-07-13 10:51:51', NULL),
('016005', '016000  ', '010000', 'Kec. Cempaka Putih', '2020-07-13 10:52:13', '2020-07-13 10:52:13', NULL),
('016006', '016000  ', '010000', 'Kec. Kemayoran', '2020-07-13 10:52:52', '2020-07-13 10:52:52', NULL),
('016007', '016000  ', '010000', 'Kec. Sawah Besar', '2020-07-13 10:52:52', '2020-07-13 10:52:52', NULL),
('016008', '016000  ', '010000', 'Kec. Gambir', '2020-07-13 10:53:12', '2020-07-13 10:53:12', NULL),
('016101', '016100  ', '010000', 'Kec. Penjaringan', '2020-07-13 10:54:47', '2020-07-13 10:54:47', NULL),
('016102', '016100  ', '010000', 'Kec. Pademangan', '2020-07-13 10:57:35', '2020-07-13 10:57:35', NULL),
('016103', '016100  ', '010000', 'Kec. Tanjung Priok', '2020-07-13 10:57:35', '2020-07-13 10:57:35', NULL),
('016104', '016100  ', '010000', 'Kec. Koja', '2020-07-13 10:58:12', '2020-07-13 10:58:12', NULL),
('016105', '016100  ', '010000', 'Kec. Kelapa Gading', '2020-07-13 10:58:12', '2020-07-13 10:58:12', NULL),
('016106', '016100  ', '010000', 'Kec. Cilincing', '2020-07-13 10:58:29', '2020-07-13 10:58:29', NULL),
('016201', '016200  ', '010000', 'Kec. Kembangan', '2020-07-13 11:00:21', '2020-07-13 11:00:21', NULL),
('016202', '016200  ', '010000', 'Kec. Kebon Jeruk', '2020-07-13 11:00:21', '2020-07-13 11:00:21', NULL),
('016203', '016200  ', '010000', 'Kec. Palmerah', '2020-07-13 11:00:55', '2020-07-13 11:00:55', NULL),
('016204', '016200  ', '010000', 'Kec. Grogol Petamburan', '2020-07-13 11:00:55', '2020-07-13 11:00:55', NULL),
('016205', '016200  ', '010000', 'Kec. Tambora', '2020-07-13 11:01:24', '2020-07-13 11:01:24', NULL),
('016206', '016200  ', '010000', 'Kec. Taman Sari', '2020-07-13 11:01:24', '2020-07-13 11:01:24', NULL),
('016207', '016200  ', '010000', 'Kec. Cengkareng', '2020-07-13 11:01:57', '2020-07-13 11:01:57', NULL),
('016208', '016200  ', '010000', 'Kec. Kali Deres', '2020-07-13 11:01:57', '2020-07-13 11:01:57', NULL),
('016301', '016300  ', '010000', 'Kec. Jagakarsa', '2020-07-13 11:03:41', '2020-07-13 11:03:41', NULL),
('016302', '016300  ', '010000', 'Kec. Pasar Minggu', '2020-07-13 11:03:41', '2020-07-13 11:03:41', NULL),
('016303', '016300  ', '010000', 'Kec. Cilandak', '2020-07-13 11:04:12', '2020-07-13 11:04:12', NULL),
('016304', '016300  ', '010000', 'Kec. Pesanggrahan', '2020-07-13 11:04:12', '2020-07-13 11:04:12', NULL),
('016305', '016300  ', '010000', 'Kec. Kebayoran Lama', '2020-07-13 11:05:12', '2020-07-13 11:05:12', NULL),
('016306', '016300  ', '010000', 'Kec. Kebayoran Baru', '2020-07-13 11:05:12', '2020-07-13 11:05:12', NULL),
('016307', '016300  ', '010000', 'Kec. Mampang Prapatan', '2020-07-13 11:06:22', '2020-07-13 11:06:22', NULL),
('016308', '016300  ', '010000', 'Kec. Pancoran', '2020-07-13 11:06:22', '2020-07-13 11:06:22', NULL),
('016309', '016300  ', '010000', 'Kec. Tebet', '2020-07-13 11:06:58', '2020-07-13 11:06:58', NULL),
('016310', '016300  ', '010000', 'Kec. Setia Budi', '2020-07-13 11:06:58', '2020-07-13 11:06:58', NULL),
('016401', '016400  ', '010000', 'Kec. Pasar Rebo', '2020-07-13 11:10:08', '2020-07-13 11:10:08', NULL),
('016402', '016400  ', '010000', 'Kec. Ciracas', '2020-07-13 11:10:08', '2020-07-13 11:10:08', NULL),
('016403', '016400  ', '010000', 'Kec. Cipayung', '2020-07-13 11:10:37', '2020-07-13 11:10:37', NULL),
('016404', '016400  ', '010000', 'Kec. Makasar', '2020-07-13 11:10:37', '2020-07-13 11:10:37', NULL),
('016405', '016400  ', '010000', 'Kec. Kramat Jati', '2020-07-13 11:11:05', '2020-07-13 11:11:05', NULL),
('016406', '016400  ', '010000', 'Kec. Jatinegara', '2020-07-13 11:11:05', '2020-07-13 11:11:05', NULL),
('016407', '016400  ', '010000', 'Kec. Duren Sawit', '2020-07-13 11:11:30', '2020-07-13 11:11:30', NULL),
('016408', '016400  ', '010000', 'Kec. Cakung', '2020-07-13 11:11:30', '2020-07-13 11:11:30', NULL),
('016409', '016400  ', '010000', 'Kec. Pulo Gadung', '2020-07-13 11:11:54', '2020-07-13 11:11:54', NULL),
('016410', '016400  ', '010000', 'Kec. Matraman', '2020-07-13 11:11:54', '2020-07-13 11:11:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ref_kompetensi_dasar`
--

CREATE TABLE `ref_kompetensi_dasar` (
  `id_kompetensi_dasar` int(10) UNSIGNED NOT NULL,
  `id_mata_pelajaran` int(10) UNSIGNED DEFAULT NULL,
  `id_kompetensi_inti` int(11) UNSIGNED DEFAULT NULL,
  `kode_kompetensi_dasar` varchar(10) NOT NULL,
  `deskripsi` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_kompetensi_dasar`
--

INSERT INTO `ref_kompetensi_dasar` (`id_kompetensi_dasar`, `id_mata_pelajaran`, `id_kompetensi_inti`, `kode_kompetensi_dasar`, `deskripsi`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 29, 16, '3.1', 'Menerapkan logika dan algoritma komputer', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(2, 29, 16, '3.2', 'Menerapkan metode peta-minda ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(3, 29, 16, '3.3', 'Mengevaluasi paragraf deskriptif, argumentatif, naratif, dan persuasif ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(4, 29, 16, '3.4', 'Menerapkan logika dan operasi perhitungan data ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(5, 29, 16, '3.5', 'Menganalisis fitur yang tepat untuk pembuatan slide ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(6, 29, 16, '3.6', 'Menerapkan teknik presentasi yang efektif ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(7, 29, 16, '3.7', 'Menganalisis pembuatan e-book ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(8, 29, 16, '3.8', 'Memahami konsep Kewargaan Digital ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(9, 29, 16, '3.9', 'Menerapkan teknik penelusuran Search Engine', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(10, 29, 16, '3.10', 'Menganalisis komunikasi sinkron dan asinkron dalam jaringan ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(11, 29, 16, '3.11', 'Menganalisis fitur perangkat lunak pembelajaran kolaboratif daring ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(12, 29, 16, '3.12', 'Merancang dokumen tahap pra-produksi ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(13, 29, 16, '3.13', 'Menganalisis produksi video, animasi dan/atau musik digital ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(14, 29, 16, '3.14', 'Mengevaluasi pasca-produksi video, animasi dan/atau musik digital ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(15, 29, 17, '4.1', 'Menggunakan fungsi-gungsi perintah (Command)', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(16, 29, 17, '4.2', 'Membuat peta-minda', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(17, 29, 17, '4.3', 'Menyusun kembali format dokumen pengolah kata', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(18, 29, 17, '4.4', 'Mengoperasikan perangkat lunak pengolah angka', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(19, 29, 17, '4.5', 'Membuat slide untuk presentasi', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(20, 29, 17, '4.6', 'Melakukan presentasi yang efektif', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(21, 29, 17, '4.7', 'Membuat e-book dengan perangkat lunak e-book editor', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(22, 29, 17, '4.8', 'Merumuskan etika Kewargaan Digital', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(23, 29, 17, '4.9', 'Melakukan penelusuran informasi', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(24, 29, 17, '4.10', 'Melakukan komunikasi sinkron dan asinkron dalam jaringan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(25, 29, 17, '4.11', 'Menggunakan fitur untuk pembelajaran kolaboratif daring (kelas maya)', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(26, 29, 17, '4.12', 'Membuat dokument ahap pra-produksi', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(27, 29, 17, '4.13', 'Membuat video dan/atau animasi dan/atau musik digital', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(28, 29, 17, '4.14', 'Membuat laporan hasil pasca-produksi', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(29, 32, 22, '3.1', 'Memahami sistem bilangan (Desimal, Biner, Heksadesimal) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(30, 32, 22, '3.2', 'Menganalisis relasi logika  dasar, kombinasi dan sekuensial (NOT, AND, OR); (NOR,NAND,EXOR,EXNOR); (', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(31, 32, 22, '3.3', 'Menerapkan operasi logika Aritmatik (Half-Full Adder, Ripple Carry Adder) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(32, 32, 22, '3.4', 'Mengklasifikasikan rangkaian Multiplexer, Decoder, Register ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(33, 32, 22, '3.5', 'Menerapkan elektronika dasar (kelistrikan, komponen elektronika dan skema rangkaian elektronika) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(34, 32, 22, '3.6', 'Menerapkan dasar dasar mikrokontroler ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(35, 32, 22, '3.7', 'Menganalisis blok diagram dari sistem mikro komputer (arsitektur komputer) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(36, 32, 22, '3.8', 'Mengevaluasi Perangkat Eksternal / Peripheral ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(37, 32, 22, '3.9', 'Menganalisis memori berdasarkan karakteristik sistem memori (lokasi,kapasitas, kecepatan, cara akses', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(38, 32, 22, '3.10', 'Menganalisa Struktur CPU dan fungsi CPU ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(39, 32, 23, '4.1', 'Mengkonversikan sistem bilangan (Desimal, Biner, Heksadesimal) dalam memecahkan masalah konversi', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(40, 32, 23, '4.2', 'Merangkai fungsi gerbang logika dasar, kombinasi dan sekuensial (NOT, AND, OR); (NOR, NAND, EXOR, EX', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(41, 32, 23, '4.3', 'Mempraktikkan operasi Logik Unit (Half-Full Adder, Ripple Carry Adder)', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(42, 32, 23, '4.4', 'Mengoperasikan aritmatik dan logik pada Arithmatic Logic Unit (Multiplexer, Decoder, Register)', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(43, 32, 23, '4.5', 'Mempratikkan fungsi kelistrikan dan komponen elektronika', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(44, 32, 23, '4.6', 'Manipulasi dasar-dasar mikrokontroller (port IO, clock, arsitektur RISK, general purpose RISK, stack', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(45, 32, 23, '4.7', 'Menyajikan gambar minimal sistem mikro komputer berdasarkan blok diagram dan sistem rangkaian (arsit', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(46, 32, 23, '4.8', 'Merangkai perangkat eksternal dengan consule unit', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(47, 32, 23, '4.9', 'Membuat alternatif kebutuhan untuk memodifikasi beberapa memori dalam sistem komputer', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(48, 32, 23, '4.10', 'Menyajikan rangkaian internal CPU', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(49, 38, 34, '3.1', 'Memahami konsep basis data ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(50, 38, 34, '3.2', 'Menerapkan perintah-perintah basis data untuk Data Definition Language (DDL) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(51, 38, 34, '3.3', 'Menganalisis permasalahan perintah-perintah basis data untuk Data Definition Language (DDL) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(52, 38, 34, '3.4', 'Mengevaluasi hasil perintahperintah basis data untuk Data Definition Language (DDL) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(53, 38, 34, '3.5', 'Menerapkan perintah-perintah basis data untuk Data Control Language (DCL) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(54, 38, 34, '3.6', 'Menganalisis permasalahan perintah-perintah basis data untuk Data Control Language (DCL) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(55, 38, 34, '3.7', 'Menganalisis perintah-perintah basis data untuk Data Control Language (DCL) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(56, 38, 34, '3.8', 'Menerapkan perintah-perintah basis data untuk Data Manipulation Language (DML) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(57, 38, 34, '3.9', 'Menganalisis permasalahan perintah-perintah basis data untuk Data Manipulation Language (DML)', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(58, 38, 34, '3.10', 'Mengevaluasi perintahperintah basis data untuk Data Manipulation Language (DML) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(59, 38, 34, '3.11', 'Menerapkan prosedur backup/recovery data ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(60, 38, 34, '3.12', 'Menganalisis permasalahan backup/recovery data', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(61, 38, 34, '3.13', 'Mengevaluasi hasil backup/recovery data ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(62, 38, 34, '3.14', 'Menerapkan prosedur replikasi basis data ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(63, 38, 34, '3.15', 'Menganalisis permasalahan replikasi basis data ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(64, 38, 34, '3.16', 'Mengevaluasi hasil replikasi basis data ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(65, 38, 34, '3.17', 'Memahami teknologi Big Data ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(66, 38, 34, '3.18', 'Menerapkan prosedur konfigurasi framework Big Data ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(67, 38, 34, '3.19', 'Menganalisis permasalahan konfigurasi framework Big Data ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(68, 38, 34, '3.20', 'Mengevaluasi konfigurasi Big Data ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(69, 38, 34, '3.21', 'Memahami konsep kecerdasan komputasi ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(70, 38, 34, '3.22', 'Menerapkan kecerdasan  komputasi pada skala kecil (smart home) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(71, 38, 34, '3.23', 'Menganalisis permasalahan kecerdasan  komputasi pada skala kecil (smart home) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(72, 38, 34, '3.24', 'Mengevaluasi hasil kecerdasan  komputasi pada skala kecil (smart home) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(73, 38, 34, '3.25', 'Menerapkan kecerdasan  komputasi pada skala menengah (smart city, smart building) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(74, 38, 34, '3.26', 'Menganalisis permasalahan kecerdasan  komputasi pada skala menengah (smart city, smart building) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(75, 38, 34, '3.27', ' Mengevaluasi kecerdasan  komputasi pada skala menengah (smart city, smart building) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(76, 38, 34, '3.28', 'Menerapkan kecerdasan  komputasi pada skala luas (IoT) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(77, 38, 34, '3.29', 'Menganalisis permasalahan kecerdasan  komputasi pada skala luas (IoT) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(78, 38, 34, '3.30', 'Menganalisis kecerdasan  komputasi pada skala luas (IoT) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(79, 38, 35, '4.1', 'Mempresentasikan  konsep basis data ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(80, 38, 35, '4.2', 'Membuat basis data baru, menghapus basis data, membuat tabel, modifikasi struktur tabel, menghapus t', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(81, 38, 35, '4.3', 'Memperbaiki permasalahan perintah-perintah basis data untuk Data Definition Language (DDL) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(82, 38, 35, '4.4', 'Mengkombinasikan perintahperintah baru basis data untuk Data Definition Language (DDL)', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(83, 38, 35, '4.5', 'Membuat user, update user, mengatur hak user, menghapus user ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(84, 38, 35, '4.6', 'Memperbaiki permasalahan perintah-perintah basis data untuk Data Control Language (DCL) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(85, 38, 35, '4.7', 'Mengkombinasi perintahperintah basis data untuk Data Control Language (DCL)', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(86, 38, 35, '4.8', 'Menampilkan record data dari sebuah tabel, menampilkan record data dari beberapa tabel yang berelasi', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(87, 38, 35, '4.9', 'Memperbaiki permasalahan perintah-perintah basis data untuk Data Manipulation Language (DML) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(88, 38, 35, '4.10', 'Mengkombinasi perintahperintah basis data untuk Data Manipulation Language (DML) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(89, 38, 35, '4.11', 'Mendemonstrasikan  backup/recovery data secara periodik ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(90, 38, 35, '4.12', 'Memperbaiki permasalahan backup/recovery data ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(91, 38, 35, '4.13', 'Merumuskan prosedur baru backup/recovery data', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(92, 38, 35, '4.14', 'Mendemonstrasikan  backup data dari basis data server master ke basis data server slave ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(93, 38, 35, '4.15', 'Memperbaiki permasalahan replikasi basis data ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(94, 38, 35, '4.16', 'Merumuskan tindakan berdasarkan hasil replikasi basis data', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(95, 38, 35, '4.17', 'Mempresentasikan teknologi Big Data ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(96, 38, 35, '4.18', 'Mengkonfigurasi framework Big Data  ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(97, 38, 35, '4.19', 'Memperbaiki konfigurasi framework Big Data ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(98, 38, 35, '4.20', 'Merumuskan tindakan berdasarkan data hasil analisis kinerja Big Data ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(99, 38, 35, '4.21', 'Mempresentasikan  konsep kecerdasan  komputasi ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(100, 38, 35, '4.22', 'Membuat kecerdasan  komputasi pada skala kecil (smart home) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(101, 38, 35, '4.23', 'Memperbaiki permasalahan kecerdasan  komputasi pada skala kecil (smart home) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(102, 38, 35, '4.24', 'Menyimpulkan hasil kecerdasan  komputasi pada skala kecil (smart home) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(103, 38, 35, '4.25', 'Membuat kecerdasan  komputasi pada skala menengah (smart city, smart building)', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(104, 38, 35, '4.26', 'Memperbaiki permasalahan kecerdasan komputasi pada skala menengah (smart city, smart building)', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(105, 38, 35, '4.27', 'Membuat kecerdasan komputasi pada skala menengah (smart city, smart building)', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(106, 38, 35, '4.28', 'Membuat kecerdasan komputasi pada skala luas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(107, 38, 35, '4.29', 'Memperbaiki permasalahan kecerdasan komputasi pada skala luas (IoT)', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(108, 38, 35, '4.30', 'Membuat kecerdasan komputasi pada skala luas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ref_kompetensi_inti`
--

CREATE TABLE `ref_kompetensi_inti` (
  `id_kompetensi_inti` int(10) UNSIGNED NOT NULL,
  `id_mata_pelajaran` int(11) UNSIGNED DEFAULT NULL,
  `kode_kompetensi_inti` varchar(10) NOT NULL,
  `deskripsi` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_kompetensi_inti`
--

INSERT INTO `ref_kompetensi_inti` (`id_kompetensi_inti`, `id_mata_pelajaran`, `kode_kompetensi_inti`, `deskripsi`, `created_at`, `updated_at`, `deleted_at`) VALUES
(8, 17, '3', 'Memahami, menerapkan, menganalisis, dan mengevaluasi tentang pengetahuan faktual, konseptual, prosed', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(9, 17, '4', 'Melaksanakan tugas spesifik dengan menggunakan alat, informasi, dan prosedur kerja yang lazim dilaku', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(10, 18, '3', 'Memahami, menerapkan, menganalisis, dan mengevaluasi tentang pengetahuan faktual, konseptual, prosed', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(11, 18, '4', 'Melaksanakan tugas spesifik dengan menggunakan alat, informasi, dan prosedur kerja yang lazim dilaku', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(12, 19, '3', 'Memahami, menerapkan, menganalisis, dan mengevaluasi tentang pengetahuan faktual, konseptual, prosed', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(13, 19, '4', 'Melaksanakan tugas spesifik dengan menggunakan alat, informasi, dan prosedur kerja yang lazim dilaku', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(14, 20, '3', 'Memahami, menerapkan, menganalisis, dan mengevaluasi tentang pengetahuan faktual, konseptual, prosed', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(15, 20, '4', 'Melaksanakan tugas spesifik dengan menggunakan alat, informasi, dan prosedur kerja yang lazim dilaku', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(16, 29, '3', 'Memahami, menerapkan, menganalisis, dan mengevaluasi tentang pengetahuan faktual, konseptual, operas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(17, 29, '4', 'Melaksanakan tugas spesifik dengan menggunakan alat, informasi, dan prosedur kerja yang lazim dilaku', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(18, 30, '3', 'Memahami, menerapkan, menganalisis, dan mengevaluasi tentang pengetahuan faktual, konseptual, operas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(19, 30, '4', 'Melaksanakan tugas spesifik dengan menggunakan alat, informasi, dan prosedur kerja yang lazim dilaku', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(20, 31, '3', 'Memahami, menerapkan, menganalisis, dan mengevaluasi tentang pengetahuan faktual, konseptual, operas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(21, 31, '4', 'Melaksanakan tugas spesifik dengan menggunakan alat, informasi, dan prosedur kerja yang lazim dilaku', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(22, 32, '3', 'Memahami, menerapkan, menganalisis, dan mengevaluasi tentang pengetahuan faktual, konseptual, operas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(23, 32, '4', 'Melaksanakan tugas spesifik, dengan menggunakan alat, informasi, dan prosedur kerja yang lazim dilak', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(24, 33, '3', 'Memahami, menerapkan, menganalisis, dan mengevaluasi tentang pengetahuan faktual, konseptual, operas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(25, 33, '4', 'Melaksanakan tugas spesifik, dengan menggunakan alat, informasi, dan prosedur kerja yang lazim dilak', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(26, 34, '3', 'Memahami, menerapkan, menganalisis, dan mengevaluasi tentang pengetahuan faktual, konseptual, operas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(27, 34, '4', 'Melaksanakan tugas spesifik, dengan menggunakan alat, informasi, dan prosedur kerja yang lazim dilak', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(28, 35, '3', 'Memahami, menerapkan, menganalisis, dan mengevaluasi tentang pengetahuan faktual, konseptual, operas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(29, 35, '4', 'Melaksanakan tugas spesifik, dengan menggunakan alat, informasi, dan prosedur kerja yang lazim dilak', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(30, 36, '3', 'Memahami, menerapkan, menganalisis, dan mengevaluasi tentang pengetahuan faktual, konseptual, operas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(31, 36, '4', 'Melaksanakan tugas spesifik dengan menggunakan alat, informasi, dan prosedur kerja yang lazim dilaku', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(32, 37, '3', 'Memahami, menerapkan, menganalisis, dan mengevaluasi tentang pengetahuan faktual, konseptual, operas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(33, 37, '4', 'Melaksanakan tugas spesifik dengan menggunakan alat, informasi, dan prosedur kerja yang lazim dilaku', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(34, 38, '3', 'Memahami, menerapkan, menganalisis, dan mengevaluasi tentang pengetahuan faktual, konseptual, operas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(35, 38, '4', 'Melaksanakan tugas spesifik dengan menggunakan alat, informasi, dan prosedur kerja yang lazim dilaku', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(36, 39, '3', 'Memahami, menerapkan, menganalisis, dan mengevaluasi tentang pengetahuan faktual, konseptual, operas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(37, 39, '4', 'Melaksanakan tugas spesifik dengan menggunakan alat, informasi, dan prosedur kerja yang lazim dilaku', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(38, 40, '3', 'Memahami, menerapkan, menganalisis, dan mengevaluasi tentang pengetahuan faktual, konseptual, operas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(39, 40, '4', 'Melaksanakan tugas spesifik dengan menggunakan alat, informasi, dan prosedur kerja yang lazim dilaku', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(40, 41, '3', 'Memahami, menerapkan, menganalisis, dan mengevaluasi tentang pengetahuan faktual, konseptual, operas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(41, 41, '4', 'Melaksanakan tugas spesifik dengan menggunakan alat, informasi, dan prosedur kerja yang lazim dilaku', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ref_kompetensi_keahlian`
--

CREATE TABLE `ref_kompetensi_keahlian` (
  `id_kompetensi_keahlian` int(10) UNSIGNED NOT NULL,
  `id_bidang_keahlian` int(11) UNSIGNED DEFAULT NULL,
  `id_program_keahlian` int(11) UNSIGNED DEFAULT NULL,
  `id_sk_dikdasmen` int(11) UNSIGNED DEFAULT NULL,
  `kode_kompetensi_keahlian` varchar(10) NOT NULL,
  `nomor_kode` varchar(10) NOT NULL,
  `nama_kompetensi_keahlian` varchar(100) NOT NULL,
  `lama_studi` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_kompetensi_keahlian`
--

INSERT INTO `ref_kompetensi_keahlian` (`id_kompetensi_keahlian`, `id_bidang_keahlian`, `id_program_keahlian`, `id_sk_dikdasmen`, `kode_kompetensi_keahlian`, `nomor_kode`, `nama_kompetensi_keahlian`, `lama_studi`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 1, '1.1.1', '001', 'Konstruksi Gedung, Sanitasi dan Perawatan', 4, '0000-00-00 00:00:00', '2020-07-18 13:57:24', NULL),
(2, 1, 1, 1, '1.1.2', '002', 'Konstruksi Jalan, Irigasi dan Jembatan', 4, '0000-00-00 00:00:00', '2020-07-18 13:57:28', NULL),
(3, 1, 1, 1, '1.1.3', '003', 'Bisnis Konstruksi dan Properti', 3, '0000-00-00 00:00:00', '2020-07-18 13:57:32', NULL),
(4, 1, 1, 1, '1.1.4', '004', 'Desain Pemodelan dan Informasi Bangunan', 3, '0000-00-00 00:00:00', '2020-07-18 13:57:34', NULL),
(5, 1, 2, 1, '1.2.1', '005', 'Teknik Geomatika', 3, '0000-00-00 00:00:00', '2020-07-18 13:57:37', NULL),
(6, 1, 2, 1, '1.2.2', '006', 'Informasi Geospasial', 4, '0000-00-00 00:00:00', '2020-07-18 13:57:42', NULL),
(7, 1, 3, 1, '1.3.1', '007', 'Teknik Pembangkit Tenaga Listrik', 3, '0000-00-00 00:00:00', '2020-07-18 13:57:45', NULL),
(8, 1, 3, 1, '1.3.2', '008', 'Teknik Jaringan Tenaga Listrik', 3, '0000-00-00 00:00:00', '2020-07-18 13:57:47', NULL),
(9, 1, 3, 1, '1.3.3', '009', 'Teknik Instalasi Tenaga Listrik', 3, '0000-00-00 00:00:00', '2020-07-18 13:57:50', NULL),
(10, 1, 3, 1, '1.3.4', '010', 'Teknik Otomasi Industri', 4, '0000-00-00 00:00:00', '2020-07-18 13:57:53', NULL),
(11, 1, 3, 1, '1.3.5', '011', 'Teknik Pendinginan dan Tata Udara', 3, '0000-00-00 00:00:00', '2020-07-18 13:58:04', NULL),
(12, 1, 3, 1, '1.3.6', '012', 'Teknik Tenaga Listrik', 4, '0000-00-00 00:00:00', '2020-07-18 13:58:07', NULL),
(13, 1, 4, 1, '1.4.1', '013', 'Teknik Pemesinan', 3, '0000-00-00 00:00:00', '2020-07-18 13:58:10', NULL),
(14, 1, 4, 1, '1.4.2', '014', 'Teknik Pengelasan', 3, '0000-00-00 00:00:00', '2020-07-18 13:58:12', NULL),
(15, 1, 4, 1, '1.4.3', '015', 'Teknik Pengecoran Logam', 3, '0000-00-00 00:00:00', '2020-07-18 13:58:14', NULL),
(16, 1, 4, 1, '1.4.4', '016', 'Teknik Mekanik Industri', 3, '0000-00-00 00:00:00', '2020-07-18 13:58:17', NULL),
(17, 1, 4, 1, '1.4.5', '017', 'Teknik Perancangan dan Gambar Mesin', 3, '0000-00-00 00:00:00', '2020-07-18 13:58:19', NULL),
(18, 1, 4, 1, '1.4.6', '018', 'Teknik Fabrikasi Logam dan Manufaktur', 4, '0000-00-00 00:00:00', '2020-07-18 13:58:22', NULL),
(19, 1, 5, 1, '1.5.1', '019', 'Airframe Power Plant', 3, '0000-00-00 00:00:00', '2020-07-18 13:58:26', NULL),
(20, 1, 5, 1, '1.5.2', '020', 'Aircraft Machining', 3, '0000-00-00 00:00:00', '2020-07-18 13:58:28', NULL),
(21, 1, 5, 1, '1.5.3', '021', 'Aircraft Sheet Metal Forming', 3, '0000-00-00 00:00:00', '2020-07-18 13:58:31', NULL),
(22, 1, 5, 1, '1.5.4', '022', 'Airframe Mechanic', 3, '0000-00-00 00:00:00', '2020-07-18 13:58:33', NULL),
(23, 1, 5, 1, '1.5.5', '023', 'Aircraft Electricity', 3, '0000-00-00 00:00:00', '2020-07-18 13:58:35', NULL),
(24, 1, 5, 1, '1.5.6', '024', 'Aviation Electronics', 3, '0000-00-00 00:00:00', '2020-07-18 13:58:37', NULL),
(25, 1, 5, 1, '1.5.7', '025', 'Electrical Avionics', 3, '0000-00-00 00:00:00', '2020-07-18 13:58:40', NULL),
(26, 1, 6, 1, '1.6.1', '026', 'Desain Grafika', 3, '0000-00-00 00:00:00', '2020-07-18 13:58:43', NULL),
(27, 1, 6, 1, '1.6.2', '027', 'Produksi Grafika', 3, '0000-00-00 00:00:00', '2020-07-18 13:58:48', NULL),
(28, 1, 7, 1, '1.7.1', '028', 'Teknik Instrumentasi Logam', 3, '0000-00-00 00:00:00', '2020-07-18 13:58:51', NULL),
(29, 1, 7, 1, '1.7.2', '029', 'Instrumentasi dan Otomatisasi Proses', 4, '0000-00-00 00:00:00', '2020-07-18 13:58:53', NULL),
(30, 1, 8, 1, '1.8.1', '030', 'Teknik Pengendalian Produksi', 3, '0000-00-00 00:00:00', '2020-07-18 13:58:55', NULL),
(31, 1, 8, 1, '1.8.2', '031', 'Teknik Logistik', 3, '0000-00-00 00:00:00', '2020-07-18 14:00:03', NULL),
(32, 1, 9, 1, '1.9.1', '032', 'Teknik Pemintalan Serat Buatan', 3, '0000-00-00 00:00:00', '2020-07-18 14:00:06', NULL),
(33, 1, 9, 1, '1.9.2', '033', 'Teknik Pembuatan Benang', 4, '0000-00-00 00:00:00', '2020-07-18 14:00:09', NULL),
(34, 1, 9, 1, '1.9.3', '034', 'Teknik Pembuatan Kain', 4, '0000-00-00 00:00:00', '2020-07-18 14:00:12', NULL),
(35, 1, 9, 1, '1.9.4', '035', 'Teknik Penyempurnaan Tekstil', 4, '0000-00-00 00:00:00', '2020-07-18 14:00:14', NULL),
(36, 1, 10, 1, '1.10.1', '036', 'Analis Pengujian Laboratorium', 3, '0000-00-00 00:00:00', '2020-07-18 14:00:16', NULL),
(37, 1, 10, 1, '1.10.2', '037', 'Kimia Industri', 3, '0000-00-00 00:00:00', '2020-07-18 14:00:18', NULL),
(38, 1, 10, 1, '1.10.3', '038', 'Kimia Analisis', 4, '0000-00-00 00:00:00', '2020-07-18 14:00:21', NULL),
(39, 1, 10, 1, '1.10.4', '039', 'Kimia Tekstil', 3, '0000-00-00 00:00:00', '2020-07-18 14:00:23', NULL),
(40, 1, 11, 1, '1.11.1', '040', 'Teknik Kendaraan Ringan Otomotif', 3, '0000-00-00 00:00:00', '2020-07-18 14:00:26', NULL),
(41, 1, 11, 1, '1.11.2', '041', 'Teknik dan Bisnis Sepeda Motor', 3, '0000-00-00 00:00:00', '2020-07-18 14:00:29', NULL),
(42, 1, 11, 1, '1.11.3', '042', 'Teknik Alat Berat', 3, '0000-00-00 00:00:00', '2020-07-18 14:00:31', NULL),
(43, 1, 11, 1, '1.11.4', '043', 'Teknik Bodi Otomotif', 3, '0000-00-00 00:00:00', '2020-07-18 14:00:33', NULL),
(44, 1, 11, 1, '1.11.5', '044', 'Teknik Ototronik', 3, '0000-00-00 00:00:00', '2020-07-18 14:00:36', NULL),
(45, 1, 11, 1, '1.11.6', '045', 'Teknik dan Manajemen Perawatan Otomotif', 4, '0000-00-00 00:00:00', '2020-07-18 14:00:38', NULL),
(46, 1, 11, 1, '1.11.7', '046', 'Otomotif Daya dan Konversi Energi', 4, '0000-00-00 00:00:00', '2020-07-18 14:00:41', NULL),
(47, 1, 12, 1, '1.12.1', '047', 'Konstruksi Kapal Baja', 3, '0000-00-00 00:00:00', '2020-07-18 14:00:44', NULL),
(48, 1, 12, 1, '1.12.2', '048', 'Konstuksi Kapal Non Baja', 3, '0000-00-00 00:00:00', '2020-07-18 14:00:46', NULL),
(49, 1, 12, 1, '1.12.3', '049', 'Teknik Pemesinan Kapal', 3, '0000-00-00 00:00:00', '2020-07-18 14:00:49', NULL),
(50, 1, 12, 1, '1.12.4', '050', 'Teknik Pengelasan Kapal', 3, '0000-00-00 00:00:00', '2020-07-18 14:00:51', NULL),
(51, 1, 12, 1, '1.12.5', '051', 'Teknik Kelistrikan Kapal', 3, '0000-00-00 00:00:00', '2020-07-18 14:00:53', NULL),
(52, 1, 12, 1, '1.12.6', '052', 'Desain dan Rancang Bangun Kapal', 3, '0000-00-00 00:00:00', '2020-07-18 14:00:56', NULL),
(53, 1, 12, 1, '1.12.7', '053', 'Interior Kapal', 3, '0000-00-00 00:00:00', '2020-07-18 14:01:00', NULL),
(54, 1, 13, 1, '1.13.1', '054', 'Teknik Audio Video', 3, '0000-00-00 00:00:00', '2020-07-18 14:01:03', NULL),
(55, 1, 13, 1, '1.13.2', '055', 'Teknik Elektronika Industri', 3, '0000-00-00 00:00:00', '2020-07-18 14:01:05', NULL),
(56, 1, 13, 1, '1.13.3', '056', 'Teknik Mekatronika', 4, '0000-00-00 00:00:00', '2020-07-18 14:01:07', NULL),
(57, 1, 13, 1, '1.13.4', '057', 'Teknik Elektronika Daya dan Komunikasi', 4, '0000-00-00 00:00:00', '2020-07-18 14:01:10', NULL),
(58, 1, 13, 1, '1.13.5', '058', 'Instrumentasi Medik', 4, '0000-00-00 00:00:00', '2020-07-18 14:01:12', NULL),
(59, 2, 14, 1, '2.1.1', '059', 'Teknik Produksi Minyak dan Gas', 3, '0000-00-00 00:00:00', '2020-07-18 14:01:15', NULL),
(60, 2, 14, 1, '2.1.2', '060', 'Teknik Pemboran Minyak dan Gas', 3, '0000-00-00 00:00:00', '2020-07-18 14:01:18', NULL),
(61, 2, 14, 1, '2.1.3', '061', 'Teknik Pengolahan Minyak, Gas dan Petrokimia', 3, '0000-00-00 00:00:00', '2020-07-18 14:01:21', NULL),
(62, 2, 15, 1, '2.2.1', '062', 'Geologi Pertambangan', 4, '0000-00-00 00:00:00', '2020-07-18 14:01:23', NULL),
(63, 2, 16, 1, '2.3.1', '063', 'Teknik Energi Surya, Hidro dan Angin', 3, '0000-00-00 00:00:00', '2020-07-18 14:01:25', NULL),
(64, 2, 16, 1, '2.3.2', '064', 'Teknik Energi Biomassa', 3, '0000-00-00 00:00:00', '2020-07-18 14:01:28', NULL),
(65, 3, 17, 1, '3.1.1', '065', 'Rekayasa Perangkat Lunak', 3, '0000-00-00 00:00:00', '2020-07-18 14:01:30', NULL),
(66, 3, 17, 1, '3.1.2', '066', 'Teknik Komputer dan Jaringan', 3, '0000-00-00 00:00:00', '2020-07-18 14:01:33', NULL),
(67, 3, 17, 1, '3.1.3', '067', 'Multimedia', 3, '0000-00-00 00:00:00', '2020-07-18 14:01:35', NULL),
(68, 3, 17, 1, '3.1.4', '068', 'Sistem Informatika, Jaringan dan Aplikasi', 4, '0000-00-00 00:00:00', '2020-07-18 14:01:38', NULL),
(69, 3, 18, 1, '3.2.1', '069', 'Teknik Transmisi Telekomunikasi', 3, '0000-00-00 00:00:00', '2020-07-18 14:01:46', NULL),
(70, 3, 18, 1, '3.2.2', '070', 'Teknik Jaringan Akses Telekomunikasi', 3, '0000-00-00 00:00:00', '2020-07-18 14:01:48', NULL),
(71, 4, 19, 1, '4.1.1', '071', 'Asisten Keperawatan', 3, '0000-00-00 00:00:00', '2020-07-18 14:02:11', NULL),
(72, 4, 20, 1, '4.2.1', '072', 'Dental Asisten', 3, '0000-00-00 00:00:00', '2020-07-18 14:02:14', NULL),
(73, 4, 21, 1, '4.3.1', '073', 'Teknologi Laboratorium Medik', 3, '0000-00-00 00:00:00', '2020-07-18 14:02:17', NULL),
(74, 4, 22, 1, '4.4.1', '074', 'Farmasi Klinis dan Komunitas', 3, '0000-00-00 00:00:00', '2020-07-18 14:02:21', NULL),
(75, 4, 22, 1, '4.4.2', '075', 'Farmasi Industri', 3, '0000-00-00 00:00:00', '2020-07-18 14:02:23', NULL),
(76, 4, 23, 1, '4.5.1', '076', 'Social Care (Keperawatan Sosial)', 3, '0000-00-00 00:00:00', '2020-07-18 14:02:26', NULL),
(77, 4, 23, 1, '4.5.2', '077', 'Caregiver', 4, '0000-00-00 00:00:00', '2020-07-18 14:02:30', NULL),
(78, 5, 24, 1, '5.1.1', '078', 'Agribisnis Tanaman Pangan dan Hortikultura', 3, '0000-00-00 00:00:00', '2020-07-18 14:02:32', NULL),
(79, 5, 24, 1, '5.1.2', '079', 'Agribisnis Tanaman Perkebunan', 3, '0000-00-00 00:00:00', '2020-07-18 14:02:34', NULL),
(80, 5, 24, 1, '5.1.3', '080', 'Pemuliaan dan Perbenihan Tanaman', 4, '0000-00-00 00:00:00', '2020-07-18 14:02:36', NULL),
(81, 5, 24, 1, '5.1.4', '081', 'Lanskap dan Pertamanan', 3, '0000-00-00 00:00:00', '2020-07-18 14:02:39', NULL),
(82, 5, 24, 1, '5.1.5', '082', 'Produksi dan Pengelolaan Perkebunan', 4, '0000-00-00 00:00:00', '2020-07-18 14:02:41', NULL),
(83, 5, 24, 1, '5.1.6', '083', 'Agribisnis Organik Ekologi', 4, '0000-00-00 00:00:00', '2020-07-18 14:02:45', NULL),
(84, 5, 25, 1, '5.2.1', '084', 'Agribisnis Ternak Ruminansia', 3, '0000-00-00 00:00:00', '2020-07-18 14:02:47', NULL),
(85, 5, 25, 1, '5.2.2', '085', 'Agribisnis Ternak Unggas', 3, '0000-00-00 00:00:00', '2020-07-18 14:02:50', NULL),
(86, 5, 25, 1, '5.2.3', '086', 'Industri Peternakan', 4, '0000-00-00 00:00:00', '2020-07-18 14:02:53', NULL),
(87, 5, 26, 1, '5.3.1', '087', 'Keperawatan Hewan', 3, '0000-00-00 00:00:00', '2020-07-18 14:02:55', NULL),
(88, 5, 26, 1, '5.3.2', '088', 'Kesehatan dan Reproduksi Hewan', 4, '0000-00-00 00:00:00', '2020-07-18 14:02:58', NULL),
(89, 5, 27, 1, '5.4.1', '089', 'Agribisnis Pengolahan Hasil Pertanian', 3, '0000-00-00 00:00:00', '2020-07-18 14:03:00', NULL),
(90, 5, 27, 1, '5.4.2', '090', 'Pengawasan Mutu Hasil Pertanian', 3, '0000-00-00 00:00:00', '2020-07-18 14:03:03', NULL),
(91, 5, 27, 1, '5.4.3', '091', 'Agroindustri', 4, '0000-00-00 00:00:00', '2020-07-18 14:03:06', NULL),
(92, 5, 28, 1, '5.5.1', '092', 'Alat Mesin Pertanian', 3, '0000-00-00 00:00:00', '2020-07-18 14:03:08', NULL),
(93, 5, 28, 1, '5.5.2', '093', 'Otomatisasi Pertanian', 4, '0000-00-00 00:00:00', '2020-07-18 14:03:11', NULL),
(94, 5, 29, 1, '5.6.1', '094', 'Teknik Inventarisasi dan Pemetaan Hutan', 3, '0000-00-00 00:00:00', '2020-07-18 14:03:13', NULL),
(95, 5, 29, 1, '5.6.2', '095', 'Teknik Konservasi Sumber Daya Hutan', 3, '0000-00-00 00:00:00', '2020-07-18 14:03:15', NULL),
(96, 5, 29, 1, '5.6.3', '096', 'Teknik Rehabilitasi dan Reklamasi Hutan', 3, '0000-00-00 00:00:00', '2020-07-18 14:03:18', NULL),
(97, 5, 29, 1, '5.6.4', '097', 'Teknologi Produksi Hasil Hutan', 3, '0000-00-00 00:00:00', '2020-07-18 14:03:20', NULL),
(98, 6, 30, 1, '6.1.1', '098', 'Nautika Kapal Penangkap Ikan', 3, '0000-00-00 00:00:00', '2020-07-18 14:03:24', NULL),
(99, 6, 30, 1, '6.1.2', '099', 'Teknika Kapal Penangkap Ikan', 3, '0000-00-00 00:00:00', '2020-07-18 14:03:27', NULL),
(100, 6, 31, 1, '6.2.1', '100', 'Nautika Kapal Niaga', 3, '0000-00-00 00:00:00', '2020-07-18 14:03:29', NULL),
(101, 6, 31, 1, '6.2.2', '101', 'Teknika Kapal Niaga', 3, '0000-00-00 00:00:00', '2020-07-18 14:03:33', NULL),
(102, 6, 32, 1, '6.3.1', '102', 'Agribisnis Perikanan Air Tawar', 3, '0000-00-00 00:00:00', '2020-07-18 14:03:35', NULL),
(103, 6, 32, 1, '6.3.2', '103', 'Agribisnis Perikanan Air Payau dan Laut', 3, '0000-00-00 00:00:00', '2020-07-18 14:03:38', NULL),
(104, 6, 32, 1, '6.3.3', '104', 'Agribisnis Ikan Hias', 3, '0000-00-00 00:00:00', '2020-07-18 14:03:40', NULL),
(105, 6, 32, 1, '6.3.4', '105', 'Agribisnis Rumput Laut', 3, '0000-00-00 00:00:00', '2020-07-18 14:03:42', NULL),
(106, 6, 32, 1, '6.3.5', '106', 'Industri Perikanan Laut', 4, '0000-00-00 00:00:00', '2020-07-18 14:03:44', NULL),
(107, 6, 33, 1, '6.4.1', '107', 'Agribisnis Pengolahan Hasil Perikanan', 3, '0000-00-00 00:00:00', '2020-07-18 14:03:46', NULL),
(108, 7, 34, 1, '7.1.1', '108', 'Bisnis Daring dan Pemasaran', 3, '0000-00-00 00:00:00', '2020-07-18 14:03:49', NULL),
(109, 7, 34, 1, '7.1.2', '109', 'Retail', 3, '0000-00-00 00:00:00', '2020-07-18 14:03:51', NULL),
(110, 7, 35, 1, '7.2.1', '110', 'Otomatisasi dan Tata Kelola Perkantoran', 3, '0000-00-00 00:00:00', '2020-07-18 14:03:54', NULL),
(111, 7, 36, 1, '7.3.1', '111', 'Akuntansi dan Keuangan Lembaga', 3, '0000-00-00 00:00:00', '2020-07-18 14:03:56', NULL),
(112, 7, 36, 1, '7.3.2', '112', 'Perbankan dan Keuangan Mikro', 3, '0000-00-00 00:00:00', '2020-07-18 14:03:58', NULL),
(113, 7, 36, 1, '7.3.3', '113', 'Perbankan Syariah', 3, '0000-00-00 00:00:00', '2020-07-18 14:04:00', NULL),
(114, 7, 37, 1, '7.4.1', '114', 'Manajemen Logistik', 4, '0000-00-00 00:00:00', '2020-07-18 14:04:02', NULL),
(115, 8, 38, 1, '8.1.1', '115', 'Usaha Perjalanan Wisata', 3, '0000-00-00 00:00:00', '2020-07-18 14:04:04', NULL),
(116, 8, 38, 1, '8.1.2', '116', 'Perhotelan', 3, '0000-00-00 00:00:00', '2020-07-18 14:04:06', NULL),
(117, 8, 38, 1, '8.1.3', '117', 'Wisata Bahari dan Ekowisata', 4, '0000-00-00 00:00:00', '2020-07-18 14:04:09', NULL),
(118, 8, 38, 1, '8.1.4', '118', 'Hotel dan Restoran ', 4, '0000-00-00 00:00:00', '2020-07-18 14:04:11', NULL),
(119, 8, 39, 1, '8.2.1', '119', 'Tata Boga', 3, '0000-00-00 00:00:00', '2020-07-18 14:04:13', NULL),
(120, 8, 40, 1, '8.3.1', '120', 'Tata Kecantikan Kulit dan Rambut', 3, '0000-00-00 00:00:00', '2020-07-18 14:04:15', NULL),
(121, 8, 40, 1, '8.3.2', '121', 'Spa dan Beauty Therapy', 4, '0000-00-00 00:00:00', '2020-07-18 14:04:18', NULL),
(122, 8, 41, 1, '8.4.1', '122', 'Tata Busana', 3, '0000-00-00 00:00:00', '2020-07-18 14:04:20', NULL),
(123, 8, 41, 1, '8.4.2', '123', 'Desain Fesyen', 4, '0000-00-00 00:00:00', '2020-07-18 14:04:22', NULL),
(124, 9, 42, 1, '9.1.1', '124', 'Seni Lukis', 3, '0000-00-00 00:00:00', '2020-07-18 14:04:24', NULL),
(125, 9, 42, 1, '9.1.2', '125', 'Seni Patung', 3, '0000-00-00 00:00:00', '2020-07-18 14:04:27', NULL),
(126, 9, 42, 1, '9.1.3', '126', 'Desain Komunikasi Visual', 3, '0000-00-00 00:00:00', '2020-07-18 14:04:29', NULL),
(127, 9, 42, 1, '9.1.4', '127', 'Desain Interior dan Teknik Furnitur', 4, '0000-00-00 00:00:00', '2020-07-18 14:04:32', NULL),
(128, 9, 42, 1, '9.1.5', '128', 'Animasi', 3, '0000-00-00 00:00:00', '2020-07-18 14:04:34', NULL),
(129, 9, 43, 1, '9.2.1', '129', 'Kriya Kreatif Batik dan Tekstil', 3, '0000-00-00 00:00:00', '2020-07-18 14:04:37', NULL),
(130, 9, 43, 1, '9.2.2', '130', 'Kriya Kreatif Batik dan Imitasi', 3, '0000-00-00 00:00:00', '2020-07-18 14:04:41', NULL),
(131, 9, 43, 1, '9.2.3', '131', 'Kriya Kreatif Keramik', 3, '0000-00-00 00:00:00', '2020-07-18 14:04:43', NULL),
(132, 9, 43, 1, '9.2.4', '132', 'Kriya Kreatif Logam dan Perhiasan', 3, '0000-00-00 00:00:00', '2020-07-18 14:04:46', NULL),
(133, 9, 43, 1, '9.2.5', '133', 'Kriya Kreatif Kayu dan Rotan', 3, '0000-00-00 00:00:00', '2020-07-18 14:04:48', NULL),
(134, 9, 44, 1, '9.3.1', '134', 'Seni Musik Klasik', 3, '0000-00-00 00:00:00', '2020-07-18 14:04:51', NULL),
(135, 9, 44, 1, '9.3.2', '135', 'Seni Musik Populer', 3, '0000-00-00 00:00:00', '2020-07-18 14:04:53', NULL),
(136, 9, 45, 1, '9.4.1', '136', 'Seni Tari', 3, '0000-00-00 00:00:00', '2020-07-18 14:04:56', NULL),
(137, 9, 45, 1, '9.4.2', '137', 'Penataan Tari', 4, '0000-00-00 00:00:00', '2020-07-18 14:04:59', NULL),
(138, 9, 46, 1, '9.5.1', '138', 'Seni Karawitan', 3, '0000-00-00 00:00:00', '2020-07-18 14:05:01', NULL),
(139, 9, 46, 1, '9.5.2', '139', 'Penataan Karawitan', 4, '0000-00-00 00:00:00', '2020-07-18 14:05:03', NULL),
(140, 9, 47, 1, '9.6.1', '140', 'Seni Pedalangan', 3, '0000-00-00 00:00:00', '2020-07-18 14:05:06', NULL),
(141, 9, 48, 1, '9.7.1', '141', 'Pemeranan', 3, '0000-00-00 00:00:00', '2020-07-18 14:05:09', NULL),
(142, 9, 48, 1, '9.7.2', '142', 'Tata Artistik Teater', 3, '0000-00-00 00:00:00', '2020-07-18 14:05:11', NULL),
(143, 9, 49, 1, '9.8.1', '143', 'Produksi dan Siaran Program Radio', 3, '0000-00-00 00:00:00', '2020-07-18 14:05:14', NULL),
(144, 9, 49, 1, '9.8.2', '144', 'Produksi dan Siaran Program Televisi', 3, '0000-00-00 00:00:00', '2020-07-18 14:05:17', NULL),
(145, 9, 49, 1, '9.8.3', '145', 'Produksi Film dan Program Televisi', 4, '0000-00-00 00:00:00', '2020-07-18 14:05:19', NULL),
(146, 9, 49, 1, '9.8.4', '146', 'Produksi Film', 3, '0000-00-00 00:00:00', '2020-07-18 14:05:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ref_kota`
--

CREATE TABLE `ref_kota` (
  `id_kota` varchar(100) NOT NULL,
  `id_provinsi` varchar(100) DEFAULT NULL,
  `nama_kota` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_kota`
--

INSERT INTO `ref_kota` (`id_kota`, `id_provinsi`, `nama_kota`, `created_at`, `updated_at`, `deleted_at`) VALUES
('016000  ', '010000  ', 'Kota Jakarta Pusat', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('016100  ', '010000  ', 'Kota Jakarta Utara', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('016200  ', '010000  ', 'Kota Jakarta Barat', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('016300  ', '010000  ', 'Kota Jakarta Selatan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('016400  ', '010000  ', 'Kota Jakarta Timur', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('060100  ', '060000  ', 'Kab. Aceh Besar', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('060300  ', '060000  ', 'Kab. Aceh Utara', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('060400  ', '060000  ', 'Kab. Aceh Timur', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('060500  ', '060000  ', 'Kab. Aceh Tengah', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('060600  ', '060000  ', 'Kab. Aceh Barat', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('060700  ', '060000  ', 'Kab. Aceh Selatan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('060800  ', '060000  ', 'Kab. Aceh Tenggara', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('061100  ', '060000  ', 'Kab. Simeulue', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('061200  ', '060000  ', 'Kab. Bireuen', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('061300  ', '060000  ', 'Kab. Aceh Singkil', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('061400  ', '060000  ', 'Kab. Aceh Tamiang', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('061500  ', '060000  ', 'Kab. Nagan Raya', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('061600  ', '060000  ', 'Kab. Aceh Jaya', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('061700  ', '060000  ', 'Kab. Aceh Barat Daya', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('061800  ', '060000  ', 'Kab. Gayo Lues', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('061900  ', '060000  ', 'Kab. Bener Meriah', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('062000  ', '060000  ', 'Kab. Pidie Jaya', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('066000  ', '060000  ', 'Kota Sabang', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('066100  ', '060000  ', 'Kota Banda Aceh', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('066200  ', '060000  ', 'Kota Lhokseumawe', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('066300  ', '060000  ', 'Kota Langsa', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('066400  ', '060000  ', 'Kota Subulussalam', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('220100  ', '220000  ', 'Kab. Buleleng', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('220200  ', '220000  ', 'Kab. Jembrana', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('220300  ', '220000  ', 'Kab. Tabanan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('220500  ', '220000  ', 'Kab. Gianyar', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('220600  ', '220000  ', 'Kab. Klungkung', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('220700  ', '220000  ', 'Kab. Bangli', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('220800  ', '220000  ', 'Kab. Karang Asem', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('226000  ', '220000  ', 'Kota Denpasar', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('﻿016500  ', '010000', 'Kab. Kepulauan Seribu', '0000-00-00 00:00:00', '2020-07-25 15:50:54', NULL),
('﻿060200  ', '060000  ', 'Kab. Pidie', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('﻿220400  ', '220000  ', 'Kab. Badung', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ref_program_keahlian`
--

CREATE TABLE `ref_program_keahlian` (
  `id_program_keahlian` int(10) UNSIGNED NOT NULL,
  `id_bidang_keahlian` int(11) UNSIGNED DEFAULT NULL,
  `id_sk_dikdasmen` int(11) UNSIGNED DEFAULT NULL,
  `kode_program_keahlian` varchar(10) NOT NULL,
  `nama_program_keahlian` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_program_keahlian`
--

INSERT INTO `ref_program_keahlian` (`id_program_keahlian`, `id_bidang_keahlian`, `id_sk_dikdasmen`, `kode_program_keahlian`, `nama_program_keahlian`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '1.1', 'Teknologi Konstruksi dan Properti', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(2, 1, 1, '1.2', 'Teknik Geomatika dan Geospasial', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(3, 1, 1, '1.3', 'Teknik Ketenagalistrikan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(4, 1, 1, '1.4', 'Teknik Mesin', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(5, 1, 1, '1.5', 'Teknologi Pesawat Udara', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(6, 1, 1, '1.6', 'Teknik Grafika', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(7, 1, 1, '1.7', 'Teknik Instrumentasi Industri', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(8, 1, 1, '1.8', 'Teknik Industri', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(9, 1, 1, '1.9', 'Teknologi Tekstil', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(10, 1, 1, '1.10', 'Teknik Kimia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(11, 1, 1, '1.11', 'Teknik Otomotif', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(12, 1, 1, '1.12', 'Teknik Perkapalan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(13, 1, 1, '1.13', 'Teknik Elektronika', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(14, 2, 1, '2.1', 'Teknik Perminyakan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(15, 2, 1, '2.2', 'Geologi Pertambangan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(16, 2, 1, '2.3', 'Teknik Energi Terbarukan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(17, 3, 1, '3.1', 'Teknik Komputer dan Informatika', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(18, 3, 1, '3.2', 'Teknik Telekomunikasi', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(19, 4, 1, '4.1', 'Keperawatan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(20, 4, 1, '4.2', 'Kesehatan Gigi', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(21, 4, 1, '4.3', 'Teknologi Laboratorium Medik', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(22, 4, 1, '4.4', 'Farmasi', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(23, 4, 1, '4.5', 'Pekerjaan Sosial', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(24, 5, 1, '5.1', 'Agribisnis Tanaman', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(25, 5, 1, '5.2', 'Agribisnis Ternak', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(26, 5, 1, '5.3', 'Kesehatan Hewan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(27, 5, 1, '5.4', 'Agribisnis Pengolahan Hasil Pertanian', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(28, 5, 1, '5.5', 'Teknik Pertanian', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(29, 5, 1, '5.6', 'Kehutanan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(30, 6, 1, '6.1', 'Pelayaran Kapal Penangkap Ikan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(31, 6, 1, '6.2', 'Pelayaran Kapal Niaga', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(32, 6, 1, '6.3', 'Perikanan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(33, 6, 1, '6.4', 'Pengolahan Hasil Perikanan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(34, 7, 1, '7.1', 'Bisnis dan Pemasaran', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(35, 7, 1, '7.2', 'Manajemen Perkantoran', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(36, 7, 1, '7.3', 'Akuntansi dan Keuangan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(37, 7, 1, '7.4', 'Logistik', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(38, 8, 1, '8.1', 'Perhotelan dan Jasa Pariwisata', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(39, 8, 1, '8.2', 'Kuliner', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(40, 8, 1, '8.3', 'Tata Kecantikan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(41, 8, 1, '8.4', 'Tata Busana', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(42, 9, 1, '9.1', 'Seni Rupa', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(43, 9, 1, '9.2', 'Desain dan Produk Kreatif Kriya', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(44, 9, 1, '9.3', 'Seni Musik', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(45, 9, 1, '9.4', 'Seni Tari', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(46, 9, 1, '9.5', 'Seni Karawitan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(47, 9, 1, '9.6', 'Seni Pedalangan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(48, 9, 1, '9.7', 'Seni Kuliner', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(49, 9, 1, '9.8', 'Seni Broadcasting dan Film', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ref_provinsi`
--

CREATE TABLE `ref_provinsi` (
  `id_provinsi` varchar(100) NOT NULL,
  `nama_provinsi` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_provinsi`
--

INSERT INTO `ref_provinsi` (`id_provinsi`, `nama_provinsi`, `created_at`, `updated_at`, `deleted_at`) VALUES
('010000', 'DKI Jakarta', '0000-00-00 00:00:00', '2020-07-13 10:21:04', NULL),
('020000', 'Jawa Barat', '0000-00-00 00:00:00', '2020-07-13 10:21:24', NULL),
('030000', 'Jawa Tengah', '0000-00-00 00:00:00', '2020-07-13 10:21:30', NULL),
('040000', 'DIY Yogyakarta', '0000-00-00 00:00:00', '2020-07-13 10:21:41', NULL),
('050000', 'Jawa Timur', '0000-00-00 00:00:00', '2020-07-13 10:21:46', NULL),
('060000', 'Nanggroe Aceh Darussalam', '0000-00-00 00:00:00', '2020-07-13 10:21:51', NULL),
('070000', 'Sumatera Utara', '0000-00-00 00:00:00', '2020-07-13 10:21:55', NULL),
('080000', 'Sumatera Barat', '0000-00-00 00:00:00', '2020-07-13 10:21:59', NULL),
('090000', 'Riau', '0000-00-00 00:00:00', '2020-07-13 10:22:03', NULL),
('100000', 'Jambi', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('110000', 'Sumatera Selatan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('120000', 'Lampung', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('130000', 'Kalimantan Barat', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('140000', 'Kalimantan Tengah', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('150000', 'Kalimantan Selatan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('160000', 'Kalimantan Timur', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('170000', 'Sulawesi Utara', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('180000', 'Sulawesi Tengah', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('190000', 'Sulawesi Selatan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('200000', 'Sulawesi Tenggara', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('210000', 'Maluku', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('220000', 'Bali', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('230000', 'Nusa Tenggara Barat', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('240000', 'Nusa Tenggara Timur', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('250000', 'Papua', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('260000', 'Bengkulu', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('270000', 'Maluku Utara', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('280000', 'Banten', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('290000', 'Bangka Belitung', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('300000', 'Gorontalo', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('310000', 'Kepulauan Riau', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('320000', 'Papua Barat', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('330000', 'Sulawesi Barat', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ref_sk_dikdasmen`
--

CREATE TABLE `ref_sk_dikdasmen` (
  `id_sk_dikdasmen` int(10) UNSIGNED NOT NULL,
  `nomor` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `tentang` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_sk_dikdasmen`
--

INSERT INTO `ref_sk_dikdasmen` (`id_sk_dikdasmen`, `nomor`, `tanggal`, `tentang`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '07/D.D5/KK/2019', '2019-05-23', 'Spektrum SMK baru', '2020-07-13 10:00:15', '2020-07-13 10:00:15', NULL),
(2, '07/D.D5/KK/2019', '2019-05-23', 'Spektrum SMK baru', '2020-07-13 10:05:05', '2020-07-13 10:05:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_aktivitas_siswa`
--

CREATE TABLE `tb_aktivitas_siswa` (
  `id_aktivitas_siswa` int(10) UNSIGNED NOT NULL,
  `id_grup` int(10) UNSIGNED DEFAULT NULL,
  `id_folder` int(10) UNSIGNED DEFAULT NULL,
  `id_entitas` int(10) UNSIGNED NOT NULL,
  `nama_entitas` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_anggota_grup`
--

CREATE TABLE `tb_anggota_grup` (
  `id_grup` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_bahan_ajar`
--

CREATE TABLE `tb_bahan_ajar` (
  `id_bahan_ajar` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED DEFAULT NULL,
  `id_grup` int(10) UNSIGNED DEFAULT NULL,
  `tipe_bahan_ajar` enum('presentasi','modul','video','audio','lkpd','lainnya') NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `tipe_file` enum('gambar','audio','video','lainnya') DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `jumlah_like` int(10) UNSIGNED DEFAULT NULL,
  `jumlah_dislike` int(10) UNSIGNED DEFAULT NULL,
  `jumlah_komentar` int(10) UNSIGNED DEFAULT NULL,
  `diunduh_sebanyak` int(10) UNSIGNED DEFAULT NULL,
  `status` enum('perbaikan','siap_validasi','valid') NOT NULL,
  `divalidasi_oleh` int(10) UNSIGNED DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_diskusi`
--

CREATE TABLE `tb_diskusi` (
  `id_diskusi` int(10) UNSIGNED NOT NULL,
  `id_grup` int(10) UNSIGNED DEFAULT NULL,
  `id_folder` int(10) UNSIGNED DEFAULT NULL,
  `judul_diskusi` varchar(100) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `aktif_sampai` datetime DEFAULT NULL,
  `status` enum('aktif','tidak_aktif') DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_file`
--

CREATE TABLE `tb_file` (
  `id_file` int(10) UNSIGNED NOT NULL,
  `id_grup` int(10) UNSIGNED DEFAULT NULL,
  `id_folder` int(10) UNSIGNED DEFAULT NULL,
  `judul_file` varchar(100) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `aktif_sampai` datetime DEFAULT NULL,
  `status` enum('aktif','tidak_aktif') DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_folder`
--

CREATE TABLE `tb_folder` (
  `id_folder` int(10) UNSIGNED NOT NULL,
  `id_grup` int(10) UNSIGNED DEFAULT NULL,
  `parent_folder` int(10) UNSIGNED DEFAULT NULL,
  `nama_folder` varchar(100) NOT NULL,
  `status` enum('aktif','tidak_aktif') DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_grup`
--

CREATE TABLE `tb_grup` (
  `id_grup` int(10) UNSIGNED NOT NULL,
  `id_mata_pelajaran` int(10) UNSIGNED DEFAULT NULL,
  `dibuat_oleh` int(10) UNSIGNED DEFAULT NULL,
  `akses_grup` enum('terbuka','tertutup') NOT NULL,
  `tipe_grup` enum('guru','kelas') NOT NULL,
  `kode` varchar(100) DEFAULT NULL,
  `deskripsi` varchar(255) DEFAULT NULL,
  `foto_grup` varchar(255) DEFAULT NULL,
  `jumlah_anggota` int(10) UNSIGNED DEFAULT NULL,
  `jumlah_maks_anggota` int(10) UNSIGNED DEFAULT NULL,
  `jumlah_soal` int(10) UNSIGNED DEFAULT NULL,
  `jumlah_bahan_ajar` int(10) UNSIGNED DEFAULT NULL,
  `jumlah_perangkat_pembelajaran` int(10) UNSIGNED DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_guru`
--

CREATE TABLE `tb_guru` (
  `id_guru` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) UNSIGNED DEFAULT NULL,
  `nuptk` varchar(20) NOT NULL,
  `nip` varchar(20) DEFAULT NULL,
  `nama_lengkap` varchar(255) NOT NULL,
  `jenis_kelamin` enum('L','P') NOT NULL,
  `tempat_lahir` varchar(25) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `foto_profile` text DEFAULT NULL,
  `bio` text DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_guru_mengampu_mata_pelajaran`
--

CREATE TABLE `tb_guru_mengampu_mata_pelajaran` (
  `id_guru` int(10) UNSIGNED NOT NULL,
  `id_mata_pelajaran` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kisi_kisi`
--

CREATE TABLE `tb_kisi_kisi` (
  `id_kisi_kisi` int(10) UNSIGNED NOT NULL,
  `id_kompetensi_dasar` int(10) UNSIGNED DEFAULT NULL,
  `id_user` int(10) UNSIGNED DEFAULT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_lampiran`
--

CREATE TABLE `tb_lampiran` (
  `id_lampiran` int(10) UNSIGNED NOT NULL,
  `id_tugas` int(10) UNSIGNED DEFAULT NULL,
  `id_diskusi` int(10) UNSIGNED DEFAULT NULL,
  `nama_file` varchar(100) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_link`
--

CREATE TABLE `tb_link` (
  `id_link` int(10) UNSIGNED NOT NULL,
  `id_grup` int(10) UNSIGNED DEFAULT NULL,
  `id_folder` int(10) UNSIGNED DEFAULT NULL,
  `judul_link` varchar(100) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `aktif_sampai` datetime DEFAULT NULL,
  `status` enum('aktif','tidak_aktif') DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_mata_pelajaran`
--

CREATE TABLE `tb_mata_pelajaran` (
  `id_mata_pelajaran` int(10) UNSIGNED NOT NULL,
  `id_bidang_keahlian` int(11) UNSIGNED DEFAULT NULL,
  `bidang_keahlian` varchar(100) DEFAULT NULL,
  `id_program_keahlian` int(11) UNSIGNED DEFAULT NULL,
  `program_keahlian` varchar(100) DEFAULT NULL,
  `id_kompetensi_keahlian` int(11) UNSIGNED DEFAULT NULL,
  `kompetensi_keahlian` varchar(100) DEFAULT NULL,
  `nama_mapel` varchar(100) NOT NULL,
  `jenis_mapel` enum('A','B','C1','C2','C3') NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_mata_pelajaran`
--

INSERT INTO `tb_mata_pelajaran` (`id_mata_pelajaran`, `id_bidang_keahlian`, `bidang_keahlian`, `id_program_keahlian`, `program_keahlian`, `id_kompetensi_keahlian`, `kompetensi_keahlian`, `nama_mapel`, `jenis_mapel`, `created_at`, `updated_at`, `deleted_at`) VALUES
(9, NULL, 'Semua Bidang Keahlian', NULL, 'Semua Program Keahlian', NULL, 'Semua Kompetensi Keahlian', 'Pendidikan Agama Islam dan Budi Pekerti', 'A', '0000-00-00 00:00:00', '2020-07-14 09:16:26', NULL),
(11, NULL, 'Semua Bidang Keahlian', NULL, 'Semua Program Keahlian', NULL, 'Semua Kompetensi Keahlian', 'Pendidikan Agama Kristen dan  Budi Pekerti', 'A', '0000-00-00 00:00:00', '2020-07-14 09:16:32', NULL),
(12, NULL, 'Semua Bidang Keahlian', NULL, 'Semua Program Keahlian', NULL, 'Semua Kompetensi Keahlian', 'Pendidikan Agama Katolik dan Budi Pekerti', 'A', '0000-00-00 00:00:00', '2020-07-14 09:16:39', NULL),
(13, NULL, 'Semua Bidang Keahlian', NULL, 'Semua Program Keahlian', NULL, 'Semua Kompetensi Keahlian', 'Pendidikan Agama Hindu dan Budi Pekerti', 'A', '0000-00-00 00:00:00', '2020-07-14 09:16:45', NULL),
(14, NULL, 'Semua Bidang Keahlian', NULL, 'Semua Program Keahlian', NULL, 'Semua Kompetensi Keahlian', 'Pendidikan Agama Buddha dan Budi Pekerti', 'A', '0000-00-00 00:00:00', '2020-07-14 09:16:58', NULL),
(15, NULL, 'Semua Bidang Keahlian', NULL, 'Semua Program Keahlian', NULL, 'Semua Kompetensi Keahlian', 'Pendidikan Agama Konghucu dan Budi Pekerti', 'A', '0000-00-00 00:00:00', '2020-07-14 09:17:04', NULL),
(16, NULL, 'Semua Bidang Keahlian', NULL, 'Semua Program Keahlian', NULL, 'Semua Kompetensi Keahlian', 'Pendidikan Pancasila dan Kewarganegaraan', 'A', '0000-00-00 00:00:00', '2020-07-14 09:17:13', NULL),
(17, NULL, 'Semua Bidang Keahlian', NULL, 'Semua Program Keahlian', NULL, 'Semua Kompetensi Keahlian', 'Bahasa Indonesia', 'A', '0000-00-00 00:00:00', '2020-07-14 09:17:20', NULL),
(18, 3, 'Teknologi Informasi dan Komunikasi', NULL, 'Semua Program Keahlian', NULL, 'Semua Kompetensi Keahlian', 'Matematika', 'A', '0000-00-00 00:00:00', '2020-07-14 11:06:20', NULL),
(19, NULL, 'Semua Bidang Keahlian', NULL, 'Semua Program Keahlian', NULL, 'Semua Kompetensi Keahlian', 'Sejarah Indonesia', 'A', '0000-00-00 00:00:00', '2020-07-18 11:53:35', NULL),
(20, NULL, 'Semua Bidang Keahlian', NULL, 'Semua Program Keahlian', NULL, 'Semua Kompetensi Keahlian', 'Bahasa Inggris', 'A', '0000-00-00 00:00:00', '2020-07-14 09:17:45', NULL),
(21, NULL, 'Semua Bidang Keahlian', NULL, 'Semua Program Keahlian', NULL, 'Semua Kompetensi Keahlian', 'Bahasa Arab', 'A', '0000-00-00 00:00:00', '2020-07-14 09:17:51', NULL),
(22, NULL, 'Semua Bidang Keahlian', NULL, 'Semua Program Keahlian', NULL, 'Semua Kompetensi Keahlian', 'Bahasa Jepang', 'A', '0000-00-00 00:00:00', '2020-07-14 09:17:57', NULL),
(23, NULL, 'Semua Bidang Keahlian', NULL, 'Semua Program Keahlian', NULL, 'Semua Kompetensi Keahlian', 'Bahasa Jerman', 'A', '0000-00-00 00:00:00', '2020-07-14 09:18:04', NULL),
(24, NULL, 'Semua Bidang Keahlian', NULL, 'Semua Program Keahlian', NULL, 'Semua Kompetensi Keahlian', 'Bahasa Korea', 'A', '0000-00-00 00:00:00', '2020-07-14 09:18:13', NULL),
(25, NULL, 'Semua Bidang Keahlian', NULL, 'Semua Program Keahlian', NULL, 'Semua Kompetensi Keahlian', 'Bahasa Mandarin', 'A', '0000-00-00 00:00:00', '2020-07-14 09:18:18', NULL),
(26, NULL, 'Semua Bidang Keahlian', NULL, 'Semua Program Keahlian', NULL, 'Semua Kompetensi Keahlian', 'Bahasa Prancis', 'A', '0000-00-00 00:00:00', '2020-07-14 09:18:24', NULL),
(27, NULL, 'Semua Bidang Keahlian', NULL, 'Semua Program Keahlian', NULL, 'Semua Kompetensi Keahlian', 'Seni Budaya', 'B', '0000-00-00 00:00:00', '2020-07-14 13:29:56', NULL),
(28, NULL, 'Semua Bidang Keahlian', NULL, 'Semua Program Keahlian', NULL, 'Semua Kompetensi Keahlian', 'Pendidikan Jasmani, Olahraga dan Kesehatan', 'B', '0000-00-00 00:00:00', '2020-07-14 13:30:00', NULL),
(29, 3, 'Teknologi Informasi dan Komunikasi', NULL, 'Semua Program Keahlian', NULL, 'Semua Kompetensi Keahlian', 'Simulasi dan Komunikasi Digital', 'C1', '0000-00-00 00:00:00', '2020-07-18 11:52:45', NULL),
(30, 3, 'Teknologi Informasi dan Komunikasi', NULL, 'Semua Program Keahlian', NULL, 'Semua Kompetensi Keahlian', 'Fisika', 'C1', '0000-00-00 00:00:00', '2020-07-18 11:52:42', NULL),
(31, 3, 'Teknologi Informasi dan Komunikasi', NULL, 'Semua Program Keahlian', NULL, 'Semua Kompetensi Keahlian', 'Kimia', 'C1', '0000-00-00 00:00:00', '2020-07-18 11:52:41', NULL),
(32, 3, 'Teknologi Informasi dan Komunikasi', 17, 'Teknik Komputer dan Informatika', NULL, 'Semua Kompetensi Keahlian', 'Sistem Komputer', 'C2', '0000-00-00 00:00:00', '2020-07-18 11:52:39', NULL),
(33, 3, 'Teknologi Informasi dan Komunikasi', 17, 'Teknik Komputer dan Informatika', NULL, 'Semua Kompetensi Keahlian', 'Komputer dan Jaringan Dasar', 'C2', '0000-00-00 00:00:00', '2020-07-18 11:52:46', NULL),
(34, 3, 'Teknologi Informasi dan Komunikasi', 17, 'Teknik Komputer dan Informatika', NULL, 'Semua Kompetensi Keahlian', 'Program Dasar', 'C2', '0000-00-00 00:00:00', '2020-07-18 11:52:49', NULL),
(35, 3, 'Teknologi Informasi dan Komunikasi', 17, 'Teknik Komputer dan Informatika', NULL, 'Semua Kompetensi Keahlian', 'Dasar Desain Grafis', 'C2', '0000-00-00 00:00:00', '2020-07-18 11:52:50', NULL),
(36, 3, 'Teknologi Informasi dan Komunikasi', 17, 'Teknik Komputer dan Informatika', 68, 'Sistem Informatika, Jaringan dan Aplikasi', 'Infrastruktur Komputasi Awan', 'C3', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(37, 3, 'Teknologi Informasi dan Komunikasi', 17, 'Teknik Komputer dan Informatika', 68, 'Sistem Informatika, Jaringan dan Aplikasi', 'Platform Komputasi Awan', 'C3', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(38, 3, 'Teknologi Informasi dan Komunikasi', 17, 'Teknik Komputer dan Informatika', 68, 'Sistem Informatika, Jaringan dan Aplikasi', 'Layanan Komputasi Awan', 'C3', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(39, 3, 'Teknologi Informasi dan Komunikasi', 17, 'Teknik Komputer dan Informatika', 68, 'Sistem Informatika, Jaringan dan Aplikasi', 'Sistem Internet of Things', 'C3', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(40, 3, 'Teknologi Informasi dan Komunikasi', 17, 'Teknik Komputer dan Informatika', 68, 'Sistem Informatika, Jaringan dan Aplikasi', 'Sistem Keamanan Jaringan', 'C3', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(41, 3, 'Teknologi Informasi dan Komunikasi', 17, 'Teknik Komputer dan Informatika', 68, 'Sistem Informatika, Jaringan dan Aplikasi', 'Produk Kreatif dan Kewirausahaan', 'C3', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(42, 3, 'Teknologi Informasi dan Komunikasi', 17, 'Teknik Komputer dan Informatika', 65, 'Rekayasa Perangkat Lunak', 'Pemodelan Perangkat Lunak', 'C3', '2020-07-16 14:25:42', '2020-07-16 14:25:42', NULL),
(43, 3, 'Teknologi Informasi dan Komunikasi', 17, 'Teknik Komputer dan Informatika', 65, 'Rekayasa Perangkat Lunak', 'Basis Data', 'C3', '2020-07-16 14:25:42', '2020-07-16 14:25:42', NULL),
(44, 3, 'Teknologi Informasi dan Komunikasi', 17, 'Teknik Komputer dan Informatika', 65, 'Rekayasa Perangkat Lunak', 'Pemrograman Berorientasi Objek', 'C3', '2020-07-16 14:25:42', '2020-07-16 14:25:42', NULL),
(45, 3, 'Teknologi Informasi dan Komunikasi', 17, 'Teknik Komputer dan Informatika', 65, 'Rekayasa Perangkat Lunak', 'Pemrograman Web dan Perangkat Bergerak', 'C3', '2020-07-16 14:25:42', '2020-07-16 14:25:42', NULL),
(46, 3, 'Teknologi Informasi dan Komunikasi', 17, 'Teknik Komputer dan Informatika', 65, 'Rekayasa Perangkat Lunak', 'Produk Kreatif dan Kewirausahaan', 'C3', '2020-07-16 14:25:42', '2020-07-16 14:25:42', NULL),
(47, 3, 'Teknologi Informasi dan Komunikasi', 17, 'Teknik Komputer dan Informatika', 66, 'Teknik Komputer dan Jaringan', 'Teknologi Jaringan Berbasis Luas (WAN)', 'C3', '2020-07-16 14:32:28', '2020-07-16 14:32:28', NULL),
(48, 3, 'Teknologi Informasi dan Komunikasi', 17, 'Teknik Komputer dan Informatika', 66, 'Teknik Komputer dan Jaringan', 'Administrasi Infrastruktur Jaringan', 'C3', '2020-07-16 14:32:28', '2020-07-16 14:32:28', NULL),
(49, 3, 'Teknologi Informasi dan Komunikasi', 17, 'Teknik Komputer dan Informatika', 66, 'Teknik Komputer dan Jaringan', 'Administrasi Sistem Jaringan', 'C3', '2020-07-16 14:32:28', '2020-07-16 14:32:28', NULL),
(50, 3, 'Teknologi Informasi dan Komunikasi', 17, 'Teknik Komputer dan Informatika', 66, 'Teknik Komputer dan Jaringan', 'Teknologi Layanan Jaringan', 'C3', '2020-07-16 14:32:28', '2020-07-16 14:32:28', NULL),
(51, 3, 'Teknologi Informasi dan Komunikasi', 17, 'Teknik Komputer dan Informatika', 66, 'Teknik Komputer dan Jaringan', 'Produk Kreatif dan Kewirausahaan', 'C3', '2020-07-16 14:32:28', '2020-07-16 14:32:28', NULL),
(52, 3, 'Teknologi Informasi dan Komunikasi', 17, 'Teknik Komputer dan Informatika', 67, 'Multimedia', 'Desain Grafis Percetakan', 'C3', '2020-07-16 14:36:27', '2020-07-16 14:36:27', NULL),
(53, 3, 'Teknologi Informasi dan Komunikasi', 17, 'Teknik Komputer dan Informatika', 67, 'Multimedia', 'Desain Media Interaktif', 'C3', '2020-07-16 14:36:27', '2020-07-16 14:36:27', NULL),
(54, 3, 'Teknologi Informasi dan Komunikasi', 17, 'Teknik Komputer dan Informatika', 67, 'Multimedia', 'Animasi 2D dan 3D', 'C3', '2020-07-16 14:36:27', '2020-07-16 14:36:27', NULL),
(55, 3, 'Teknologi Informasi dan Komunikasi', 17, 'Teknik Komputer dan Informatika', 67, 'Multimedia', 'Teknik Pengolahan Audio dan Video', 'C3', '2020-07-16 14:36:27', '2020-07-16 14:36:27', NULL),
(56, 3, 'Teknologi Informasi dan Komunikasi', 17, 'Teknik Komputer dan Informatika', 67, 'Multimedia', 'Produk Kreatif dan Kewirausahaan', 'C3', '2020-07-16 14:36:27', '2020-07-16 14:36:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_objek_pemberitahuan`
--

CREATE TABLE `tb_objek_pemberitahuan` (
  `id_objek_pemberitahuan` int(10) UNSIGNED NOT NULL,
  `id_tipe_entitas_pemberitahuan` int(10) UNSIGNED DEFAULT NULL,
  `id_entitas` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_opsi_jawaban`
--

CREATE TABLE `tb_opsi_jawaban` (
  `id_opsi_jawaban` int(10) UNSIGNED NOT NULL,
  `id_soal` int(10) UNSIGNED DEFAULT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `tipe_file` enum('gambar','audio','video','lainnya') DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `feedback` varchar(255) DEFAULT NULL,
  `is_kunci` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_paket_soal`
--

CREATE TABLE `tb_paket_soal` (
  `id_paket_soal` int(10) UNSIGNED NOT NULL,
  `dibuat_oleh` int(10) UNSIGNED DEFAULT NULL,
  `id_grup` int(10) UNSIGNED DEFAULT NULL,
  `id_mata_pelajaran` int(10) UNSIGNED DEFAULT NULL,
  `poin_otomatis` tinyint(1) NOT NULL,
  `poin_maksimal` int(11) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `jumlah_soal` int(10) UNSIGNED NOT NULL,
  `digunakan_sebanyak` int(10) UNSIGNED DEFAULT NULL,
  `diunduh_sebanyak` int(10) UNSIGNED DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_paket_soal_berisi_soal`
--

CREATE TABLE `tb_paket_soal_berisi_soal` (
  `id_paket_soal` int(10) UNSIGNED NOT NULL,
  `id_soal` int(10) UNSIGNED NOT NULL,
  `poin` int(10) UNSIGNED DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_pelaku_pemberitahuan`
--

CREATE TABLE `tb_pelaku_pemberitahuan` (
  `id_pelaku_pemberitahuan` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED DEFAULT NULL,
  `id_objek_pemberitahuan` int(10) UNSIGNED DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_penerima_pemberitahuan`
--

CREATE TABLE `tb_penerima_pemberitahuan` (
  `id_penerima_pemberitahuan` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED DEFAULT NULL,
  `id_objek_pemberitahuan` int(10) UNSIGNED DEFAULT NULL,
  `pesan` varchar(255) NOT NULL,
  `tautan` varchar(255) NOT NULL,
  `dilihat` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_perangkat_pembelajaran`
--

CREATE TABLE `tb_perangkat_pembelajaran` (
  `id_perangkat_pembelajaran` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED DEFAULT NULL,
  `id_grup` int(10) UNSIGNED DEFAULT NULL,
  `tipe_perangkat_pembelajaran` enum('prota','prosem','silabus','rpp','lainnya') NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `jumlah_like` int(10) UNSIGNED DEFAULT NULL,
  `jumlah_dislike` int(10) UNSIGNED DEFAULT NULL,
  `jumlah_komentar` int(10) UNSIGNED DEFAULT NULL,
  `diunduh_sebanyak` int(10) UNSIGNED DEFAULT NULL,
  `status` enum('perbaikan','siap_validasi','valid') NOT NULL,
  `divalidasi_oleh` int(10) UNSIGNED DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_post`
--

CREATE TABLE `tb_post` (
  `id_post` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED DEFAULT NULL,
  `id_grup` int(10) UNSIGNED DEFAULT NULL,
  `tipe_akses` enum('public','teman','pribadi','grup') NOT NULL,
  `deskripsi` varchar(255) DEFAULT NULL,
  `tipe_file` enum('gambar','audio','video','dokumen','lainnya') DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `jumlah_like` int(10) UNSIGNED DEFAULT NULL,
  `jumlah_komentar` int(10) UNSIGNED DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_quiz`
--

CREATE TABLE `tb_quiz` (
  `id_quiz` int(10) UNSIGNED NOT NULL,
  `id_paket_soal` int(10) UNSIGNED DEFAULT NULL,
  `id_grup` int(10) UNSIGNED DEFAULT NULL,
  `feedback` enum('skor','skor_jawaban','skor_jawaban_feedback') NOT NULL,
  `aktif` tinyint(1) NOT NULL DEFAULT 0,
  `review_jawaban` tinyint(1) NOT NULL DEFAULT 1,
  `nama_quiz` varchar(255) NOT NULL,
  `acak_soal` tinyint(1) NOT NULL DEFAULT 1,
  `acak_opsi_jawaban` tinyint(1) NOT NULL DEFAULT 1,
  `limit_waktu` tinyint(1) NOT NULL DEFAULT 1,
  `durasi_pengerjaan` time DEFAULT NULL,
  `aktif_dari` datetime DEFAULT NULL,
  `aktif_sampai` datetime DEFAULT NULL,
  `jumlah_pengerjaan` int(11) NOT NULL DEFAULT 1,
  `bisa_tunda` tinyint(1) NOT NULL DEFAULT 0,
  `skala_nilai` enum('100','10','abc','a+') NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_quiz_berisi_soal`
--

CREATE TABLE `tb_quiz_berisi_soal` (
  `id_quiz` int(10) UNSIGNED NOT NULL,
  `id_soal` int(10) UNSIGNED NOT NULL,
  `poin` int(10) UNSIGNED DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_riwayat_pendidikan`
--

CREATE TABLE `tb_riwayat_pendidikan` (
  `id_riwayat_pendidikan` int(10) UNSIGNED NOT NULL,
  `id_guru` int(11) UNSIGNED DEFAULT NULL,
  `institusi` varchar(100) NOT NULL,
  `program_studi` varchar(100) NOT NULL,
  `jenjang` varchar(10) NOT NULL,
  `gelar_akademik` varchar(20) NOT NULL,
  `tahun_lulus` varchar(10) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_riwayat_revisi_bahan_ajar`
--

CREATE TABLE `tb_riwayat_revisi_bahan_ajar` (
  `id_riwayat_revisi_bahan_ajar` int(10) UNSIGNED NOT NULL,
  `id_bahan_ajar` int(10) UNSIGNED DEFAULT NULL,
  `kode_revisi` varchar(255) NOT NULL,
  `catatan_revisi` varchar(255) NOT NULL,
  `tipe_bahan_ajar` enum('presentasi','modul','video','audio') NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `tipe_file` enum('gambar','dokumen','audio','video','lainnya') DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_riwayat_revisi_opsi_jawaban`
--

CREATE TABLE `tb_riwayat_revisi_opsi_jawaban` (
  `id_riwayat_revisi_opsi_jawaban` int(10) UNSIGNED NOT NULL,
  `id_riwayat_revisi_soal` int(10) UNSIGNED DEFAULT NULL,
  `tipe_opsi` enum('teks','gambar','audio','video') NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `tipe_file` enum('gambar','audio','video','lainnya') DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `feedback` varchar(255) NOT NULL,
  `is_kunci` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_riwayat_revisi_perangkat_pembelajaran`
--

CREATE TABLE `tb_riwayat_revisi_perangkat_pembelajaran` (
  `id_riwayat_revisi_perangkat_pembelajaran` int(10) UNSIGNED NOT NULL,
  `id_perangkat_pembelajaran` int(10) UNSIGNED DEFAULT NULL,
  `kode_revisi` varchar(255) NOT NULL,
  `catatan_revisi` varchar(255) NOT NULL,
  `tipe_perangkat_pembelajaran` enum('prota','prosem','silabus','rpp') NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_riwayat_revisi_soal`
--

CREATE TABLE `tb_riwayat_revisi_soal` (
  `id_riwayat_revisi_soal` int(10) UNSIGNED NOT NULL,
  `kode_revisi` varchar(255) NOT NULL,
  `id_soal` int(10) UNSIGNED DEFAULT NULL,
  `catatan_revisi` varchar(255) NOT NULL,
  `tipe_soal` enum('pilihan_ganda','isian_singkat','benar_salah') NOT NULL,
  `jumlah_jawaban` int(11) NOT NULL,
  `teks_soal` varchar(255) NOT NULL,
  `tipe_file` enum('gambar','audio','video','lainnya') DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `tingkat_kesulitan` enum('c1','c2','c3','c4','c5','c6') NOT NULL,
  `kunci_essay` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_sekolah`
--

CREATE TABLE `tb_sekolah` (
  `id_provinsi` varchar(100) DEFAULT NULL,
  `provinsi` varchar(100) DEFAULT NULL,
  `id_kota` varchar(100) DEFAULT NULL,
  `kota` varchar(100) DEFAULT NULL,
  `id_kecamatan` varchar(100) DEFAULT NULL,
  `kecamatan` varchar(100) DEFAULT NULL,
  `id_sekolah` varchar(100) NOT NULL,
  `npsn` varchar(100) NOT NULL,
  `nama_sekolah` varchar(100) NOT NULL,
  `bentuk` varchar(100) NOT NULL,
  `status` enum('N','S') NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `lintang` varchar(255) DEFAULT NULL,
  `bujur` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_sekolah`
--

INSERT INTO `tb_sekolah` (`id_provinsi`, `provinsi`, `id_kota`, `kota`, `id_kecamatan`, `kecamatan`, `id_sekolah`, `npsn`, `nama_sekolah`, `bentuk`, `status`, `alamat`, `lintang`, `bujur`, `created_at`, `updated_at`, `deleted_at`) VALUES
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016104  ', 'Kec. Koja', '003061B7-6FF4-49BC-874B-239549A21C7B', '20107465', 'SMKS TANJUNG PRIOK 1 JAKARTA', 'SMK', 'S', 'JL. MANGGA NO.3', '-6.1111000', '106.9103000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016204  ', 'Kec. Grogol Petamburan', '00415B1A-A9AE-4981-873A-04FBACD3ED83', 'NP932528', 'SMK PARIWISATA SUMBANGSIH', 'SMK', 'S', 'Jl. Dr. Muwardi II/13', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016309  ', 'Kec. Tebet', '005DC4C1-AB5A-44FE-A291-26AE9E3AA3E1', '20102537', 'SMK ASISI', 'SMK', 'S', 'JL. H. RAMLI NO 24 RT 05/03', '-6.2314000', '106.8422000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016207  ', 'Kec. Cengkareng', '005F87BB-0610-4C7B-A309-E31F92E96652', '20109526', 'SMKS YANDIKA', 'SMK', 'S', 'JL. MASJID NURUL HUDA NO. 25', '-6.1537000', '106.7310000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016203  ', 'Kec. Palmerah', '017A2286-F59D-488E-BB23-522AB342E471', '20101677', 'SMKS IBU PERTIWI 2 JAKARTA', 'SMK', 'S', 'JL. S. PARMAN NO. 69', '-6.1944000', '106.7975000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016403  ', 'Kec. Cipayung', '02506ED6-8480-40A2-8920-F9C47572F109', '69758520', 'SMK MEDICAL HIGH SCHOOL', 'SMK', 'S', ' Jl. Raya Munjul No. 2', '-6.3462280', '106.8997750', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016206  ', 'Kec. Taman Sari', '027B5F57-E51F-4CE2-8280-57DF3B5293AB', '20101472', 'SMKS SANTO LEO', 'SMK', 'S', 'JL. MANGGA BESAR RAYA/43', '-6.1493000', '106.8208000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', '02A59D54-04FF-4BA8-B053-D1CF14437AB9', '20103730', 'SMKS PAMI JAYA', 'SMK', 'S', 'JL. RAYA KALIMALANG', '-6.2486000', '106.9248000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016305  ', 'Kec. Kebayoran Lama', '02A65AA3-4027-4216-8FF7-15DA3DE27D7B', '20107364', 'SMKS YPUI JAKARTA', 'SMK', 'S', 'Jl. Praja Lapangan No. 8, RT. 014/01', '-6.2486000', '106.7799000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016306  ', 'Kec. Kebayoran Baru', '030F91AC-5871-4348-881D-723A43C9EBB5', '20102611', 'SMKS PURNAMA 1 JAKARTA', 'SMK', 'S', 'JL. TIRTAYASA 2/5', '-6.2412000', '106.8056000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016007  ', 'Kec. Sawah Besar', '03177E51-BB30-4A4E-9E06-044E74025959', '20107258', 'SMKS STRADA JAKARTA', 'SMK', 'S', 'JL. RAJAWALI SELATAN 2 NO. 1', '-6.1462000', '106.8393000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016103  ', 'Kec. Tanjung Priok', '0375A496-1CE0-44C4-B61A-DA371F7A2BD8', '20107415', 'SMKS DHARMA BUDHI BHAKTI PLUS', 'SMK', 'S', 'JL. AGUNG TENGAH 7 NO.1BLOK I-6 NO. 1', '-6.1283000', '106.8529000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', '047F633E-4225-4F7B-9CC6-CA1714FEC8B5', '20103262', 'SMKS BPS&K 1 JAKARTA', 'SMK', 'S', 'JL. BINA KARYA NO. 2', '-6.2334000', '106.9388000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016410  ', 'Kec. Matraman', '05703B82-CC61-4132-95E6-EF3D083A5245', '20103527', 'SMKS SATYA BHAKTI 2 JAKARTA', 'SMK', 'S', 'JL. SLAMET RIYADI 3', '-6.2104000', '106.8546000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016105  ', 'Kec. Kelapa Gading', '05F0362A-490F-45F0-AAD6-A3887EC77607', '20107409', 'SMKS HANG TUAH 2', 'SMK', 'S', 'JL. PERINTIS KEMERDEKAAN, KOMP TNI-AL', '-6.1681000', '106.8863000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016102  ', 'Kec. Pademangan', '05F5C6E7-BE4A-4166-8973-A9F020BB63C0', '20107454', 'SMKS RAUDHATUL JANNATUNNAIM', 'SMK', 'S', 'JL. BUDI MULIA', '-6.1389000', '106.8357000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016306  ', 'Kec. Kebayoran Baru', '0682C63E-335B-4E10-BC57-A216460E05F7', '20102578', 'SMKN 15 JAKARTA', 'SMK', 'N', 'JL. MATARAM 1', '-6.2332000', '106.8000000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016409  ', 'Kec. Pulo Gadung', '06E2B6AB-7F13-451D-A708-BBCBE99C4D1B', '20103778', 'SMKN 7 JAKARTA', 'SMK', 'N', 'JL. TENGGIRI NO. 1 RAWAMANGUN', '-6.1981000', '106.8919000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016105  ', 'Kec. Kelapa Gading', '074B9A07-0A4A-4D5A-B306-0E6FFE259A63', '20100775', 'SMKS JAYA JAKARTA', 'SMK', 'S', 'JL. PERINTIS KEMERDEKAAN', '-6.1748000', '106.9048000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016306  ', 'Kec. Kebayoran Baru', '076E93B8-C88A-4126-8007-2E4623EE94D9', '20102435', 'SMKS TARAKANITA JAKARTA', 'SMK', 'S', 'JL. PULO RAYA 4/17', '-6.2473000', '106.8140000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016405  ', 'Kec. Kramat Jati', '0780B88D-9971-4D10-9475-A0B0BBA7980F', '20103247', 'SMKS AL FATHIYAH JAKARTA', 'SMK', 'S', 'JL. MANUNGGAL 13 NO. 28 CONDET BALEKAMBANG KRAMAT JATI', '-6.2788000', '106.8537000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016104  ', 'Kec. Koja', '07C2D629-3375-43C9-A9D8-E39995C916CC', '20107441', 'SMKS NUSANTARA 2 JAKARTA', 'SMK', 'S', 'JL. KRAMAT JAYA GG. VIII', '-6.1190000', '106.9205000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016402  ', 'Kec. Ciracas', '0826DF4C-610D-4D07-9AB4-D653892E19C1', '20103525', 'SMKS WIDYA MANGGALA JAKARTA', 'SMK', 'S', 'JL. MUJAHIDIN 17', '-6.3022000', '106.8725000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016004  ', 'Kec. Johar Baru', '0917434B-73BB-4843-BDBC-F26D38377AB6', '20100105', 'SMKS KAMPUNG JAWA JAKARTA', 'SMK', 'S', 'JL. PERCETAKAN NEGARA II', '-6.1869000', '106.8564000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016402  ', 'Kec. Ciracas', '09924072-2A09-4D56-AD77-D1621680C93B', '20103761', 'SMKS MAHADHIKA 3 JAKARTA', 'SMK', 'S', 'JL.MANUNGGAL II RT04/04 NO.4', '-6.3083000', '106.8794000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016103  ', 'Kec. Tanjung Priok', '09CEB0F9-81FF-4D94-B3E6-A0D103914F92', '20107469', 'SMKS WIYATAMANDALA JAKARTA', 'SMK', 'S', 'JL. SWASEMBADA TIMUR XIII NO. 46-50, KEBON BAWANG', '-6.1251000', '106.8913000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016403  ', 'Kec. Cipayung', '0A51C8DC-5D11-41D1-AB88-8FA417A809F4', '20103779', 'SMKN 58 JAKARTA', 'SMK', 'N', 'JL. SMIK BAMBU APUS/TMII CIPAYUNG JAKARTA TIMUR', '-6.3194000', '106.8985000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016207  ', 'Kec. Cengkareng', '0A7D0DB6-CA7F-4955-B336-272835450C20', '20101685', 'SMKS EKA SAKTI', 'SMK', 'S', 'JL. UKIR RAYA NO. 30', '-6.1525000', '106.7465000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016310  ', 'Kec. Setia Budi', '0AC38A50-1B59-42A8-9FB7-E6E6F53B510A', '20102553', 'SMK DAARUL ULUUM', 'SMK', 'S', 'JL. KARET PEDURENAN RAYA NO 53', '-6.2325000', '106.8343000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016309  ', 'Kec. Tebet', '0AD8499C-7A82-4D8B-84D6-C70EB3CCB5B1', '20102640', 'SMKS KARYA GUNA JAKARTA', 'SMK', 'S', 'JL. MANGGARAI UTARA 1', '-6.2135000', '106.8534000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016208  ', 'Kec. Kali Deres', '0AF30EB8-5B68-451A-8288-A7F4E99C046A', '20101597', 'SMKS BENTENG GADING JAKARTA', 'SMK', 'S', 'JL. BOUGENVILLE NO. 50,KAMAL', '-6.1064000', '106.6992000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016404  ', 'Kec. Makasar', '0B52FD14-35EF-4F7D-8CD2-F8BFE18ECD16', '20103652', 'SMKS GUTAMA JAKARTA', 'SMK', 'S', 'JL. PERMATA NO. 17', '-6.2538000', '106.8795000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016204  ', 'Kec. Grogol Petamburan', '0B7732B4-9A38-4EEE-B12B-A55D79EDF81D', '77298527', 'SMK INSAN AQILAH 4', 'SMK', 'S', 'JL. PENERANGAN RAYA NO 1 A', '-6.1579040', '106.7726020', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016301  ', 'Kec. Jagakarsa', '0B90D91C-40D2-4BB2-8562-92728017EDE6', '20112443', 'SMK NEGERI 63 JAKARTA', 'SMK', 'N', 'Jl. Aselih No. 100', '-6.3576000', '106.8056000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016206  ', 'Kec. Taman Sari', '0BF2EA3F-CB08-4D7F-9933-4FDEC2C5A2C7', '20101504', 'SMKN 11 JAKARTA', 'SMK', 'N', 'JL. PINANGSIA I NO. 20', '-6.1403000', '106.8165000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016106  ', 'Kec. Cilincing', '0C2CCCA6-41B7-477C-9366-F4A6E6F17368', '20107446', 'SMKS PELAYARAN MALAHAYATI JAKARTA', 'SMK', 'S', 'Jl.Sungi Tiram No.43 Marunda Baru Cilincing Jakarta Utara', '-6.1244000', '106.9571000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016410  ', 'Kec. Matraman', '0D13BFDC-7086-439F-B443-8069C2D0E210', '20107593', 'SMKS BINA PANGUDI LUHUR JAKARTA', 'SMK', 'S', 'JL. KRAMAT ASEM RAYA NO. 54', '-6.2037000', '106.8704000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016103  ', 'Kec. Tanjung Priok', '0D6F888A-038F-47E6-9AE7-C7F1015F5E8D', '20107474', 'SMKS YASPI JAYA', 'SMK', 'S', 'JL. MASJID AL-BARKAH NO.16', '-6.1593000', '106.8740000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016405  ', 'Kec. Kramat Jati', '0D948487-FE73-49A6-BD17-A4854997529C', '20103537', 'SMKS RAHAYU MULYO', 'SMK', 'S', 'JL. RAYA BOGOR DEPAN PUSDISKES', '-6.2858000', '106.8714000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016103  ', 'Kec. Tanjung Priok', '0DE91B9C-8309-4EFF-9ADA-3CBD08A77A1A', '20107405', 'SMKS AL-KHAIRIYAH BAHARI', 'SMK', 'S', 'JL. BAHARI 3 A8 NO. 152', '-6.1183000', '106.8732000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016409  ', 'Kec. Pulo Gadung', '0E4B74E6-6F98-4819-B8C1-25EA1CA29BFF', '20103701', 'SMKS DIPONEGORO 1', 'SMK', 'S', 'JLN. SUNAN GIRI NO. 5', '-6.1968000', '106.8908000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016307  ', 'Kec. Mampang Prapatan', '0E64C33B-FE1F-4347-98F5-E0F092539F53', '20102530', 'SMKS BINA PUTRA JAKARTA', 'SMK', 'S', 'JL. KEMANG TIMUR RAYA NO. 50', '-6.2655000', '106.8235000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016304  ', 'Kec. Pesanggrahan', '0F5ECD56-5696-4371-B101-B0C85EBD0417', '20102533', 'SMKS BINA KUSUMA 1', 'SMK', 'S', 'JL. MERPATI RAYA 38 PESANGGRAHAN', '-6.2501000', '106.7596000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016101  ', 'Kec. Penjaringan', '0FA611C6-379B-4F0C-90EB-99C54DEAF24B', '20107439', 'SMKN 56 JAKARTA', 'SMK', 'N', 'JL. RAYA PLUIT TIMUR NO. 1 PENJARINGAN', '-6.1191000', '106.7951000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016207  ', 'Kec. Cengkareng', '0FCE8457-7E95-4536-8569-656DDF248826', '20101681', 'SMKS HARAPAN RAYA', 'SMK', 'S', 'JL. KAPUK RAYA GG SINAR 5', '-6.1538000', '106.7328000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', '101BED73-DEBE-41BB-9B9F-8E8E10953C23', '20103725', 'SMKS PARAMITHA JAKARTA', 'SMK', 'S', 'JL. RAYA KALIMALANG NO.68 CURUG', '-6.2493000', '106.9214000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016409  ', 'Kec. Pulo Gadung', '115D61E2-1F51-4D33-AB1F-FDF274DF4AD1', '20103265', 'SMKS BINA PEMBANGUNAN 1', 'SMK', 'S', 'JL. KAYU PUTIH SELIVE 44', '-6.1858000', '106.8959000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', '11C490ED-FD0F-44B6-B635-E09EE21018BF', '20177894', 'SMKS KAPIN 2', 'SMK', 'S', 'JL. RAYA KALIMALANG', '-6.2487000', '106.9299000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016406  ', 'Kec. Jatinegara', '12264201-0478-4EB8-9CDD-3731F0231F84', '20103774', 'SMKS MUARA INDONESIA JAKARTA', 'SMK', 'S', 'JL. CIPINANG MUARA 3 KAV PLN', '-6.2213000', '106.8929000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016001  ', 'Kec. Tanah Abang', '129F8251-59A7-4A01-B8A0-B476A054E11F', '20100142', 'SMKS MUHAMMADIYAH 2 JAKARTA', 'SMK', 'S', 'JL. KH. MAS MANSYUR NO. 65', '-6.1969000', '106.8144000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016004  ', 'Kec. Johar Baru', '12CCBC91-AAE4-4402-99C7-0BD88C64A274', '20100165', 'SMKN 31 JAKARTA', 'SMK', 'N', 'JL. KRAMAT JAYA BARU DII', '-6.1820000', '106.8518000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016403  ', 'Kec. Cipayung', '134BABC7-2A60-4BB7-AA48-C720B55961EE', '20103550', 'SMKS TRI SASTRA 2 JAKARTA', 'SMK', 'S', 'JL. SMP 157 RT.006/007 Lubang Buaya Cipayung Jakarta Timur', '-6.2930000', '106.9005000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016308  ', 'Kec. Pancoran', '13C32D26-78CF-4CF8-B377-9833624864D1', '20102450', 'SMKS WALISONGO JAKARTA', 'SMK', 'S', 'JL. KALIBATA TIMUR NO. 30', '-6.2628000', '106.8450000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016402  ', 'Kec. Ciracas', '14066D3E-9764-4CAB-BDF4-9E961B9EC7D6', '69873590', 'SMK Bina Nusa Mandiri', 'SMK', 'S', 'Jl. Raya Ciracas No. 1 Jakarta Timur', '-6.3466520', '106.8891290', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016306  ', 'Kec. Kebayoran Baru', '148B06DB-7FC7-44F9-A326-BB7E1A9DA9CF', '69867717', 'SMKS ASSA`ADAH', 'SMK', 'S', 'JL. H. SAID RADIO DALAM NO.29', '-6.2637900', '106.7955010', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016104  ', 'Kec. Koja', '151FDB13-5812-42A6-B3AA-49A4C0F39548', '20107460', 'SMKS SILIWANGI JAKARTA', 'SMK', 'S', 'JL. MAJA NO. 40 RT.10/06', '-6.1156000', '106.9034000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016103  ', 'Kec. Tanjung Priok', '1572D30A-CDAD-4CB6-86BF-3AC433CC2D81', '20107471', 'SMKS YANINDO JAKARTA', 'SMK', 'S', 'JL. R. E. MARTADINATA', '-6.1198000', '106.8668000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016405  ', 'Kec. Kramat Jati', '16A49519-E1CD-4583-80B9-5AC4C7B33889', '20103763', 'SMKS MAHADHIKA 2 JAKARTA', 'SMK', 'S', 'JL. PENGGILINGAN BARU I NO. 39', '-6.3026000', '106.8787000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016405  ', 'Kec. Kramat Jati', '17C068CC-B334-4FD5-9147-10DCE8A48AC0', '20103791', 'SMKN 10 JAKARTA', 'SMK', 'N', 'JL. SMEA 6 MAYJEND SUTOYO', '-6.2560000', '106.8698000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', '17DD89EE-FE38-4CD3-AF9F-DA81E47A206D', '20109934', 'SMKS FARMASI IKIFA', 'SMK', 'S', 'BUARAN II NO. 30A- I GUSTI NGURAH RAI', '-6.2160000', '106.9211000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016410  ', 'Kec. Matraman', '18150327-5F43-4B9D-96F3-92CC9B78F75D', '20103553', 'SMKS TIRTA SARI SURYA', 'SMK', 'S', 'JL. NANAS 1 UTAN KAYU', '-6.1990000', '106.8720000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016301  ', 'Kec. Jagakarsa', '193B71F6-5A55-4320-9FAA-5636226ACFA3', '20102629', 'SMKS ISLAM WIJAYA KUSUMA', 'SMK', 'S', 'Jl. Raya Depok, No. 16', '-6.3329000', '106.8357000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016302  ', 'Kec. Pasar Minggu', '1976FB2E-A709-4F09-AA39-BF26FE71D342', '20102598', 'SMKN 57 JAKARTA', 'SMK', 'N', 'JL. TAMAN MARGASATWA NO. 38 B', '-6.2926000', '106.8238000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', '1981EB83-56F7-4671-88C3-BF0BA6B00EC5', '69829196', 'SMKS GLOBAL CENDEKIA', 'SMK', 'S', 'JL. RAYA BEKASI KM 23 NO. 37 RT 02 RW 02', '-6.1835000', '106.9383000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016006  ', 'Kec. Kemayoran', '19BD31C3-5C82-45FF-8966-1BCB6A7C69EC', '20100115', 'SMKS MUHAMMADIYAH 1 JAKARTA', 'SMK', 'S', 'JL. GARUDA NO. 33 KEMAYORAN', '-6.1623000', '106.8429000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016006  ', 'Kec. Kemayoran', '1A519A52-36C6-4374-A26B-63EBEE90B7D0', '20100296', 'SMKS TAMAN SISWA 2', 'SMK', 'S', 'JL. GARUDA NO. 44 KEMAYORAN', '-6.1619000', '106.8449000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016301  ', 'Kec. Jagakarsa', '1A553109-ADFC-4269-BE00-04449A3384F4', '20102620', 'SMKS PERGURUAN RAKYAT', 'SMK', 'S', 'Jalan Yon Zikon 14', '-6.3449000', '106.8261000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016208  ', 'Kec. Kali Deres', '1A6C1302-A786-450B-BE39-9608D70179E6', '69774556', 'SMKS CITRA UTAMA', 'SMK', 'S', 'JL. LINGKUNGAN III RT 006/03', '-6.1184000', '106.7063000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016303  ', 'Kec. Cilandak', '1AB11014-F7FA-4301-982E-6B9F1AC98BAB', '20102458', 'SMKS YAPRI JAKARTA', 'SMK', 'S', 'JL. KH. MUHASYIM IV/7I', '-6.2879000', '106.7912000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016304  ', 'Kec. Pesanggrahan', '1ADA84EE-B761-447D-9AEA-164ED238FEE2', '20107357', 'SMKS MEDIA INFORMATIKA', 'SMK', 'S', 'JL. KOSTRAD RAYA NO.2', '-6.2334000', '106.7571000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016008  ', 'Kec. Gambir', '1C37B86C-EA9A-40B1-9063-5E2252F0F6F5', '20108514', 'SMKS REX MUNDI', 'SMK', 'S', 'ALAYDRUS NO. 42', '-6.1640000', '106.8174000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016006  ', 'Kec. Kemayoran', '1D0B4024-8D70-4C30-9971-27C8DF716909', '20100157', 'SMKN 44 JAKARTA', 'SMK', 'N', 'JL. HARAPAN JAYA 9/5A KEMAYORAN', '-6.1681000', '106.8639000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016405  ', 'Kec. Kramat Jati', '1D33CF9A-F35E-4E8F-A08A-25FF6C0CA64D', '20103776', 'SMKS NASIONAL JAKARTA', 'SMK', 'S', 'JL. SMEA 6 NO. 53', '-6.2557000', '106.8685000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016302  ', 'Kec. Pasar Minggu', '1D950564-D084-4163-93BA-FC4808E7BEB3', '20102621', 'SMKS PEMBANGUNAN JAYA YAKAPI', 'SMK', 'S', 'JL. PALAPA RAYA 2', '-6.2885000', '106.8425000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016101  ', 'Kec. Penjaringan', '1DC3AB54-E161-4F6C-BE58-81C70FE3D34B', '20107467', 'SMKS TRIDHARMA BUDHIDAYA', 'SMK', 'S', 'JL. BIDARA RAYA NO. 36', '-6.1312000', '106.7703000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016205  ', 'Kec. Tambora', '1DFBB19E-7E2D-4879-BD66-FBF20329392B', '20178281', 'SMK KAFAH UNGGUL 2', 'SMK', 'S', 'JL. JEMBATAN BESI NO. 12', '-6.1558000', '106.7965000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016207  ', 'Kec. Cengkareng', '1E76CEF6-3E2A-4540-95D3-696DCFC9B926', '20101675', 'SMKS IP YAKIN', 'SMK', 'S', 'JL. BANGUN NUSA NO. 10', '-6.1515000', '106.7300000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016301  ', 'Kec. Jagakarsa', '1E9FCF88-BB46-4915-A978-351A79860BC5', '20102463', 'SMKS YANUBA', 'SMK', 'S', 'JL. TIMBUL NO. 60', '-6.3512000', '106.8082000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016306  ', 'Kec. Kebayoran Baru', '1F47E97A-74F4-43DE-BAB2-B2C64825E13D', '20102593', 'SMKS PURNAMA 3 JAKARTA', 'SMK', 'S', 'JL. TIRTAYASA II/V', '-6.2422000', '106.8066000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016006  ', 'Kec. Kemayoran', '20DA331D-25E1-44D5-BAAE-30D84421768C', '20100303', 'SMKS STRADA 1 JAKARTA', 'SMK', 'S', 'JL. GUNUNG SAHARI NO. 88', '-6.1691000', '106.8400000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016305  ', 'Kec. Kebayoran Lama', '20F30DC0-EFC6-4D81-A2F3-927444AC614B', '20102644', 'SMKS MAKARYA 1 JAKARTA', 'SMK', 'S', 'JL.  PONDOK PINANG TIMUR NO. 4', '-6.2703000', '106.7740000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016307  ', 'Kec. Mampang Prapatan', '2256A5F5-99F6-4644-ACFC-8D1DA4E13881', '20102584', 'SMKS 28 OKTOBER 1928 1', 'SMK', 'S', 'JL. MAMPANG PRAPATAN XI', '-6.2515000', '106.8260000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016405  ', 'Kec. Kramat Jati', '231C349E-C85A-4996-B538-A97E9387B6AB', '20103736', 'SMKS PELAYARAN SANTA LUSIANA', 'SMK', 'S', 'Jl Dewi Sartika Gg Masjid Bendungan', '-6.2550000', '106.8650000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016303  ', 'Kec. Cilandak', '2445AC1D-F2F7-4698-A786-D53E2765EE0D', '20102645', 'SMKS LP3 ISTANA', 'SMK', 'S', 'JL. MARGASATWA NO. 12', '-6.3129000', '106.7972000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016303  ', 'Kec. Cilandak', '246322B3-3BC1-46A1-B4E5-F8ADAB1D844C', '20109547', 'SMKS FARMASI BHUMI HUSADA', 'SMK', 'S', 'JL. RAYA RS. FATMAWATI NO. 7', '-6.1433000', '106.7995000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016302  ', 'Kec. Pasar Minggu', '24AF6484-98AF-4443-84B7-4CB97E7BAB36', '20102541', 'SMKS AL HIDAYAH 1 JAKARTA', 'SMK', 'S', 'JL. BHAKTI 25', '-6.3076000', '106.8113000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', '24F8EB51-8394-4587-B998-8822A0B20C38', '20103751', 'SMKS PERBANKAN NASIONAL JAKARTA', 'SMK', 'S', 'JL. RAYA BEKASI KM. 28', '-6.1850000', '106.9630000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016204  ', 'Kec. Grogol Petamburan', '251CDEDE-0E42-470C-B32E-A09BCF2995EB', '20101686', 'SMKS DUTA MAS JAKARTA', 'SMK', 'S', 'JL. WIJAYA 8, KOMPLEK TAMAN DUTA MAS', '-6.1507000', '106.7792000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016208  ', 'Kec. Kali Deres', '25518981-4B0E-4025-BC4E-9A4724EA884F', '20101645', 'SMKS KEBUDAYAAN', 'SMK', 'S', 'Jl. Darul Huda No.55 Kalideres/JL. SEMANAN RAYA No. 90', '-6.1651000', '106.7027000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016309  ', 'Kec. Tebet', '264F0A02-97B9-47A8-A9B9-59399077005E', '20102604', 'SMKS MUHAMMADIYAH 7 JAKARTA', 'SMK', 'S', 'JL. TEBET TIMUR RAYA 565', '-6.2351000', '106.8537000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016102  ', 'Kec. Pademangan', '27C895DE-4AB8-46B5-8EE2-BD2927A864EB', '20107417', 'SMKS FAJAR INDAH', 'SMK', 'S', 'WASPADA RAYA 1/30', '-6.1354000', '106.8335000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', '27FEAB94-3B28-4849-A743-9A0972EBC3BD', '20103663', 'SMKS T KAPIN JAKARTA', 'SMK', 'S', 'JL. RAYA KALIMALANG PONDOK KELAPA', '-6.2515000', '106.9298000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016204  ', 'Kec. Grogol Petamburan', '2966D90F-7F7B-4177-A761-0811D42B5B1F', '20101591', 'SMKS AL CHASANAH JAKARTA', 'SMK', 'S', 'JL. TANJUNG DUREN BARAT III/1', '-6.1778000', '106.7824000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016401  ', 'Kec. Pasar Rebo', '296E6CAE-6867-4FF7-A7FD-A5938D9D7B78', '20103659', 'SMKS ISLAM PB SOEDIRMAN 1 JAKARTA', 'SMK', 'S', 'JL.RAYA BOGOR KM.24', '-6.3136000', '106.8620000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016008  ', 'Kec. Gambir', '297B053D-8BFA-4F81-AC88-BBDFB9E0677C', '20100299', 'SMKS SANTA MARIA JAKARTA', 'SMK', 'S', 'JL. IR. H. JUANDA NO. 29', '-6.1668000', '106.8239000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016301  ', 'Kec. Jagakarsa', '2A024AC4-6615-413C-BD57-A7E22B607659', '20102436', 'SMKS TANJUNG BARAT', 'SMK', 'S', 'Jl. Melati No. 100', '-6.2982000', '106.8425000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016101  ', 'Kec. Penjaringan', '2A8F28B4-3A7E-4555-9285-CE01C37D72CD', '20107453', 'SMKS PSKD 3 JAKARTA', 'SMK', 'S', 'JL. TANJUNG WANGI PLUIT', '-6.1354000', '106.7942000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016305  ', 'Kec. Kebayoran Lama', '2BFFD594-F668-4D40-AD2E-6E9F13B5E730', '20102451', 'SMKS YP MULIA', 'SMK', 'S', 'JL.  CIPUTAT RAYA NO. 1-2', '-6.2601000', '106.7762000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', '2C3A859C-3FAA-44C8-851D-63E903AFB183', '20107267', 'SMKS KARYA EKOPIN', 'SMK', 'S', 'JL. RAYA PULO GADUNG 25', '-6.1991000', '106.9560000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016306  ', 'Kec. Kebayoran Baru', '2D1E6C21-F145-4C9A-8E0E-DC2D0655041E', '20102602', 'SMKN 29 JAKARTA', 'SMK', 'N', 'JL. PROF JOKO SUTONO SH NO. 1', '-6.2393000', '106.8078000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016405  ', 'Kec. Kramat Jati', '2D40F2CA-9406-4BD9-B468-AB3EEC903B8D', '20112511', 'SMKS BINA PRESTASI', 'SMK', 'S', 'JL.RAYA CONDET NO.28 KEL.BATU AMPAR KRAMAT JATI', '-6.2777000', '106.8655000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016106  ', 'Kec. Cilincing', '2D745B5B-DBE2-4DE7-80A5-F30A63333839', '20107414', 'SMKS DARUL MAARIF', 'SMK', 'S', 'JL. MADYA KEBANTENAN NO. 14', '-6.1112000', '106.9385000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016301  ', 'Kec. Jagakarsa', '2DE5FD53-DC16-4F71-876D-21B51488963B', '20102650', 'SMKS KHARISMAWITA 2 JAKARTA', 'SMK', 'S', 'Jl. Swadaya II/30', '-6.2997000', '106.8459000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016106  ', 'Kec. Cilincing', '2E0E5043-30C3-437C-8A37-B51D52D76569', '20177823', 'SMK BINTANG NUSANTARA', 'SMK', 'S', 'JL. SARANG BANGO NO 27', '-6.1248640', '106.9529600', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', '2E315527-B008-4B9F-A73B-3892F24D3420', '20103720', 'SMKS PELAYARAN DEWARUCI', 'SMK', 'S', 'JL. RAYA BEKASI KM. 26', '-6.1890000', '106.9657000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016003  ', 'Kec. Senen', '2EC5F1B5-7FC9-41EF-BEC2-69C9BC7115EF', '20100110', 'SMKS KATOLIK SAINT JOSEPH', 'SMK', 'S', 'JL. KRAMAT RAYA NO. 134', '-6.1867000', '106.8450000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', '2EDE3714-CBD9-4990-80E7-2318108D8652', '20103664', 'SMKS KALPATARU CAKUNG JAKARTA', 'SMK', 'S', 'JL. TIPAR CAKUNG BARAT', '-6.1725000', '106.9366000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016106  ', 'Kec. Cilincing', '2FADEBC8-50D2-4D94-A7A4-8B03829570C1', '20107451', 'SMKS PGRI 3 JAKARTA', 'SMK', 'S', 'JL. MARUNDA BARU III', '-6.1219000', '106.9154000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016208  ', 'Kec. Kali Deres', '3010386B-357E-4D13-BEC5-859F20FE7857', '20101477', 'SMKS PGRI 36', 'SMK', 'S', 'JL.PETA UTARA NO.46', '-6.1277000', '106.7196000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016207  ', 'Kec. Cengkareng', '30D82E03-0F58-4CBD-ACB7-26409293D555', '69764515', 'SMKS CENGKARENG 2', 'SMK', 'S', 'JL.KAWASAN RUSUN CENGKARENG NO.2 PERUMNAS CENGKARENG INDAH CENGKARENG', '-6.1345000', '106.7381000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016204  ', 'Kec. Grogol Petamburan', '31DF9A8D-D79B-4B5C-B9D1-6EFC75839C16', '20101595', 'SMKS BHARA TRIKORA 1 JAKARTA', 'SMK', 'S', 'KAV POLRI BLOK F', '-6.1594000', '106.7839000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016103  ', 'Kec. Tanjung Priok', '32352F0F-B195-4839-BDC3-727B9EEA4942', '20107447', 'SMKS PERINTIS NASIONAL JAKARTA', 'SMK', 'S', 'JL. SWASEMBADA TIMURX/02', '-6.1229000', '106.8909000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016305  ', 'Kec. Kebayoran Lama', '32F01B0B-5639-4C0C-BB73-8A58E1EA3B47', '20102536', 'SMKS AVERUS', 'SMK', 'S', 'JL. CIPUTAT RAYA NO.11 KEBAYORAN LAMA', '-6.2617000', '106.7749000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016005  ', 'Kec. Cempaka Putih', '336CB068-91BB-47EB-BD5F-0F8C84ABC471', '20100168', 'SMKN 39 JAKARTA', 'SMK', 'N', 'JALAN CEMPAKA PUTIH TENGAH VI NO. 2', '-6.1713000', '106.8702000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016409  ', 'Kec. Pulo Gadung', '33B17B93-9049-45F4-997B-251C9673E02C', '20112288', 'SMKS ATLANTICA WISATA JAKARTA', 'SMK', 'S', 'JALAN PACUAN KUDA NO 1-5', '-6.1726000', '106.8887000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016005  ', 'Kec. Cempaka Putih', '33D9A410-784D-41A9-9451-34FFF2E1E8D3', '20100153', 'SMKS PGRI 4 JAKARTA', 'SMK', 'S', 'JL. PERCETAKAN NEGARA XI A', '-6.1912000', '106.8646000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016208  ', 'Kec. Kali Deres', '34A0DB7B-6D48-4B2B-945B-AE319D533648', '69900099', 'SMK BINA BANGSA', 'SMK', 'S', 'Jl. Tanjung Pura V Rt. 05/05 No. 68', '-6.1294380', '106.7045870', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016104  ', 'Kec. Koja', '34BA87A5-2173-4E14-A971-53F26D676008', '20107468', 'SMKS WALANG JAYA', 'SMK', 'S', 'JL.STM WALANG JAYA NO.01 TUGU SELATAN KOJA JAKARTA UTARA', '-6.1336000', '106.9074000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016207  ', 'Kec. Cengkareng', '35A3D4E5-50C0-44AE-A898-D39E4480008A', '20101632', 'SMKS MUHAMMADIYAH 13 JAKARTA', 'SMK', 'S', 'JL. BERINGIN RAYA NO. 36-37', '-6.1510000', '106.7199000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016006  ', 'Kec. Kemayoran', '36AC0050-382E-4868-AAB9-D2DA46F681F6', '20100294', 'SMKS TAMAN SISWA 1 JAKARTA', 'SMK', 'S', 'JL GARUDA NO. 25', '-6.1614000', '106.8421000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016202  ', 'Kec. Kebon Jeruk', '372C0DDA-822E-4400-9DC9-D45CCA11CD24', '20101468', 'SMKS WIYATA SATYA', 'SMK', 'S', 'Jl. Taman Mutiara Prima No. 3', '-6.1961000', '106.7945000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016307  ', 'Kec. Mampang Prapatan', '3942D7C7-6C0F-4866-BBE1-98E073BFB2C6', '20102583', 'SMKS 28 OKTOBER 1928 2', 'SMK', 'S', 'JL. PEMBANGUNAN BUNCIT 8', '-6.2515000', '106.8260000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016304  ', 'Kec. Pesanggrahan', '3A1298BA-328C-4487-A3EE-9B3FB96E992F', '20107347', 'SMKS PLUS AL MUSYARROFAH', 'SMK', 'S', 'JL. H. MUCHTAR RAYA RT 12 RW 11 NO 95', '-6.2238000', '106.7431000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016405  ', 'Kec. Kramat Jati', '3A424C50-AD9C-4590-933B-8C0723ECE919', '20103775', 'SMKS NASIONAL ALFAN', 'SMK', 'S', 'JL. DUKUH V NO. I A', '-6.2931000', '106.8770000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016303  ', 'Kec. Cilandak', '3AFBBEED-0508-4E0E-9844-E432A8456F98', '20102635', 'SMKS HIDATHA JAKARTA', 'SMK', 'S', 'JL. CILANDAK TENGAH III/53 CILANDAK BARAT', '-6.2899000', '106.8040000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016202  ', 'Kec. Kebon Jeruk', '3B79D939-F41D-4998-8937-33BEB59C86CB', '20101592', 'SMKS AL HUDA KEBON JERUK', 'SMK', 'S', 'JL. H. ALIMUNKELAPA DUA', '-6.2032000', '106.7686000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016405  ', 'Kec. Kramat Jati', '3C186DF5-5DB3-40C6-88CA-0F5B83A6BDD0', '69760657', 'SMKS NUSANTARA WISATA RESPATI', 'SMK', 'S', 'JL. RAYA INPRES', '-6.2133000', '106.8683000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016410  ', 'Kec. Matraman', '3CF07E93-D0FB-4834-B7C7-A99B81B4595B', '20103792', 'SMKS MUHAMMADIYAH 6 JAKARTA', 'SMK', 'S', 'JL. KH. AHMAD DAHLAN NO. 20', '-6.2088000', '106.8630000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016307  ', 'Kec. Mampang Prapatan', '3CFF29B7-4853-4B83-8099-A1EE0BE97652', '20102558', 'SMKS AL FALAH', 'SMK', 'S', 'MAMPANG PRAPATAN I NO. 4', '-6.2439000', '106.8249000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016203  ', 'Kec. Palmerah', '3DDAB686-4B07-48D4-820E-A9C826223086', '20101506', 'SMKS MUHAMMADIYAH 4 JAKARTA', 'SMK', 'S', 'JL. ANGGREK NELI MURNI BLOK B - C', '-6.1928000', '106.7930000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016404  ', 'Kec. Makasar', '3E12EAAA-A023-43F3-A82F-AF46F2761F28', '20103548', 'SMKS TRIDAYA JAKARTA', 'SMK', 'S', 'JL. PANGKALAN JATI II NO. 19', '-6.2497000', '106.9061000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016106  ', 'Kec. Cilincing', '3E26D1F6-C559-44D4-80E3-23861EE46ECF', '20107437', 'SMKN 49 JAKARTA', 'SMK', 'N', 'JALAN SARANG BANGO NO. 1', '-6.1294000', '106.9549000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016006  ', 'Kec. Kemayoran', '3E6537BB-504D-4719-A146-CBC8891682AB', '20100298', 'SMKS SAINT JOHN', 'SMK', 'S', 'JL. BUNGUR BESAR NO. 82A', '-6.1673000', '106.8420000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016002  ', 'Kec. Menteng', '3F2BE196-2051-4483-8B44-1C17F9D4669A', '20100300', 'SMKS ST THERESIA', 'SMK', 'S', 'JL. GEREJA THERESIA NO.04', '-6.1893000', '106.8260000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016304  ', 'Kec. Pesanggrahan', '3F8A702B-B929-4012-B7FB-8E44FAB0055E', '20102465', 'SMK PUTRA SATRIA', 'SMK', 'S', 'JL. CILEDUG RAYA NO 46', '-6.2359000', '106.7577000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', '3FAFD49D-9DDB-4C90-BA22-AEF4CBDD2D2E', '20103793', 'SMKS KAWULA INDONESIA', 'SMK', 'S', 'JALAN RAYA SMA KAPIN NO.3 JAKARTA TIMUR', '-6.2490000', '106.9295000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', '3FE07996-E116-45C6-BF74-63321ED034BC', '20107206', 'SMKS CITRA MANDIRI JAKARTA', 'SMK', 'S', 'P.KOMARUDDIN UJ KRAWANG', '-6.1968000', '106.9426000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('﻿010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016002  ', 'Kec. Menteng', '3FEEC738-55DB-4EA8-BEB3-160A4183453F', '20100286', 'SMKS YAPERMAS JAKARTA', 'SMK', 'S', 'JL. ANYER NO. 7', '-6.2029000', '106.8441000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016409  ', 'Kec. Pulo Gadung', '401A522C-37E3-41B6-8C44-522682F9FF70', '20103547', 'SMKS TUNAS MARKATIN', 'SMK', 'S', 'JL. WARU NO. 20 B', '-6.1960000', '106.8882000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016304  ', 'Kec. Pesanggrahan', '41426FC0-120B-47CC-A1B4-0CA986B1507F', '20102634', 'SMKS ISLAM AL HIKMAH', 'SMK', 'S', 'JL. KEMAJUAN NO. 48-55', '-6.2360000', '106.7557000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016404  ', 'Kec. Makasar', '4221AE7E-E8CB-4EBF-B290-5851F189556C', '20103546', 'SMKS USWATUN HASANAH', 'SMK', 'S', 'JL. RAYA DEPNAKER NO.2 Pinang Ranti', '-6.2855000', '106.8820000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016302  ', 'Kec. Pasar Minggu', '42ADD42C-1A19-45F5-9DB2-048DC082090C', '20102545', 'SMKS BOROBUDUR 2 JAKARTA', 'SMK', 'S', 'JL. RAYA CILANDAK KKO', '-6.3072000', '106.8136000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016201  ', 'Kec. Kembangan', '4331CB36-59B9-4F7F-A3D8-FA42A3C7481D', '20101471', 'SMKS SATRIA JAKARTA', 'SMK', 'S', 'JL. RAYA SRENGSENG NO. 26 A', '-6.2115000', '106.7595000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016101  ', 'Kec. Penjaringan', '4367632C-1BAD-4748-8553-0D94F08F4D6E', '20107452', 'SMKS PLUIT RAYA', 'SMK', 'S', 'JL. JEMBATAN TIGA NO.1', '-6.1285000', '106.7915000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', '452E4CE7-6300-4A62-B304-196B4E4367CC', '20103769', 'SMK CARAKA NUSANTARA', 'SMK', 'S', 'KOMPLEKS PULO GEBANG PERMAI BLOK H4 NO.10', '-6.1968000', '106.9496000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016305  ', 'Kec. Kebayoran Lama', '4575310D-826B-4DC4-A6C7-2ED3AA4D359D', '20102462', 'SMKS YANUSA JAKARTA', 'SMK', 'S', 'JL. H. SAIKIN NO. 15 RT. 08 RW. 08', '-6.2848000', '106.7709000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016409  ', 'Kec. Pulo Gadung', '45B4C0A4-3C91-4394-A15F-5AE46421DE5F', '20177925', 'SMKS SARASVATI', 'SMK', 'S', 'JL. PACUAN KUDA NO. 1-5 PULO MAS', '-6.1724000', '106.8887000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016008  ', 'Kec. Gambir', '45D7C2B4-952D-4BA5-9612-25BD0B7CF94D', '20107252', 'SMKS BUNDA MULIA 2', 'SMK', 'S', 'JL. AM. SANGAJI N0. 20 PETOJO UTARA', '-6.1635000', '106.8125000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016208  ', 'Kec. Kali Deres', '45EB80D8-B6C9-4B9A-9D6A-6B6B20B35DB0', '20101509', 'SMKS PATRIOT NUSANTARA', 'SMK', 'S', 'JL. KAMPUNG BELAKANG NO. 26-27', '-6.1034000', '106.7034000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016409  ', 'Kec. Pulo Gadung', '460881A0-E139-4CD9-958D-FC75AA80502F', '20103530', 'SMKS SANTA LUCIA', 'SMK', 'S', 'JALAN RAWAMANGUN MUKA BARAT NO. 2', '-6.1993000', '106.8805000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016007  ', 'Kec. Sawah Besar', '46AFF8D9-63DF-4874-B566-881E076AF4BF', '20100104', 'SMKS KARTINI 1 JAKARTA', 'SMK', 'S', 'JL KARTINI RAYA NO 26', '-6.1684000', '106.8353000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016403  ', 'Kec. Cipayung', '470D8483-8D5B-46D9-964B-221854E1B4F2', '20109930', 'SMKS ANALIS KESEHATAN DITKESAD', 'SMK', 'S', 'JL. BUNTU MUNJUL', '-6.3549000', '106.9030000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', '474BE210-2330-42EC-9DF5-4057AC7B2E3F', '20103243', 'SMKS AL AKHYAR 1 JAKARTA', 'SMK', 'S', 'JL. BALAI RAKYAT GEMPOL 99', '-6.1822000', '106.9588000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016301  ', 'Kec. Jagakarsa', '481284F8-04BF-409F-8799-CF657D0E0DE5', '20102552', 'SMKS DAARUSSALAAM JAKARTA', 'SMK', 'S', 'JL. M. KHAFI II No. 28', '-6.3431000', '106.8176000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016401  ', 'Kec. Pasar Rebo', '48A12493-C6CE-4B6E-88C7-DC03BF955B71', '20103768', 'SMKS KIMIA TUNAS HARAPAN', 'SMK', 'S', 'JL. BENDERA RAYA / INTISARI III', '-6.3303000', '106.8587000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016103  ', 'Kec. Tanjung Priok', '48DC7A95-0F1F-4949-B3CB-98640C215ED8', '20107418', 'SMKS GITA KIRTTI 2 JAKARTA', 'SMK', 'S', 'JL. SUNTER JAYA 4', '-6.1558000', '106.8674000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016406  ', 'Kec. Jatinegara', '497060E1-F233-49A0-A63E-CBAEF6DB7D59', '69774850', 'SMKS MULTIMEDIA NUSANTARA', 'SMK', 'S', 'JL. CIPINANG BESAR NO.2', '-6.2365000', '106.8852000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', '49CC1759-E4D2-4820-AB04-C119AE3917AA', '20103733', 'SMKS NURUL ISLAM JAKARTA', 'SMK', 'S', 'JL. MAWAR MERAH', '-6.2208000', '106.9385000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016003  ', 'Kec. Senen', '4A67DD4B-5E09-4073-B950-EDFE8567CF0A', '20100116', 'SMKS MUHAMMADIYAH 10 JAKARTA', 'SMK', 'S', 'JL. KRAMAT SAWAH BARU', '-6.1877000', '106.8513000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016405  ', 'Kec. Kramat Jati', '4A7EFE78-8724-49C5-9A9E-0EE064FB4DAE', '69856220', 'SMK Bina Citra Asia', 'SMK', 'S', 'Jl. Al-Amin No. 1 RT. 13/06', '-6.2707000', '106.8715000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016106  ', 'Kec. Cilincing', '4AD249C7-CD56-4D30-9DE5-7A41F55E5786', '20107458', 'SMKS SARI PUTRA', 'SMK', 'S', 'KEBON BARU BLOK X NO. 25', '-6.1148000', '106.9206000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016006  ', 'Kec. Kemayoran', '4AE91992-FA23-48F0-88AD-FF574663C5FB', '20100164', 'SMKN 3 JAKARTA', 'SMK', 'N', 'JL. GARUDA NO. 63 KEMAYORAN', '-6.1616000', '106.8448000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', '4C02C395-91CB-4129-B277-D87EA7ED33A1', '20107208', 'SMKS KIMIA FARMASI CARAKA NUSANTARA', 'SMK', 'S', 'KOMPLEKS PULO GEBANG PERMAI BLOK H4 NO.10', '-6.1966000', '106.9496000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016306  ', 'Kec. Kebayoran Baru', '4C2641F6-B75B-43B2-88E4-CC28223B1656', '20102582', 'SMKN 30 JAKARTA', 'SMK', 'N', 'JL. PAKUBUWONO 6', '-6.2354000', '106.7910000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);
INSERT INTO `tb_sekolah` (`id_provinsi`, `provinsi`, `id_kota`, `kota`, `id_kecamatan`, `kecamatan`, `id_sekolah`, `npsn`, `nama_sekolah`, `bentuk`, `status`, `alamat`, `lintang`, `bujur`, `created_at`, `updated_at`, `deleted_at`) VALUES
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016001  ', 'Kec. Tanah Abang', '4DD752B6-FF11-45A0-879B-558FFBECDDB9', '20109319', 'SMKS PRG SEKESAL JAKARTA', 'SMK', 'S', 'JL.BENDUNGAN JATILUHUR NO.130', '-6.2125000', '106.8094000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016302  ', 'Kec. Pasar Minggu', '4DFE3278-D7C8-4C93-9407-BE392E45F379', '20102606', 'SMKS MITRA PEMBANGUNAN', 'SMK', 'S', 'Jl. Benda/Madrasah No. 31 A Pedurenan', '-6.2793000', '106.8157000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016202  ', 'Kec. Kebon Jeruk', '4E4FC66B-98BE-48BF-AA46-B4AF52469731', '20101638', 'SMKS JAKARTA IV', 'SMK', 'S', 'RAYA KEDOYA NO 200', '-6.1768000', '106.7573000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016301  ', 'Kec. Jagakarsa', '4E9A8B14-05F4-4046-B05C-B5BFB562B92E', '20102627', 'SMK ISLAM YPS', 'SMK', 'S', 'JL. Raya Lenteng Agung No. 19A RT. 005/001', '-6.3148000', '106.8377000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016002  ', 'Kec. Menteng', '4EB77B75-B20B-45C6-AA02-A8242FF6D653', '20100295', 'SMKS TAMAN SISWA 3 JAKARTA', 'SMK', 'S', 'JL. MATRAMAN DALAM II/13', '-6.2038000', '106.8546000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016401  ', 'Kec. Pasar Rebo', '4F38348D-1941-4C6E-8B24-370A5A6D4072', '20107215', 'SMKS PASKITA GLOBAL', 'SMK', 'S', 'JL. RAYA BOGOR KM. 25', '-6.3257000', '106.8636000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016405  ', 'Kec. Kramat Jati', '4F39A0A4-2656-4C48-B52D-1C7DC80D8F3A', '20103767', 'SMKS KRAMAT JATI JAKARTA', 'SMK', 'S', 'JL. Raya Cipayung Setu No 33', '-6.3237000', '106.9066000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016405  ', 'Kec. Kramat Jati', '4F708869-B8D1-4220-A90B-757E10855258', '20103724', 'SMKS PARIWISATA ADI LUHUR JAKARTA', 'SMK', 'S', 'JL. CONDET RAYA NO. 4 RT/RW 005/03', '-6.2930000', '106.8557000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016005  ', 'Kec. Cempaka Putih', '5015EB24-9748-4F95-B32B-1E7564EB6D61', '20178283', 'SMKS CEMPAKA', 'SMK', 'S', 'JL CEMPAKA PUTIH BARAT', '-6.1791000', '106.8636000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016404  ', 'Kec. Makasar', '50DD0FD3-2B18-4A95-BAD9-107C80F6731D', '20103704', 'SMKS DHARMA SURYA', 'SMK', 'S', 'PANGKALAN JATI IV NO. 47, JATIWARINGIN-JAKARTA TIMUR', '-6.2509000', '106.9079000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016304  ', 'Kec. Pesanggrahan', '50EE1BDC-4CCE-455A-A412-22F5EB76B39C', '20102633', 'SMKS ISLAM AL IHSAN JAKARTA', 'SMK', 'S', 'JL. PESANGGRAHAN INDAH NO. 1', '-6.2583000', '106.7559000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016409  ', 'Kec. Pulo Gadung', '513ABB68-EAC6-424C-B2A7-509764866DF9', '20103787', 'SMKN 26 JAKARTA', 'SMK', 'N', 'JL. BALAI PUSTAKA BARU 1', '-6.1942000', '106.8865000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016302  ', 'Kec. Pasar Minggu', '51539977-03E5-4BC0-A98C-914837C252F8', '20107350', 'SMKS FARMASI LPK', 'SMK', 'S', 'JL. RAYA LENTENG AGUNG NO.33', '-6.3144000', '106.8357000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', '51551511-4AED-45EA-8822-3225B397B12C', '20103649', 'SMKS HARAPAN 1 JAKARTA', 'SMK', 'S', 'JL. I GUSTI NGURAH RAI', '-6.2145000', '106.9206000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016401  ', 'Kec. Pasar Rebo', '519F3823-5E63-4499-9671-7E3A96948EE1', '20103640', 'SMKS PGRI 28 JAKARTA', 'SMK', 'S', 'JL. CONDET RAYA PASAR REBO JAKARTA TIMUR', '-6.2949000', '106.8558000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016202  ', 'Kec. Kebon Jeruk', '51BD91DA-3636-488F-B67C-6948F1841064', '20109572', 'SMKS NUSANTARA', 'SMK', 'S', 'JL SMUN 57', '-6.1863000', '106.7591000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016301  ', 'Kec. Jagakarsa', '523818AF-3C9D-448A-A8A2-FE0CF033420D', '20102546', 'SMKS KAHURIPAN 1 JAKARTA', 'SMK', 'S', 'JL. NANGKA NO. 17', '-6.3033000', '106.8468000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016103  ', 'Kec. Tanjung Priok', '524F2DEB-D1D8-46D9-A020-46AC1C22B767', '20100783', 'SMKN 12 JAKARTA', 'SMK', 'N', 'JL. KEBON BAWANG 15', '-6.1149000', '106.8861000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', '536AEB5A-34D7-4A18-B094-D5742CDA28E2', '20112290', 'SMKS BOSS MULTIMEDIA', 'SMK', 'S', 'PULO ASEM UTARA DAN JALAN RAYA BEKASI KM. 24', '-6.1772000', '106.9603000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016305  ', 'Kec. Kebayoran Lama', '5437B90D-2B33-47B7-8258-BDA1768F29AD', '20109541', 'SMKS TRIKARYA', 'SMK', 'S', 'CIPUTAT RAYA', '-6.2547000', '106.7786000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016103  ', 'Kec. Tanjung Priok', '544CD453-613B-475A-BB3A-BB263F7CB57E', '20107419', 'SMKS GLOBAL 21 JAKARTA', 'SMK', 'S', 'JL. LANJI NO. 2', '-6.1281000', '106.8751000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016306  ', 'Kec. Kebayoran Baru', '5465264F-A269-4E5A-9727-45AE546DFEB3', '20102448', 'SMKS TUNAS PEMBANGUNAN', 'SMK', 'S', 'JL. ABD MAJID GG. KH. MOCH NAIN I/68', '-6.2650000', '106.8067000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016106  ', 'Kec. Cilincing', '552806A1-B357-4CF5-A936-DFD8496AD8EF', '20107413', 'SMKS CILINCING 3 JAKARTA', 'SMK', 'S', 'JL.KEBANTENAN IV NO.26 SEMPER TIMUR', '-6.1359000', '106.9118000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016104  ', 'Kec. Koja', '5557CFCB-0AFF-4CB7-8462-9AE9DCAE0837', '20107466', 'SMKS TANJUNG PRIOK 2 JAKARTA', 'SMK', 'S', 'JL.MANGGA NO.3', '-6.1159000', '106.9117000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016409  ', 'Kec. Pulo Gadung', '5585E8EA-0BF8-40D6-B8B6-B900786AA568', '20103561', 'SMKS FRANSISKUS 1 JAKARTA', 'SMK', 'S', 'JL. BANGUNAN BARAT NO. 29 KAMPUNG AMBON', '-6.1850000', '106.8808000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016306  ', 'Kec. Kebayoran Baru', '55DB9789-2A99-42C3-AE00-5BDB18BCFEB5', '20102547', 'SMKS GITA KIRTTI 1 JAKARTA', 'SMK', 'S', 'JL. BRI RADIO DALAM', '-6.2588000', '106.7918000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016003  ', 'Kec. Senen', '562FB615-563F-451B-AE09-A18EC440ECCD', '20107255', 'SMKS FARMASI TUNAS BANGSA', 'SMK', 'S', 'SALEMBA TENGAH II NO.1', '-6.1942000', '106.8563000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016303  ', 'Kec. Cilandak', '5667640C-9ED4-4B52-8F7B-FFFEFAC23052', '20102585', 'SMKN 28 JAKARTA', 'SMK', 'N', 'Jl. Maritim No. 26', '-6.2845000', '106.7944000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016204  ', 'Kec. Grogol Petamburan', '5698990C-42D2-4A4B-9C7F-424521FB996F', '20109617', 'SMKS MUTIARA BANGSA TIGA', 'SMK', 'S', 'Jl. Jelambar Barat III No. 5B', '-6.1453000', '106.7823000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016302  ', 'Kec. Pasar Minggu', '569F17E9-E397-4DF4-B46A-20165559B32C', '20107362', 'SMKS WISATA INDONESIA', 'SMK', 'S', 'RAYA LENTENG AGUNG GG LANGGAR NO.1', '-6.3108000', '106.8352000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016204  ', 'Kec. Grogol Petamburan', '5738E34A-15B3-48EB-A45D-78EF41093D0F', '20101521', 'SMKS MUHAMMADIYAH 3 JAKARTA', 'SMK', 'S', 'JL. GELONG BARU NO. 23A', '-6.1765000', '106.7971000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016204  ', 'Kec. Grogol Petamburan', '58A6A096-5596-4E48-9D28-49D831C9D9F8', '20101642', 'SMKS ISLAM PERTI JAKARTA', 'SMK', 'S', 'JL. TAWAKAL RAYA NO. 99', '-6.1696000', '106.7944000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016406  ', 'Kec. Jatinegara', '59A09865-7943-4C7A-BC4E-25A538B78B18', '20103785', 'SMKN 46 JAKARTA', 'SMK', 'N', 'JL. B7 CIPINANG PULO', '-6.2277000', '106.8889000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016306  ', 'Kec. Kebayoran Baru', '5A29489A-48F8-4201-9068-C9113EA39122', '69821209', 'SMKS TRIGUNA 1956', 'SMK', 'S', 'JL. HANG LEKIU III', '-6.2420000', '106.8002000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', '5A66B37E-7061-46AC-B1A5-12BDDA17F2AF', '20103687', 'SMKS BUDI MULIA UTAMA', 'SMK', 'S', 'JL. RAWA JAYA NO.35', '-6.2350000', '106.9438000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016204  ', 'Kec. Grogol Petamburan', '5A9B791F-EB6F-4AF4-B699-B299C0D0482C', '20101636', 'SMKS K PANCARAN BERKAT', 'SMK', 'S', 'JL. SETIA JAYA RAYA NO. 100', '-6.1607000', '106.7824000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016207  ', 'Kec. Cengkareng', '5ABAAE72-BE5E-4950-A00B-0D1007F4830F', '20101505', 'SMKS MUTIARA BANGSA', 'SMK', 'S', 'JL. UTAMA RAYA NO. 2', '-6.1500000', '106.7352000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016405  ', 'Kec. Kramat Jati', '5B742C00-3188-4E22-8EF4-B30DFF285ADC', '20103552', 'SMKS TRAMPIL JAKARTA', 'SMK', 'S', 'JL. OLAH RAGA 2', '-6.2677000', '106.8613000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016404  ', 'Kec. Makasar', '5B7F6250-04A9-4366-93DA-3C3A680FB736', '20103504', 'SMKS YAMAS JAKARTA', 'SMK', 'S', 'JL. PUSDIKLAT DEPNAKER', '-6.2805000', '106.8785000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016206  ', 'Kec. Taman Sari', '5B92CB33-F252-47B2-A0AF-2B910025A712', '20101485', 'SMKS TRI RATNA JAKARTA', 'SMK', 'S', 'JL. TALIB 1 NO. 35-37', '-6.1784000', '106.7578000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016203  ', 'Kec. Palmerah', '5BCDA8A3-F3BF-470D-8CBD-80200C75F0FF', '20101489', 'SMKS TOMANG RAYA', 'SMK', 'S', 'JL. KEMUNING NO.14 A', '-6.1779000', '106.7997000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016304  ', 'Kec. Pesanggrahan', '5BD447C8-D97C-41AC-B077-75A7CD3321F9', '20102577', 'SMKS 10 NOPEMBER', 'SMK', 'S', 'JL. MESJID DARUL FALLAH PESANGGRAHAN', '-6.2322000', '106.7512000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016308  ', 'Kec. Pancoran', '5BDCC6D2-5988-4999-95A4-79032913BCB2', '20107257', 'SMKS MUSIK PERGURUAN CIKINI', 'SMK', 'S', 'Jl. Duren Tiga Raya No. 1', '-6.1950000', '106.8395000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016001  ', 'Kec. Tanah Abang', '5BF4CA29-F235-4106-B13F-6114068DA992', '20100132', 'SMKS BETHEL', 'SMK', 'S', 'JL. KS TUBUN N0. 253 TANAH ABANG', '-6.1963000', '106.8064000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016201  ', 'Kec. Kembangan', '5C1565D3-D061-4012-8096-6C8B0B950BA4', '20107268', 'SMKS PRIMA WISATA', 'SMK', 'S', 'RAYA BUGIS KEMBANGAN NO.109', '-6.1851000', '106.7338000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016204  ', 'Kec. Grogol Petamburan', '5CBC8F55-1729-4403-B886-2485930E4AB4', '20101596', 'SMKS BHAKTI', 'SMK', 'S', 'JL. KARYA BARAT I UJUNG 24', '-6.1611000', '106.7738000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016302  ', 'Kec. Pasar Minggu', '5CE8808E-1212-4366-BFF0-E9583F379BD4', '20102599', 'SMKN 47 JAKARTA', 'SMK', 'N', 'JL. CONDET PEJATEN', '-6.2770000', '106.8311000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016403  ', 'Kec. Cipayung', '5D33C487-AB8F-4463-9D38-CA73AF0D1111', '20103742', 'SMKS PGRI 16 JAKARTA', 'SMK', 'S', 'JL. SMAN 64 Cipayung RT 05/02 No. 23 Jakarta Timur', '-6.3302000', '106.8907000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', '5DDD669F-4C81-4BB5-AD45-183F7E4CD376', '20103700', 'SMKS DIPONEGORO 2 JAKARTA', 'SMK', 'S', 'JL. KAYU TINGGI CAKUNG', '-6.1799000', '106.9473000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016103  ', 'Kec. Tanjung Priok', '5E915626-7E8E-4B0F-8D14-E79984032FEE', '20107472', 'SMKS YAPPENDA JAKARTA', 'SMK', 'S', 'JL. SWASEMBADA TIMUR IV NO.12', '-6.1301000', '106.8819000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016309  ', 'Kec. Tebet', '5F2DFA7F-08B6-411C-8244-4E977D2A10E0', '20102581', 'SMKN 32 JAKARTA', 'SMK', 'N', 'JL. TEBET DALAM 4 NO. 1', '-6.2252000', '106.8508000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016405  ', 'Kec. Kramat Jati', '5FD1A131-E81A-4DE6-BC90-0F8159507673', '20103535', 'SMKS RESPATI 2 JAKARTA', 'SMK', 'S', 'JL. RAYA INPRES', '-6.2864000', '106.8708000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016303  ', 'Kec. Cilandak', '604D2C97-208F-4FDA-A744-8C12264BC8FB', '20109254', 'SMKS BAKTI IDHATA', 'SMK', 'S', 'JL. MELATI NO. 25 CILANDAK', '-6.2836000', '106.7943000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016403  ', 'Kec. Cipayung', '625AD6D2-E83B-4E16-A88C-72B54123B648', '20109304', 'SMKS BINA MEDIKA JAKARTA', 'SMK', 'S', 'AS SYAFIIYAH NO. 121', '-6.3411000', '106.9072000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016208  ', 'Kec. Kali Deres', '62907485-0A10-48A5-8A28-BB7375AE4A9C', '20101495', 'SMKS PGRI 24 JAKARTA', 'SMK', 'S', 'JL. PETA UTARA 70', '-6.1428000', '106.7026000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016005  ', 'Kec. Cempaka Putih', '62AF0DD3-636F-4551-9130-185CABFF6650', '20107259', 'SMKS TUNAS HARAPAN', 'SMK', 'S', 'JL. CEMP. PUTIH BARAT 14 CEMPAKA PUTIH', '-6.1805000', '106.8655000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016104  ', 'Kec. Koja', '62D074B1-AC1B-43E1-ADD7-34ADA3646AD0', '20107431', 'SMKS MUHAMMADIYAH 12', 'SMK', 'S', 'JL.HAJI MURTADO NO.2A KOJA', '-6.1291000', '106.9191000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016202  ', 'Kec. Kebon Jeruk', '62DFE36A-36C0-46C1-B41E-64F2F7D9CCC4', '20101661', 'SMKS DEWI SARTIKA', 'SMK', 'S', 'JL. TANJUNG DUREN BARAT 1BLOK AY KOMP.GREEN VILE', '-6.1865000', '106.7756000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016001  ', 'Kec. Tanah Abang', '62E90423-E758-4F36-A915-7EDA155A1DCD', '20100140', 'SMKS AL IHSAN JAKARTA', 'SMK', 'S', 'JL. KEBON KACANG 17/57', '-6.1924000', '106.8190000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016106  ', 'Kec. Cilincing', '647E4474-968E-4FFA-8592-BA6E449EA818', '20107440', 'SMKS NUSANTARA 1 JAKARTA', 'SMK', 'S', 'JL. KRAMAT JAYA  GANG 8', '-6.1312000', '106.9317000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016406  ', 'Kec. Jatinegara', '64822010-C895-44B6-8A9F-57AD9B190998', '20103672', 'SMKS JAKARTA TIMUR 1', 'SMK', 'S', 'JL. CAWANG BARU NO. 543', '-6.2426000', '106.8704000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016002  ', 'Kec. Menteng', '64F3EAB6-243D-44A1-B678-67913F743331', '20100306', 'SMKS AL MAKMUR', 'SMK', 'S', 'JL. RADEN SALEH, CIKINI MENTENG', '-6.1914000', '106.8248000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016207  ', 'Kec. Cengkareng', '65747F50-94B0-4863-AB22-B930AB546937', '20101674', 'SMKS DUTA BANGSA JAKARTA', 'SMK', 'S', 'JL. KAPUK RAYA GG. MESJID NO. 71', '-6.1364000', '106.7448000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', '658A9547-53FB-4235-9086-93DC330AD4E9', '20103702', 'SMKS DINAMIKA PEMBANGUNAN 2 JAKARTA', 'SMK', 'S', 'JL. RAYA PENGGILIAN NO. 99', '-6.1906000', '106.9372000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016302  ', 'Kec. Pasar Minggu', '65B89B61-B995-47D5-A31D-36579CD60E07', '20102597', 'SMKN 8 JAKARTA', 'SMK', 'N', 'JL. RAYA PEJATEN', '-6.2796000', '106.8382000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016201  ', 'Kec. Kembangan', '65E4CF5E-1B9D-49DA-8F4A-7C0AFB31F910', '20109458', 'SMKS GLOBAL PERSADA', 'SMK', 'S', 'Jl. Raya Kembangan No.14', '-6.1845000', '106.7326000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016103  ', 'Kec. Tanjung Priok', '6647118B-28AA-40C4-B33D-6698F0017090', '20107427', 'SMKS KENCANA 2 JAKARTA', 'SMK', 'S', 'JL.SUNGAI BAMBU POOL/13A TG.PRIOK', '-6.1288000', '106.8889000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016003  ', 'Kec. Senen', '6708FE56-05BC-4533-984E-C62C7EA17733', '20100133', 'SMKS BINA DARMA DKI', 'SMK', 'S', 'JL. ABDUL RAHMAN SALEH I NO.  10/A', '-6.1672000', '106.8751000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016304  ', 'Kec. Pesanggrahan', '675CEB77-3138-499E-AE66-48320BD0AD5E', '20102619', 'SMKS PERWIRA JAKARTA', 'SMK', 'S', 'JL. H. DILUN NO. 4', '-6.2462000', '106.7641000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016302  ', 'Kec. Pasar Minggu', '67EEF23E-FBF3-44CD-8DA1-97ACD5E1EB3D', '20109266', 'SMKS INFORMATIKA YASMA', 'SMK', 'S', 'JL. JERUK PURUT', '-6.3094000', '106.8143000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016207  ', 'Kec. Cengkareng', '67FE88B0-7B42-4264-97ED-E7655BD64BEC', '20104454', 'SMKS CINTA KASIH TZU CHI', 'SMK', 'S', 'JL. KAMAL RAYA NO. 20 OUTER RING ROADCENGKARENG TIMUR', '-6.1317000', '106.7379000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016202  ', 'Kec. Kebon Jeruk', '68A21E29-15C9-44B2-83AF-96BCC3D81F29', '20101644', 'SMKS KEBON JERUK JAKARTA', 'SMK', 'S', 'JL. AYUB DALAM NO. 26 B', '-6.2052000', '106.7768000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016401  ', 'Kec. Pasar Rebo', '68B44857-5DCF-45F4-A936-AFE6BAB8F903', '20103276', 'SMKS ANKES TUNAS HARAPAN', 'SMK', 'S', 'JL. BENDERA RAYA - INTISARI III KALISARI', '-6.3316000', '106.8594000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016205  ', 'Kec. Tambora', '68F88677-96A7-46B7-9244-5C35EBF7C5A4', '20101518', 'SMKS PELITA IV', 'SMK', 'S', 'JL. POS DURI RAYA NO. 23-29', '-6.1560000', '106.8019000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016406  ', 'Kec. Jatinegara', '69A098B7-5A65-4CEB-821E-9054EFCEF733', '20103647', 'SMKS HATAWANA', 'SMK', 'S', 'JL. CIPINANG BESAR SELATAN NO. 1', '-6.2294000', '106.8789000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016406  ', 'Kec. Jatinegara', '69D5B117-36F1-4A62-9BF8-83997F1B67DE', '20103271', 'SMKS BERLIAN JAKARTA', 'SMK', 'S', 'JL. BERLIAN NO. 9A', '-6.2444000', '106.8739000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016309  ', 'Kec. Tebet', '69ED6704-86FD-4280-B92B-94E8DB148B5D', '20102433', 'SMKS YPK KESATUAN JAKARTA', 'SMK', 'S', 'JL. MANGGARAI UTARA VII', '-6.2260000', '106.8485000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016406  ', 'Kec. Jatinegara', '6A3EDE65-355E-420B-A192-6DCE9B7D7275', '20103728', 'SMKS PANDAWA', 'SMK', 'S', 'JL. RAYA CIPINANG MUARA NO. 12', '-6.2232000', '106.8963000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016403  ', 'Kec. Cipayung', '6AB5B033-6143-41C0-ACF1-37E75B1DF0CD', '20103788', 'SMKN 24 JAKARTA', 'SMK', 'N', 'JL. BAMBU HITAM', '-6.3210000', '106.9002000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', '6B60DF35-605D-4F76-B4BA-BD3C10B58326', '20103558', 'SMKS TADIKA PURI JAKARTA', 'SMK', 'S', 'JL. MALAKA I/207 KELENDER', '-6.2220000', '106.9290000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016208  ', 'Kec. Kali Deres', '6B721C89-6269-4CF7-BCC4-024DFFD239AA', '20109274', 'SMKS PADINDI', 'SMK', 'S', 'PREPEDAN DALAM', '-6.1164000', '106.7073000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016102  ', 'Kec. Pademangan', '6B9DA8FE-E255-4719-9C12-CE5E681BFA1B', '20107433', 'SMKN 23 JAKARTA', 'SMK', 'N', 'JL. PADEMANGAN TIMUR III/9', '-6.1375000', '106.8425000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', '6BF2BC8A-26B5-4DE6-A511-859DB92602FC', '20103259', 'SMKS AL AKHYAR 2 JAKARTA', 'SMK', 'S', 'JL. KAYU TINGGI NO.25', '-6.1824000', '106.9456000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016002  ', 'Kec. Menteng', '6CEC66B7-779D-49F5-9E21-B076ABD7A4E7', '20108510', 'SMKS JAYAWISATA 1 JAKARTA', 'SMK', 'S', 'JL. TAMAN SUNDA KELAPA NO. 16 A', '-6.2009000', '106.8320000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', '6D259029-4B1A-49EE-BDF9-EE4FFC0AC898', '20103562', 'SMKS ST BONAVENTURA JAKARTA', 'SMK', 'S', 'JL. AMAL NO. 2', '-6.2262000', '106.8993000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016308  ', 'Kec. Pancoran', '6E2B0B42-B020-48D1-9E71-40EDAFB6BACD', '20102461', 'SMKS YAPENAP JAKARTA', 'SMK', 'S', 'JL. PANCORAN BARAT VI B/ 72', '-6.2497000', '106.8402000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016305  ', 'Kec. Kebayoran Lama', '6E4E5E76-0D4F-4A03-B8C6-D38CD4CF40F5', '20107361', 'SMKS TIRTAYASA', 'SMK', 'S', 'JL. BINTARO RAYA NO. 11', '-6.2530000', '106.7738000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', '6F87FC29-B1B2-4853-BAE0-43C456206C93', '20103500', 'SMKS YPIA AL FALAH JAKARTA', 'SMK', 'S', 'JL. RAYA BEKASI KM. 36', '-6.1875000', '106.9659000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016309  ', 'Kec. Tebet', '6FCE9AFF-C8B1-4A16-A248-F9EC2F24EB40', '20102457', 'SMK YASDA', 'SMK', 'S', 'Jl. Patra XV RT 011/13', '-6.2295000', '106.8369000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016205  ', 'Kec. Tambora', '6FE1DA82-AA01-4931-8C87-0B13C25536E5', '20101593', 'SMKS BHINEKA TUNGGAL IKA', 'SMK', 'S', 'JL. KHM. MANSYUR NO. 222 - A', '-6.1472000', '106.8066000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016104  ', 'Kec. Koja', '70998780-2E0A-49C9-ABE0-BBFE20185A9A', '20107403', 'SMKS AL IRSYAD JAKARTA', 'SMK', 'S', 'JL. MINDI RAYA NO. 29-35', '-6.1136000', '106.9106000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016208  ', 'Kec. Kali Deres', '70FEA044-E25C-42C6-827D-E327068FA7F0', '20101640', 'SMKS JAKARTA 1', 'SMK', 'S', 'JL. PETA SELATAN NO. 24 KALIDERES', '-6.1472000', '106.7027000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', '710BA17C-7DA3-4DB6-AB66-A51FB0C9284A', '20103754', 'SMKS NURUL HUDA', 'SMK', 'S', 'JL TIPAR CAKUNG. JAKARTA TIMUR', '-6.1766000', '106.9354000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016403  ', 'Kec. Cipayung', '72BDFB69-A10E-4FD0-A113-612DBB97A0CB', '20103674', 'SMKS JAKARTA RAYA 2', 'SMK', 'S', 'JL. MONUMEN P. SAKTI NO. 36', '-6.2883000', '106.8993000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016401  ', 'Kec. Pasar Rebo', '72D82C38-7121-4F0F-B802-F62589738FBC', '20103691', 'SMKS BUDI WARMAN 2 JAKARTA', 'SMK', 'S', 'JL. RAYA BOGOR KM.28', '-6.3407000', '106.8650000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016302  ', 'Kec. Pasar Minggu', '73059089-A3D3-4251-8514-ED445C235CB5', '69819337', 'SMK SUMBANGSIH', 'SMK', 'S', 'JL. AMPERA RAYA NO 3-4', '-6.3084000', '106.8154000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016008  ', 'Kec. Gambir', '737C163A-8AF2-43BE-A3D2-25B78F053205', '20100288', 'SMKS YP IPPI PETOJO', 'SMK', 'S', 'JL. PETOJO BARAT III/2', '-6.1638000', '106.8113000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', '741AD4E1-BAC2-45A4-AC3B-EDD90325BF17', '20103686', 'SMKS BUDI MURNI 1 JAKARTA', 'SMK', 'S', 'JL. SAWAH BARAT', '-6.2308000', '106.9102000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016104  ', 'Kec. Koja', '74709C8D-DA40-491F-BBB1-7AA838EDC297', '20107459', 'SMKS SEJAHTERA JAKARTA', 'SMK', 'S', 'JL. WALANG BARU VI NO. 19', '-6.1250000', '106.9040000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016301  ', 'Kec. Jagakarsa', '7482F40D-BDB2-4DD3-A3E3-1228740BE2E2', '20102434', 'SMK TELADAN', 'SMK', 'S', 'JL RAYA SRENGSENG SAWAH NO 74', '-6.3475000', '106.8246000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016403  ', 'Kec. Cipayung', '74844FA1-4CA6-41F5-ADEE-B8DE470C0644', '69758521', 'SMK PRESTASI PRIMA', 'SMK', 'S', 'JL HANKAM RAYA NO 89', '-6.3201000', '106.9141000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', '750B9E00-C4A9-4F1F-A621-EC0D5045BF10', '20103707', 'SMKS CORPATARIN 1 JAKARTA', 'SMK', 'S', 'JL. PONDOK KOPI RAYA NO. 87', '-6.2277000', '106.9430000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', '76095880-3F01-4BDE-BFEF-7872841D56CC', '20178278', 'SMKS GITA WISATA', 'SMK', 'S', 'JL. RAYA BEKASI KM 26, RT 03/RW 01', '-6.1858000', '106.9631000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', '76BA98EF-7BD6-415F-B095-060887A47025', '20103545', 'SMKS SRIWIJAYA JAKARTA', 'SMK', 'S', 'JL. DR. KRT. RADJIMAN WIDYONINGRAT', '-6.2007000', '106.9221000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', '76FD491C-AD54-45BA-8C9D-CA17163FF0C7', '20103684', 'SMKS BUDI MURNI 3 JAKARTA', 'SMK', 'S', 'JL. SAWAH BARAT', '-6.2308000', '106.9102000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016303  ', 'Kec. Cilandak', '7742B24C-A5BF-4B42-9D71-10DBAEDDEAE3', '20177923', 'SMKS PGRI 37 JAKARTA', 'SMK', 'S', 'JL. PONDOK LABU I NO. 29', '-6.3034000', '106.7930000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016103  ', 'Kec. Tanjung Priok', '78242F83-FBB4-4E8A-8B12-B3F378DD71FC', '20107416', 'SMKS DHARMA PUTRA1', 'SMK', 'S', 'JL. SWASEMBADA BARAT XI NO.92', '-6.1234000', '106.8909000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016104  ', 'Kec. Koja', '7899FAC8-B112-4007-8E75-CD94892F8C8B', '20107411', 'SMK PERGURUAN CIKINI JAKARTA', 'SMK', 'S', 'Jl. Alur Laut Blok NN No. 1', '-6.1247170', '106.9013410', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016106  ', 'Kec. Cilincing', '78AEBC98-5299-418D-9957-385816C8D8DB', '20107450', 'SMKS PGRI 11 JAKARTA', 'SMK', 'S', 'JL. ROROTAN VI, MALAKA 3', '-6.1412000', '106.9542000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016003  ', 'Kec. Senen', '79DCF149-077B-4D4F-B4FF-DF2FA89B5A56', '20108511', 'SMKS KESDAM JAYA', 'SMK', 'S', 'JL. KRAMAT RAYA NO. 174', '-6.1911000', '106.8474000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016304  ', 'Kec. Pesanggrahan', '7A204FEB-0BC8-4326-A622-A6FA025B8E37', '20102528', 'SMK PUSPITA PERSADA', 'SMK', 'S', 'JL. SMU 63 NO. 60', '-6.2280000', '106.7483000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016003  ', 'Kec. Senen', '7AE5C636-518D-4451-96CA-3043D7BFAB75', '20100166', 'SMKN 34 JAKARTA', 'SMK', 'N', 'JL. KRAMAT RAYA NO. 39', '-6.1905000', '106.8475000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016206  ', 'Kec. Taman Sari', '7B0AC8AE-43D7-43F8-8D5F-05ED79B1CA42', '20101652', 'SMKS K RAHMANI', 'SMK', 'S', 'JL. MANGGA BESAR XI NO.8-10', '-6.1463000', '106.8213000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016403  ', 'Kec. Cipayung', '7B589BFA-EF27-4EE6-894F-A14AF8A850CE', '20177829', 'SMKS ANALIS KESEHATAN TUNAS MEDIKA', 'SMK', 'S', 'SETU RAYA - PINANG NO. 67', '-6.3303000', '106.8587000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016404  ', 'Kec. Makasar', '7BB45814-9047-4888-A19C-2A2FC1D9C0F7', '20103667', 'SMKS JAYAWISATA 2 JAKARTA', 'SMK', 'S', 'JL. PANGKALAN JATI II NO. 19', '-6.2485000', '106.9060000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016103  ', 'Kec. Tanjung Priok', '7BBCFCA9-4758-40E3-916D-AB6178F25117', '20107432', 'SMKS MUTIARA 1 JAKARTA', 'SMK', 'S', 'JL. KOMP YOS SUDARSO II/19', '-6.1196000', '106.8926000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016202  ', 'Kec. Kebon Jeruk', '7BC1B7BA-8F7C-481B-817B-CDE4A04748C6', '20101670', 'SMKS BUDI MURNI 2', 'SMK', 'S', 'JL. PURI KEMBANGAN NO. 2', '-6.1770000', '106.7466000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016403  ', 'Kec. Cipayung', '7C14E17B-1BCB-47AF-8C67-0BB520754F08', '20107598', 'SMKS TRI SASTRA 1 JAKARTA', 'SMK', 'S', 'JL. SMP 157', '-6.2934000', '106.9034000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016404  ', 'Kec. Makasar', '7C4771BF-92EA-4B91-8BB9-00002F3F21DE', '20103743', 'SMKS PGRI 1 JAKARTA', 'SMK', 'S', 'JL. PLK 2 NO. 25', '-6.2665000', '106.8751000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016303  ', 'Kec. Cilandak', '7CC79E9A-7B54-4BF6-A663-ABFD66601CC3', '20102441', 'SMKS SELAGAN JAYA', 'SMK', 'S', 'JL. H. IPIN NO.10 PONDOK LABU', '-6.3113000', '106.7929000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', '7CF9F200-58AA-4242-A5F3-760DE7281DC6', '20103244', 'SMKS AL WATHONIYAH JAKARTA', 'SMK', 'S', 'JL. RAYA BEKASI TIMUR KM 17', '-6.2147000', '106.9211000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016207  ', 'Kec. Cengkareng', '7D5D8DCE-2B15-435C-8EF1-434770B6B512', '20101649', 'SMKS LAMAHOLOT JAKARTA', 'SMK', 'S', 'JL. MANGGIS RAYA BOJONG INDAH', '-6.1640000', '106.7367000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016203  ', 'Kec. Palmerah', '7E16008D-1896-4ABF-A76F-D0E5B72D9457', '20101502', 'SMKN 17 JAKARTA', 'SMK', 'N', 'JL. G. SLIPI', '-6.1914000', '106.7995000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016202  ', 'Kec. Kebon Jeruk', '7E215150-EE70-4E61-883D-092B3E32DFF1', '69768184', 'SMKS PUTRA MANDIRI', 'SMK', 'S', 'JL ASIA BARU KEPA DURI NO 60 RT 08/04', '-6.1774000', '106.7656000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016402  ', 'Kec. Ciracas', '7E545228-F001-4AC1-940A-DE20D1C41E5D', '20103541', 'SMKS PKP 1 JAKARTA', 'SMK', 'S', 'JL. RAYA PKP', '-6.3390000', '106.8834000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016307  ', 'Kec. Mampang Prapatan', '7E985A6D-1F41-4B20-88D9-3AC60AE9E35E', '20102439', 'SMKS ISLAM ANDALUS', 'SMK', 'S', 'JL. BANGKA 2 / C 32', '-6.2455000', '106.8194000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016401  ', 'Kec. Pasar Rebo', '7EE539D7-4DB8-4BBC-8CC8-5A09D89870E5', '20103746', 'SMKS PERTIWI JAKARTA', 'SMK', 'S', 'JL. H. TAIMAN BARAT', '-6.2963000', '106.8658000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016303  ', 'Kec. Cilandak', '7F5CFF43-CF04-4D7D-9ED2-7C952A5ED2D8', '69774854', 'SMKS GAPURA MERAH PUTIH', 'SMK', 'S', 'JL. Raya Jagakarsa no. 20 Setu Ciganjur Kec. Jagakarsa, Jakarta Selatan', '-6.3072000', '106.7824000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016202  ', 'Kec. Kebon Jeruk', '7FB9790B-2EFF-42F1-9A65-9262B7CA5583', '20101600', 'SMKS AL HAMIDIYAH', 'SMK', 'S', 'JL. RAYA KEDOYA SELATAN NO. 50', '-6.1918000', '106.7571000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016304  ', 'Kec. Pesanggrahan', '7FC96D43-B04C-4BA7-ADA0-880C993CB46D', '20101511', 'SMKS PGRI 15 JAKARTA', 'SMK', 'S', 'JALAN MUCHTAR RAYA GG. H. DOEL RT 005/01', '-6.2301990', '106.7471270', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', '805EF0DF-5627-42FE-9917-DF4733460E2A', '20107210', 'SMKS GEMA NUSANTARA', 'SMK', 'S', 'Jl. Duasembilan No. 18', '-6.2428000', '106.9167000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016201  ', 'Kec. Kembangan', '80F37D0E-8F7E-40A2-BAFD-6824469C9BBF', '20101474', 'SMKS REFORMASI', 'SMK', 'S', 'JL. SANGGRAHAN RAYA RT.010/005', '-6.1807000', '106.7314000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016202  ', 'Kec. Kebon Jeruk', '81AE9E89-829C-4855-AEF0-41A1A776033F', '20101487', 'SMKS TRI ARGA 2', 'SMK', 'S', 'Jln. HH No.63', '-6.2003000', '106.7638000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016403  ', 'Kec. Cipayung', '81B6DC18-04D2-4A5E-83F9-4A01EE78C7C8', '20177915', 'SMKS AS SYAFIIYAH 2', 'SMK', 'S', 'JL. AS-SYAFIIYAH NO. 37', '-6.3374000', '106.9001000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016302  ', 'Kec. Pasar Minggu', '823E70AA-DF49-4F08-A21D-1ADF93C52A2D', '20102459', 'SMKS YAPIMDA', 'SMK', 'S', 'JL. POLTANGAN IV NO. 34 PEJATEN TIMUR PASAR MINGGU', '-6.2961000', '106.8444000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016409  ', 'Kec. Pulo Gadung', '83C65811-2F8F-4A99-A5E9-B105D79168EC', '20103711', 'SMKS CIKRA 1 JAKARTA', 'SMK', 'S', 'JL. CIPINANG KEBEMBEM V/32', '-6.2102000', '106.8835000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016401  ', 'Kec. Pasar Rebo', '842570AA-BD73-4228-91C8-0D1EF28AD16B', '20103727', 'SMKS PANGUDI RAHAYU I JAKARTA', 'SMK', 'S', 'JL. RAYA BOGOR KM. 24.5', '-6.3198000', '106.8643000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', '846E20AA-1F06-4EE3-B507-B910F24C34B1', '20103501', 'SMKS YP IPPI JAKARTA', 'SMK', 'S', 'JL. RAYA ANEKA ELOK BLOK G', '-6.1994000', '106.9412000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', '84D18780-41FD-4E19-AAAC-733467FFE43D', '69849606', 'SMK PERGURUAN RAKYAT 2', 'SMK', 'S', 'Jl. AD Lampiri 28 Pondok Kelapa', '-6.2439160', '106.9341400', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016406  ', 'Kec. Jatinegara', '84E23864-0464-4DBD-BBBA-257664E90D45', '20103712', 'SMKS CAWANG JAKARTA', 'SMK', 'S', 'JL. CAWANG BARU NO. 71', '-6.2420000', '106.8730000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016410  ', 'Kec. Matraman', '84EDD6D9-089C-4E7E-BFA1-4F66866666F7', '20103709', 'SMKS CIPTA KARYA JAKARTA', 'SMK', 'S', 'KAYU MANIS TIMUR NO. 52D', '-6.1775000', '106.8859000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016208  ', 'Kec. Kali Deres', '852D20CB-A798-4B4E-8995-137C6973A5F5', '20109187', 'SMKS INSAN GLOBAL', 'SMK', 'S', 'JL. TANJUNG PURA NO. 5', '-6.1290000', '106.6952000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016305  ', 'Kec. Kebayoran Lama', '85B14F9D-A8AF-437A-9A91-FB9982C57E44', '20102626', 'SMKS JAKARTA MANAJEMEN', 'SMK', 'S', 'JL. PRAJA DALAM E/3 ARTERI PONDOK INDAH KEBAYORAN LAMA', '-6.2524000', '106.7806000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016402  ', 'Kec. Ciracas', '85F98B14-A3D3-4483-8B10-B2524E4DAE5A', '20103717', 'SMKS KARYA WIJAYA KUSUMA JAKARTA', 'SMK', 'S', 'JL. MUJAHIDIN NO. 17A', '-6.3031000', '106.8726000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016404  ', 'Kec. Makasar', '867C6FC6-ABE3-40BF-8BB1-4EE5CA442419', '20103749', 'SMKS PERDANA KUSUMA', 'SMK', 'S', 'JL. HALIM PERDANA KUSUMA', '-6.2499000', '106.8796000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016305  ', 'Kec. Kebayoran Lama', '87E69A76-273F-4C80-8F91-CFD80869BB74', '20102560', 'SMK AL FAJAR JAKARTA', 'SMK', 'S', 'JL. SULTAN ISKANDAR MUDA NO. 32', '-6.2449000', '106.7819000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016103  ', 'Kec. Tanjung Priok', '8801BEC3-6A06-463F-AB47-78CA90F3894D', '20109505', 'SMKS DHARMA PUTRA 2 JAKARTA', 'SMK', 'S', 'JL. SWASEMBADA BARAT XI NO. 92', '-6.1226000', '106.8872000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', '88432138-D91A-41FF-A896-A9004C22FFC8', '20103703', 'SMKS DINAMIKA PEMBANGUNAN 1 JAKARTA', 'SMK', 'S', 'JL. RAYA PENGGILINGAN 99', '-6.1898000', '106.9372000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016001  ', 'Kec. Tanah Abang', '88DDC786-556D-4014-B357-D54AECA78CFF', '20100151', 'SMKS PGRI 31 JAKARTA', 'SMK', 'S', 'JL. SABENI NO. 12', '-6.1932000', '106.8139000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016402  ', 'Kec. Ciracas', '8A335551-8283-4D45-B55B-C3A3A4E33675', '20103643', 'SMKS INSAN TEKNOLOGI JAKARTA', 'SMK', 'S', 'JL. CIBUBUR 4 NO.29', '-6.3518000', '106.8755000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016003  ', 'Kec. Senen', '8B9C4C90-8525-4F93-BDA6-1646F94EDDCF', '20100282', 'SMKS PSKD 1 JAKARTA', 'SMK', 'S', 'JL. Prajurit KKO Usman & Harun No. 26', '-6.1866000', '106.8414000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '010100  ', 'Kab. Kepulauan Seribu', '010101  ', 'Kec. Kepulauan Seribu Selatan', '8C57BE23-E673-4FC3-A8E0-18A6D68F43EF', '20109165', 'SMKN 61 JAKARTA', 'SMK', 'N', 'PANTAI SELATAN 1 PULAU TIDUNG.', '-5.8023000', '106.5099000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016001  ', 'Kec. Tanah Abang', '8D1681E1-775B-4CBC-8D75-BCDB18E4F3A6', '20100149', 'SMKS PGRI 25 JAKARTA', 'SMK', 'S', 'JL. MESJID I KARET TENGSI TANAH ABANG', '-6.2106000', '106.8150000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016106  ', 'Kec. Cilincing', '8DADFE09-D02F-4C26-8371-9FB85ACD66F2', '20109260', 'SMKS AL MIFTAHIYYAH', 'SMK', 'S', 'MUARA GEDUNG KALIBARU BARAT VII', '-6.0996000', '106.9359000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016208  ', 'Kec. Kali Deres', '8EE7C130-9F77-49A9-AA02-2B51A5EF25E1', '20101665', 'SMKS CENGKARENG JAKARTA', 'SMK', 'S', 'JL. PETA SELATAN NO.53', '-6.1416000', '106.6975000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016303  ', 'Kec. Cilandak', '9058AF64-BD78-439C-9A95-E4C27363E042', '20102580', 'SMK NEGERI 41 JAKARTA', 'SMK', 'N', 'JL. MARGASATWA KOMPLEK TIMAH', '-6.3141000', '106.8024000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016301  ', 'Kec. Jagakarsa', '90BD246A-19EC-4184-A459-942A0763F93D', '69888484', 'SMK AN NURIYAH', 'SMK', 'S', 'JL. TIMBUL No.60', '-6.3269640', '106.8348360', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016401  ', 'Kec. Pasar Rebo', '90D8348E-95EA-420D-843F-6136DB33870F', '20177896', 'SMKS MAHADHIKA 4', 'SMK', 'S', 'JL. RAYA BOGOR KM. 25', '-6.3226000', '106.8624000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016301  ', 'Kec. Jagakarsa', '90E839EF-1558-4CD6-A0A0-F6A52F7A6CD5', '20107360', 'SMKS TELADAN UTAMA', 'SMK', 'S', 'Jl. Raya Srengseng Sawah No. 74', '-6.3466000', '106.8219000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016008  ', 'Kec. Gambir', '90FBF286-2EA7-4BDB-827D-C640C13146CF', '20100161', 'SMKN 2 JAKARTA', 'SMK', 'N', 'JL. BATU 3', '-6.1781000', '106.8335000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016204  ', 'Kec. Grogol Petamburan', '9114A051-FE61-4312-9488-F2169A93BECF', '20101648', 'SMKS MAARIF JAKARTA', 'SMK', 'S', 'JL. DR. MUWARDI RAYA NO. 19', '-6.1627000', '106.7947000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016409  ', 'Kec. Pulo Gadung', '911B9631-7219-4674-A724-21C31609E527', '20177895', 'SMKS MANDALA TIARA BANGSA', 'SMK', 'S', 'JL. RAWAMANGUN MUKA SELATAN 21-23', '-6.1993000', '106.8802000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016104  ', 'Kec. Koja', '91A39474-A2DB-45E3-B076-4C75137C70CA', '20107424', 'SMKS KARYA BAHARI JAKARTA', 'SMK', 'S', 'JL. MINDI NO. 36 LAGOA KOJA', '-6.1137000', '106.9161000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016403  ', 'Kec. Cipayung', '922261EC-9104-44C9-9E7D-8CE3B9C1E5A4', '69773001', 'SMKS Islam Al-Makiyah', 'SMK', 'S', 'Jl. Gorda, Lubang Buaya', '-6.2777000', '106.8825000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016008  ', 'Kec. Gambir', '92399B3B-74FC-4ED0-92A0-2DBDC6C3B4CF', '20100301', 'SMKS SANTO PAULUS', 'SMK', 'S', 'SETIA KAWAN RAYA NO. 39-41', '-6.1644000', '106.8016000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);
INSERT INTO `tb_sekolah` (`id_provinsi`, `provinsi`, `id_kota`, `kota`, `id_kecamatan`, `kecamatan`, `id_sekolah`, `npsn`, `nama_sekolah`, `bentuk`, `status`, `alamat`, `lintang`, `bujur`, `created_at`, `updated_at`, `deleted_at`) VALUES
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016403  ', 'Kec. Cipayung', '923F0E19-7B57-46C9-A44E-CB5454E15123', '20112507', 'SMKS MITRA WISATA', 'SMK', 'S', 'JL. GIRI KENCANA NO. 31', '-6.3324000', '106.9040000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016401  ', 'Kec. Pasar Rebo', '92528338-5DF7-4CFC-976C-C53132FB0B8C', '69773002', 'SMK BHAKTI NEGERI', 'SMK', 'S', 'JL. BELLY/MEKAR V', '-6.3266000', '106.8620000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016301  ', 'Kec. Jagakarsa', '9301B406-11F3-47C1-BA33-14305D57CAE1', '20102542', 'SMK AL HIDAYAH 2', 'SMK', 'S', 'JL. KESATUAN 47, SRENGSENG SAWAH, JAGAKARSA', '-6.3554000', '106.8223000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016203  ', 'Kec. Palmerah', '93993754-4729-4D60-BDE4-D51695F596AB', '20101637', 'SMKS JOSUA JAKARTA', 'SMK', 'S', 'JL. KOTABAMBU UTARA 2 NO. 8', '-6.1858000', '106.8074000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016008  ', 'Kec. Gambir', '94E8132A-2B7B-48D0-B476-CD949C0285EE', '20100119', 'SMKS AL IRSYAD', 'SMK', 'S', 'JL. KH. HASYIM ASHARI 27 GAMBIR', '-6.1659000', '106.8158000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016202  ', 'Kec. Kebon Jeruk', '94E931AD-8F7D-4481-A42D-17D506297444', '20101497', 'SMKN 60 JAKARTA', 'SMK', 'N', 'JL. DURI RAYA NO 15A', '-6.1729000', '106.7758000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016310  ', 'Kec. Setia Budi', '94F6C9AC-3CD6-478E-89E9-B60A6D5F740C', '20107337', 'SMKS TRISULA PERWARI 1', 'SMK', 'S', 'PARIAMAN NO. 17', '-6.2088000', '106.8438000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016206  ', 'Kec. Taman Sari', '951BA15A-E582-43DF-8B89-7EAFA046281B', '20107273', 'SMKS STRADA II JAKARTA', 'SMK', 'S', 'JL. TAMANSARI VIII NO. 83A', '-6.1568000', '106.8233000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016106  ', 'Kec. Cilincing', '9561E512-3164-4922-BCC4-EC134FE66675', '20107445', 'SMKS PELAYARAN DJADAJAT JAKARTA', 'SMK', 'S', 'JL. MARUNDA TIRAM NO.28', '-6.1181000', '106.9581000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016406  ', 'Kec. Jatinegara', '956AFB12-82F0-4586-972E-CAD2DAA679FF', '20103782', 'SMKN 50 JAKARTA', 'SMK', 'N', 'JL. CIPINANG MUARA 1', '-6.2279000', '106.8905000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016105  ', 'Kec. Kelapa Gading', '959715C2-09D3-48EF-8581-12EC7CEF0191', '20107421', 'SMKS HANG TUAH I JAKARTA', 'SMK', 'S', 'JL. TABAH RAYA KOMP. TNI-AL', '-6.1666000', '106.8834000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016302  ', 'Kec. Pasar Minggu', '9600E8A8-84BF-4A7F-8159-4343A29577A2', '20102608', 'SMKS PASAR MINGGU JAKARTA', 'SMK', 'S', 'JL. ASEM PEJATEN INDAH 2', '-6.2805000', '106.8411000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016410  ', 'Kec. Matraman', '96EAA586-C120-4C70-A55B-E9E28601C7C3', '20103734', 'SMKS NURUL IMAN JAKARTA', 'SMK', 'S', 'JL. PISANGAN BARU TIMUR NO. 4A', '-6.2139000', '106.8725000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016207  ', 'Kec. Cengkareng', '97BDC812-5BCD-4A90-B48E-143734F4F5CA', '20101500', 'SMKN 42 JAKARTA', 'SMK', 'N', 'JL. KAMAL RAYACENGKARENG TIMUR', '-6.1553000', '106.7727000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', '97E418B8-1A42-48D4-AAE3-560E9205E0A9', '20103263', 'SMKS BISNIS INDONESIA JAKARTA', 'SMK', 'S', 'JL. IRIGASI RAYA (MENTENG METROPOLITAN)', '-6.1862000', '106.9623000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016207  ', 'Kec. Cengkareng', '983EDBB5-FE59-4EF2-9EEE-30BD78CA43E0', '20101476', 'SMKS PGRI 5 JAKARTA', 'SMK', 'S', 'Jl. Peta Barat No. 40', '-6.1316000', '106.7383000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016404  ', 'Kec. Makasar', '98E6C4E1-AA56-4F33-BE1B-E85E190DAB25', '20103273', 'SMKS ANGKASA 2 JAKARTA', 'SMK', 'S', 'JL. RAJAWALI RAYA', '-6.2520000', '106.8900000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016403  ', 'Kec. Cipayung', '98FB0965-1F71-431D-B7AD-4BEF18CC9978', '20103708', 'SMKS CITRA DHARMA JAKARTA', 'SMK', 'S', 'JL. SWADAYA I NO. 59', '-6.3185000', '106.9009000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016101  ', 'Kec. Penjaringan', '9903C08E-273F-4392-BCFA-7C5322975618', '20107455', 'SMKS REMAJA PLUIT', 'SMK', 'S', 'JL.PLUIT SELATAN I NO. 1 PLUIT', '-6.1252000', '106.7908000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016006  ', 'Kec. Kemayoran', '99465BE0-5DB5-4F09-8F05-E728DBFD868D', '20100162', 'SMKN 21 JAKARTA', 'SMK', 'N', 'JL. SIAGA I GG. SWADAYA III', '-6.1586000', '106.8549000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016205  ', 'Kec. Tambora', '99649C59-B126-4A27-8EB7-06668E05CD17', '20101680', 'SMKS HARVARD JAKARTA', 'SMK', 'S', 'JL. P. TB. ANGKE NO. 33', '-6.1427000', '106.7960000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016202  ', 'Kec. Kebon Jeruk', '998862AA-45AA-4910-BA4F-0574A57FF705', '20101639', 'SMKS JAKARTA III', 'SMK', 'S', 'JL. KEDOYA RAYA NO. 200', '-6.1790000', '106.7578000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', '99B419E0-1416-4EBC-93EF-8ECDD2A2F9EC', '20103756', 'SMKS MERCUSUAR', 'SMK', 'S', 'JL. RAYA BKS KM.26 NO.51', '-6.1853000', '106.9631000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016007  ', 'Kec. Sawah Besar', '99BD9320-1E73-4BA3-B1A9-A10781C529FA', '20100131', 'SMKS AT TAQWA JAKARTA', 'SMK', 'S', 'JL. RAJAWALI SELATAN VI NO. 18', '-6.1454000', '106.8396000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016104  ', 'Kec. Koja', '99F8BE31-5372-4C6D-A1AB-4A1DC158CE11', '20107475', 'SMKS YUSHA JAKARTA', 'SMK', 'S', 'JL. MAWAR LUAR NO. 1', '-6.1185000', '106.9231000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', '9AC12794-6364-4280-A72A-3FA6B9BCF9CA', '20103760', 'SMKS MALAKA JAKARTA', 'SMK', 'S', 'JL. RAYA MAWAR MERAH NO. 23', '-6.2214000', '106.9392000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016306  ', 'Kec. Kebayoran Baru', '9AEC13E6-EBAD-403C-947B-0B97D475F18C', '20102579', 'SMKN 6 JAKARTA', 'SMK', 'N', 'JL. PROF JOKO SUTONO, SH NO. 2 A', '-6.2428000', '106.8074000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016301  ', 'Kec. Jagakarsa', '9B749154-AB08-4EFE-997E-30F0BBDB7ACB', '20112473', 'SMKS YASPIA', 'SMK', 'S', 'BENDA DALAM GG. H. SAENAN II NO.12', '-6.3309000', '106.8057000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016202  ', 'Kec. Kebon Jeruk', '9B839F13-B990-416B-B20E-EE684B13E5A0', '20101483', 'SMKS WIDYA PATRIA 2', 'SMK', 'S', 'Jl. Pos Pengumben No. 30,', '-6.2165000', '106.7740000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', '9C189924-151B-45E7-B5F0-E767088547F0', '20103676', 'SMKS JAKARTA 1', 'SMK', 'S', 'JL. RAYA PONDOK KOPI NO. 75', '-6.2319000', '106.9386000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016308  ', 'Kec. Pancoran', '9D6D17BE-3B23-4529-B283-064722552664', '20102438', 'SMKS SUMBER DAYA MANUSIA', 'SMK', 'S', 'AMIL BUNCIT INDAH IB', '-6.2740000', '106.8314000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016208  ', 'Kec. Kali Deres', '9D8854B7-0DBB-453E-9B7C-BF744D895F6B', '20101560', 'SMKS YADIKA 3 JAKARTA', 'SMK', 'S', 'JL. KAMAL RAYA NO. 42', '-6.1218000', '106.7237000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016305  ', 'Kec. Kebayoran Lama', '9DA91FE6-926F-46CB-934F-6D034FC0E42B', '20102594', 'SMKS PANDAWA BUDI LUHUR JAKARTA', 'SMK', 'S', 'JL. KOMP HANKAM CIDODOL NO. 3', '-6.2280000', '106.7771000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016007  ', 'Kec. Sawah Besar', '9DAC05AB-60B5-46A4-B199-D3D970E1F847', '20109314', 'SMKS FARMASI BPK PENABUR', 'SMK', 'S', 'PINTU AIR RAYA NO. 11', '-6.1667000', '106.8314000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', 'A0017C5D-4748-4E87-86FA-F6B5B13C2996', '20103745', 'SMKS PETRI JAYA', 'SMK', 'S', 'Jl. Curug Raya No.7', '-6.2495000', '106.9296000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016303  ', 'Kec. Cilandak', 'A093EEDD-BDFA-4510-B7FB-5803E0A792EB', '20107354', 'SMKS KELUARGA WIDURI', 'SMK', 'S', 'JL. GUNUNG BALONG I', '-6.3037000', '106.7879000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', 'A0C3CECC-7C43-405F-A350-7E061C3D9942', '20103526', 'SMKS WAWASAN NUSANTARA JAKARTA', 'SMK', 'S', 'RAYA PENGGILINGAN NO. 50 & 7', '-6.2059000', '106.9368000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016204  ', 'Kec. Grogol Petamburan', 'A13428E1-5905-483A-8800-F9D2FD04BB91', '20101484', 'SMKS TUNAS HARAPAN', 'SMK', 'S', 'JL. KOMP. BNI 46 PESING', '-6.1553000', '106.7722000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016402  ', 'Kec. Ciracas', 'A1605519-E107-42ED-AF9B-CA6A4C776E59', '20103780', 'SMKN 52 JAKARTA', 'SMK', 'N', 'JL. TARUNA JAYA', '-6.3651000', '106.8858000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016204  ', 'Kec. Grogol Petamburan', 'A1644257-3718-4294-AF25-A342E0ACC054', '20101641', 'SMKS ISLAM ASSAADATUL ABADIYAH JAKARTA', 'SMK', 'S', 'JL. TG. DUREN DALAM IV/25JAKARTA BARAT', '-6.1783000', '106.7810000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', 'A17EA796-9C50-4AC6-BA7F-77407B947EFB', '20107207', 'SMKS DHARMA PARAMITHA', 'SMK', 'S', 'SULTAN HAMENGKU BUWONO IX', '-6.1876000', '106.9613000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016409  ', 'Kec. Pulo Gadung', 'A1EFF606-3D96-4905-BD49-3DED556B7507', '20103737', 'SMKS PELITA TIGA NO 1 JAKARTA', 'SMK', 'S', 'JL. JENDRAL AHMAD YANI / BY PASS KAV. 98', '-6.2042000', '106.8743000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016302  ', 'Kec. Pasar Minggu', 'A2421905-3E3C-40FD-9830-2ED01B2582F2', '20102586', 'SMKN 25 JAKARTA', 'SMK', 'N', 'JL. RAYA RAGUNAN PASAR MINGGU', '-6.2843000', '106.8370000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016409  ', 'Kec. Pulo Gadung', 'A25A0326-203A-47CA-B938-5E4F81119DD4', '20103655', 'SMKS FENSENSIUS JAKARTA', 'SMK', 'S', 'JL. TAMAN PULO ASEM UTARA NO. 60', '-6.1908000', '106.8938000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', 'A380DFFC-9A9D-43BA-861C-5EACDEEB87FB', '20103538', 'SMKS PUSAKA 1 JAKARTA', 'SMK', 'S', 'JL. TARUNA PAHLAWAN REVOLUSI NO. 89', '-6.2385000', '106.8940000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016308  ', 'Kec. Pancoran', 'A3F2C74D-32A1-4340-97A8-F358BCD76B4B', '20102653', 'SMK KARYA TELADAN', 'SMK', 'S', 'JL. RAYA PASAR MINGGU NO 65', '-6.2575000', '106.8430000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016007  ', 'Kec. Sawah Besar', 'A4535A2F-B639-45EF-ADBA-E485971652F2', '20100163', 'SMKN 27 JAKARTA', 'SMK', 'N', 'JL. DR. SUTOMO NO. 1', '-6.1657000', '106.8359000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016201  ', 'Kec. Kembangan', 'A46C009A-2A83-4922-B4AB-E5ED126B30B4', '20101480', 'SMKS SUMPAH PEMUDA', 'SMK', 'S', 'JL. RAYA JOGLO NO.36', '-6.2202000', '106.7363000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016206  ', 'Kec. Taman Sari', 'A46CED26-77A0-43F9-ACC3-119D80D49C49', '20107272', 'SMKS ISLAM FATAHILLAH JAKARTA', 'SMK', 'S', 'JL. KEUTAMAAN NO. 89', '-6.1555000', '106.8151000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016409  ', 'Kec. Pulo Gadung', 'A83C805B-509A-4A58-94ED-44F671BC04A9', '20103245', 'SMKS AL WASHLIYAH JAKARTA', 'SMK', 'S', 'JL. AL-WASHLIYAH NO. 14', '-6.1923000', '106.8979000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', 'A897616C-AAB0-4824-9BB7-045F01B50CE7', '69900081', 'SMK PGRI 40 JAKARTA', 'SMK', 'S', 'Gg. Mushollah Al Ikhsan, JL. RAWA KUNING NO. 119', '-6.1985000', '106.9630000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016105  ', 'Kec. Kelapa Gading', 'A8F23844-4AF6-4BCE-A0B5-69F52BEEABE5', '20178282', 'SMKS PGRI 38 JAKARTA', 'SMK', 'S', 'JL TABAH I NO 5 KOMP TNI AL KODAMAR', '-6.1629000', '106.8849000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016406  ', 'Kec. Jatinegara', 'A9067728-7D2E-4233-A555-946DCAAE9650', '20103721', 'SMKS PELAYARAN BIMASAKTI JAKARTA', 'SMK', 'S', 'JL. CIPINANG BESAR NO. 2', '-6.2306000', '106.8757000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016103  ', 'Kec. Tanjung Priok', 'A977146F-4FA2-4F69-A98F-16184F0BC09A', '20107426', 'SMKS KENCANA 1 JAKARTA', 'SMK', 'S', 'JL.SUNGAI BAMBU POL 13A TG. PRIOK', '-6.1288000', '106.8890000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', 'A9907B48-55D8-4160-A0DE-23978188C3C4', '20103706', 'SMKS CORPATARIN 2 JAKARTA', 'SMK', 'S', 'JL. PONDOK KOPI RAYA NO. 87', '-6.2273000', '106.9439000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016401  ', 'Kec. Pasar Rebo', 'A9B8FF3B-777C-4413-BB39-D586A6A8AD37', '20103758', 'SMKS MARDI BAKTI JAKARTA', 'SMK', 'S', 'JL. RAYA BOGOR KM. 24', '-6.3177000', '106.8637000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016207  ', 'Kec. Cengkareng', 'AA638F67-DC89-45B5-B638-CF9F28B65847', '20112446', 'SMKS DIDAKTIKA', 'SMK', 'S', 'PULO HARAPAN INDAH', '-6.1326000', '106.7229000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', 'AAE9F544-4758-4E57-9538-82501C747692', '20103266', 'SMKS BINA MANAJEMEN JAKARTA', 'SMK', 'S', 'JL. DR. KRT. RADJIMAN WD', '-6.1903000', '106.9231000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016208  ', 'Kec. Kali Deres', 'AC04C4B8-A7C3-492B-A753-765084804263', '20104455', 'SMKS ERA PEMBANGUNAN', 'SMK', 'S', 'JL. GAGA ALAS TUA No. 120', '-6.1699000', '106.6921000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016301  ', 'Kec. Jagakarsa', 'AC40444E-62F8-4E3E-8DE9-90CD25738653', '69786987', 'SMK 3 PERGURUAN CIKINI', 'SMK', 'S', 'JL. SRENGSENG SAWAH NO 79', '-6.3534000', '106.8146000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', 'AC79FDE3-0450-470B-9430-8B9D461D1448', '20103533', 'SMKS RISTEK KIKIN JAKARTA', 'SMK', 'S', 'JL. RAYA PENGGILINGAN NO. 149', '-6.2070000', '106.9348000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016409  ', 'Kec. Pulo Gadung', 'ACB7D00E-D748-43FF-85F4-3978D2EF6184', '20108512', 'SMKS NUSANTARA WISATA', 'SMK', 'S', 'Jl. Kayu Putih Utara III C', '-6.1970000', '106.9075000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', 'ADDFF671-BA9B-4F07-B46E-6FA9E9D575A6', '20107204', 'SMKS BINA KARYA UTAMA JAKARTA', 'SMK', 'S', 'JL. RAYA BEKASI KM. 26 MENTENG METROPOLITAN', '-6.1871000', '106.9654000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016302  ', 'Kec. Pasar Minggu', 'B0228381-F96C-46E6-BFF8-DB8312971461', '20102557', 'SMKS BUNDA KANDUNG JAKARTA', 'SMK', 'S', 'JL. PALAPA RAYA NO. 3', '-6.2891000', '106.8419000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016103  ', 'Kec. Tanjung Priok', 'B077B759-B1D5-48B4-AF47-AAD575807009', '20107422', 'SMKS PELAYARAN JAKARTA', 'SMK', 'S', 'JL. WARAKASVI / 19 NO. 107', '-6.1268000', '106.8742000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016207  ', 'Kec. Cengkareng', 'B0D09996-1D39-4A15-988A-5AE674D7DE19', '20104458', 'SMKS KRISTEN CENDRAWASIH', 'SMK', 'S', 'JL. CENDRAWASIH RAYA NO. 60 CENGKARENG', '-6.1439000', '106.7238000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016106  ', 'Kec. Cilincing', 'B200F851-D03A-4052-BD65-A9B247C04229', '20107423', 'SMKS P JALASENA', 'SMK', 'S', 'JL. SARANG BANGO NO.27  RT : 05/04', '-6.1094000', '106.9285000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016206  ', 'Kec. Taman Sari', 'B2EBF71A-0C03-415A-A406-6DC740A4BFD7', '20101498', 'SMKN 53 JAKARTA', 'SMK', 'N', 'JL. Flamboyan No. 53', '-6.1321000', '106.8240000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016004  ', 'Kec. Johar Baru', 'B304513D-249C-40F2-8565-1F502CFF34EE', '20100158', 'SMKN 14 JAKARTA', 'SMK', 'N', 'JL. PERCETAKAN NEGARA II A', '-6.1857000', '106.8567000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016305  ', 'Kec. Kebayoran Lama', 'B30EEB53-A270-452F-AD63-0363F4647EEE', '20102613', 'SMK PONDOK INDAH', 'SMK', 'S', 'JL. PUPAN NO. 29', '-6.2750000', '106.7735000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016205  ', 'Kec. Tambora', 'B3C7A1AD-353A-44F5-879F-0E8B9A09AE1B', '20101496', 'SMKN 9 JAKARTA', 'SMK', 'N', 'JL. GEDONG PANJANG 2 NO. 17', '-6.1434000', '106.7914000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016410  ', 'Kec. Matraman', 'B3E33FA5-C823-495B-8466-49C49E512447', '20103528', 'SMKS SATYA BHAKTI 1 JAKARTA', 'SMK', 'S', 'JL. SLAMET RIYADI 3', '-6.2116000', '106.8587000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016103  ', 'Kec. Tanjung Priok', 'B43BD925-C660-4769-A572-6E50B7B32512', '20107410', 'SMKS BARUNAWATI', 'SMK', 'S', 'JL. GANGGENG II NO.1 TG. PRIOK', '-6.1150000', '106.8821000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016403  ', 'Kec. Cipayung', 'B45C3007-A368-43B6-BAAB-81553C1336AC', '69775769', 'SMKS INSAN MULIA INFORMATIKA', 'SMK', 'S', 'Jl. Kapuk RT 003 RW 011', '-6.3108000', '106.9105000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016303  ', 'Kec. Cilandak', 'B4D4FBA2-CB7E-468A-A8C3-9316CE697614', '20102647', 'SMKS LEBAK BULUS JAKARTA', 'SMK', 'S', 'JL. PASAR JUMAT', '-6.2901000', '106.7729000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016208  ', 'Kec. Kali Deres', 'B4FB9312-8434-42FD-91EB-60205F0A2EBE', '20101482', 'SMKS SYANGGIT CENDEKIA', 'SMK', 'S', 'JALAN PETA SELATAN NO. 58', '-6.1528000', '106.7053000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016207  ', 'Kec. Cengkareng', 'B58E3DF4-C1ED-4C88-AF54-8F8EC289FA36', '20104462', 'SMKS TELKOM SANDHY PUTRA JAKARTA', 'SMK', 'S', 'JL. DAAN MOGOT KM. 11', '-6.1549000', '106.7511000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016207  ', 'Kec. Cengkareng', 'B5FDC70D-1177-4BE6-BA7C-192282DAC3DC', '20101590', 'SMKS AD DAWAH', 'SMK', 'S', 'JL. MADRASAH TANAH KOJA', '-6.1707000', '106.7256000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', 'B61F19F4-C7C2-4CCA-95E8-621E8D618AA2', '20103556', 'SMKS TERATAI PUTIH JAKARTA', 'SMK', 'S', 'JL. NUSA INDAH 4 A PERUMNAS KLENDER', '-6.2207000', '106.9366000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016305  ', 'Kec. Kebayoran Lama', 'B7443FC5-26F9-4A75-A0BF-9E6D4BEECC10', '20102548', 'SMKS FATAHILLAH', 'SMK', 'S', 'JL. CIPUTAT NO.5 PD.PINANG KEBAYORAN LAMA', '-6.2765000', '106.7737000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016104  ', 'Kec. Koja', 'B8773134-C7F3-431C-A787-163510F200E6', '20107463', 'SMKS STRADA 3 JAKARTA', 'SMK', 'S', 'JL. BHAYANGKARA NO. 38', '-6.1219000', '106.9132000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', 'B880CC2B-0461-40B9-89F7-D8A22BF29C3C', '20103542', 'SMKS PGRI 8 JAKARTA', 'SMK', 'S', 'JL. CIP. MUARA I', '-6.2281000', '106.8955000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016308  ', 'Kec. Pancoran', 'B8D5020E-82BE-453B-98AA-0651FBB77769', '20102615', 'SMKS PGRI 33 JAKARTA', 'SMK', 'S', 'JL. GURU ALIF', '-6.2626000', '106.8372000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016205  ', 'Kec. Tambora', 'B8E1F904-EB7E-40B1-A5D0-6DDBDB9507CD', '20101658', 'SMKS ISLAM BAHAGIA', 'SMK', 'S', 'JL. PERSIMA RAYA NO.15', '-6.1589000', '106.7972000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016301  ', 'Kec. Jagakarsa', 'B9384814-9F0B-4422-A0A6-0DEFB6551890', '20107348', 'SMKS AMALIYAH', 'SMK', 'S', 'AKSES UI NO. 3', '-6.3543000', '106.8348000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016201  ', 'Kec. Kembangan', 'B97F6186-D80E-4CCC-8191-1985CADF1AB3', '20101559', 'SMKS YMIK JAKARTA', 'SMK', 'S', 'JL. RAYA JOGLO (KOMP.DPR.RI.PRIBADI)', '-6.2174000', '106.7335000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016406  ', 'Kec. Jatinegara', 'B99FFAFE-D917-4184-8C3A-8FE148CE039C', '69759304', 'SMKS DEWI SARTIKA', 'SMK', 'S', 'JL KEBON NANAS UTARA', '-6.2399000', '106.8772000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016406  ', 'Kec. Jatinegara', 'B9CE77D8-B58F-46DC-9F84-3293EFC398A1', '20103715', 'SMKS CAHAYA SAKTI JAKARTA', 'SMK', 'S', 'JL. OTISTA 1 NO. 11', '-6.2255000', '106.8688000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016205  ', 'Kec. Tambora', 'BA2F9383-1A40-4E67-BFEB-95D55B2D0B96', '20101687', 'SMKS BINA KARYA JAKARTA', 'SMK', 'S', 'JL. K. H. MOCH MANSYUR NO. 19-21-25', '-6.1540000', '106.8070000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016403  ', 'Kec. Cipayung', 'BA6ECA7E-68C8-4172-8813-8174ECCEE229', '20103675', 'SMKS JAKARTA RAYA 1', 'SMK', 'S', 'JL. RAYA PONDOK GEDE 36', '-6.2883000', '106.8993000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016308  ', 'Kec. Pancoran', 'BA83D5A5-3D54-4688-911D-54CAE8097E4A', '20109265', 'SMKS CYBER MEDIA', 'SMK', 'S', 'JL. DUREN TIGA NO. 12', '-6.2531000', '106.8396000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', 'BA8850F0-D792-4834-9908-A08198C9DDEB', '20177921', 'SMKS LABORATORIUM JAKARTA', 'SMK', 'S', 'JL. RAWAJAYA NO. 37', '-6.2350000', '106.9440000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016303  ', 'Kec. Cilandak', 'BABDEBEF-5FB7-4308-9DDF-BCDE7EB8CDFE', '20177826', 'SMKS PURNAMA MANDIRI', 'SMK', 'S', 'JL. MARGASATWA GG. H . BEDEN NO. 105 RT. 010/02 PONDOK LABU', '-6.3105000', '106.8036000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016304  ', 'Kec. Pesanggrahan', 'BABE931D-ED3B-4545-83DB-081CB1B16FC9', '20102630', 'SMK TRI MULIA JAKARTA', 'SMK', 'S', 'JL. H. ADAM MALIK NO. 1', '-6.2381000', '106.7478000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016006  ', 'Kec. Kemayoran', 'BAF74FB9-126B-45D5-B63B-3C8C83ABE5F7', '20100307', 'SMKS PONCOL', 'SMK', 'S', 'JL. MUTIARA RAYA', '-6.1643000', '106.8694000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016303  ', 'Kec. Cilandak', 'BB338B74-65F5-45B6-AADC-B468AAD66F2F', '20102587', 'SMKN 20 JAKARTA', 'SMK', 'N', 'Jl. Melati No. 24', '-6.2841000', '106.7938000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016008  ', 'Kec. Gambir', 'BB6117B5-D960-438A-A8F3-B2BDCD4116C9', '20100144', 'SMKS PARAMITHA 2 JAKARTA', 'SMK', 'S', 'JL. SURYO PRANOTO NO. 20', '-6.2073000', '106.8596000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016203  ', 'Kec. Palmerah', 'BB842223-330F-4C96-BD3A-E95AC5F167D3', '20101679', 'SMKS IBU PERTIWI 1 JAKARTA', 'SMK', 'S', 'JL. LET. JEND. S. PARMAN NO. 69', '-6.1943000', '106.7975000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016301  ', 'Kec. Jagakarsa', 'BBCF1345-8B12-4ABD-9B28-D8DB568D7E43', '20107351', 'SMKS KAHURIPAN 2 JAKARTA', 'SMK', 'S', 'JL. NANGKA NO. 17', '-6.3034000', '106.8468000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016101  ', 'Kec. Penjaringan', 'BC278332-3F45-4058-B1C6-0EA4087C91E2', '20107429', 'SMKS KRISTEN NAZARETH', 'SMK', 'S', 'Jl. Jembatan Dua Blok H/29 Kec. Penjaringan', '-6.1372000', '106.7815000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016208  ', 'Kec. Kali Deres', 'BC3D14D4-74A6-45BE-9B94-C86B1954A7A3', '20109415', 'SMKS AL FIRDAUS', 'SMK', 'S', 'JL. PETA UTARA', '-6.1399000', '106.7023000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016403  ', 'Kec. Cipayung', 'BC44EF80-A55D-4CF7-BD0B-C766D1522556', '20103781', 'SMKN 51 JAKARTA', 'SMK', 'N', 'JL. SMEA 33 - SMIK BAMBU APUS', '-6.3202000', '106.8985000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016402  ', 'Kec. Ciracas', 'BD28DF32-7373-4FF0-ADA6-645201B4690B', '20103656', 'SMKS ERA PEMBANGUNAN UMAT JAKARTA', 'SMK', 'S', 'JL. USMAN NO. 71', '-6.3324000', '106.8836000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016106  ', 'Kec. Cilincing', 'BD34151A-0C86-4C8A-A53C-D3F9882C6848', '20107412', 'SMKS CILINCING 1 JAKARTA', 'SMK', 'S', 'JL.BARU GANG II CILINCING', '-6.1054000', '106.9283000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016106  ', 'Kec. Cilincing', 'BD70104C-6CDF-42A4-A3B2-DFE301BB55C3', '20107435', 'SMKN 36 JAKARTA', 'SMK', 'N', 'JL. BARU KOSAMBI III', '-6.0999000', '106.9313000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016201  ', 'Kec. Kembangan', 'BF03FE1C-2AF0-4C5A-A0E3-C2D738DC1F61', '20101602', 'SMKS AL IHSAN JAKARTA', 'SMK', 'S', 'JL. RAYA MASJID AL IHSAN', '-6.1979000', '106.7298000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016303  ', 'Kec. Cilandak', 'C077597D-9F96-42EC-B7B2-12B0C00FB17E', '20102529', 'SMKS GRAFIKA YAYASAN LEKTUR', 'SMK', 'S', 'JL. PASAR JUMAT', '-6.2898000', '106.7728000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016105  ', 'Kec. Kelapa Gading', 'C0A3BAE0-016F-412F-9F91-AB8FAB6EAE30', '20107443', 'SMKS PELAYARAN JAKARTA RAYA', 'SMK', 'S', 'JL. PERINTIS KEMERDEKAAN KOMPLEK TNI-AL', '-6.1686000', '106.8858000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016302  ', 'Kec. Pasar Minggu', 'C0A6BB5D-FD4C-46CA-8DAE-8F7DE4269148', '20102601', 'SMKN 37 JAKARTA', 'SMK', 'N', 'JL. PERTANIAN 3', '-6.2879000', '106.8428000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016301  ', 'Kec. Jagakarsa', 'C10F3BCA-BFB2-4A53-8DB2-022F8E4C1C27', '20102460', 'SMKS YAPERJASA JAKARTA', 'SMK', 'S', 'JL. Belimbing Jagakarsa, RT. 008/007', '-6.3328000', '106.8236000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016410  ', 'Kec. Matraman', 'C1240388-ED48-4E90-9E37-1539121D601E', '20103783', 'SMKN 5 JAKARTA', 'SMK', 'N', 'JL. PISANGAN BARU TIMUR 7', '-6.2128000', '106.8727000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016001  ', 'Kec. Tanah Abang', 'C1CA98B9-B961-40C6-B8B5-6D7161FFC1E0', '20100160', 'SMKN 19 JAKARTA', 'SMK', 'N', 'JL. DANAU LIMBOTO NO. 11', '-6.2084000', '106.8068000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016102  ', 'Kec. Pademangan', 'C20143D9-68AD-4123-A89A-1EDC78AC40F2', '20107428', 'SMKS K HARAPAN MULIA', 'SMK', 'S', 'JL.PADEMANGAN V BLOK D PADEMANGAN', '-6.1326000', '106.8326000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016301  ', 'Kec. Jagakarsa', 'C2F33E66-513F-49E5-A0F2-A3029E61EC31', '20109459', 'SMKS KESEHATAN MULIA KARYA HUSADA', 'SMK', 'S', 'M. KAHFI II GG MESJID AN-NUR', '-6.3467000', '106.8157000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016402  ', 'Kec. Ciracas', 'C30E6492-42E3-4BC3-B076-A3434C03B709', '20103246', 'SMKS AL WAHYU JAKARTA', 'SMK', 'S', 'JL. MADRASAH NO. 24', '-6.3682000', '106.8871000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016005  ', 'Kec. Cempaka Putih', 'C31FD93D-4F0B-42DC-AE30-5A7CD2A39D72', '20100114', 'SMKS KSATRYA', 'SMK', 'S', 'JL. PERCETAKAN NEGARA NO. D 232', '-6.1866000', '106.8671000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016001  ', 'Kec. Tanah Abang', 'C35CB384-7A9B-47D9-B58E-D341ED6EF81C', '20108509', 'SMKS MUHAMMADIYAH 5 JAKARTA', 'SMK', 'S', 'JL. TAMAN BENDUNGAN JATILUHUR 18', '-6.2155000', '106.8113000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016401  ', 'Kec. Pasar Rebo', 'C388D2F1-BDFB-4653-979E-79E03586C6ED', '20103658', 'SMKS ISLAM MALAHAYATI', 'SMK', 'S', 'JL. BIMA NO. 3', '-6.3249000', '106.8593000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016103  ', 'Kec. Tanjung Priok', 'C39D31EB-6489-4DD8-A045-32B95C342627', '20107404', 'SMKS AL JIHAD JAKARTA', 'SMK', 'S', 'JL. PAPANGGO 1 NO. 1', '-6.1301000', '106.8819000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016201  ', 'Kec. Kembangan', 'C3CBDEE9-C8F9-41ED-8103-99403926707C', '20101510', 'SMKS PGRI 2 JAKARTA', 'SMK', 'S', 'JL. KEMBANGAN UTARA NO.1', '-6.1615000', '106.7482000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016403  ', 'Kec. Cipayung', 'C4FF263C-4DC9-4D38-8659-9ABBCB80A46C', '20103682', 'SMKS BUDI MURNI 5 JAKARTA', 'SMK', 'S', 'JL. BUDI MURNI 10', '-6.3307000', '106.8953000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016401  ', 'Kec. Pasar Rebo', 'C52C6A17-AB48-418C-A273-22C4BB1943FF', '20103677', 'SMKS ISLAM YASMIN JAKARTA', 'SMK', 'S', 'JL. RAYA BOGOR KM.27', '-6.3405000', '106.8696000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016208  ', 'Kec. Kali Deres', 'C57EED50-5D6C-489F-9BF5-34FDA9190B50', '20101634', 'SMKS KARTIKA X 1', 'SMK', 'S', 'JL. DAAN MOGOT KM 17', '-6.1563000', '106.6979000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016208  ', 'Kec. Kali Deres', 'C60F23D2-DB8D-4E64-B4DE-F9BB53B9871F', '69775233', 'SMKS PUTRA NUSANTARA', 'SMK', 'S', 'JL.BAMBU LARANGAN RT.008 / 009 KALIDERES', '-6.1483000', '106.7028000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016304  ', 'Kec. Pesanggrahan', 'C63285EA-8C09-41BC-8CEF-82799E8C71CD', '20107353', 'SMKS KARTIKA X 2', 'SMK', 'S', 'JL. ANGGREK NO. 1', '-6.2524000', '106.7558000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016106  ', 'Kec. Cilincing', 'C63E0DFF-92D3-4657-902A-EC9979CC730E', '20107464', 'SMKS SYAHID', 'SMK', 'S', 'JL. BAKTI NO.27 KEL.CILINCING', '-6.1137000', '106.9106000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016403  ', 'Kec. Cipayung', 'C68867DF-DF64-44EF-967A-4AD4D343E6C1', '20103698', 'SMKS CAGAR BUDAYA 1 JAKARTA', 'SMK', 'S', 'JL. GEMPOL NO.14', '-6.3081000', '106.8949000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016404  ', 'Kec. Makasar', 'C6EBF1DF-508B-4A2D-B74C-C9264F6AFAE0', '20103274', 'SMKS ANGKASA 1 JAKARTA', 'SMK', 'S', 'JL. RAJAWALI RAYA HALIM PERDANAKUSUMA', '-6.2556000', '106.8918000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016301  ', 'Kec. Jagakarsa', 'C7167F73-CD62-4FC0-A3B9-F8A2DBB2A83B', '20102551', 'SMKS GRAFIKA DESA PUTERA JAKARTA', 'SMK', 'S', 'JL. DESA PUTERA NO. 1', '-6.3373000', '106.8286000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016305  ', 'Kec. Kebayoran Lama', 'C7A64B1B-DEE5-4C22-8078-37B227ACB337', '20102643', 'SMKS MAKARYA 2 JAKARTA', 'SMK', 'S', 'Jl. Harun No. 13A Tanah Kusir', '-6.2543000', '106.7790000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016008  ', 'Kec. Gambir', 'C7BD7A52-DB67-42ED-A868-47EEF2E628D6', '20100167', 'SMKN 38 JAKARTA', 'SMK', 'N', 'JL. KEBON SIRIH NO. 98', '-6.2036981', '106.8179896', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016206  ', 'Kec. Taman Sari', 'C7EBE574-E143-46D2-B213-C615879BC1B7', '20101470', 'SMKS SENTOSA', 'SMK', 'S', 'JL. MADU NO. 276', '-6.1440000', '106.8203000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016202  ', 'Kec. Kebon Jeruk', 'C7F720FA-D76E-4B9F-99D4-19042C156DB8', '20101520', 'SMK PELAYARAN TARUNA BAHARI', 'SMK', 'S', 'JL. PURI KEMBANGAN 2', '-6.1768000', '106.7469000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', 'C8DAEF49-72DD-4E69-BF5A-E9D7348C92E5', '20103242', 'SMKS BPS&K 2 JAKARTA', 'SMK', 'S', 'JL. BINA KARYA NO. 2', '-6.2332000', '106.9386000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016103  ', 'Kec. Tanjung Priok', 'C92E7679-06C7-421C-83D9-18320D902236', '20107462', 'SMKS SANTO LUKAS', 'SMK', 'S', 'JL. ANCOL SELATAN II NO. 1', '-6.1311000', '106.8778000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016103  ', 'Kec. Tanjung Priok', 'C95761B0-62DB-45F9-B606-AB3A99AAAC16', '20107470', 'SMKS WIYATAMANDALA BAKTI JAKARTA', 'SMK', 'S', 'JL. SWASEMBADA TIMUR XIII/46-50', '-6.1252000', '106.8842000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016102  ', 'Kec. Pademangan', 'C96B23D6-0F02-472E-87FE-52C84CFB681B', '20107438', 'SMKN 55 JAKARTA', 'SMK', 'N', 'JL. PADEMANGAN TIMUR VII', '-6.1315000', '106.8444000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016301  ', 'Kec. Jagakarsa', 'C9D34BC6-0BD7-442C-8D48-4632C8AEB288', '20102651', 'SMKS KHARISMAWITA 1 JAKARTA', 'SMK', 'S', 'JL. SWADAYA 2', '-6.2997000', '106.8459000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016201  ', 'Kec. Kembangan', 'CA0ACC9B-D1DC-4061-83EE-1D288D0EB34D', '20101582', 'SMKS WIDYA PATRIA 1', 'SMK', 'S', 'SWADARMA RAYA NO.15', '-6.1677000', '106.7753000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016409  ', 'Kec. Pulo Gadung', 'CA65B9A5-DC54-44A9-9F61-F6578246CAA7', '20107209', 'SMKS GARUDA JAKARTA', 'SMK', 'S', 'JALAN RAYA BEKASI TIMUR KM. 21', '-6.2117000', '106.9010000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016409  ', 'Kec. Pulo Gadung', 'CB2983AB-C495-4458-BED6-BD6F97DD3D1C', '20103765', 'SMKS L`PINA JAKARTA', 'SMK', 'S', 'JL. RAYA BEKASI TIMUR KM. 14.8 NO. 5', '-6.2127000', '106.8989000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016406  ', 'Kec. Jatinegara', 'CB3750ED-8A26-4751-A348-7CA1B05D50B0', '20107266', 'SMKS BHAKTI PERTIWI JAKARTA', 'SMK', 'S', 'JL. BEKASI TIMUR VI', '-6.2187000', '106.8767000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016207  ', 'Kec. Cengkareng', 'CB9FBF7C-9D0A-4FCA-9B49-DA4AE8DD79DC', '20101673', 'SMKS DIAN JAKARTA', 'SMK', 'S', 'JL. PEDONGKELAN BELAKANG NO. 24', '-6.1415000', '106.7468000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016301  ', 'Kec. Jagakarsa', 'CBC9039A-4CFB-4A05-A00D-4B60E3F680FE', '20102628', 'SMKS ISLAM YPIK JAKARTA', 'SMK', 'S', 'Jl. Srengseng Sawah No. 87 RT. 001/07', '-6.3468000', '106.8207000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016202  ', 'Kec. Kebon Jeruk', 'CC1F75DF-19E5-44AD-89EF-98D0E585C0C1', '20109907', 'SMKS MULTIMEDIA MANDIRI', 'SMK', 'S', 'JALAN RAYA KEDOYA NO. 2', '-6.1939000', '106.7599000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', 'CCB342F0-4EFC-497F-B8A4-3BC93AE936EA', '69772516', 'SMKS PGRI 39 JAKARTA', 'SMK', 'S', 'JL I GUSTI NGURAH RAI', '-6.2032000', '106.8656000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016104  ', 'Kec. Koja', 'CCD58F80-D63C-4286-8FEC-5B5A7E31C41A', '20107430', 'SMKS LAGOA', 'SMK', 'S', 'JL. LAGOA 3 NO 135', '-6.1178000', '106.9135000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016006  ', 'Kec. Kemayoran', 'CD69BE4B-B872-4246-B83D-D542ABB79196', '20100156', 'SMKN 54 JAKARTA', 'SMK', 'N', 'JL. BENDUNGAN JAGO NO. 53', '-6.1555000', '106.8608000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016401  ', 'Kec. Pasar Rebo', 'CDB3F875-F822-4AA4-9317-BA177544F28A', '20103726', 'SMKS PANGUDI RAHAYU II JAKARTA', 'SMK', 'S', 'JL. RAYA BOGOR KM. 24,5', '-6.3198000', '106.8642000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016310  ', 'Kec. Setia Budi', 'CE719C87-B6B9-4B0D-B790-F965CDA541FF', '20102559', 'SMKS BUDI ASIH', 'SMK', 'S', 'JL. BUKIT TINGGI 3,S. BUDI', '-6.2103000', '106.8457000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016208  ', 'Kec. Kali Deres', 'CE8CB9B7-EAE4-4AD5-A581-FA9032291E38', '20177897', 'SMKS INSAN CITA', 'SMK', 'S', 'JL. RAYA DAAN MOGOT KM.18 WARUNG GANTUNG NO. 28 KP. KOJAN RT. 005/06', '-6.1533000', '106.7082000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016405  ', 'Kec. Kramat Jati', 'CEA3C6B7-8DE4-437F-B842-1E8E9FA498CC', '20112296', 'SMKS TEKNIK 10 NOPEMBER', 'SMK', 'S', 'JL. RAYA BOGOR NO.1', '-6.2654000', '106.8663000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016203  ', 'Kec. Palmerah', 'CF719AC2-B599-4543-890B-5FCACAA61D5F', '20101503', 'SMKN 13 JAKARTA', 'SMK', 'N', 'JL. RAWA BELONG II-EPALMERAH', '-6.2033000', '106.7856000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016402  ', 'Kec. Ciracas', 'CF83FEFF-8163-4D42-8208-40CA2C72DFFC', '20103764', 'SMKS MAHADHIKA 1 JAKARTA', 'SMK', 'S', 'JL. RAYA CENTEX NO. 29-31', '-6.3268000', '106.8771000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016302  ', 'Kec. Pasar Minggu', 'CFDF51A3-6DFE-4A10-9874-582C6210D751', '20107359', 'SMKS PATRIA WISATA JAKARTA', 'SMK', 'S', 'JL. PEJATEN BARAT NO. 22-24', '-6.2771000', '106.8247000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', 'D062FB91-C4FE-4117-A629-97058CBBD4C2', '20103784', 'SMKN 48 JAKARTA', 'SMK', 'N', 'JL. RADIN INTEN 2 NO. 3', '-6.2201000', '106.9237000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016104  ', 'Kec. Koja', 'D1754883-2157-483C-AB7A-F09BD832F62E', '20107408', 'SMKS AR RAUDHAH JAKARTA', 'SMK', 'S', 'JL. CEMARA BLOK I/79KEL. LAGOA', '-6.1131000', '106.9126000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016003  ', 'Kec. Senen', 'D20DF0D2-5A76-4FC1-B7A5-64D6FDABB1EB', '20107254', 'SMKS FARMASI DITKESAD', 'SMK', 'S', 'JL. ABDUL RACHAM SALEH, NO.18', '-6.1772000', '106.8377000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016206  ', 'Kec. Taman Sari', 'D2707FCC-4E8B-4B14-8CF8-32F0D0E87368', '20101515', 'SMKS PERMATA BUNDA 2 JAKARTA', 'SMK', 'S', 'JL. TAMAN SARI RAYA NO. 75 C-D', '-6.1501000', '106.8227000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016103  ', 'Kec. Tanjung Priok', 'D2EFE5EB-4BFE-4A78-929D-918B245AF8FD', '69761954', 'SMKS KESEHATAN GLOBAL CENDEKIA', 'SMK', 'S', 'JL GADANG NO.83-85', '-6.1129000', '106.8862000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016410  ', 'Kec. Matraman', 'D2F46159-D8AC-43DE-9807-FF711A90DE57', '20103753', 'SMKS PEMBANGUNAN', 'SMK', 'S', 'JL. PENGAYOMAN 3 NO. 95', '-6.1936000', '106.8711000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016402  ', 'Kec. Ciracas', 'D36C0AA2-CE55-4A43-AAD1-956E965CFE17', '20103540', 'SMKS PKP 2 JAKARTA', 'SMK', 'S', 'JL. RAYA PKP KELAPA DUA WETAN', '-6.3390000', '106.8834000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016202  ', 'Kec. Kebon Jeruk', 'D3812603-53E4-410E-877E-8219818025EF', '20101499', 'SMKN 45 JAKARTA', 'SMK', 'N', 'JL. KPBD SUKABUMI SELATAN', '-6.2240000', '106.7747000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016305  ', 'Kec. Kebayoran Lama', 'D4359FE1-BC15-4642-B2D3-5FAB71D9AF60', '20109247', 'SMKS HARNASTO INSTITUT JAKARTA', 'SMK', 'S', 'JL. CIDODOL RAYA NO.40', '-6.2284000', '106.7737000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016205  ', 'Kec. Tambora', 'D497BF60-FC97-4813-848A-69C469460D1F', '20104453', 'SMKS CANDRA NAYA', 'SMK', 'S', 'Jl. Jembatan Besi II No. 26', '-6.1535000', '106.7986000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);
INSERT INTO `tb_sekolah` (`id_provinsi`, `provinsi`, `id_kota`, `kota`, `id_kecamatan`, `kecamatan`, `id_sekolah`, `npsn`, `nama_sekolah`, `bentuk`, `status`, `alamat`, `lintang`, `bujur`, `created_at`, `updated_at`, `deleted_at`) VALUES
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016301  ', 'Kec. Jagakarsa', 'D4D84D6B-1DDD-4BA4-BD5D-02094CE5F98E', '20109246', 'SMKN 62 JAKARTA', 'SMK', 'N', 'CAMAT GABUN II LENTENG AGUNG', '-6.3372000', '106.8393000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016208  ', 'Kec. Kali Deres', 'D58D6D0A-AFD2-4D42-8C48-A08C0894A267', '20101493', 'SMKS TAMAN SAKTI JAKARTA', 'SMK', 'S', 'JL. KAMAL RAYA NO. 2', '-6.1279000', '106.7196000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016203  ', 'Kec. Palmerah', 'D5C6DB77-8D37-4A6C-BFE8-572E5AB54BEC', '69774558', 'SMKS 1 BARUNAWATI', 'SMK', 'S', 'JL. AIPDA KS TUBUN II/III NO 7', '-6.1975000', '106.8022000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016106  ', 'Kec. Cilincing', 'D606FFBF-9012-49B0-B89E-1983DD2C0C9F', '20107436', 'SMKN 4 JAKARTA', 'SMK', 'N', 'JL. ROROTAN VI NO. 1, CILINCING, JAKARTA UTARA', '-6.1405000', '106.9535000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', 'D63283CF-EC25-4677-886C-7035588C1A24', '20103534', 'SMKS RISTEK JAYA JAKARTA', 'SMK', 'S', 'JL. RAYA BEKASI', '-6.1855000', '106.9620000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016309  ', 'Kec. Tebet', 'D6720F48-A78C-47D8-BE11-E87C6F961928', '20102590', 'SMKS 17 AGUSTUS 1945 2 JAKARTA', 'SMK', 'S', 'JL. TEBET DALAM III-A', '-6.2285000', '106.8496000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016103  ', 'Kec. Tanjung Priok', 'D6C7A32E-CA30-4D00-9B98-7AE11D68074D', '20107461', 'SMKS SISMADI SMIP JAKARTA', 'SMK', 'S', 'JL. WARAKAS RAYA NO.17', '-6.1239000', '106.8749000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016305  ', 'Kec. Kebayoran Lama', 'D708815B-9E98-49AC-ACA9-01506EE4DF95', '20107358', 'SMKN 59 JAKARTA', 'SMK', 'N', 'JL. PENINGGARAN BARAT I KEBAYORAN LAMA UTARA', '-6.2462000', '106.7696000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', 'D7264872-74BC-46BB-9FE7-297DDBFB22F2', '20103645', 'SMKS IMTAQ DARURRAHIM JAKARTA', 'SMK', 'S', 'JL. TIPAR CAKUNG GANG PELAJAR', '-6.1737000', '106.9366000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016310  ', 'Kec. Setia Budi', 'D7679367-BDEB-4879-A189-EC390ECA803A', '20109252', 'SMKS MUHAMMADIYAH 15', 'SMK', 'S', 'JL. KARET BELAKANG RAYA NO 4', '-6.2156000', '106.8261000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016410  ', 'Kec. Matraman', 'D797F44D-DC0D-4BAA-8B98-F7ED281A6EB6', '20107597', 'SMKS PANCASILA JAKARTA', 'SMK', 'S', 'JL. KAYU MANIS BARAT NO. 99 MATRAMAN', '-6.2958000', '106.8947000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016101  ', 'Kec. Penjaringan', 'D7AB830F-8AEA-45AF-92E7-DD5DA1E05F37', '20107442', 'SMKS PANGERAN WIJAYAKUSUMA', 'SMK', 'S', 'JL. BANDENGAN UTARA NO. 80', '-6.1360000', '106.8017000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016101  ', 'Kec. Penjaringan', 'D825289A-EF79-4813-A3D7-872DD6F08448', '20107449', 'SMK PERMATA INDAH', 'SMK', 'S', 'JL. KAMP GUSTI BLOK A/1Taman Permata Indah II No.1', '-6.1391000', '106.7781000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016306  ', 'Kec. Kebayoran Baru', 'D8C5A61E-3533-4149-9B94-25C985584A64', '20102550', 'SMKS DHARMA KARYA', 'SMK', 'S', 'JL.MELAWAI XII/207 KEBAYORAN BARU', '-6.2472000', '106.7999000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016204  ', 'Kec. Grogol Petamburan', 'D98CF170-AC86-43EA-A069-FB723BD1E2B0', '20101492', 'SMKS TANJUNG JAKARTA', 'SMK', 'S', 'JL. DR. NURDIN 4 NO 1', '-6.1594000', '106.7952000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016405  ', 'Kec. Kramat Jati', 'D9BFC45C-228E-4767-B11B-76CB4CCC6215', '20103536', 'SMKS RESPATI 1', 'SMK', 'S', 'Jalan Inpres Tengah', '-6.2874000', '106.8662000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016301  ', 'Kec. Jagakarsa', 'DA316E56-DA24-4439-8DA5-87D05E5FB5EB', '20102617', 'SMKS PGRI 23 JAKARTA', 'SMK', 'S', 'JL. SRENGSENG SAWAH', '-6.3524000', '106.8179000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016404  ', 'Kec. Makasar', 'DB0EEE2B-A396-4B57-A0D4-5157034A8BB1', '20103270', 'SMKS BHAKTI 1 JAKARTA', 'SMK', 'S', 'JL. PERINDUSTRIAN NO. 7', '-6.2506000', '106.8756000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016305  ', 'Kec. Kebayoran Lama', 'DB70F7DA-10C1-4099-87A6-D3615662687E', '20107352', 'SMK JAKARTA WISATA', 'SMK', 'S', 'JL. PRAJA DALAM E NO.3 (SULTAN ISKANDAR MUDA)', '-6.2530000', '106.7821000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', 'DBFA4936-3CBD-426A-A95E-B9E84FADB10E', '20103671', 'SMKS JAKARTA TIMUR 2', 'SMK', 'S', 'JL. CEMPAKA VI', '-6.1742000', '106.9507000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016104  ', 'Kec. Koja', 'DC9441E6-2BA0-4276-AEF7-EB787A9C9345', '20107407', 'SMKS AL KHAIRIYAH 2 JAKARTA', 'SMK', 'S', 'JL. MINDI NO. 2', '-6.1156000', '106.9104000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016207  ', 'Kec. Cengkareng', 'DD1BD937-3B47-478B-8CA2-C7D5F293C4D7', '20101656', 'SMKS KESATUAN', 'SMK', 'S', 'JL. KESATUAN NO. 1 KLINGKIT RAWA BUAYA', '-6.1613000', '106.7451000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016305  ', 'Kec. Kebayoran Lama', 'DDDB700C-DF97-4323-B189-ACC42FAC8017', '69787516', 'SMK KESEHATAN SEKAWAN', 'SMK', 'S', 'JL. KEMANDORAN I NO 20', '-6.2160000', '106.7872000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016401  ', 'Kec. Pasar Rebo', 'DE1DD8BD-418F-4F71-A087-63C3F766855C', '20109490', 'SMKS KARYA DHARMA 2 JAKARTA', 'SMK', 'S', 'JL. RAYA BOGOR KM.28', '-6.3229000', '106.8638000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016105  ', 'Kec. Kelapa Gading', 'DF25D043-C108-43D1-8595-F3E59F906E58', '20107434', 'SMKN 33 JAKARTA', 'SMK', 'N', 'JL. GADING MAS TIMUR 2 KELAPA GADING JAKARTA UTARA', '-6.1756000', '106.9107000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016410  ', 'Kec. Matraman', 'DF3A86BE-81B7-4B8A-84C2-477523A1DCFA', '20103786', 'SMKN 40 JAKARTA', 'SMK', 'N', 'JL. NANAS 2', '-6.1985000', '106.8693000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016101  ', 'Kec. Penjaringan', 'DF8A6DF0-274F-4994-AE80-9D5392CA57E2', '69761967', 'SMKS AL MUTTAQIN', 'SMK', 'S', 'JL. KAPUK MUARA RT 10/ RW 4', '-6.1088000', '106.7275000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016402  ', 'Kec. Ciracas', 'DFF15ACA-43A5-4438-858D-21FF7C044042', '20103531', 'SMKS SAHID JAKARTA', 'SMK', 'S', 'JL. HAJI BAPING KAV. 42-43', '-6.3150000', '106.8737000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016304  ', 'Kec. Pesanggrahan', 'E0DA36E4-D3D5-4490-A062-8009A6C977F8', '20178280', 'SMK TUNAS GRAFIKA INFORMATIKA', 'SMK', 'S', 'JL. H. ILYAS NO. 6 PETUKANGAN UTARA', '-6.2308000', '106.7517000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016403  ', 'Kec. Cipayung', 'E0FA5095-1B06-406E-A60D-B52E80A1174C', '69762614', 'SMKS AR-RAISIYAH HUSADA', 'SMK', 'S', 'JL. AL-BAIDLO RAYA NO. 29 JAKARTA TIMUR', '-6.2223000', '106.9364000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', 'E19A467D-2EDF-49A8-998E-5B558D7332C6', '20103272', 'SMKS AS SAADAH', 'SMK', 'S', 'JL. SWAKARSA I B NO 40', '-6.2468000', '106.9392000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', 'E1D5879A-2AF3-432C-A52E-8C4D9C70C3E8', '20103642', 'SMKS IPTEK JAKARTA', 'SMK', 'S', 'JL. RAYA PULOGEBANG', '-6.2151000', '106.9551000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016008  ', 'Kec. Gambir', 'E3DA834F-2DB4-45E3-891E-08AAFCBC832E', '20100108', 'SMKS JAKARTA PUSAT 1', 'SMK', 'S', 'JL. ABDUL MUIS NO. 44', '-6.1807000', '106.8168000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016409  ', 'Kec. Pulo Gadung', 'E40A13B7-461D-4148-B87A-14EE4A01A339', '20103770', 'SMKS KEMALA BHAYANGKARI 1 JAKARTA', 'SMK', 'S', 'JL. RAYA BEKASI TIMUR, CIPINANG', '-6.2100000', '106.8943000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016007  ', 'Kec. Sawah Besar', 'E4868A3B-6CE9-40D0-A121-E5249D0E6621', '20100143', 'SMKN 1 JAKARTA', 'SMK', 'N', 'JL. BUDI UTOMO NO.7 SAWAH BESAR', '-6.1669000', '106.8372000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016408  ', 'Kec. Cakung', 'E51FFEF7-0418-4CFB-B79D-F05524CFE768', '69849427', 'SMKS YASPIA 17', 'SMK', 'S', 'Jl. Raya Pulogebang RT. 14/04 Cakung Jakarta Timur', '-6.1987000', '106.9577000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016205  ', 'Kec. Tambora', 'E567D27F-2C75-48F6-A638-91E59C82B82F', '20101517', 'SMKS PERMATA BUNDA I JAKARTA', 'SMK', 'S', 'JL. JAMLANG RAYA NO. 2 LM', '-6.1610000', '106.8048000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016208  ', 'Kec. Kali Deres', 'E5BC1569-F8D2-45BB-9B13-2D28F60E1C40', '20101577', 'SMKS YADIKA 1 JAKARTA', 'SMK', 'S', 'JL. KAMAL RAYA NO. 42', '-6.1230000', '106.7224000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016404  ', 'Kec. Makasar', 'E62A4A34-E43E-49D8-8F1E-53890C56C67D', '20103269', 'SMKS BHAKTI 2 JAKARTA', 'SMK', 'S', 'JL. PERINDUSTRIAN NO. 7', '-6.2503000', '106.8767000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016301  ', 'Kec. Jagakarsa', 'E65FB091-AFC2-4502-87D6-2BF1DF409B2F', '20102539', 'SMKS AL MAKMUR JAKARTA', 'SMK', 'S', 'JL. RM. KAHFI I CIGANJUR', '-6.3457000', '106.8037000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016205  ', 'Kec. Tambora', 'E7BC1C27-77EE-4A86-A290-11E408627545', '20101662', 'SMKS CINDERA MATA INDAH JAKARTA', 'SMK', 'S', 'JL. DURI RAYA', '-6.1587000', '106.8034000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016303  ', 'Kec. Cilandak', 'E7CA8CBA-C046-4428-83B6-A958DAC1D9A5', '20102607', 'SMKS PANCA SAKTI', 'SMK', 'S', 'Jl. Pinang I No..2', '-6.2977000', '106.7923000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016301  ', 'Kec. Jagakarsa', 'E83FD185-472C-421E-BA46-4EDFE8D4D979', '20110107', 'SMKS BAKTI 17', 'SMK', 'S', 'Jl. Persahabatan No. 23, RT. 007/005', '-6.3482000', '106.8103000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016401  ', 'Kec. Pasar Rebo', 'E841F261-F4A8-4321-902C-7C85B5C26D41', '20103660', 'SMKS ISLAM PB SOEDIRMAN 2 JAKARTA', 'SMK', 'S', 'JL. RAYA BOGOR KM. 24', '-6.3158000', '106.8617000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016303  ', 'Kec. Cilandak', 'E85C8735-7E2F-4813-B960-6505A761A8A9', '20102610', 'SMKS PURNAMA 2 JAKARTA', 'SMK', 'S', 'Jalan Margasatwa II Gg. H. Beden', '-6.3053000', '106.8134000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016006  ', 'Kec. Kemayoran', 'E86C62A3-73DF-4092-B64F-11B61FA27347', '20177909', 'SMKS KRISTEN KANAAN', 'SMK', 'S', 'JL. KRAN RAYA NO. 7  JAKARTA', '-6.1595000', '106.8447000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016207  ', 'Kec. Cengkareng', 'E96A5D52-C08A-4113-9E06-7F425307E99E', '20101580', 'SMKS 1 CITRA ADHI PRATAMA', 'SMK', 'S', 'JL. OUTER RING ROAD NO.21', '-6.1297000', '106.7325000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016409  ', 'Kec. Pulo Gadung', 'E99E5582-545F-410E-A7B2-ACE781B955A9', '20103694', 'SMKS BUDHAYA 2 JAKARTA', 'SMK', 'S', 'CIPINANG KEBEMBEM I NO.25', '-6.2097000', '106.8849000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', 'EA15239C-A151-4355-9571-DD1FB37CACF0', '20103772', 'SMKS MITRA KENCANA', 'SMK', 'S', 'JL. MAWAR MERAH NO. 1', '-6.2208000', '106.9385000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016403  ', 'Kec. Cipayung', 'EAA2BF14-26CC-46F7-82A9-8FF38C8448A8', '20103683', 'SMKS BUDI MURNI 4 JAKARTA', 'SMK', 'S', 'JL. BUDI MURNI NO. 10', '-6.3306000', '106.8953000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016410  ', 'Kec. Matraman', 'EAEF2AE0-6E4F-4C2C-AE46-41E623A4B77B', '20103752', 'SMKS PEMUDA JAKARTA', 'SMK', 'S', 'JL. SKIP UJUNG UTAN KAYU SELATAN', '-6.2084000', '106.8718000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016305  ', 'Kec. Kebayoran Lama', 'EB0A76D4-8FD4-4704-BB24-63CDF204DC8E', '20102600', 'SMKN 43 JAKARTA', 'SMK', 'N', 'JL. CIPULIR 1', '-6.2363000', '106.7755000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016103  ', 'Kec. Tanjung Priok', 'EB5E8E70-385C-4187-854F-201D42591A20', '20107420', 'SMKS GUNUNG JATI JAKARTA', 'SMK', 'S', 'JL. SUNGAI BAMBU IV NO. 35', '-6.1287000', '106.8861000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016008  ', 'Kec. Gambir', 'EB799607-9A4D-4FFA-A600-7047CDCF719E', '20100152', 'SMKS PGRI 34 JAKARTA', 'SMK', 'S', 'JL. SANGIHE NO. 26', '-6.1706000', '106.8097000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016405  ', 'Kec. Kramat Jati', 'EB9C9FC0-EBD7-4259-BEC8-9D3790C0FD7F', '20103249', 'SMKS ADI LUHUR 2 JAKARTA', 'SMK', 'S', 'JL. RAYA CONDET NO. 4', '-6.2804000', '106.8638000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016402  ', 'Kec. Ciracas', 'EBD20783-7A03-4AC8-A5FF-E1B64675D550', '69758983', 'SMKS KAWULA JAKARTA', 'SMK', 'S', 'JL BUNGUR IV NO 29', '-6.3140000', '106.8864000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016207  ', 'Kec. Cengkareng', 'EC4A1F60-E854-478D-9F97-17A324B1D89B', '69760735', 'SMKS PERMATA DUNIA', 'SMK', 'S', 'JL PEDONGKELAN BELAKANG NO 46 RT 06/16', '-6.1403000', '106.7264000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016402  ', 'Kec. Ciracas', 'EC812335-6ACF-4FFB-B597-FEF06D8C145B', '20103731', 'SMKS OTOMINDO', 'SMK', 'S', 'JL. RAWA BOLA NO 49', '-6.3496000', '106.8877000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016305  ', 'Kec. Kebayoran Lama', 'EC86FE13-04E3-42F0-AB51-20B12F953761', '20102589', 'SMKN 18 JAKARTA', 'SMK', 'N', 'Jl. Ciputat Raya Komplek Bank Mandiri RT 08/01', '-6.2646000', '106.7727000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016201  ', 'Kec. Kembangan', 'ED37B3A2-8B4F-404E-8481-FDAB2278358A', '20112434', 'SMKS PATMOS', 'SMK', 'S', 'Taman Meruya Ilir Blok E-7 Rt.018 Rw.004 Meruya Utara', '-6.1958000', '106.7539000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016302  ', 'Kec. Pasar Minggu', 'ED54CC5C-F0B8-441D-A4A0-164EB078E00B', '20102652', 'SMKS KEMALA BHAYANGKARI DELOG', 'SMK', 'S', 'JL. C NO. 1 KOMPLEK POLRI RAGUNAN', '-6.2835000', '106.8195000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016309  ', 'Kec. Tebet', 'EEBF1835-CF13-4C94-819B-273D032B5ED4', '20107355', 'SMKS KRISANTI', 'SMK', 'S', 'JL. KAMPUNG MELAYU KECIL V NO. 22', '-6.2234000', '106.8612000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016006  ', 'Kec. Kemayoran', 'EEFCAB67-16DE-4176-B963-FFD5127D9A4C', '20100117', 'SMKS JAKARTA DUA', 'SMK', 'S', 'CEMPAKA BARU TENGAH NO. 1-3', '-6.1716000', '106.8606000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', 'EF136CC3-F12B-4A93-B2C3-4290AE45A289', '69854751', 'SMK SWADAYA GLOBAL SCHOOL', 'SMK', 'S', 'JL. KECUBUNG V', '-6.2345060', '106.9158930', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016310  ', 'Kec. Setia Budi', 'EF618139-2746-42E0-840C-4ACECC1EA4C6', '20107363', 'SMKS YASPEN', 'SMK', 'S', 'JL. MENTENG WADAS TENGAH NO.30', '-6.2107000', '106.8379000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016105  ', 'Kec. Kelapa Gading', 'EFB1B276-7A07-4A11-B0F0-733BEB6BC486', '20107425', 'SMKS KASIH ANANDA', 'SMK', 'S', 'Jl. Pegangsaan Dua Km. 5', '-6.1386000', '106.9144000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016306  ', 'Kec. Kebayoran Baru', 'EFC7E805-4026-4740-BCCE-F2D78F846524', '20102543', 'SMKS AL KAUTSAR', 'SMK', 'S', 'JL. JEMBATAN SELATAN NO. 6', '-6.2528000', '106.7957000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016401  ', 'Kec. Pasar Rebo', 'EFD9A4F2-C331-4677-BD53-3CBBDEBB9794', '20103790', 'SMKN 22 JAKARTA', 'SMK', 'N', 'Jl. Raya Condet', '-6.2950000', '106.8555000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016001  ', 'Kec. Tanah Abang', 'EFDE5322-D1AC-441B-8FD5-9540048439E3', '20100118', 'SMKS SAID NAUM', 'SMK', 'S', 'JL. KH. MAS MANSYUR  NO.25', '-6.1913000', '106.8178000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016002  ', 'Kec. Menteng', 'F07C0412-E34D-48F5-9F03-8E0A9448103D', '20100159', 'SMKN 16 JAKARTA', 'SMK', 'N', 'JL. TAMAN AMIR HAMZAH MENTENG', '-6.2051000', '106.8496000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016402  ', 'Kec. Ciracas', 'F0CB0597-BB08-4504-9C84-B25BA310E91E', '20103735', 'SMKS PELAYARAN PEMBANGUNAN JAKARTA', 'SMK', 'S', 'JL. MANUNGGAL II NO. 67', '-6.3091000', '106.8795000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016201  ', 'Kec. Kembangan', 'F0CBEBDC-EC71-46A6-AF77-C9DED26D9AFF', '20101578', 'SMKS BINA INSAN MANDIRI', 'SMK', 'S', 'JL. RAYA MERUYA HILIR KOMP. PQT', '-6.2068000', '106.7320000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016003  ', 'Kec. Senen', 'F17E2AF0-02AB-4426-AF18-AF34B0F4C979', '20100139', 'SMKS FRANCISKUS 2', 'SMK', 'S', 'JL. KRAMAT RAYA, NO.67', '-6.1879000', '106.8456000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016207  ', 'Kec. Cengkareng', 'F1D42C5B-C02C-4BFD-AB1E-F925B4705A9B', '20101682', 'SMKS HARAPAN JAYA', 'SMK', 'S', 'JL. DAAN MOGOT KM 13', '-6.1538000', '106.7328000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016310  ', 'Kec. Setia Budi', 'F210F6B7-A2F1-4BCD-93A4-84F0705784FC', '20102445', 'SMKS RPI JAKARTA', 'SMK', 'S', 'JL. HR. RASUNA SAID KAV X2-2', '-6.2279000', '106.8348000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', 'F2AD61B2-219A-4785-B483-6DA84CE3C9D1', '20103696', 'SMKS BUDAYA JAKARTA', 'SMK', 'S', 'JL. DERMAGA BARU NO. 48', '-6.2206000', '106.9057000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016407  ', 'Kec. Duren Sawit', 'F32C010D-CFA7-4D0C-B9C0-4E21D9659FBC', '20103668', 'SMKS JAYAKARTA JAKARTA', 'SMK', 'S', 'JL. BUNGA RAMPAI 9/1 NO. 47', '-6.2270000', '106.9435000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016406  ', 'Kec. Jatinegara', 'F37DBA7A-37E4-4584-A5BF-81F0BBD6A35A', '20103502', 'SMKS YP DARUL MUKMININ', 'SMK', 'S', 'JL. MATRAMAN RAYA NO. 177', '-6.2187000', '106.8767000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016203  ', 'Kec. Palmerah', 'F3812488-C0E1-4017-AE4E-4D816085C4F3', '20101490', 'SMKS TELADAN JAKARTA', 'SMK', 'S', 'JL. ANGGREK CENDRAWASIH 11 NO 28', '-6.1986000', '106.7969000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016402  ', 'Kec. Ciracas', 'F38344B9-89C6-4C00-ADBA-A04731E58CC0', '20103267', 'SMKS BINA DHARMA', 'SMK', 'S', 'JL. CIRACAS NO. 39', '-6.3275000', '106.8848000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016405  ', 'Kec. Kramat Jati', 'F4913DCB-E859-44CA-929A-3AC6630D252F', '20103759', 'SMKS MARDHIKA', 'SMK', 'S', 'JL. RAYA CONDET NO.63', '-6.2651000', '106.8630000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016104  ', 'Kec. Koja', 'F4A4158A-7D2E-4839-A62E-F453B48F197A', '20107406', 'SMKS AL KHAIRIYAH 1 JAKARTA', 'SMK', 'S', 'JL. MINDI NO. 2', '-6.1156000', '106.9104000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016305  ', 'Kec. Kebayoran Lama', 'F5AB451D-BEF9-4387-BBE5-A5F1BCB4E2C3', '20102623', 'SMKS MIFTAHUL FALAH JAKARTA', 'SMK', 'S', 'JL. ALMUBAROK 2', '-6.2402000', '106.7699000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016409  ', 'Kec. Pulo Gadung', 'F5BF316E-A78C-4C11-84CD-F88B4EE0E48C', '69950308', 'SMK PRESTASI AGUNG', 'SMK', 'S', 'Jl. Sunan Giri No. 5 A', '-6.1974021', '106.8933924', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016309  ', 'Kec. Tebet', 'F604C827-F7BD-4AC9-899A-840CF17F9E5F', '20102538', 'SMK AS-SYAFI`IYAH JAKARTA', 'SMK', 'S', 'JL BUKIT DURI SELATAN NO. 29', '-6.2206000', '106.8536000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016405  ', 'Kec. Kramat Jati', 'F63B58AF-6F43-4FA2-9725-95D52DDD0B07', '20103692', 'SMKS BUDHI WARMAN 1 JAKARTA', 'SMK', 'S', 'JL. RAYA BOGOR KM. 19', '-6.3089000', '106.8905000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016208  ', 'Kec. Kali Deres', 'F6C6936F-3F2E-46A7-A0A1-6444C2209102', '20104460', 'SMKS SETIA GAMA', 'SMK', 'S', 'JL. RAYA SEMANAN NO. 90', '-6.1718000', '106.6966000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016305  ', 'Kec. Kebayoran Lama', 'F6F73EDB-6416-412C-822E-A9FE6F84514C', '20102605', 'SMKS MUHAMMADIYAH 9', 'SMK', 'S', 'Jl. Panjang', '-6.2348000', '106.7717000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016301  ', 'Kec. Jagakarsa', 'F755D7FF-8EA6-4462-8830-80A37FD8FFCA', '69882414', 'SMK INSAN MANDIRI JAKARTA', 'SMK', 'S', 'JL. MOCH KAHFI II NO.48', '-6.3332030', '106.8209080', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016205  ', 'Kec. Tambora', 'F7611F15-AAF3-4789-AB95-DBC92F3F6C87', '20101660', 'SMKS DHAMMA SAVANA JAKARTA', 'SMK', 'S', 'JL. PADAMULYA VI NO. 176.B', '-6.1437000', '106.7926000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016302  ', 'Kec. Pasar Minggu', 'F89C90C6-CEA6-4BFC-9F6C-14860B9D6F28', '20102544', 'SMKS BOROBUDUR 1 JAKARTA', 'SMK', 'S', 'JL. RAYA CILANDAK KKO', '-6.3074000', '106.8136000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016202  ', 'Kec. Kebon Jeruk', 'F8F84749-5497-4171-BC4D-B8572BD77453', '20101494', 'SMKS PGRI 29 JAKARTA', 'SMK', 'S', 'Komplek Green Ville Barat, Jl. Tanjung Duren Barat I Blok AY No.1', '-6.1919000', '106.8004000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016207  ', 'Kec. Cengkareng', 'F94796F6-9555-456E-83DC-4B990E1845F6', '20101581', 'SMKS CENGKARENG 1', 'SMK', 'S', 'BAMBU LARANGAN RT 05 RW 05 NO.67, KOMPLEK DINAS KEBERSIHAN DKI', '-6.1505000', '106.7172000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016203  ', 'Kec. Palmerah', 'FA657FDA-1A3C-43FE-B7F3-DFB1F54DDC05', '20101601', 'SMKS AL MAFATIH JAKARTA', 'SMK', 'S', 'JL. H. JUNAIDINO.79 RT. 001 RW. 017', '-6.1979000', '106.7912000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016303  ', 'Kec. Cilandak', 'FBA0CEAE-E081-4DA3-845D-FE12D84BFFF1', '20102618', 'SMKS PGRI 14 JAKARTA', 'SMK', 'S', 'JL. INTAN UJUNG NO.19/ 20', '-6.2951000', '106.8043000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016206  ', 'Kec. Taman Sari', 'FBA8FE5F-414F-45AD-92AC-9BC5245780AC', '20101501', 'SMKN 35 JAKARTA', 'SMK', 'N', 'JL. KERAJINAN NO.42', '-6.1541000', '106.8139000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016000  ', 'Kota Jakarta Pusat', '016006  ', 'Kec. Kemayoran', 'FBEFFAE4-7D8B-49E8-8BD2-BAD38A4A0EDD', '20100141', 'SMKS MUHAMMADIYAH 11 JAKARTA', 'SMK', 'S', 'JL. CEMPAKA WANGI II/12', '-6.1679000', '106.8566000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016207  ', 'Kec. Cengkareng', 'FC4DFEF7-2ECB-4E96-91FE-5E95159C8D97', '20101478', 'SMKS PGRI 35 JAKARTA', 'SMK', 'S', 'JL. JATI RAYA I NO. 52', '-6.1457000', '106.7391000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016207  ', 'Kec. Cengkareng', 'FC85ED5D-DC7A-4E23-A720-3B2F64C5B754', '20101668', 'SMKS BUNGA NUSA I', 'SMK', 'S', 'JL. RAWA BENGKEL SDN 19 PAGI', '-6.1414000', '106.7259000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016300  ', 'Kota Jakarta Selatan', '016303  ', 'Kec. Cilandak', 'FCA5D2BE-037A-45F0-BF3B-73702EA637EC', '20102540', 'SMKS AL HIDAYAH LESTARI', 'SMK', 'S', 'JL. KANA LESTARI BLOK K/I', '-6.2940000', '106.7781000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016100  ', 'Kota Jakarta Utara', '016105  ', 'Kec. Kelapa Gading', 'FD74F4E9-9F45-4595-808A-E0EBAC25AFEC', '20112463', 'SMKS 11 MARET', 'SMK', 'S', 'JL. RAYA BEKASI KM. 20', '-6.1812000', '106.9182000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016201  ', 'Kec. Kembangan', 'FEA7DD11-4106-4747-AA78-8240E7CF4359', '20101599', 'SMKS AL WASHILAH 1 JAKARTA', 'SMK', 'S', 'JL. KAMPUNG BARU NO. 20', '-6.1683000', '106.7506000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016400  ', 'Kota Jakarta Timur', '016402  ', 'Kec. Ciracas', 'FF06FF30-1FC0-4E9A-AB88-8FFBE2561F37', '20103738', 'SMKS PGRI 20 JAKARTA', 'SMK', 'S', 'JL. H. ABDURAHMAN NO. 54', '-6.3626000', '106.8806000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('010000  ', 'Prov. D.K.I. Jakarta', '016200  ', 'Kota Jakarta Barat', '016204  ', 'Kec. Grogol Petamburan', 'FF59B8E1-CAD2-4B8F-B95D-7C7F73ED8C3A', '20101561', 'SMKS YADIKA 2 JAKARTA', 'SMK', 'S', 'JL. TG. DUREN BARAT 4 NO. 8', '-6.1780000', '106.7817000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_siswa`
--

CREATE TABLE `tb_siswa` (
  `id_siswa` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED DEFAULT NULL,
  `nisn` varchar(10) NOT NULL,
  `nama_lengkap` varchar(255) NOT NULL,
  `jenis_kelamin` enum('L','P') NOT NULL,
  `tempat_lahir` varchar(25) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `foto_profile` text DEFAULT NULL,
  `bio` text DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_soal`
--

CREATE TABLE `tb_soal` (
  `id_soal` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED DEFAULT NULL,
  `id_kompetensi_dasar` int(10) UNSIGNED DEFAULT NULL,
  `id_kisi_kisi` int(10) UNSIGNED DEFAULT NULL,
  `id_grup` int(10) UNSIGNED DEFAULT NULL,
  `tipe_soal` enum('pilihan_ganda','isian_singkat','benar_salah') DEFAULT NULL,
  `jumlah_jawaban` int(11) DEFAULT NULL,
  `teks_soal` varchar(255) NOT NULL,
  `tipe_file` enum('gambar','audio','video','lainnya') DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `tingkat_kesulitan` enum('c1','c2','c3','c4','c5','c6') DEFAULT NULL,
  `kunci_essay` varchar(255) DEFAULT NULL,
  `jumlah_like` int(10) UNSIGNED DEFAULT NULL,
  `jumlah_dislike` int(10) UNSIGNED DEFAULT NULL,
  `jumlah_komentar` int(10) UNSIGNED DEFAULT NULL,
  `digunakan_sebanyak` int(10) UNSIGNED DEFAULT NULL,
  `status` enum('perbaikan','siap_validasi','valid') NOT NULL,
  `divalidasi_oleh` int(10) UNSIGNED DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_tipe_entitas_pemberitahuan`
--

CREATE TABLE `tb_tipe_entitas_pemberitahuan` (
  `id_tipe_entitas_pemberitahuan` int(10) UNSIGNED NOT NULL,
  `nama_entitas` varchar(255) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_tugas`
--

CREATE TABLE `tb_tugas` (
  `id_tugas` int(10) UNSIGNED NOT NULL,
  `id_grup` int(10) UNSIGNED DEFAULT NULL,
  `id_folder` int(10) UNSIGNED DEFAULT NULL,
  `judul_tugas` varchar(100) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `aktif_sampai` datetime DEFAULT NULL,
  `status` enum('aktif','tidak_aktif') DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` enum('admin','siswa','guru') NOT NULL,
  `email` varchar(255) NOT NULL,
  `kode_verifikasi` varchar(255) NOT NULL,
  `refresh_token` text DEFAULT NULL,
  `verified_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL,
  `login` tinyint(1) DEFAULT 0,
  `last_login` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `username`, `password`, `role`, `email`, `kode_verifikasi`, `refresh_token`, `verified_at`, `created_at`, `updated_at`, `deleted_at`, `login`, `last_login`) VALUES
(1, 'swardi', '$2b$10$qytGh3q1frymHnhIlB/3SeEx9ebEl1dV978j3Dha0hBwFI2hFk2LK', 'admin', 'swardyantara@gmail.com', 'kcok719ifeilg79fry8', NULL, NULL, '2020-07-16 15:56:18', '2020-07-16 15:56:18', NULL, 0, NULL),
(3, 'swardi2', '$2b$10$c6XZNPPGBTTV54Nx/Jl..ev4WNvBRIPnqiacd6..RmNtgxC0NDmeu', 'admin', 'swardiantara@gmail.com', 'kcok864t52rc66jdrc2', NULL, NULL, '2020-07-16 15:57:11', '2020-07-16 15:57:11', NULL, 0, NULL),
(4, 'guru', '$2a$10$JghQVFAnXeXNhElfFlyfUuz6wKyyrTcJZKrF/UA5aAFIwkNn.EYCe', 'guru', 'guru@gmail.com', '$2a$10$JghQVFAnXeXNhElfFlyfUuz6wKyyrTcJZKrF/UA5aAFIwkNn.EYCe', NULL, NULL, '2020-08-17 00:18:23', '2020-08-17 00:18:23', NULL, 0, NULL),
(5, 'siswa', '$2a$10$4d6avNpU2uq/01WR1Na44uR9CMouZBRXI9wi0adfOrQV.P3y8va4i', 'siswa', 'siswa@gmail.com', '$2a$10$4d6avNpU2uq/01WR1Na44uR9CMouZBRXI9wi0adfOrQV.P3y8va4i', NULL, NULL, '2020-08-17 00:18:23', '2020-08-17 00:18:23', NULL, 0, NULL),
(6, 'admin', '$2a$10$AXFeShX8KU/Ied33L4ySDOd5JsoVmP.biSUNqY3x6oeRIFxp1W8yO', 'admin', 'admin@gmail.com', '$2a$10$AXFeShX8KU/Ied33L4ySDOd5JsoVmP.biSUNqY3x6oeRIFxp1W8yO', NULL, NULL, '2020-08-17 00:18:23', '2020-08-17 00:18:23', NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_dislike_bahan_ajar`
--

CREATE TABLE `tb_user_dislike_bahan_ajar` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_bahan_ajar` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_dislike_perangkat_pembelajaran`
--

CREATE TABLE `tb_user_dislike_perangkat_pembelajaran` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_perangkat_pembelajaran` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_dislike_soal`
--

CREATE TABLE `tb_user_dislike_soal` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_soal` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_follow_user`
--

CREATE TABLE `tb_user_follow_user` (
  `id_user_pengikut` int(10) UNSIGNED NOT NULL,
  `id_user_yang_diikuti` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_jawab_soal`
--

CREATE TABLE `tb_user_jawab_soal` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_soal` int(10) UNSIGNED NOT NULL,
  `id_opsi_jawaban` int(10) UNSIGNED DEFAULT NULL,
  `jawaban_essay` varchar(255) DEFAULT NULL,
  `benar` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_komentar_bahan_ajar`
--

CREATE TABLE `tb_user_komentar_bahan_ajar` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_bahan_ajar` int(10) UNSIGNED NOT NULL,
  `komentar` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_komentar_diskusi`
--

CREATE TABLE `tb_user_komentar_diskusi` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_diskusi` int(10) UNSIGNED NOT NULL,
  `komentar` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_komentar_perangkat_pembelajaran`
--

CREATE TABLE `tb_user_komentar_perangkat_pembelajaran` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_perangkat_pembelajaran` int(10) UNSIGNED NOT NULL,
  `komentar` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_komentar_post`
--

CREATE TABLE `tb_user_komentar_post` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_post` int(10) UNSIGNED NOT NULL,
  `komentar` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_komentar_quiz`
--

CREATE TABLE `tb_user_komentar_quiz` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_quiz` int(10) UNSIGNED NOT NULL,
  `komentar` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_komentar_soal`
--

CREATE TABLE `tb_user_komentar_soal` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_soal` int(10) UNSIGNED NOT NULL,
  `komentar` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_komentar_tugas`
--

CREATE TABLE `tb_user_komentar_tugas` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_tugas` int(10) UNSIGNED NOT NULL,
  `komentar` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_like_bahan_ajar`
--

CREATE TABLE `tb_user_like_bahan_ajar` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_bahan_ajar` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_like_perangkat_pembelajaran`
--

CREATE TABLE `tb_user_like_perangkat_pembelajaran` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_perangkat_pembelajaran` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_like_post`
--

CREATE TABLE `tb_user_like_post` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_post` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_like_soal`
--

CREATE TABLE `tb_user_like_soal` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_soal` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_mengerjakan_quiz`
--

CREATE TABLE `tb_user_mengerjakan_quiz` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_quiz` int(10) UNSIGNED NOT NULL,
  `quiz_ke` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `waktu_mulai` datetime DEFAULT NULL,
  `waktu_selesai` datetime DEFAULT NULL,
  `status` enum('sedang','sudah','pause','belum') DEFAULT 'belum',
  `hasil` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_mengerjakan_tugas`
--

CREATE TABLE `tb_user_mengerjakan_tugas` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_tugas` int(10) UNSIGNED NOT NULL,
  `diperiksa_oleh` int(10) UNSIGNED DEFAULT NULL,
  `file` varchar(255) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `feedback` varchar(255) DEFAULT NULL,
  `nilai` int(10) UNSIGNED DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_message_user`
--

CREATE TABLE `tb_user_message_user` (
  `id_user_pengirim` int(10) UNSIGNED NOT NULL,
  `id_user_penerima` int(10) UNSIGNED NOT NULL,
  `pesan` varchar(255) NOT NULL,
  `dibaca` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_unduh_bahan_ajar`
--

CREATE TABLE `tb_user_unduh_bahan_ajar` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_bahan_ajar` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_unduh_paket_soal`
--

CREATE TABLE `tb_user_unduh_paket_soal` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_paket_soal` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_unduh_perangkat_pembelajaran`
--

CREATE TABLE `tb_user_unduh_perangkat_pembelajaran` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_perangkat_pembelajaran` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `knex_migrations`
--
ALTER TABLE `knex_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `knex_migrations_lock`
--
ALTER TABLE `knex_migrations_lock`
  ADD PRIMARY KEY (`index`);

--
-- Indexes for table `ref_bidang_keahlian`
--
ALTER TABLE `ref_bidang_keahlian`
  ADD PRIMARY KEY (`id_bidang_keahlian`),
  ADD KEY `ref_bidang_keahlian_id_sk_dikdasmen_foreign` (`id_sk_dikdasmen`);

--
-- Indexes for table `ref_captcha`
--
ALTER TABLE `ref_captcha`
  ADD PRIMARY KEY (`id_captcha`);

--
-- Indexes for table `ref_kecamatan`
--
ALTER TABLE `ref_kecamatan`
  ADD PRIMARY KEY (`id_kecamatan`),
  ADD KEY `ref_kecamatan_id_kota_foreign` (`id_kota`),
  ADD KEY `ref_kecamatan_id_provinsi_foreign` (`id_provinsi`);

--
-- Indexes for table `ref_kompetensi_dasar`
--
ALTER TABLE `ref_kompetensi_dasar`
  ADD PRIMARY KEY (`id_kompetensi_dasar`),
  ADD KEY `ref_kompetensi_dasar_id_mata_pelajaran_foreign` (`id_mata_pelajaran`),
  ADD KEY `ref_kompetensi_dasar_id_kompetensi_inti_foreign` (`id_kompetensi_inti`);

--
-- Indexes for table `ref_kompetensi_inti`
--
ALTER TABLE `ref_kompetensi_inti`
  ADD PRIMARY KEY (`id_kompetensi_inti`),
  ADD KEY `ref_kompetensi_inti_id_mata_pelajaran_foreign` (`id_mata_pelajaran`);

--
-- Indexes for table `ref_kompetensi_keahlian`
--
ALTER TABLE `ref_kompetensi_keahlian`
  ADD PRIMARY KEY (`id_kompetensi_keahlian`),
  ADD KEY `ref_kompetensi_keahlian_id_bidang_keahlian_foreign` (`id_bidang_keahlian`),
  ADD KEY `ref_kompetensi_keahlian_id_program_keahlian_foreign` (`id_program_keahlian`),
  ADD KEY `ref_kompetensi_keahlian_id_sk_dikdasmen_foreign` (`id_sk_dikdasmen`);

--
-- Indexes for table `ref_kota`
--
ALTER TABLE `ref_kota`
  ADD PRIMARY KEY (`id_kota`),
  ADD KEY `ref_kota_id_provinsi_foreign` (`id_provinsi`);

--
-- Indexes for table `ref_program_keahlian`
--
ALTER TABLE `ref_program_keahlian`
  ADD PRIMARY KEY (`id_program_keahlian`),
  ADD KEY `ref_program_keahlian_id_bidang_keahlian_foreign` (`id_bidang_keahlian`),
  ADD KEY `ref_program_keahlian_id_sk_dikdasmen_foreign` (`id_sk_dikdasmen`);

--
-- Indexes for table `ref_provinsi`
--
ALTER TABLE `ref_provinsi`
  ADD PRIMARY KEY (`id_provinsi`);

--
-- Indexes for table `ref_sk_dikdasmen`
--
ALTER TABLE `ref_sk_dikdasmen`
  ADD PRIMARY KEY (`id_sk_dikdasmen`);

--
-- Indexes for table `tb_aktivitas_siswa`
--
ALTER TABLE `tb_aktivitas_siswa`
  ADD PRIMARY KEY (`id_aktivitas_siswa`),
  ADD KEY `tb_aktivitas_siswa_id_grup_foreign` (`id_grup`),
  ADD KEY `tb_aktivitas_siswa_id_folder_foreign` (`id_folder`);

--
-- Indexes for table `tb_anggota_grup`
--
ALTER TABLE `tb_anggota_grup`
  ADD PRIMARY KEY (`id_grup`,`id_user`),
  ADD KEY `tb_anggota_grup_id_user_foreign` (`id_user`);

--
-- Indexes for table `tb_bahan_ajar`
--
ALTER TABLE `tb_bahan_ajar`
  ADD PRIMARY KEY (`id_bahan_ajar`),
  ADD KEY `tb_bahan_ajar_id_user_foreign` (`id_user`),
  ADD KEY `tb_bahan_ajar_id_grup_foreign` (`id_grup`),
  ADD KEY `tb_bahan_ajar_divalidasi_oleh_foreign` (`divalidasi_oleh`);

--
-- Indexes for table `tb_diskusi`
--
ALTER TABLE `tb_diskusi`
  ADD PRIMARY KEY (`id_diskusi`),
  ADD KEY `tb_diskusi_id_grup_foreign` (`id_grup`),
  ADD KEY `tb_diskusi_id_folder_foreign` (`id_folder`);

--
-- Indexes for table `tb_file`
--
ALTER TABLE `tb_file`
  ADD PRIMARY KEY (`id_file`),
  ADD KEY `tb_file_id_grup_foreign` (`id_grup`),
  ADD KEY `tb_file_id_folder_foreign` (`id_folder`);

--
-- Indexes for table `tb_folder`
--
ALTER TABLE `tb_folder`
  ADD PRIMARY KEY (`id_folder`),
  ADD KEY `tb_folder_id_grup_foreign` (`id_grup`),
  ADD KEY `tb_folder_parent_folder_foreign` (`parent_folder`);

--
-- Indexes for table `tb_grup`
--
ALTER TABLE `tb_grup`
  ADD PRIMARY KEY (`id_grup`),
  ADD UNIQUE KEY `tb_grup_kode_unique` (`kode`),
  ADD KEY `tb_grup_id_mata_pelajaran_foreign` (`id_mata_pelajaran`),
  ADD KEY `tb_grup_dibuat_oleh_foreign` (`dibuat_oleh`);

--
-- Indexes for table `tb_guru`
--
ALTER TABLE `tb_guru`
  ADD PRIMARY KEY (`id_guru`),
  ADD UNIQUE KEY `tb_guru_nuptk_unique` (`nuptk`),
  ADD UNIQUE KEY `tb_guru_nip_unique` (`nip`),
  ADD KEY `tb_guru_id_user_foreign` (`id_user`);

--
-- Indexes for table `tb_guru_mengampu_mata_pelajaran`
--
ALTER TABLE `tb_guru_mengampu_mata_pelajaran`
  ADD PRIMARY KEY (`id_guru`,`id_mata_pelajaran`),
  ADD KEY `tb_guru_mengampu_mata_pelajaran_id_mata_pelajaran_foreign` (`id_mata_pelajaran`);

--
-- Indexes for table `tb_kisi_kisi`
--
ALTER TABLE `tb_kisi_kisi`
  ADD PRIMARY KEY (`id_kisi_kisi`),
  ADD KEY `tb_kisi_kisi_id_kompetensi_dasar_foreign` (`id_kompetensi_dasar`),
  ADD KEY `tb_kisi_kisi_id_user_foreign` (`id_user`);

--
-- Indexes for table `tb_lampiran`
--
ALTER TABLE `tb_lampiran`
  ADD PRIMARY KEY (`id_lampiran`),
  ADD KEY `tb_lampiran_id_tugas_foreign` (`id_tugas`),
  ADD KEY `tb_lampiran_id_diskusi_foreign` (`id_diskusi`);

--
-- Indexes for table `tb_link`
--
ALTER TABLE `tb_link`
  ADD PRIMARY KEY (`id_link`),
  ADD KEY `tb_link_id_grup_foreign` (`id_grup`),
  ADD KEY `tb_link_id_folder_foreign` (`id_folder`);

--
-- Indexes for table `tb_mata_pelajaran`
--
ALTER TABLE `tb_mata_pelajaran`
  ADD PRIMARY KEY (`id_mata_pelajaran`),
  ADD KEY `tb_mata_pelajaran_id_bidang_keahlian_foreign` (`id_bidang_keahlian`),
  ADD KEY `tb_mata_pelajaran_id_program_keahlian_foreign` (`id_program_keahlian`),
  ADD KEY `tb_mata_pelajaran_id_kompetensi_keahlian_foreign` (`id_kompetensi_keahlian`);

--
-- Indexes for table `tb_objek_pemberitahuan`
--
ALTER TABLE `tb_objek_pemberitahuan`
  ADD PRIMARY KEY (`id_objek_pemberitahuan`),
  ADD KEY `tb_objek_pemberitahuan_id_tipe_entitas_pemberitahuan_foreign` (`id_tipe_entitas_pemberitahuan`);

--
-- Indexes for table `tb_opsi_jawaban`
--
ALTER TABLE `tb_opsi_jawaban`
  ADD PRIMARY KEY (`id_opsi_jawaban`),
  ADD KEY `tb_opsi_jawaban_id_soal_foreign` (`id_soal`);

--
-- Indexes for table `tb_paket_soal`
--
ALTER TABLE `tb_paket_soal`
  ADD PRIMARY KEY (`id_paket_soal`),
  ADD KEY `tb_paket_soal_dibuat_oleh_foreign` (`dibuat_oleh`),
  ADD KEY `tb_paket_soal_id_grup_foreign` (`id_grup`),
  ADD KEY `tb_paket_soal_id_mata_pelajaran_foreign` (`id_mata_pelajaran`);

--
-- Indexes for table `tb_paket_soal_berisi_soal`
--
ALTER TABLE `tb_paket_soal_berisi_soal`
  ADD PRIMARY KEY (`id_paket_soal`,`id_soal`),
  ADD KEY `tb_paket_soal_berisi_soal_id_soal_foreign` (`id_soal`);

--
-- Indexes for table `tb_pelaku_pemberitahuan`
--
ALTER TABLE `tb_pelaku_pemberitahuan`
  ADD PRIMARY KEY (`id_pelaku_pemberitahuan`),
  ADD KEY `tb_pelaku_pemberitahuan_id_user_foreign` (`id_user`),
  ADD KEY `tb_pelaku_pemberitahuan_id_objek_pemberitahuan_foreign` (`id_objek_pemberitahuan`);

--
-- Indexes for table `tb_penerima_pemberitahuan`
--
ALTER TABLE `tb_penerima_pemberitahuan`
  ADD PRIMARY KEY (`id_penerima_pemberitahuan`),
  ADD KEY `tb_penerima_pemberitahuan_id_user_foreign` (`id_user`),
  ADD KEY `tb_penerima_pemberitahuan_id_objek_pemberitahuan_foreign` (`id_objek_pemberitahuan`);

--
-- Indexes for table `tb_perangkat_pembelajaran`
--
ALTER TABLE `tb_perangkat_pembelajaran`
  ADD PRIMARY KEY (`id_perangkat_pembelajaran`),
  ADD KEY `tb_perangkat_pembelajaran_id_user_foreign` (`id_user`),
  ADD KEY `tb_perangkat_pembelajaran_id_grup_foreign` (`id_grup`),
  ADD KEY `tb_perangkat_pembelajaran_divalidasi_oleh_foreign` (`divalidasi_oleh`);

--
-- Indexes for table `tb_post`
--
ALTER TABLE `tb_post`
  ADD PRIMARY KEY (`id_post`),
  ADD KEY `tb_post_id_user_foreign` (`id_user`),
  ADD KEY `tb_post_id_grup_foreign` (`id_grup`);

--
-- Indexes for table `tb_quiz`
--
ALTER TABLE `tb_quiz`
  ADD PRIMARY KEY (`id_quiz`),
  ADD KEY `tb_quiz_id_paket_soal_foreign` (`id_paket_soal`),
  ADD KEY `tb_quiz_id_grup_foreign` (`id_grup`);

--
-- Indexes for table `tb_quiz_berisi_soal`
--
ALTER TABLE `tb_quiz_berisi_soal`
  ADD PRIMARY KEY (`id_quiz`,`id_soal`),
  ADD KEY `tb_quiz_berisi_soal_id_soal_foreign` (`id_soal`);

--
-- Indexes for table `tb_riwayat_pendidikan`
--
ALTER TABLE `tb_riwayat_pendidikan`
  ADD PRIMARY KEY (`id_riwayat_pendidikan`),
  ADD KEY `tb_riwayat_pendidikan_id_guru_foreign` (`id_guru`);

--
-- Indexes for table `tb_riwayat_revisi_bahan_ajar`
--
ALTER TABLE `tb_riwayat_revisi_bahan_ajar`
  ADD PRIMARY KEY (`id_riwayat_revisi_bahan_ajar`),
  ADD UNIQUE KEY `tb_riwayat_revisi_bahan_ajar_kode_revisi_unique` (`kode_revisi`),
  ADD KEY `tb_riwayat_revisi_bahan_ajar_id_bahan_ajar_foreign` (`id_bahan_ajar`);

--
-- Indexes for table `tb_riwayat_revisi_opsi_jawaban`
--
ALTER TABLE `tb_riwayat_revisi_opsi_jawaban`
  ADD PRIMARY KEY (`id_riwayat_revisi_opsi_jawaban`),
  ADD KEY `tb_riwayat_revisi_opsi_jawaban_id_riwayat_revisi_soal_foreign` (`id_riwayat_revisi_soal`);

--
-- Indexes for table `tb_riwayat_revisi_perangkat_pembelajaran`
--
ALTER TABLE `tb_riwayat_revisi_perangkat_pembelajaran`
  ADD PRIMARY KEY (`id_riwayat_revisi_perangkat_pembelajaran`),
  ADD UNIQUE KEY `tb_riwayat_revisi_perangkat_pembelajaran_kode_revisi_unique` (`kode_revisi`),
  ADD KEY `id_perangkat_foreign_revisi` (`id_perangkat_pembelajaran`);

--
-- Indexes for table `tb_riwayat_revisi_soal`
--
ALTER TABLE `tb_riwayat_revisi_soal`
  ADD PRIMARY KEY (`id_riwayat_revisi_soal`),
  ADD UNIQUE KEY `tb_riwayat_revisi_soal_kode_revisi_unique` (`kode_revisi`),
  ADD KEY `tb_riwayat_revisi_soal_id_soal_foreign` (`id_soal`);

--
-- Indexes for table `tb_sekolah`
--
ALTER TABLE `tb_sekolah`
  ADD PRIMARY KEY (`id_sekolah`),
  ADD KEY `tb_sekolah_id_provinsi_foreign` (`id_provinsi`),
  ADD KEY `tb_sekolah_id_kota_foreign` (`id_kota`),
  ADD KEY `tb_sekolah_id_kecamatan_foreign` (`id_kecamatan`);

--
-- Indexes for table `tb_siswa`
--
ALTER TABLE `tb_siswa`
  ADD PRIMARY KEY (`id_siswa`),
  ADD UNIQUE KEY `tb_siswa_nisn_unique` (`nisn`),
  ADD KEY `tb_siswa_id_user_foreign` (`id_user`);

--
-- Indexes for table `tb_soal`
--
ALTER TABLE `tb_soal`
  ADD PRIMARY KEY (`id_soal`),
  ADD KEY `tb_soal_id_user_foreign` (`id_user`),
  ADD KEY `tb_soal_id_kompetensi_dasar_foreign` (`id_kompetensi_dasar`),
  ADD KEY `tb_soal_id_kisi_kisi_foreign` (`id_kisi_kisi`),
  ADD KEY `tb_soal_id_grup_foreign` (`id_grup`),
  ADD KEY `tb_soal_divalidasi_oleh_foreign` (`divalidasi_oleh`);

--
-- Indexes for table `tb_tipe_entitas_pemberitahuan`
--
ALTER TABLE `tb_tipe_entitas_pemberitahuan`
  ADD PRIMARY KEY (`id_tipe_entitas_pemberitahuan`);

--
-- Indexes for table `tb_tugas`
--
ALTER TABLE `tb_tugas`
  ADD PRIMARY KEY (`id_tugas`),
  ADD KEY `tb_tugas_id_grup_foreign` (`id_grup`),
  ADD KEY `tb_tugas_id_folder_foreign` (`id_folder`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `tb_user_username_unique` (`username`),
  ADD UNIQUE KEY `tb_user_email_unique` (`email`),
  ADD UNIQUE KEY `tb_user_kode_verifikasi_unique` (`kode_verifikasi`);

--
-- Indexes for table `tb_user_dislike_bahan_ajar`
--
ALTER TABLE `tb_user_dislike_bahan_ajar`
  ADD PRIMARY KEY (`id_user`,`id_bahan_ajar`),
  ADD KEY `tb_user_dislike_bahan_ajar_id_bahan_ajar_foreign` (`id_bahan_ajar`);

--
-- Indexes for table `tb_user_dislike_perangkat_pembelajaran`
--
ALTER TABLE `tb_user_dislike_perangkat_pembelajaran`
  ADD PRIMARY KEY (`id_user`,`id_perangkat_pembelajaran`),
  ADD KEY `id_perangkat_foreign_dislike` (`id_perangkat_pembelajaran`);

--
-- Indexes for table `tb_user_dislike_soal`
--
ALTER TABLE `tb_user_dislike_soal`
  ADD PRIMARY KEY (`id_user`,`id_soal`),
  ADD KEY `tb_user_dislike_soal_id_soal_foreign` (`id_soal`);

--
-- Indexes for table `tb_user_follow_user`
--
ALTER TABLE `tb_user_follow_user`
  ADD PRIMARY KEY (`id_user_pengikut`,`id_user_yang_diikuti`),
  ADD KEY `tb_user_follow_user_id_user_yang_diikuti_foreign` (`id_user_yang_diikuti`);

--
-- Indexes for table `tb_user_jawab_soal`
--
ALTER TABLE `tb_user_jawab_soal`
  ADD PRIMARY KEY (`id_user`,`id_soal`),
  ADD KEY `tb_user_jawab_soal_id_soal_foreign` (`id_soal`),
  ADD KEY `tb_user_jawab_soal_id_opsi_jawaban_foreign` (`id_opsi_jawaban`);

--
-- Indexes for table `tb_user_komentar_bahan_ajar`
--
ALTER TABLE `tb_user_komentar_bahan_ajar`
  ADD PRIMARY KEY (`id_user`,`id_bahan_ajar`),
  ADD KEY `tb_user_komentar_bahan_ajar_id_bahan_ajar_foreign` (`id_bahan_ajar`);

--
-- Indexes for table `tb_user_komentar_diskusi`
--
ALTER TABLE `tb_user_komentar_diskusi`
  ADD PRIMARY KEY (`id_user`,`id_diskusi`),
  ADD KEY `tb_user_komentar_diskusi_id_diskusi_foreign` (`id_diskusi`);

--
-- Indexes for table `tb_user_komentar_perangkat_pembelajaran`
--
ALTER TABLE `tb_user_komentar_perangkat_pembelajaran`
  ADD PRIMARY KEY (`id_user`,`id_perangkat_pembelajaran`),
  ADD KEY `id_perangkat_foreign` (`id_perangkat_pembelajaran`);

--
-- Indexes for table `tb_user_komentar_post`
--
ALTER TABLE `tb_user_komentar_post`
  ADD PRIMARY KEY (`id_user`,`id_post`),
  ADD KEY `tb_user_komentar_post_id_post_foreign` (`id_post`);

--
-- Indexes for table `tb_user_komentar_quiz`
--
ALTER TABLE `tb_user_komentar_quiz`
  ADD PRIMARY KEY (`id_user`,`id_quiz`),
  ADD KEY `tb_user_komentar_quiz_id_quiz_foreign` (`id_quiz`);

--
-- Indexes for table `tb_user_komentar_soal`
--
ALTER TABLE `tb_user_komentar_soal`
  ADD PRIMARY KEY (`id_user`,`id_soal`),
  ADD KEY `tb_user_komentar_soal_id_soal_foreign` (`id_soal`);

--
-- Indexes for table `tb_user_komentar_tugas`
--
ALTER TABLE `tb_user_komentar_tugas`
  ADD PRIMARY KEY (`id_user`,`id_tugas`),
  ADD KEY `tb_user_komentar_tugas_id_tugas_foreign` (`id_tugas`);

--
-- Indexes for table `tb_user_like_bahan_ajar`
--
ALTER TABLE `tb_user_like_bahan_ajar`
  ADD PRIMARY KEY (`id_user`,`id_bahan_ajar`),
  ADD KEY `tb_user_like_bahan_ajar_id_bahan_ajar_foreign` (`id_bahan_ajar`);

--
-- Indexes for table `tb_user_like_perangkat_pembelajaran`
--
ALTER TABLE `tb_user_like_perangkat_pembelajaran`
  ADD PRIMARY KEY (`id_user`,`id_perangkat_pembelajaran`),
  ADD KEY `id_perangkat_foreign_like` (`id_perangkat_pembelajaran`);

--
-- Indexes for table `tb_user_like_post`
--
ALTER TABLE `tb_user_like_post`
  ADD PRIMARY KEY (`id_user`,`id_post`),
  ADD KEY `tb_user_like_post_id_post_foreign` (`id_post`);

--
-- Indexes for table `tb_user_like_soal`
--
ALTER TABLE `tb_user_like_soal`
  ADD PRIMARY KEY (`id_user`,`id_soal`),
  ADD KEY `tb_user_like_soal_id_soal_foreign` (`id_soal`);

--
-- Indexes for table `tb_user_mengerjakan_quiz`
--
ALTER TABLE `tb_user_mengerjakan_quiz`
  ADD PRIMARY KEY (`id_user`,`id_quiz`),
  ADD KEY `tb_user_mengerjakan_quiz_id_quiz_foreign` (`id_quiz`);

--
-- Indexes for table `tb_user_mengerjakan_tugas`
--
ALTER TABLE `tb_user_mengerjakan_tugas`
  ADD PRIMARY KEY (`id_user`,`id_tugas`),
  ADD KEY `tb_user_mengerjakan_tugas_id_tugas_foreign` (`id_tugas`),
  ADD KEY `tb_user_mengerjakan_tugas_diperiksa_oleh_foreign` (`diperiksa_oleh`);

--
-- Indexes for table `tb_user_message_user`
--
ALTER TABLE `tb_user_message_user`
  ADD PRIMARY KEY (`id_user_pengirim`,`id_user_penerima`),
  ADD KEY `tb_user_message_user_id_user_penerima_foreign` (`id_user_penerima`);

--
-- Indexes for table `tb_user_unduh_bahan_ajar`
--
ALTER TABLE `tb_user_unduh_bahan_ajar`
  ADD PRIMARY KEY (`id_user`,`id_bahan_ajar`),
  ADD KEY `tb_user_unduh_bahan_ajar_id_bahan_ajar_foreign` (`id_bahan_ajar`);

--
-- Indexes for table `tb_user_unduh_paket_soal`
--
ALTER TABLE `tb_user_unduh_paket_soal`
  ADD PRIMARY KEY (`id_user`,`id_paket_soal`),
  ADD KEY `tb_user_unduh_paket_soal_id_paket_soal_foreign` (`id_paket_soal`);

--
-- Indexes for table `tb_user_unduh_perangkat_pembelajaran`
--
ALTER TABLE `tb_user_unduh_perangkat_pembelajaran`
  ADD PRIMARY KEY (`id_user`,`id_perangkat_pembelajaran`),
  ADD KEY `id_perangkat_foreign_unduh` (`id_perangkat_pembelajaran`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `knex_migrations`
--
ALTER TABLE `knex_migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `knex_migrations_lock`
--
ALTER TABLE `knex_migrations_lock`
  MODIFY `index` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ref_bidang_keahlian`
--
ALTER TABLE `ref_bidang_keahlian`
  MODIFY `id_bidang_keahlian` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `ref_captcha`
--
ALTER TABLE `ref_captcha`
  MODIFY `id_captcha` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ref_kompetensi_dasar`
--
ALTER TABLE `ref_kompetensi_dasar`
  MODIFY `id_kompetensi_dasar` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT for table `ref_kompetensi_inti`
--
ALTER TABLE `ref_kompetensi_inti`
  MODIFY `id_kompetensi_inti` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `ref_kompetensi_keahlian`
--
ALTER TABLE `ref_kompetensi_keahlian`
  MODIFY `id_kompetensi_keahlian` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;

--
-- AUTO_INCREMENT for table `ref_program_keahlian`
--
ALTER TABLE `ref_program_keahlian`
  MODIFY `id_program_keahlian` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `ref_sk_dikdasmen`
--
ALTER TABLE `ref_sk_dikdasmen`
  MODIFY `id_sk_dikdasmen` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_aktivitas_siswa`
--
ALTER TABLE `tb_aktivitas_siswa`
  MODIFY `id_aktivitas_siswa` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_bahan_ajar`
--
ALTER TABLE `tb_bahan_ajar`
  MODIFY `id_bahan_ajar` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_diskusi`
--
ALTER TABLE `tb_diskusi`
  MODIFY `id_diskusi` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_file`
--
ALTER TABLE `tb_file`
  MODIFY `id_file` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_folder`
--
ALTER TABLE `tb_folder`
  MODIFY `id_folder` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_grup`
--
ALTER TABLE `tb_grup`
  MODIFY `id_grup` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_guru`
--
ALTER TABLE `tb_guru`
  MODIFY `id_guru` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_kisi_kisi`
--
ALTER TABLE `tb_kisi_kisi`
  MODIFY `id_kisi_kisi` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_lampiran`
--
ALTER TABLE `tb_lampiran`
  MODIFY `id_lampiran` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_link`
--
ALTER TABLE `tb_link`
  MODIFY `id_link` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_mata_pelajaran`
--
ALTER TABLE `tb_mata_pelajaran`
  MODIFY `id_mata_pelajaran` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `tb_objek_pemberitahuan`
--
ALTER TABLE `tb_objek_pemberitahuan`
  MODIFY `id_objek_pemberitahuan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_opsi_jawaban`
--
ALTER TABLE `tb_opsi_jawaban`
  MODIFY `id_opsi_jawaban` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_paket_soal`
--
ALTER TABLE `tb_paket_soal`
  MODIFY `id_paket_soal` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_pelaku_pemberitahuan`
--
ALTER TABLE `tb_pelaku_pemberitahuan`
  MODIFY `id_pelaku_pemberitahuan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_penerima_pemberitahuan`
--
ALTER TABLE `tb_penerima_pemberitahuan`
  MODIFY `id_penerima_pemberitahuan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_perangkat_pembelajaran`
--
ALTER TABLE `tb_perangkat_pembelajaran`
  MODIFY `id_perangkat_pembelajaran` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_post`
--
ALTER TABLE `tb_post`
  MODIFY `id_post` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_quiz`
--
ALTER TABLE `tb_quiz`
  MODIFY `id_quiz` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_riwayat_pendidikan`
--
ALTER TABLE `tb_riwayat_pendidikan`
  MODIFY `id_riwayat_pendidikan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_riwayat_revisi_bahan_ajar`
--
ALTER TABLE `tb_riwayat_revisi_bahan_ajar`
  MODIFY `id_riwayat_revisi_bahan_ajar` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_riwayat_revisi_opsi_jawaban`
--
ALTER TABLE `tb_riwayat_revisi_opsi_jawaban`
  MODIFY `id_riwayat_revisi_opsi_jawaban` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_riwayat_revisi_perangkat_pembelajaran`
--
ALTER TABLE `tb_riwayat_revisi_perangkat_pembelajaran`
  MODIFY `id_riwayat_revisi_perangkat_pembelajaran` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_riwayat_revisi_soal`
--
ALTER TABLE `tb_riwayat_revisi_soal`
  MODIFY `id_riwayat_revisi_soal` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_siswa`
--
ALTER TABLE `tb_siswa`
  MODIFY `id_siswa` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_soal`
--
ALTER TABLE `tb_soal`
  MODIFY `id_soal` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_tipe_entitas_pemberitahuan`
--
ALTER TABLE `tb_tipe_entitas_pemberitahuan`
  MODIFY `id_tipe_entitas_pemberitahuan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_tugas`
--
ALTER TABLE `tb_tugas`
  MODIFY `id_tugas` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ref_bidang_keahlian`
--
ALTER TABLE `ref_bidang_keahlian`
  ADD CONSTRAINT `ref_bidang_keahlian_id_sk_dikdasmen_foreign` FOREIGN KEY (`id_sk_dikdasmen`) REFERENCES `ref_sk_dikdasmen` (`id_sk_dikdasmen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ref_kecamatan`
--
ALTER TABLE `ref_kecamatan`
  ADD CONSTRAINT `ref_kecamatan_id_kota_foreign` FOREIGN KEY (`id_kota`) REFERENCES `ref_kota` (`id_kota`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ref_kecamatan_id_provinsi_foreign` FOREIGN KEY (`id_provinsi`) REFERENCES `ref_provinsi` (`id_provinsi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ref_kompetensi_dasar`
--
ALTER TABLE `ref_kompetensi_dasar`
  ADD CONSTRAINT `ref_kompetensi_dasar_id_kompetensi_inti_foreign` FOREIGN KEY (`id_kompetensi_inti`) REFERENCES `ref_kompetensi_inti` (`id_kompetensi_inti`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ref_kompetensi_dasar_id_mata_pelajaran_foreign` FOREIGN KEY (`id_mata_pelajaran`) REFERENCES `tb_mata_pelajaran` (`id_mata_pelajaran`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ref_kompetensi_inti`
--
ALTER TABLE `ref_kompetensi_inti`
  ADD CONSTRAINT `ref_kompetensi_inti_id_mata_pelajaran_foreign` FOREIGN KEY (`id_mata_pelajaran`) REFERENCES `tb_mata_pelajaran` (`id_mata_pelajaran`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ref_kompetensi_keahlian`
--
ALTER TABLE `ref_kompetensi_keahlian`
  ADD CONSTRAINT `ref_kompetensi_keahlian_id_bidang_keahlian_foreign` FOREIGN KEY (`id_bidang_keahlian`) REFERENCES `ref_bidang_keahlian` (`id_bidang_keahlian`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ref_kompetensi_keahlian_id_program_keahlian_foreign` FOREIGN KEY (`id_program_keahlian`) REFERENCES `ref_program_keahlian` (`id_program_keahlian`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ref_kompetensi_keahlian_id_sk_dikdasmen_foreign` FOREIGN KEY (`id_sk_dikdasmen`) REFERENCES `ref_sk_dikdasmen` (`id_sk_dikdasmen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ref_kota`
--
ALTER TABLE `ref_kota`
  ADD CONSTRAINT `ref_kota_id_provinsi_foreign` FOREIGN KEY (`id_provinsi`) REFERENCES `ref_provinsi` (`id_provinsi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ref_program_keahlian`
--
ALTER TABLE `ref_program_keahlian`
  ADD CONSTRAINT `ref_program_keahlian_id_bidang_keahlian_foreign` FOREIGN KEY (`id_bidang_keahlian`) REFERENCES `ref_bidang_keahlian` (`id_bidang_keahlian`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ref_program_keahlian_id_sk_dikdasmen_foreign` FOREIGN KEY (`id_sk_dikdasmen`) REFERENCES `ref_sk_dikdasmen` (`id_sk_dikdasmen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_aktivitas_siswa`
--
ALTER TABLE `tb_aktivitas_siswa`
  ADD CONSTRAINT `tb_aktivitas_siswa_id_folder_foreign` FOREIGN KEY (`id_folder`) REFERENCES `tb_folder` (`id_folder`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_aktivitas_siswa_id_grup_foreign` FOREIGN KEY (`id_grup`) REFERENCES `tb_grup` (`id_grup`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_anggota_grup`
--
ALTER TABLE `tb_anggota_grup`
  ADD CONSTRAINT `tb_anggota_grup_id_grup_foreign` FOREIGN KEY (`id_grup`) REFERENCES `tb_grup` (`id_grup`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_anggota_grup_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_bahan_ajar`
--
ALTER TABLE `tb_bahan_ajar`
  ADD CONSTRAINT `tb_bahan_ajar_divalidasi_oleh_foreign` FOREIGN KEY (`divalidasi_oleh`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_bahan_ajar_id_grup_foreign` FOREIGN KEY (`id_grup`) REFERENCES `tb_grup` (`id_grup`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_bahan_ajar_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_diskusi`
--
ALTER TABLE `tb_diskusi`
  ADD CONSTRAINT `tb_diskusi_id_folder_foreign` FOREIGN KEY (`id_folder`) REFERENCES `tb_folder` (`id_folder`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_diskusi_id_grup_foreign` FOREIGN KEY (`id_grup`) REFERENCES `tb_grup` (`id_grup`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_file`
--
ALTER TABLE `tb_file`
  ADD CONSTRAINT `tb_file_id_folder_foreign` FOREIGN KEY (`id_folder`) REFERENCES `tb_folder` (`id_folder`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_file_id_grup_foreign` FOREIGN KEY (`id_grup`) REFERENCES `tb_grup` (`id_grup`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_folder`
--
ALTER TABLE `tb_folder`
  ADD CONSTRAINT `tb_folder_id_grup_foreign` FOREIGN KEY (`id_grup`) REFERENCES `tb_grup` (`id_grup`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_folder_parent_folder_foreign` FOREIGN KEY (`parent_folder`) REFERENCES `tb_folder` (`id_folder`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_grup`
--
ALTER TABLE `tb_grup`
  ADD CONSTRAINT `tb_grup_dibuat_oleh_foreign` FOREIGN KEY (`dibuat_oleh`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_grup_id_mata_pelajaran_foreign` FOREIGN KEY (`id_mata_pelajaran`) REFERENCES `tb_mata_pelajaran` (`id_mata_pelajaran`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_guru`
--
ALTER TABLE `tb_guru`
  ADD CONSTRAINT `tb_guru_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_guru_mengampu_mata_pelajaran`
--
ALTER TABLE `tb_guru_mengampu_mata_pelajaran`
  ADD CONSTRAINT `tb_guru_mengampu_mata_pelajaran_id_guru_foreign` FOREIGN KEY (`id_guru`) REFERENCES `tb_guru` (`id_guru`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_guru_mengampu_mata_pelajaran_id_mata_pelajaran_foreign` FOREIGN KEY (`id_mata_pelajaran`) REFERENCES `tb_mata_pelajaran` (`id_mata_pelajaran`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_kisi_kisi`
--
ALTER TABLE `tb_kisi_kisi`
  ADD CONSTRAINT `tb_kisi_kisi_id_kompetensi_dasar_foreign` FOREIGN KEY (`id_kompetensi_dasar`) REFERENCES `ref_kompetensi_dasar` (`id_kompetensi_dasar`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_kisi_kisi_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_lampiran`
--
ALTER TABLE `tb_lampiran`
  ADD CONSTRAINT `tb_lampiran_id_diskusi_foreign` FOREIGN KEY (`id_diskusi`) REFERENCES `tb_diskusi` (`id_diskusi`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_lampiran_id_tugas_foreign` FOREIGN KEY (`id_tugas`) REFERENCES `tb_tugas` (`id_tugas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_link`
--
ALTER TABLE `tb_link`
  ADD CONSTRAINT `tb_link_id_folder_foreign` FOREIGN KEY (`id_folder`) REFERENCES `tb_folder` (`id_folder`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_link_id_grup_foreign` FOREIGN KEY (`id_grup`) REFERENCES `tb_grup` (`id_grup`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_mata_pelajaran`
--
ALTER TABLE `tb_mata_pelajaran`
  ADD CONSTRAINT `tb_mata_pelajaran_id_bidang_keahlian_foreign` FOREIGN KEY (`id_bidang_keahlian`) REFERENCES `ref_bidang_keahlian` (`id_bidang_keahlian`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_mata_pelajaran_id_kompetensi_keahlian_foreign` FOREIGN KEY (`id_kompetensi_keahlian`) REFERENCES `ref_kompetensi_keahlian` (`id_kompetensi_keahlian`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_mata_pelajaran_id_program_keahlian_foreign` FOREIGN KEY (`id_program_keahlian`) REFERENCES `ref_program_keahlian` (`id_program_keahlian`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_objek_pemberitahuan`
--
ALTER TABLE `tb_objek_pemberitahuan`
  ADD CONSTRAINT `tb_objek_pemberitahuan_id_tipe_entitas_pemberitahuan_foreign` FOREIGN KEY (`id_tipe_entitas_pemberitahuan`) REFERENCES `tb_tipe_entitas_pemberitahuan` (`id_tipe_entitas_pemberitahuan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_opsi_jawaban`
--
ALTER TABLE `tb_opsi_jawaban`
  ADD CONSTRAINT `tb_opsi_jawaban_id_soal_foreign` FOREIGN KEY (`id_soal`) REFERENCES `tb_soal` (`id_soal`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_paket_soal`
--
ALTER TABLE `tb_paket_soal`
  ADD CONSTRAINT `tb_paket_soal_dibuat_oleh_foreign` FOREIGN KEY (`dibuat_oleh`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_paket_soal_id_grup_foreign` FOREIGN KEY (`id_grup`) REFERENCES `tb_grup` (`id_grup`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_paket_soal_id_mata_pelajaran_foreign` FOREIGN KEY (`id_mata_pelajaran`) REFERENCES `tb_mata_pelajaran` (`id_mata_pelajaran`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_paket_soal_berisi_soal`
--
ALTER TABLE `tb_paket_soal_berisi_soal`
  ADD CONSTRAINT `tb_paket_soal_berisi_soal_id_paket_soal_foreign` FOREIGN KEY (`id_paket_soal`) REFERENCES `tb_paket_soal` (`id_paket_soal`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_paket_soal_berisi_soal_id_soal_foreign` FOREIGN KEY (`id_soal`) REFERENCES `tb_soal` (`id_soal`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_pelaku_pemberitahuan`
--
ALTER TABLE `tb_pelaku_pemberitahuan`
  ADD CONSTRAINT `tb_pelaku_pemberitahuan_id_objek_pemberitahuan_foreign` FOREIGN KEY (`id_objek_pemberitahuan`) REFERENCES `tb_objek_pemberitahuan` (`id_objek_pemberitahuan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_pelaku_pemberitahuan_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_penerima_pemberitahuan`
--
ALTER TABLE `tb_penerima_pemberitahuan`
  ADD CONSTRAINT `tb_penerima_pemberitahuan_id_objek_pemberitahuan_foreign` FOREIGN KEY (`id_objek_pemberitahuan`) REFERENCES `tb_objek_pemberitahuan` (`id_objek_pemberitahuan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_penerima_pemberitahuan_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_perangkat_pembelajaran`
--
ALTER TABLE `tb_perangkat_pembelajaran`
  ADD CONSTRAINT `tb_perangkat_pembelajaran_divalidasi_oleh_foreign` FOREIGN KEY (`divalidasi_oleh`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_perangkat_pembelajaran_id_grup_foreign` FOREIGN KEY (`id_grup`) REFERENCES `tb_grup` (`id_grup`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_perangkat_pembelajaran_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_post`
--
ALTER TABLE `tb_post`
  ADD CONSTRAINT `tb_post_id_grup_foreign` FOREIGN KEY (`id_grup`) REFERENCES `tb_grup` (`id_grup`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_post_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_quiz`
--
ALTER TABLE `tb_quiz`
  ADD CONSTRAINT `tb_quiz_id_grup_foreign` FOREIGN KEY (`id_grup`) REFERENCES `tb_grup` (`id_grup`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_quiz_id_paket_soal_foreign` FOREIGN KEY (`id_paket_soal`) REFERENCES `tb_paket_soal` (`id_paket_soal`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_quiz_berisi_soal`
--
ALTER TABLE `tb_quiz_berisi_soal`
  ADD CONSTRAINT `tb_quiz_berisi_soal_id_quiz_foreign` FOREIGN KEY (`id_quiz`) REFERENCES `tb_quiz` (`id_quiz`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_quiz_berisi_soal_id_soal_foreign` FOREIGN KEY (`id_soal`) REFERENCES `tb_soal` (`id_soal`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_riwayat_pendidikan`
--
ALTER TABLE `tb_riwayat_pendidikan`
  ADD CONSTRAINT `tb_riwayat_pendidikan_id_guru_foreign` FOREIGN KEY (`id_guru`) REFERENCES `tb_guru` (`id_guru`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_riwayat_revisi_bahan_ajar`
--
ALTER TABLE `tb_riwayat_revisi_bahan_ajar`
  ADD CONSTRAINT `tb_riwayat_revisi_bahan_ajar_id_bahan_ajar_foreign` FOREIGN KEY (`id_bahan_ajar`) REFERENCES `tb_bahan_ajar` (`id_bahan_ajar`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_riwayat_revisi_opsi_jawaban`
--
ALTER TABLE `tb_riwayat_revisi_opsi_jawaban`
  ADD CONSTRAINT `tb_riwayat_revisi_opsi_jawaban_id_riwayat_revisi_soal_foreign` FOREIGN KEY (`id_riwayat_revisi_soal`) REFERENCES `tb_riwayat_revisi_soal` (`id_riwayat_revisi_soal`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_riwayat_revisi_perangkat_pembelajaran`
--
ALTER TABLE `tb_riwayat_revisi_perangkat_pembelajaran`
  ADD CONSTRAINT `id_perangkat_foreign_revisi` FOREIGN KEY (`id_perangkat_pembelajaran`) REFERENCES `tb_perangkat_pembelajaran` (`id_perangkat_pembelajaran`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_riwayat_revisi_soal`
--
ALTER TABLE `tb_riwayat_revisi_soal`
  ADD CONSTRAINT `tb_riwayat_revisi_soal_id_soal_foreign` FOREIGN KEY (`id_soal`) REFERENCES `tb_soal` (`id_soal`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_sekolah`
--
ALTER TABLE `tb_sekolah`
  ADD CONSTRAINT `tb_sekolah_id_kecamatan_foreign` FOREIGN KEY (`id_kecamatan`) REFERENCES `ref_kecamatan` (`id_kecamatan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_sekolah_id_kota_foreign` FOREIGN KEY (`id_kota`) REFERENCES `ref_kota` (`id_kota`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_sekolah_id_provinsi_foreign` FOREIGN KEY (`id_provinsi`) REFERENCES `ref_provinsi` (`id_provinsi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_siswa`
--
ALTER TABLE `tb_siswa`
  ADD CONSTRAINT `tb_siswa_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_soal`
--
ALTER TABLE `tb_soal`
  ADD CONSTRAINT `tb_soal_divalidasi_oleh_foreign` FOREIGN KEY (`divalidasi_oleh`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_soal_id_grup_foreign` FOREIGN KEY (`id_grup`) REFERENCES `tb_grup` (`id_grup`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_soal_id_kisi_kisi_foreign` FOREIGN KEY (`id_kisi_kisi`) REFERENCES `tb_kisi_kisi` (`id_kisi_kisi`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_soal_id_kompetensi_dasar_foreign` FOREIGN KEY (`id_kompetensi_dasar`) REFERENCES `ref_kompetensi_dasar` (`id_kompetensi_dasar`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_soal_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_tugas`
--
ALTER TABLE `tb_tugas`
  ADD CONSTRAINT `tb_tugas_id_folder_foreign` FOREIGN KEY (`id_folder`) REFERENCES `tb_folder` (`id_folder`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_tugas_id_grup_foreign` FOREIGN KEY (`id_grup`) REFERENCES `tb_grup` (`id_grup`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_user_dislike_bahan_ajar`
--
ALTER TABLE `tb_user_dislike_bahan_ajar`
  ADD CONSTRAINT `tb_user_dislike_bahan_ajar_id_bahan_ajar_foreign` FOREIGN KEY (`id_bahan_ajar`) REFERENCES `tb_bahan_ajar` (`id_bahan_ajar`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_user_dislike_bahan_ajar_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_user_dislike_perangkat_pembelajaran`
--
ALTER TABLE `tb_user_dislike_perangkat_pembelajaran`
  ADD CONSTRAINT `id_perangkat_foreign_dislike` FOREIGN KEY (`id_perangkat_pembelajaran`) REFERENCES `tb_perangkat_pembelajaran` (`id_perangkat_pembelajaran`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_user_dislike_perangkat_pembelajaran_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_user_dislike_soal`
--
ALTER TABLE `tb_user_dislike_soal`
  ADD CONSTRAINT `tb_user_dislike_soal_id_soal_foreign` FOREIGN KEY (`id_soal`) REFERENCES `tb_soal` (`id_soal`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_user_dislike_soal_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_user_follow_user`
--
ALTER TABLE `tb_user_follow_user`
  ADD CONSTRAINT `tb_user_follow_user_id_user_pengikut_foreign` FOREIGN KEY (`id_user_pengikut`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_user_follow_user_id_user_yang_diikuti_foreign` FOREIGN KEY (`id_user_yang_diikuti`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_user_jawab_soal`
--
ALTER TABLE `tb_user_jawab_soal`
  ADD CONSTRAINT `tb_user_jawab_soal_id_opsi_jawaban_foreign` FOREIGN KEY (`id_opsi_jawaban`) REFERENCES `tb_opsi_jawaban` (`id_opsi_jawaban`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_user_jawab_soal_id_soal_foreign` FOREIGN KEY (`id_soal`) REFERENCES `tb_soal` (`id_soal`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_user_jawab_soal_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_user_komentar_bahan_ajar`
--
ALTER TABLE `tb_user_komentar_bahan_ajar`
  ADD CONSTRAINT `tb_user_komentar_bahan_ajar_id_bahan_ajar_foreign` FOREIGN KEY (`id_bahan_ajar`) REFERENCES `tb_bahan_ajar` (`id_bahan_ajar`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_user_komentar_bahan_ajar_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_user_komentar_diskusi`
--
ALTER TABLE `tb_user_komentar_diskusi`
  ADD CONSTRAINT `tb_user_komentar_diskusi_id_diskusi_foreign` FOREIGN KEY (`id_diskusi`) REFERENCES `tb_diskusi` (`id_diskusi`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_user_komentar_diskusi_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_user_komentar_perangkat_pembelajaran`
--
ALTER TABLE `tb_user_komentar_perangkat_pembelajaran`
  ADD CONSTRAINT `id_perangkat_foreign` FOREIGN KEY (`id_perangkat_pembelajaran`) REFERENCES `tb_perangkat_pembelajaran` (`id_perangkat_pembelajaran`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_user_komentar_perangkat_pembelajaran_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_user_komentar_post`
--
ALTER TABLE `tb_user_komentar_post`
  ADD CONSTRAINT `tb_user_komentar_post_id_post_foreign` FOREIGN KEY (`id_post`) REFERENCES `tb_post` (`id_post`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_user_komentar_post_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_user_komentar_quiz`
--
ALTER TABLE `tb_user_komentar_quiz`
  ADD CONSTRAINT `tb_user_komentar_quiz_id_quiz_foreign` FOREIGN KEY (`id_quiz`) REFERENCES `tb_quiz` (`id_quiz`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_user_komentar_quiz_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_user_komentar_soal`
--
ALTER TABLE `tb_user_komentar_soal`
  ADD CONSTRAINT `tb_user_komentar_soal_id_soal_foreign` FOREIGN KEY (`id_soal`) REFERENCES `tb_soal` (`id_soal`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_user_komentar_soal_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_user_komentar_tugas`
--
ALTER TABLE `tb_user_komentar_tugas`
  ADD CONSTRAINT `tb_user_komentar_tugas_id_tugas_foreign` FOREIGN KEY (`id_tugas`) REFERENCES `tb_tugas` (`id_tugas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_user_komentar_tugas_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_user_like_bahan_ajar`
--
ALTER TABLE `tb_user_like_bahan_ajar`
  ADD CONSTRAINT `tb_user_like_bahan_ajar_id_bahan_ajar_foreign` FOREIGN KEY (`id_bahan_ajar`) REFERENCES `tb_bahan_ajar` (`id_bahan_ajar`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_user_like_bahan_ajar_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_user_like_perangkat_pembelajaran`
--
ALTER TABLE `tb_user_like_perangkat_pembelajaran`
  ADD CONSTRAINT `id_perangkat_foreign_like` FOREIGN KEY (`id_perangkat_pembelajaran`) REFERENCES `tb_perangkat_pembelajaran` (`id_perangkat_pembelajaran`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_user_like_perangkat_pembelajaran_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_user_like_post`
--
ALTER TABLE `tb_user_like_post`
  ADD CONSTRAINT `tb_user_like_post_id_post_foreign` FOREIGN KEY (`id_post`) REFERENCES `tb_post` (`id_post`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_user_like_post_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_user_like_soal`
--
ALTER TABLE `tb_user_like_soal`
  ADD CONSTRAINT `tb_user_like_soal_id_soal_foreign` FOREIGN KEY (`id_soal`) REFERENCES `tb_soal` (`id_soal`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_user_like_soal_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_user_mengerjakan_quiz`
--
ALTER TABLE `tb_user_mengerjakan_quiz`
  ADD CONSTRAINT `tb_user_mengerjakan_quiz_id_quiz_foreign` FOREIGN KEY (`id_quiz`) REFERENCES `tb_quiz` (`id_quiz`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_user_mengerjakan_quiz_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_user_mengerjakan_tugas`
--
ALTER TABLE `tb_user_mengerjakan_tugas`
  ADD CONSTRAINT `tb_user_mengerjakan_tugas_diperiksa_oleh_foreign` FOREIGN KEY (`diperiksa_oleh`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_user_mengerjakan_tugas_id_tugas_foreign` FOREIGN KEY (`id_tugas`) REFERENCES `tb_tugas` (`id_tugas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_user_mengerjakan_tugas_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_user_message_user`
--
ALTER TABLE `tb_user_message_user`
  ADD CONSTRAINT `tb_user_message_user_id_user_penerima_foreign` FOREIGN KEY (`id_user_penerima`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_user_message_user_id_user_pengirim_foreign` FOREIGN KEY (`id_user_pengirim`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_user_unduh_bahan_ajar`
--
ALTER TABLE `tb_user_unduh_bahan_ajar`
  ADD CONSTRAINT `tb_user_unduh_bahan_ajar_id_bahan_ajar_foreign` FOREIGN KEY (`id_bahan_ajar`) REFERENCES `tb_bahan_ajar` (`id_bahan_ajar`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_user_unduh_bahan_ajar_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_user_unduh_paket_soal`
--
ALTER TABLE `tb_user_unduh_paket_soal`
  ADD CONSTRAINT `tb_user_unduh_paket_soal_id_paket_soal_foreign` FOREIGN KEY (`id_paket_soal`) REFERENCES `tb_paket_soal` (`id_paket_soal`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_user_unduh_paket_soal_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_user_unduh_perangkat_pembelajaran`
--
ALTER TABLE `tb_user_unduh_perangkat_pembelajaran`
  ADD CONSTRAINT `id_perangkat_foreign_unduh` FOREIGN KEY (`id_perangkat_pembelajaran`) REFERENCES `tb_perangkat_pembelajaran` (`id_perangkat_pembelajaran`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_user_unduh_perangkat_pembelajaran_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
