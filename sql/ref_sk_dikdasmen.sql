-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 14, 2020 at 08:30 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kolaborasi_guru`
--

-- --------------------------------------------------------

--
-- Table structure for table `ref_sk_dikdasmen`
--

-- CREATE TABLE `ref_sk_dikdasmen` (
--   `id_sk_dikdasmen` int(10) UNSIGNED NOT NULL,
--   `nomor` varchar(100) NOT NULL,
--   `tanggal` date NOT NULL,
--   `tentang` varchar(255) NOT NULL,
--   `created_at` datetime NOT NULL DEFAULT current_timestamp(),
--   `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
--   `deleted_at` datetime DEFAULT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_sk_dikdasmen`
--

INSERT INTO `ref_sk_dikdasmen` (`id_sk_dikdasmen`, `nomor`, `tanggal`, `tentang`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '07/D.D5/KK/2019', '2019-05-23', 'Spektrum SMK baru', '2020-07-13 10:00:15', '2020-07-13 10:00:15', NULL),
(2, '07/D.D5/KK/2019', '2019-05-23', 'Spektrum SMK baru', '2020-07-13 10:05:05', '2020-07-13 10:05:05', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ref_sk_dikdasmen`
--
ALTER TABLE `ref_sk_dikdasmen`
  ADD PRIMARY KEY (`id_sk_dikdasmen`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ref_sk_dikdasmen`
--
ALTER TABLE `ref_sk_dikdasmen`
  MODIFY `id_sk_dikdasmen` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
