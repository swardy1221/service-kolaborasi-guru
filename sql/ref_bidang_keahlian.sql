-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 14, 2020 at 08:46 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kolaborasi_guru`
--

-- --------------------------------------------------------

--
-- Table structure for table `ref_bidang_keahlian`
--

-- CREATE TABLE `ref_bidang_keahlian` (
--   `id_bidang_keahlian` int(10) UNSIGNED NOT NULL,
--   `id_sk_dikdasmen` int(11) UNSIGNED DEFAULT NULL,
--   `kode_bidang_keahlian` varchar(10) NOT NULL,
--   `nama_bidang_keahlian` varchar(100) NOT NULL,
--   `created_at` datetime NOT NULL DEFAULT current_timestamp(),
--   `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
--   `deleted_at` datetime DEFAULT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_bidang_keahlian`
--

INSERT INTO `ref_bidang_keahlian` (`id_bidang_keahlian`, `id_sk_dikdasmen`, `kode_bidang_keahlian`, `nama_bidang_keahlian`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '1', 'Teknologi dan Rekayasa', '2020-07-13 14:42:01', '2020-07-13 14:42:01', NULL),
(2, 1, '2', 'Energi dan Pertambangan', '2020-07-13 14:42:01', '2020-07-13 14:42:01', NULL),
(3, 1, '3', 'Teknologi Informasi dan Komunikasi', '2020-07-13 14:42:01', '2020-07-13 14:42:01', NULL),
(4, 1, '4', 'Kesehatan dan Pekerjaan Sosial', '2020-07-13 14:42:01', '2020-07-13 14:42:01', NULL),
(5, 1, '5', 'Agribisnis dan Agroteknologi', '2020-07-13 14:42:01', '2020-07-13 14:42:01', NULL),
(6, 1, '6', 'Kemaritiman', '2020-07-13 14:42:01', '2020-07-13 14:42:01', NULL),
(7, 1, '7', 'Bisnis dan Manajemen', '2020-07-13 14:42:01', '2020-07-13 14:42:01', NULL),
(8, 1, '8', 'Pariwisata', '2020-07-13 14:42:01', '2020-07-13 14:42:01', NULL),
(9, 1, '9', 'Seni dan Industri Kreatif', '2020-07-13 14:42:01', '2020-07-13 14:42:01', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ref_bidang_keahlian`
--
-- ALTER TABLE `ref_bidang_keahlian`
--   ADD PRIMARY KEY (`id_bidang_keahlian`),
--   ADD KEY `ref_bidang_keahlian_id_sk_dikdasmen_foreign` (`id_sk_dikdasmen`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ref_bidang_keahlian`
--
-- ALTER TABLE `ref_bidang_keahlian`
--   MODIFY `id_bidang_keahlian` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ref_bidang_keahlian`
--
-- ALTER TABLE `ref_bidang_keahlian`
--   ADD CONSTRAINT `ref_bidang_keahlian_id_sk_dikdasmen_foreign` FOREIGN KEY (`id_sk_dikdasmen`) REFERENCES `ref_sk_dikdasmen` (`id_sk_dikdasmen`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
