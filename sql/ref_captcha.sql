-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 14, 2020 at 08:49 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kolaborasi_guru`
--

-- --------------------------------------------------------

--
-- Table structure for table `ref_captcha`
--

-- CREATE TABLE `ref_captcha` (
--   `id_captcha` int(10) UNSIGNED NOT NULL,
--   `pertanyaan` varchar(100) NOT NULL,
--   `jawaban` varchar(100) NOT NULL,
--   `created_at` datetime NOT NULL DEFAULT current_timestamp(),
--   `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
--   `deleted_at` datetime DEFAULT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_captcha`
--

INSERT INTO `ref_captcha` (`id_captcha`, `pertanyaan`, `jawaban`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Berapakah hasil 6 dibagi 3', '2', '2020-07-16 14:40:34', '2020-07-16 14:40:34', NULL),
(2, 'Hasil dari 7 ditambah 4', '11', '2020-07-16 14:40:34', '2020-07-16 14:40:34', NULL),
(3, 'Hasil dari 3 faktorial', '6', '2020-07-16 14:41:49', '2020-07-16 14:41:49', NULL),
(4, 'Hasil dari 5 ditambah 4', '9', '2020-07-16 14:41:49', '2020-07-16 14:41:49', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ref_captcha`
--
-- ALTER TABLE `ref_captcha`
--   ADD PRIMARY KEY (`id_captcha`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ref_captcha`
--
-- ALTER TABLE `ref_captcha`
--   MODIFY `id_captcha` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
