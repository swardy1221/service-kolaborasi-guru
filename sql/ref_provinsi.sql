-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 14, 2020 at 09:01 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kolaborasi_guru`
--

-- --------------------------------------------------------

--
-- Table structure for table `ref_provinsi`
--

-- CREATE TABLE `ref_provinsi` (
--   `id_provinsi` varchar(100) NOT NULL,
--   `nama_provinsi` varchar(100) NOT NULL,
--   `created_at` datetime NOT NULL DEFAULT current_timestamp(),
--   `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
--   `deleted_at` datetime DEFAULT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_provinsi`
--

INSERT INTO `ref_provinsi` (`id_provinsi`, `nama_provinsi`, `created_at`, `updated_at`, `deleted_at`) VALUES
('010000', 'DKI Jakarta', '0000-00-00 00:00:00', '2020-07-13 10:21:04', NULL),
('020000', 'Jawa Barat', '0000-00-00 00:00:00', '2020-07-13 10:21:24', NULL),
('030000', 'Jawa Tengah', '0000-00-00 00:00:00', '2020-07-13 10:21:30', NULL),
('040000', 'DIY Yogyakarta', '0000-00-00 00:00:00', '2020-07-13 10:21:41', NULL),
('050000', 'Jawa Timur', '0000-00-00 00:00:00', '2020-07-13 10:21:46', NULL),
('060000', 'Nanggroe Aceh Darussalam', '0000-00-00 00:00:00', '2020-07-13 10:21:51', NULL),
('070000', 'Sumatera Utara', '0000-00-00 00:00:00', '2020-07-13 10:21:55', NULL),
('080000', 'Sumatera Barat', '0000-00-00 00:00:00', '2020-07-13 10:21:59', NULL),
('090000', 'Riau', '0000-00-00 00:00:00', '2020-07-13 10:22:03', NULL),
('100000', 'Jambi', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('110000', 'Sumatera Selatan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('120000', 'Lampung', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('130000', 'Kalimantan Barat', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('140000', 'Kalimantan Tengah', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('150000', 'Kalimantan Selatan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('160000', 'Kalimantan Timur', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('170000', 'Sulawesi Utara', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('180000', 'Sulawesi Tengah', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('190000', 'Sulawesi Selatan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('200000', 'Sulawesi Tenggara', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('210000', 'Maluku', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('220000', 'Bali', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('230000', 'Nusa Tenggara Barat', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('240000', 'Nusa Tenggara Timur', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('250000', 'Papua', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('260000', 'Bengkulu', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('270000', 'Maluku Utara', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('280000', 'Banten', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('290000', 'Bangka Belitung', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('300000', 'Gorontalo', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('310000', 'Kepulauan Riau', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('320000', 'Papua Barat', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('330000', 'Sulawesi Barat', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ref_provinsi`
--
-- ALTER TABLE `ref_provinsi`
--   ADD PRIMARY KEY (`id_provinsi`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
