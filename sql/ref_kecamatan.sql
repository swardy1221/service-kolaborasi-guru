-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 14, 2020 at 09:02 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kolaborasi_guru`
--

-- --------------------------------------------------------

--
-- Table structure for table `ref_kecamatan`
--

-- CREATE TABLE `ref_kecamatan` (
--   `id_kecamatan` varchar(100) NOT NULL,
--   `id_kota` varchar(100) DEFAULT NULL,
--   `id_provinsi` varchar(100) DEFAULT NULL,
--   `nama_kecamatan` varchar(100) NOT NULL,
--   `created_at` datetime NOT NULL DEFAULT current_timestamp(),
--   `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
--   `deleted_at` datetime DEFAULT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_kecamatan`
--

INSERT INTO `ref_kecamatan` (`id_provinsi`, `provinsi`, `id_kota`, `kota`,`id_kecamatan`, `kecamatan`, `created_at`) VALUES
("010000"  ,'DKI Jakarta',"010100"  ,'Kab. Kepulauan Seribu',"010101",'Kec. Kepulauan Seribu Selatan',1607847015655),
    ("010000"  ,'DKI Jakarta',"010100"  ,'Kab. Kepulauan Seribu',"010102",'Kec. Kepulauan Seribu Utara',1607847015655),
    ("010000"  ,'DKI Jakarta',"016000"  ,'Kota Jakarta Pusat',"016001",'Kec. Tanah Abang',1607847015655),
    ("010000"  ,'DKI Jakarta',"016000"  ,'Kota Jakarta Pusat',"016002",'Kec. Menteng',1607847015655),
    ("010000"  ,'DKI Jakarta',"016000"  ,'Kota Jakarta Pusat',"016003",'Kec. Senen',1607847015655),
    ("010000"  ,'DKI Jakarta',"016000"  ,'Kota Jakarta Pusat',"016004",'Kec. Johar Baru',1607847015655),
    ("010000"  ,'DKI Jakarta',"016000"  ,'Kota Jakarta Pusat',"016005",'Kec. Cempaka Putih',1607847015655),
    ("010000"  ,'DKI Jakarta',"016000"  ,'Kota Jakarta Pusat',"016006",'Kec. Kemayoran',1607847015655),
    ("010000"  ,'DKI Jakarta',"016000"  ,'Kota Jakarta Pusat',"016007",'Kec. Sawah Besar',1607847015655),
    ("010000"  ,'DKI Jakarta',"016000"  ,'Kota Jakarta Pusat',"016008",'Kec. Gambir',1607847015655),
    ("010000"  ,'DKI Jakarta',"016100"  ,'Kota Jakarta Utara',"016101",'Kec. Penjaringan',1607847015655),
    ("010000"  ,'DKI Jakarta',"016100"  ,'Kota Jakarta Utara',"016102",'Kec. Pademangan',1607847015655),
    ("010000"  ,'DKI Jakarta',"016100"  ,'Kota Jakarta Utara',"016103",'Kec. Tanjung Priok',1607847015655),
    ("010000"  ,'DKI Jakarta',"016100"  ,'Kota Jakarta Utara',"016104",'Kec. Koja',1607847015655),
    ("010000"  ,'DKI Jakarta',"016100"  ,'Kota Jakarta Utara',"016105",'Kec. Kelapa Gading',1607847015655),
    ("010000"  ,'DKI Jakarta',"016100"  ,'Kota Jakarta Utara',"016106",'Kec. Cilincing',1607847015655),
    ("010000"  ,'DKI Jakarta',"016200"  ,'Kota Jakarta Barat',"016201",'Kec. Kembangan',1607847015655),
    ("010000"  ,'DKI Jakarta',"016200"  ,'Kota Jakarta Barat',"016202",'Kec. Kebon Jeruk',1607847015655),
    ("010000"  ,'DKI Jakarta',"016200"  ,'Kota Jakarta Barat',"016203",'Kec. Palmerah',1607847015655),
    ("010000"  ,'DKI Jakarta',"016200"  ,'Kota Jakarta Barat',"016204",'Kec. Grogol Petamburan',1607847015655),
    ("010000"  ,'DKI Jakarta',"016200"  ,'Kota Jakarta Barat',"016205",'Kec. Tambora',1607847015655),
    ("010000"  ,'DKI Jakarta',"016200"  ,'Kota Jakarta Barat',"016206",'Kec. Taman Sari',1607847015655),
    ("010000"  ,'DKI Jakarta',"016200"  ,'Kota Jakarta Barat',"016207",'Kec. Cengkareng',1607847015655),
    ("010000"  ,'DKI Jakarta',"016200"  ,'Kota Jakarta Barat',"016208",'Kec. Kali Deres',1607847015655),
    ("010000"  ,'DKI Jakarta',"016300"  ,'Kota Jakarta Selatan',"016301",'Kec. Jagakarsa',1607847015655),
    ("010000"  ,'DKI Jakarta',"016300"  ,'Kota Jakarta Selatan',"016302",'Kec. Pasar Minggu',1607847015655),
    ("010000"  ,'DKI Jakarta',"016300"  ,'Kota Jakarta Selatan',"016303",'Kec. Cilandak',1607847015655),
    ("010000"  ,'DKI Jakarta',"016300"  ,'Kota Jakarta Selatan',"016304",'Kec. Pesanggrahan',1607847015655),
    ("010000"  ,'DKI Jakarta',"016300"  ,'Kota Jakarta Selatan',"016305",'Kec. Kebayoran Lama',1607847015655),
    ("010000"  ,'DKI Jakarta',"016300"  ,'Kota Jakarta Selatan',"016306",'Kec. Kebayoran Baru',1607847015655),
    ("010000"  ,'DKI Jakarta',"016300"  ,'Kota Jakarta Selatan',"016307",'Kec. Mampang Prapatan',1607847015655),
    ("010000"  ,'DKI Jakarta',"016300"  ,'Kota Jakarta Selatan',"016308",'Kec. Pancoran',1607847015655),
    ("010000"  ,'DKI Jakarta',"016300"  ,'Kota Jakarta Selatan',"016309",'Kec. Tebet',1607847015655),
    ("010000"  ,'DKI Jakarta',"016300"  ,'Kota Jakarta Selatan',"016310",'Kec. Setia Budi',1607847015655),
    ("010000"  ,'DKI Jakarta',"016400"  ,'Kota Jakarta Timur',"016401",'Kec. Pasar Rebo',1607847015655),
    ("010000"  ,'DKI Jakarta',"016400"  ,'Kota Jakarta Timur',"016402",'Kec. Ciracas',1607847015655),
    ("010000"  ,'DKI Jakarta',"016400"  ,'Kota Jakarta Timur',"016403",'Kec. Cipayung',1607847015655),
    ("010000"  ,'DKI Jakarta',"016400"  ,'Kota Jakarta Timur',"016404",'Kec. Makasar',1607847015655),
    ("010000"  ,'DKI Jakarta',"016400"  ,'Kota Jakarta Timur',"016405",'Kec. Kramat Jati',1607847015655),
    ("010000"  ,'DKI Jakarta',"016400"  ,'Kota Jakarta Timur',"016406",'Kec. Jatinegara',1607847015655),
    ("010000"  ,'DKI Jakarta',"016400"  ,'Kota Jakarta Timur',"016407",'Kec. Duren Sawit',1607847015655),
    ("010000"  ,'DKI Jakarta',"016400"  ,'Kota Jakarta Timur',"016408",'Kec. Cakung',1607847015655),
    ("010000"  ,'DKI Jakarta',"016400"  ,'Kota Jakarta Timur',"016409",'Kec. Pulo Gadung',1607847015655);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ref_kecamatan`
--
-- ALTER TABLE `ref_kecamatan`
--   ADD PRIMARY KEY (`id_kecamatan`),
--   ADD KEY `ref_kecamatan_id_kota_foreign` (`id_kota`),
--   ADD KEY `ref_kecamatan_id_provinsi_foreign` (`id_provinsi`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ref_kecamatan`
--
-- ALTER TABLE `ref_kecamatan`
--   ADD CONSTRAINT `ref_kecamatan_id_kota_foreign` FOREIGN KEY (`id_kota`) REFERENCES `ref_kota` (`id_kota`) ON DELETE CASCADE ON UPDATE CASCADE,
--   ADD CONSTRAINT `ref_kecamatan_id_provinsi_foreign` FOREIGN KEY (`id_provinsi`) REFERENCES `ref_provinsi` (`id_provinsi`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
