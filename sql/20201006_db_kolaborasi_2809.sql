-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 06, 2020 at 04:41 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kolaborasi_2809`
--

-- --------------------------------------------------------

--
-- Table structure for table `knex_migrations`
--

CREATE TABLE `knex_migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `batch` int(11) DEFAULT NULL,
  `migration_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `knex_migrations`
--

INSERT INTO `knex_migrations` (`id`, `name`, `batch`, `migration_time`) VALUES
(6, '20200611202820_create_tb_user.js', 1, '2020-09-28 17:20:12'),
(7, '20200611222608_create_tb_user_siswa.js', 1, '2020-09-28 17:20:13'),
(8, '20200611224316_create_tb_user_guru.js', 1, '2020-09-28 17:20:14'),
(9, '20200710092058_create_ref_captcha.js', 1, '2020-09-28 17:20:15'),
(10, '20200921191952_create_tb_user_log.js', 1, '2020-09-28 17:20:15');

-- --------------------------------------------------------

--
-- Table structure for table `knex_migrations_lock`
--

CREATE TABLE `knex_migrations_lock` (
  `index` int(10) UNSIGNED NOT NULL,
  `is_locked` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `knex_migrations_lock`
--

INSERT INTO `knex_migrations_lock` (`index`, `is_locked`) VALUES
(1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ref_captcha`
--

CREATE TABLE `ref_captcha` (
  `id_captcha` int(10) UNSIGNED NOT NULL,
  `pertanyaan` varchar(100) NOT NULL,
  `jawaban` varchar(100) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `updated_at` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_captcha`
--

INSERT INTO `ref_captcha` (`id_captcha`, `pertanyaan`, `jawaban`, `created_at`, `updated_at`) VALUES
(1, 'Hasil dari tujuh ditambah lima adalah', '12', 1601314044023, 1601314044023),
(2, 'Berapakah hasil tiga ditambah lima', '8', 1601314044023, 1601314044023);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `kode_user` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` enum('admin','siswa','guru') NOT NULL,
  `kode_verifikasi` varchar(255) DEFAULT NULL,
  `kode_reset` varchar(255) DEFAULT NULL,
  `refresh_token` text DEFAULT NULL,
  `verified_at` bigint(20) DEFAULT NULL,
  `created_at` bigint(20) NOT NULL,
  `updated_at` bigint(20) NOT NULL,
  `deleted_at` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `kode_user`, `email`, `password`, `role`, `kode_verifikasi`, `kode_reset`, `refresh_token`, `verified_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
(24, '7534050c-5630-4b38-81f7-d544792127d2', 'swardiantara@gmail.com', '$2b$10$xcNYhLdh22bkz4.cRsTkC.16qbsO76cANy5r8.ALuliQagqESNrTa', 'siswa', '', NULL, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZFVzZXIiOjI0LCJyb2xlIjoic2lzd2EiLCJpYXQiOjE2MDEzOTY4MTAzNTksImV4cCI6MTYwMTM5NjgxMzk1OX0.DhHr6p7Gf6HwGuDxigBj5jV7IdhQaPMxJB9gPx0uezE', 1601393156467, 1601393026859, 1601396810361, NULL),
(25, '61ef3ebc-3487-41c3-9cd7-d1762df2b62b', 'swardyantara@gmail.com', '$2b$10$eaQyx4BiZUkMpdU/VLlvX.9e3QbeBjfsdKY/ytfizAJTaxMfa1ih.', 'siswa', '', NULL, NULL, 1601926079445, 1601925812191, 1601927809346, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_guru`
--

CREATE TABLE `tb_user_guru` (
  `id_guru` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) UNSIGNED NOT NULL,
  `nuptk` varchar(50) NOT NULL,
  `nip` varchar(50) DEFAULT NULL,
  `nama_lengkap` varchar(255) DEFAULT NULL,
  `jenis_kelamin` enum('laki-laki','perempuan') DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `foto_profile` text DEFAULT NULL,
  `bio` text DEFAULT NULL,
  `npsn` varchar(255) DEFAULT NULL,
  `nama_sekolah` varchar(255) DEFAULT NULL,
  `created_at` bigint(20) NOT NULL,
  `updated_at` bigint(20) NOT NULL,
  `deleted_at` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_log`
--

CREATE TABLE `tb_user_log` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_log` int(10) UNSIGNED NOT NULL,
  `jenis_log` enum('login','logout','ubah_sandi','reset_sandi','verifikasi','hapus','daftar') NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `negara` varchar(255) DEFAULT NULL,
  `kode_negara` varchar(10) DEFAULT NULL,
  `kota` varchar(255) DEFAULT NULL,
  `benua` varchar(50) DEFAULT NULL,
  `lintang` varchar(255) DEFAULT NULL,
  `bujur` varchar(255) DEFAULT NULL,
  `isp` varchar(255) DEFAULT NULL,
  `asn` varchar(255) DEFAULT NULL,
  `created_at` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_user_log`
--

INSERT INTO `tb_user_log` (`id_user`, `id_log`, `jenis_log`, `ip_address`, `negara`, `kode_negara`, `kota`, `benua`, `lintang`, `bujur`, `isp`, `asn`, `created_at`) VALUES
(24, 43, 'daftar', '0000:0000:0000:0000:0000:0000:0000:0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1601393026962),
(24, 44, 'verifikasi', '0000:0000:0000:0000:0000:0000:0000:0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1601393156470),
(24, 45, 'login', '0000:0000:0000:0000:0000:0000:0000:0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1601393759524),
(24, 46, 'login', '0000:0000:0000:0000:0000:0000:0000:0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1601394971883),
(24, 47, 'login', '0000:0000:0000:0000:0000:0000:0000:0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1601394980549),
(24, 48, 'login', '0000:0000:0000:0000:0000:0000:0000:0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1601395014850),
(24, 49, 'login', '0000:0000:0000:0000:0000:0000:0000:0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1601395115312),
(24, 50, 'login', '0000:0000:0000:0000:0000:0000:0000:0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1601396437113),
(24, 51, 'login', '0000:0000:0000:0000:0000:0000:0000:0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1601396444201),
(24, 52, 'login', '0000:0000:0000:0000:0000:0000:0000:0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1601396461413),
(24, 53, 'login', '0000:0000:0000:0000:0000:0000:0000:0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1601396489542),
(24, 54, 'login', '0000:0000:0000:0000:0000:0000:0000:0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1601396519817),
(24, 55, 'login', '0000:0000:0000:0000:0000:0000:0000:0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1601396545642),
(24, 56, 'login', '0000:0000:0000:0000:0000:0000:0000:0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1601396570454),
(24, 57, 'login', '0000:0000:0000:0000:0000:0000:0000:0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1601396594732),
(24, 58, 'login', '0000:0000:0000:0000:0000:0000:0000:0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1601396679652),
(24, 59, 'login', '0000:0000:0000:0000:0000:0000:0000:0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1601396713774),
(24, 60, 'login', '0000:0000:0000:0000:0000:0000:0000:0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1601396727886),
(24, 61, 'login', '0000:0000:0000:0000:0000:0000:0000:0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1601396810397),
(25, 62, 'daftar', '0000:0000:0000:0000:0000:0000:0000:0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1601925812287),
(25, 63, 'verifikasi', '0000:0000:0000:0000:0000:0000:0000:0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1601926079752),
(25, 64, 'login', '0000:0000:0000:0000:0000:0000:0000:0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1601926428498),
(25, 65, 'login', '0000:0000:0000:0000:0000:0000:0000:0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1601926751941),
(25, 66, 'logout', '0000:0000:0000:0000:0000:0000:0000:0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1601927545187),
(25, 67, 'login', '0000:0000:0000:0000:0000:0000:0000:0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1601927595926),
(25, 68, 'login', '0000:0000:0000:0000:0000:0000:0000:0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1601927665387),
(25, 69, 'login', '0000:0000:0000:0000:0000:0000:0000:0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1601927809383),
(25, 70, 'logout', '0000:0000:0000:0000:0000:0000:0000:0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1601927851954);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_siswa`
--

CREATE TABLE `tb_user_siswa` (
  `id_siswa` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `nisn` varchar(50) DEFAULT NULL,
  `nama_lengkap` varchar(255) DEFAULT NULL,
  `jenis_kelamin` enum('laki-laki','perempuan') DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `foto_profile` text DEFAULT NULL,
  `bio` text DEFAULT NULL,
  `npsn` varchar(255) DEFAULT NULL,
  `nama_sekolah` varchar(255) DEFAULT NULL,
  `created_at` bigint(20) NOT NULL,
  `updated_at` bigint(20) NOT NULL,
  `deleted_at` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_user_siswa`
--

INSERT INTO `tb_user_siswa` (`id_siswa`, `id_user`, `nisn`, `nama_lengkap`, `jenis_kelamin`, `tempat_lahir`, `tanggal_lahir`, `foto_profile`, `bio`, `npsn`, `nama_sekolah`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 24, NULL, NULL, NULL, NULL, NULL, 'http://localhost:3000/images/user/default/default-ava.jpg', NULL, NULL, NULL, 1601393026859, 1601393026859, NULL),
(3, 25, NULL, NULL, NULL, NULL, NULL, 'http://localhost:3000/images/user/default/default-ava.jpg', NULL, NULL, NULL, 1601925812191, 1601925812191, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `knex_migrations`
--
ALTER TABLE `knex_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `knex_migrations_lock`
--
ALTER TABLE `knex_migrations_lock`
  ADD PRIMARY KEY (`index`);

--
-- Indexes for table `ref_captcha`
--
ALTER TABLE `ref_captcha`
  ADD PRIMARY KEY (`id_captcha`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `tb_user_email_unique` (`email`);

--
-- Indexes for table `tb_user_guru`
--
ALTER TABLE `tb_user_guru`
  ADD PRIMARY KEY (`id_guru`),
  ADD UNIQUE KEY `tb_user_guru_nuptk_unique` (`nuptk`),
  ADD UNIQUE KEY `tb_user_guru_nip_unique` (`nip`),
  ADD KEY `tb_user_guru_id_user_foreign` (`id_user`);

--
-- Indexes for table `tb_user_log`
--
ALTER TABLE `tb_user_log`
  ADD PRIMARY KEY (`id_log`),
  ADD KEY `tb_user_log_id_user_foreign` (`id_user`);

--
-- Indexes for table `tb_user_siswa`
--
ALTER TABLE `tb_user_siswa`
  ADD PRIMARY KEY (`id_siswa`),
  ADD UNIQUE KEY `tb_user_siswa_nisn_unique` (`nisn`),
  ADD KEY `tb_user_siswa_id_user_foreign` (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `knex_migrations`
--
ALTER TABLE `knex_migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `knex_migrations_lock`
--
ALTER TABLE `knex_migrations_lock`
  MODIFY `index` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ref_captcha`
--
ALTER TABLE `ref_captcha`
  MODIFY `id_captcha` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `tb_user_guru`
--
ALTER TABLE `tb_user_guru`
  MODIFY `id_guru` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tb_user_log`
--
ALTER TABLE `tb_user_log`
  MODIFY `id_log` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `tb_user_siswa`
--
ALTER TABLE `tb_user_siswa`
  MODIFY `id_siswa` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_user_guru`
--
ALTER TABLE `tb_user_guru`
  ADD CONSTRAINT `tb_user_guru_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_user_log`
--
ALTER TABLE `tb_user_log`
  ADD CONSTRAINT `tb_user_log_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_user_siswa`
--
ALTER TABLE `tb_user_siswa`
  ADD CONSTRAINT `tb_user_siswa_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
