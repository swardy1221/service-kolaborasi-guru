const db = require('../../config/database');

const service = async (params, options) => {
    try {
        const page = parseInt(options.page || 1);
        const limit = parseInt(options.limit || 10);
        const offset = (page - 1) * limit;
        let kompetensiDasar = await db('ref_kompetensi_dasar')
            .where({
                id_kompetensi_inti: params.idKompetensiInti,
                id_mata_pelajaran: params.idMataPelajaran
            })
            .limit(limit)
            .offset(offset);

        if (!kompetensiDasar.length) return false
        return kompetensiDasar;
    } catch (error) {
        throw error;
    }
}

module.exports = service;