const response = require('../../config/responses');
const {
    byAllParams,
    byMapelKompetensiInti,
    byMapelQuery,
    byMapel,
    byKompetensiIntiQuery,
    byKompetensiInti,
    byQuery,
    byNoParams
} = require('../services');

const controller = async (req, res) => {
    try {
        let {
            idMataPelajaran,
            idKompetensiInti,
            query,
            page,
            limit
        } = req.query || "";
        query = query ? decodeURI(query) : "";
        console.log(query)
        if (idMataPelajaran) {
            if (idKompetensiInti) {
                if (query) {
                    //mapel ki query
                    let kompetensiDasar = await byAllParams({ query, idKompetensiInti, idMataPelajaran }, { limit, page })
                    if (!kompetensiDasar) return res.status(404).send(response.notFound('kompetensi dasar'));
                    return res.status(200).send(response.success(kompetensiDasar, 'kompetensi dasar'));
                } else {
                    // mapel ki
                    let kompetensiDasar = await byMapelKompetensiInti({ idKompetensiInti, idMataPelajaran }, { limit, page })
                    if (!kompetensiDasar) return res.status(404).send(response.notFound('kompetensi dasar'));
                    return res.status(200).send(response.success(kompetensiDasar, 'kompetensi dasar'));
                }
            } else {
                if (query) {
                    //mapel query
                    let kompetensiDasar = await byMapelQuery({ idMataPelajaran, query }, { limit, page })
                    if (!kompetensiDasar) return res.status(404).send(response.notFound('kompetensi dasar'));
                    return res.status(200).send(response.success(kompetensiDasar, 'kompetensi dasar'));
                } else {
                    // mapel
                    let kompetensiDasar = await byMapel({ idMataPelajaran }, { limit, page })
                    if (!kompetensiDasar) return res.status(404).send(response.notFound('kompetensi dasar'));
                    return res.status(200).send(response.success(kompetensiDasar, 'kompetensi dasar'));
                }
            }
        } else {
            if (idKompetensiInti) {
                if (query) {
                    //ki query
                    let kompetensiDasar = await byKompetensiIntiQuery({ idKompetensiInti, query }, { limit, page })
                    if (!kompetensiDasar) return res.status(404).send(response.notFound('kompetensi dasar'));
                    return res.status(200).send(response.success(kompetensiDasar, 'kompetensi dasar'));
                } else {
                    // ki
                    let kompetensiDasar = await byKompetensiInti({ idKompetensiInti }, { limit, page })
                    if (!kompetensiDasar) return res.status(404).send(response.notFound('kompetensi dasar'));
                    return res.status(200).send(response.success(kompetensiDasar, 'kompetensi dasar'));
                }
            } else {
                if (query) {
                    //query
                    let kompetensiDasar = await byQuery(query, { limit, page })
                    if (!kompetensiDasar) return res.status(404).send(response.notFound('kompetensi dasar'));
                    return res.status(200).send(response.success(kompetensiDasar, 'kompetensi dasar'));
                } else {
                    // no params
                    let kompetensiDasar = await byNoParams({ limit, page })
                    if (!kompetensiDasar) return res.status(404).send(response.notFound('kompetensi dasar'));
                    return res.status(200).send(response.success(kompetensiDasar, 'kompetensi dasar'));
                }
            }
        }
    } catch (error) {
        throw error;
    }
}

module.exports = controller;