const express = require("express");
const router = express.Router();
const search = require('./controllers/search');
const {
    authUser
} = require('../middlewares/index');

router.get('/', authUser, search);

module.exports = router;