const tambahService = require("../../sk-dikdasmen/services/create");

let tambahController = async (req, res) => {
  try {
    let buat = await tambahService(req);
    let hasil;
    if (buat) {
      hasil = {
        code: 200,
        message: "Berhasil menambahkan SK-Dikdasmen!",
        data: buat,
      };
    } else {
      hasil = {
        code: 400,
        message: "Terjadi kesalahan!",
      };
    }

    res.status(hasil.code).send(hasil);
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = tambahController;
