const express = require("express");
const router = express.Router();
const tambah = require("./controllers/create");

//Route bagi Model SK-Dikdasmen
router.post("/", tambah);

module.exports = router;
