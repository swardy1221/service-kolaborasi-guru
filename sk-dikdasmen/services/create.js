const db = require("../../config/database");

let create = async (req) => {
  try {
    // let tanggal = moment(req.body.tanggal, "YYYY-MM-DD");
    // console.log(tanggal);
    let buat = db("ref_sk_dikdasmen").insert({
      nomor: req.body.nomor,
      tanggal: req.body.tanggal,
      tentang: req.body.tentang,
      created_at: new Date().getTime()
    });
    return buat;
  } catch (error) {
    return error;
  }
};

module.exports = create;
