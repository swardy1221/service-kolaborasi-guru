const express = require("express");
const router = express.Router();
const getAll = require("./controllers/getAll");
// const kompetensiKeahlian = require("./controllers/getKompetensiKeahlian");
// const mataPelajaran = require("./controllers/getMataPelajaran");

//Route bagi Model Program Kehalian
/**
 * GET Method
 */
router.get("/", getAll);

module.exports = router;
