const db = require("../../config/database");
const response = require("../../config/responses");

let getAllService = async () => {
  try {
    let kompetensiInti = await db("ref_kompetensi_inti").select("*");

    if (kompetensiInti.length) {
      return response.success(kompetensiInti, "kompetensi inti");
    } else {
      return response.notFound("kompetensi inti");
    }
  } catch (error) {
    return response.serverError(error);
  }
};

module.exports = getAllService;
