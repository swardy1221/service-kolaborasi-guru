const getAllService = require("../services/getAll");
const searchService = require("../services/search");
const getBykodeKI = require("../services/getByKode");
const getByMapel = require("../services/getByMapel");
const getByMapelKode = require("../services/getByMapelKode");

let controller = async (req, res) => {
  try {
    let { query, idMapel, kodeKI } = req.query || "";
    let hasil;

    if (query) {
      hasil = await searchService(query);
    } else if (!idMapel) {
      if (!kodeKI) {
        hasil = await getAllService();
      } else {
        hasil = await getBykodeKI(kodeKI);
      }
    } else {
      if (!kodeKI) {
        hasil = await getByMapel(idMapel);
      } else {
        hasil = await getByMapelKode(idMapel, kodeKI);
      }
    }
    res.status(hasil.code).send(hasil);
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = controller;
