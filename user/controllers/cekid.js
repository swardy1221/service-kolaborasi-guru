const { required } = require("joi");

const { getLastIdUser } = require("../services/index");

const controller = async (req, res) => {
  try {
    let data = await getLastIdUser();
    res.status(200).send(data);
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = controller;
