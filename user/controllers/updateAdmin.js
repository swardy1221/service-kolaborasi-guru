const response = require('../../config/responses');
const updateAdmin = require('../services/updateAdmin');
const getUserById = require('../services/getUserById');

const controller = async (req, res) => {
    try {
        let oldAdmin = req.user.idUser;
        let newAdmin = req.params.idUser;
        let cekUser = await getUserById(newAdmin);
        if (!cekUser) return res.status(404).send(response.notFound('user'));
        let updatedAdmin = await updateAdmin(oldAdmin, newAdmin);
        if (!updatedAdmin) return res.status(404).send(response.badRequest('gagal mengubah admin'));
        return res.status(200).send(response.customSuccess('berhasil mengubah admin'));
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;