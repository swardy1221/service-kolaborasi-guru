const response = require('../../config/responses');
const {
    kirimPesan,
    getPesanById,
    getUserById,
} = require('../services/index');

const controller = async (req, res) => {
    try {
        let { idPenerima } = req.params || "";
        let idPengirim = req.user.idUser || "";
        let { isiPesan } = req.body || "";

        let cekPenerima = await getUserById(idPenerima);
        if (!cekPenerima) return res.status(404).send(response.notFound('user penerima'))

        let dataPesan = {
            idPengirim,
            idPenerima,
            isiPesan,
            createdAt: new Date().getTime()
        }

        let createKirimPesan = await kirimPesan(dataPesan);
        if (!createKirimPesan) return res.status(400).send(response.badRequest('gagal mengirim pesan'));

        let createdKirimPesan = await getPesanById(createKirimPesan);
        return res.status(201).send(response.created(createdKirimPesan, 'berhasil mengirim pesan'));
    } catch (error) {
        return res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;