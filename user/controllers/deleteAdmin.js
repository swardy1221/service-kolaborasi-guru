const response = require('../../config/responses');
const deleteAdmin = require('../services/deleteAdmin');
const getUserById = require('../services/getUserById');

const controller = async (req, res) => {
    try {
        let newAdmin = req.params.idUser;
        let cekUser = await getUserById(newAdmin);
        if (!cekUser) return res.status(404).send(response.notFound('user'));
        if (cekUser.is_admin != 1) return res.status(400).send(response.badRequest('akun bukan admin'));
        let updatedAdmin = await deleteAdmin(newAdmin);
        if (!updatedAdmin) return res.status(404).send(response.badRequest('gagal menghapus admin'));
        return res.status(200).send(response.customSuccess('berhasil menghapus admin'));
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;