const response = require('../../config/responses');
const { updateAvatar } = require('../services');

const controller = async (req, res) => {
    try {
        let updateStatus = await updateAvatar(req.user, req.file);

        if (!updateStatus) return res.status(400).send(response.badRequest('gagal mengubah avatar'))
        return res.status(200).send(response.updated(updateStatus, 'avatar'));
    } catch (error) {
        res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;
