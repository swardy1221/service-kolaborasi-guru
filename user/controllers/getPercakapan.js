const response = require('../../config/responses');
const getPercakapan = require('../services/getPercakapan');

const controller = async (req, res) => {
    try {
        let { idPenerima } = req.params || "";
        let idPengirim = req.user.idUser || "";
        console.log(idPenerima);
        console.log(idPengirim);

        let percakapan = await getPercakapan(idPengirim, idPenerima);
        if (!percakapan.length) return res.status(404).send(response.notFound('percakapan'));

        return res.status(200).send(response.success(percakapan, 'percakapan'));
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;