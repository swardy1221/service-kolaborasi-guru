const response = require("../../../config/responses");
const { auth, getUserById } = require("../../services/index");
const jwt = require("jsonwebtoken");
const { ACCESS_TOKEN_SECRET } = require("../../../config/authsecret");

const controller = async (req, res) => {
  try {
    let token = req.body.refreshToken || "";
    let callback = (err, payload) => {
      if (err) return err;
      return payload;
    };
    let cekToken = await auth.verifyToken("refresh", token, callback);
    let tokenInvalid = cekToken instanceof Error;
    if (tokenInvalid)
      return res.status(401).send(response.unauthorized("Token expired"));
    let user = await getUserById(cekToken.idUser);
    if (user.refresh_token != token)
      return res.status(401).send(response.unauthorized("Token expired"));
    let newAccessToken = jwt.sign(
      {
        idUser: cekToken.idUser,
        role: cekToken.role,
        iat: new Date().getTime(),
        exp: new Date().getTime() + 15 * 60, //15 minutes
      },
      ACCESS_TOKEN_SECRET
    );
    return res.status(200).send({
      code: 200,
      message: "Berhasil refresh token",
      accessToken: newAccessToken,
    });
  } catch (error) {
    return res.status(500).send(response.serverError(error));
  }
};

module.exports = controller;
