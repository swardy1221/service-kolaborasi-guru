const {
  auth,
  getMetaRequest,
  getUserByRefreshToken,
} = require("../../services/index");
const response = require("../../../config/responses");

const controller = async (req, res) => {
  try {
    const authHeader = req.headers["authorization"] || "";
    const token = authHeader && authHeader.split(" ")[1];
    let user = await getUserByRefreshToken(token);
    if (!user) return res.status(404).send(response.notFound("pengguna"));
    let meta = await getMetaRequest(req.clientIp || "");
    let status = await auth.logout(user, meta);
    if (!status)
      return res.status(400).send(response.badRequest("terjadi kesalahan"));

    return res.status(200).send(response.customSuccess("berhasil logout"));
  } catch (error) {
    return res.status(500).send(response.serverError(error));
  }
};

module.exports = controller;
