const { auth, getMetaRequest, getUserByKode } = require("../../services/index");
const response = require("../../../config/responses");

const controller = async (req, res) => {
  try {
    let { kodeUser, kodeVerifikasi } = req.params || "";
    let meta = await getMetaRequest(req.clientIp);
    let user = await getUserByKode(kodeUser);
    if (!user) return res.status(404).send(response.notFound("pengguna"));
    let status = await auth.verifikasiUser(user, kodeVerifikasi, meta);
    if (!status)
      return res
        .status(400)
        .send({ code: 400, message: "Kode Verifikasi tidak cocok" });
    return res.status(202).send(response.updated(status, "pengguna"));
  } catch (error) {
    return res.status(500).send(response.serverError(error));
  }
};

module.exports = controller;
