const jwt = require("jsonwebtoken");
const {
  auth,
  getUserByEmail,
  getMetaRequest,
} = require("../../services/index");
const response = require("../../../config/responses");
const { hasil } = require("../../../kompetensi-keahlian/services");

const controller = async (req, res) => {
  try {
    let { email, password } = req.body || "";
    // console.log(email)
    let user = await getUserByEmail(email);
    if (!user) return res.status(404).send(response.notFound("pengguna"));
    // console.log(user.refresh_token)
    // if (user.refresh_token) {
    //   let callback = (err, payload) => {
    //     if (err) return err;
    //     return payload;
    //   };
    //   let statusRefreshToken = await auth.verifyToken(
    //     "refresh",
    //     user.refresh_token,
    //     callback
    //   );
    //   let hasilError = statusRefreshToken instanceof Error;
    //   if (!hasilError)
    //     return res
    //       .status(403)
    //       .send(response.forbidden("Akun sedang digunakan, tidak dapat login"));
    // }

    let userData = await auth.cekPassword(user, password);
    if (!userData)
      return res.status(400).send(response.badRequest("Kata sandi salah"));
    let meta = await getMetaRequest(req.clientIp);
    let tokens = await auth.login(user, meta);
    if (!tokens)
      return res.status(500).send({ code: 500, message: "Server Error" });
    return res.status(200).send({
      id_user: user.id_user,
      role: user.role,
      is_admin: user.is_admin,
      ...tokens,
    });
  } catch (error) {
    return res.status(500).send(response.serverError(error));
  }
};

module.exports = controller;
