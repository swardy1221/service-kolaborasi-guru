const cekEmailService = require("../../services/auth/cekEmail");
const response = require("../../../config/responses");
const controller = async (req, res) => {
  try {
    let email = req.body.email || "";
    let hasil = await cekEmailService(email);
    if (hasil) return res.send(response.badRequest("Email sudah digunakan"));
    return res.send(response.customSuccess("Email belum digunakan"));
  } catch (error) {
    return res.status(500).send(response.serverError(error));
  }
};

module.exports = controller;
