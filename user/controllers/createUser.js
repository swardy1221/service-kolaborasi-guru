const {
  createUser,
  generateKodeVerifikasi,
  generateKodeUser,
  kirimEmailVerifikasi,
  getUserById,
  getMetaRequest,
} = require("../services/index");
// const getDetail = require("../services/getDetail");
const bcrypt = require("bcrypt");
const response = require("../../config/responses");
require("dotenv").config();

const controller = async (req, res) => {
  try {
    let { email, nuptk, password, role } = req.body || "";
    let meta = await getMetaRequest(req.clientIp);
    let kodeUser = await generateKodeUser();
    let kodeVerifikasi = await generateKodeVerifikasi();
    let createdAt = new Date().getTime();
    let updatedAt = new Date().getTime();
    password = await bcrypt.hash(password, 10);
    let fotoProfile = `${process.env.APP_URL}/image/default/avatar.png`;
    let dataUser = {
      email,
      nuptk,
      password,
      role,
      kodeUser,
      kodeVerifikasi,
      fotoProfile,
      createdAt,
      updatedAt,
    };

    let buatAkun = await createUser(dataUser, meta);

    if (!buatAkun)
      return res.send(response.serverError({ message: "Gagal membuat akun" }));
    let hasil;
    //send email
    // console.log(buatAkun.results);
    let createdUser = await getUserById(buatAkun);
    if (!createdUser) return res.send(response.notFound("pengguna"));
    // console.log(createdUser);
    let statusEmail = await kirimEmailVerifikasi(createdUser);
    if (!statusEmail)
      return res.send(response.badRequest("Email tidak terkirim"));
    return res.status(201).send(response.created(createdUser, "pengguna"));
  } catch (error) {
    return res.status(500).send(response.serverError(error));
  }
};

module.exports = controller;
