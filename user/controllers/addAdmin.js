const response = require('../../config/responses');
const addAdmin = require('../services/addAdmin');
const getUserById = require('../services/getUserById');

const controller = async (req, res) => {
    try {
        let newAdmin = req.params.idUser;
        let cekUser = await getUserById(newAdmin);
        if (!cekUser) return res.status(404).send(response.notFound('user'));
        if (cekUser.is_admin == 1) return res.status(400).send(response.badRequest('akun sudah admin'));
        let updatedAdmin = await addAdmin(newAdmin);
        if (!updatedAdmin) return res.status(404).send(response.badRequest('gagal menambah admin'));
        return res.status(200).send(response.customSuccess('berhasil menambah admin'));
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;