const { getDetail } = require("../services");

const controller = async (req, res) => {
  let { username } = req.params || "";
  try {
    let hasil = await getDetail(username);
    res.status(hasil.code).send(hasil);
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = controller;
