const response = require('../../config/responses');
const { update, getUserById, getGuruByIdUser } = require('../services')

const controller = async (req, res) => {
    try {
        let now = new Date().getTime();
        let { namaLengkap, nuptk, nip, npsn, namaSekolah, jenisKelamin, tempatLahir, tanggalLahir, bio, email, noHp } = req.body || "";
        let idUser = req.params.idUser || "";
        let dataUser = {
            idUser, namaLengkap, nuptk, nip, npsn, namaSekolah, jenisKelamin, tempatLahir, tanggalLahir, bio, email, noHp, updatedAt: now
        }
        let mapel = req.body.mapel || [];
        let riwayatPendidikan = req.body.riwayatPendidikan || [];
        dataUser.tanggalLahir = new Date(tanggalLahir);
        let guru = await getGuruByIdUser(idUser);
        if (!guru) return res.status(400).send(response.notFound('guru'))

        dataUser.mapel = [];
        dataUser.riwayatPendidikan = [];
        mapel.forEach(element => {
            dataUser.mapel.push({ id_guru: guru.id_guru, id_mata_pelajaran: element.idMataPelajaran, created_at: now })
        });

        riwayatPendidikan.forEach(element => {
            dataUser.riwayatPendidikan.push({
                id_guru: guru.id_guru,
                institusi: element.institusi,
                program_studi: element.programStudi,
                jenjang: element.jenjang,
                gelar_akademik: element.gelarAkademik,
                tahun_lulus: element.tahunLulus,
                created_at: now,
                updated_at: now
            })
        });

        let updateUser = await update(dataUser);
        if (!updateUser) return res.status(400).send(response.badRequest('gagal update user'));

        let updatedUser = await getUserById(idUser);
        return res.status(200).send(response.updated(updatedUser, 'user'));
    } catch (error) {
        res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;
