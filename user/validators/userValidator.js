const { check, validationResult } = require("express-validator");

let uppercase = new RegExp("^(?=.*[A-Z])");
let lowercase = new RegExp("^(?=.*[a-z])");
let numeric = new RegExp("^(?=.*[0-9])");
let character = new RegExp("^(?=.*[.!@#$%^&*])");
const cekEmail = require("../services/auth/cekEmail");

exports.userValidationRules = [
  check("email")
    .isEmail()
    .withMessage("Gunakan email yang valid")
    .custom(async (email) => {
      let cek = await cekEmail(email);
      if (cek) {
        throw new Error("Email sudah digunakan");
      }
    }),
  check("nuptk")
    .optional()
    .isLength({ min: 16, max: 16 })
    .withMessage("Harus berisi 16 karakter")
    .isInt()
    .withMessage("Hanya mengandung angka"),
  check("password")
    .isLength({
      min: 6,
    })
    .withMessage("Minimal 6 karakter")
    .matches(uppercase)
    .withMessage("Harus mengandung huruf besar")
    .matches(lowercase)
    .withMessage("Harus mengandung huruf kecil")
    .matches(numeric)
    .withMessage("Harus mengandung angka")
    .matches(character)
    .withMessage("Harus mengandung karakter spesial"),
  check("confirm_password", "Confirm password harus sama")
    .exists()
    .withMessage("Confirm password harus diisi")
    .custom((value, { req }) => value === req.body.password),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      const extractedErrors = [];
      errors
        .array()
        .map((err) => extractedErrors.push({ [err.param]: err.msg }));

      return res.status(422).json({
        code: 400,
        message: extractedErrors,
      });
    }
    next();
  },
];
