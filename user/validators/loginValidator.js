const { check, validationResult } = require("express-validator");

exports.loginValidationRules = [
  check("email").isEmail().withMessage("Gunakan email yang valid"),
  check("idCaptcha").isNumeric().withMessage("idCaptcha harus angka"),
  check("captchaJawaban").isNumeric().withMessage("captchaJawaban harus angka"),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      const extractedErrors = [];
      errors
        .array()
        .map((err) => extractedErrors.push({ [err.param]: err.msg }));

      return res.status(422).json({
        code: 400,
        message: extractedErrors,
      });
    }
    next();
  },
];
