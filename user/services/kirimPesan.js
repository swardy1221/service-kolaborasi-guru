const db = require('../../config/database');

const service = async (dataPesan) => {
    try {
        let kirimPesan = await db('tb_user_kirim_pesan').insert({
            id_pengirim: dataPesan.idPengirim,
            id_penerima: dataPesan.idPenerima,
            pesan: dataPesan.isiPesan,
            created_at: dataPesan.createdAt
        })

        if (!kirimPesan) return false;
        return kirimPesan;
    } catch (error) {
        throw error;
    }
}

module.exports = service;