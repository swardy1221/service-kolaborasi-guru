const db = require('../../config/database');

const service = async (newAdmin) => {
    const trxProvider = db.transactionProvider();
    const trx = await trxProvider();
    try {
        let userGuru = await trx('tb_user').where('id_user', newAdmin).update({
            is_admin: 1
        });

        if (!userGuru) {
            await trx.rollback();
            return false
        }
        await trx.commit();
        return true;
    } catch (error) {
        throw error;
    }
}

module.exports = service;