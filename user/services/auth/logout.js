const db = require("../../../config/database");

const service = async (user, meta) => {
  const trxProvider = db.transactionProvider();
  const trx = await trxProvider();
  try {
    let update = await trx("tb_user").where("id_user", user.id_user).update({
      refresh_token: null,
    });

    let userLog = await db("tb_user_log").transacting(trx).insert({
      id_user: user.id_user,
      jenis_log: "logout",
      ip_address: meta.ip,
      negara: meta.country,
      kode_negara: meta.country_code,
      kota: meta.city,
      benua: meta.continent,
      lintang: meta.latitude,
      bujur: meta.longtitude,
      isp: meta.org,
      asn: meta.asn,
      created_at: new Date().getTime(),
    });

    if (update && userLog) {
      await trx.commit();
      return true;
    }
    return false;
  } catch (error) {
    await trx.rollback();
    throw error;
  }
};

module.exports = service;
