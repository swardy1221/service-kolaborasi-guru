const jwt = require("jsonwebtoken");
const {
  ACCESS_TOKEN_SECRET,
  REFRESH_TOKEN_SECRET,
} = require("../../../config/authsecret");

const service = async (tokenType, token, callback) => {
  // console.log("verify token");
  try {
    let secret;
    switch (tokenType) {
      case "access":
        secret = ACCESS_TOKEN_SECRET;
        break;
      case "refresh":
        secret = REFRESH_TOKEN_SECRET;
      default:
        break;
    }
    return jwt.verify(token, secret, function (err, payload) {
      if (err) return callback(err, null);
      return callback(null, payload);
    });
  } catch (error) {
    console.log(error);
    throw error;
  }
};

module.exports = service;
