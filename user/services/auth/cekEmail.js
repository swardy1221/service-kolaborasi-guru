const db = require("../../../config/database");

const service = async (email) => {
  try {
    let cekEmail = await db("tb_user")
      .select("*")
      .where("email", email)
      .first();
    // console.log(cekEmail);
    if (!cekEmail) return false;
    return true;
  } catch (error) {
    throw error;
  }
};

module.exports = service;
