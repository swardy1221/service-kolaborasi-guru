const db = require("../../../config/database");
const response = require("../../../config/responses");
const bcrypt = require("bcrypt");

const service = async (user, password) => {
  try {
    let cocok = await bcrypt.compare(password, user.password);

    if (!cocok) return false;
    return true;
  } catch (error) {
    throw error;
  }
};

module.exports = service;
