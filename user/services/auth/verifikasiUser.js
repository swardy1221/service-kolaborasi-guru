const db = require("../../../config/database");

const service = async (user, kodeVerifikasi, meta) => {
  const trxProvider = db.transactionProvider();
  const trx = await trxProvider();
  try {
    if (user.kode_verifikasi != kodeVerifikasi) return false;

    let updateStatus = await trx("tb_user")
      .where("id_user", user.id_user)
      .update({
        kode_verifikasi: null, //hapus kode_verif di tb_user
        verified_at: new Date().getTime(),
        updated_at: new Date().getTime(),
      });

    let userLog = await db("tb_user_log").transacting(trx).insert({
      id_user: user.id_user,
      jenis_log: "verifikasi",
      ip_address: meta.ip,
      negara: meta.country,
      kode_negara: meta.country_code,
      kota: meta.city,
      benua: meta.continent,
      lintang: meta.latitude,
      bujur: meta.longtitude,
      isp: meta.org,
      asn: meta.asn,
      created_at: new Date().getTime(),
    });

    if (updateStatus && userLog) {
      await trx.commit();
      return updateStatus;
    }
    await trx.rollback();
    throw new Error("Terjadi kesalahan");
  } catch (error) {
    await trx.rollback();
    throw error;
  }
};

module.exports = service;
