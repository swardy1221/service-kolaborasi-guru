const db = require("../../../config/database");
const jwt = require("jsonwebtoken");
const {
  ACCESS_TOKEN_SECRET,
  REFRESH_TOKEN_SECRET,
} = require("../../../config/authsecret");

const service = async (user, meta) => {
  try {
    let accessToken = jwt.sign(
      {
        idUser: user.id_user,
        role: user.role,
        kodeUser: user.kode_user,
        isAdmin: user.is_admin,
        iat: new Date().getTime(),
        // exp: new Date().getTime() + 15 * 60, // 15 minutes
      },
      ACCESS_TOKEN_SECRET
    );

    let refreshToken = jwt.sign(
      {
        idUser: user.id_user,
        role: user.role,
        kodeUser: user.kode_user,
        isAdmin: user.is_admin,
        iat: new Date().getTime(),
        // exp: new Date().getTime() + 24 * 60 * 60, // 1 day
      },
      REFRESH_TOKEN_SECRET
    );

    let trxProvider = db.transactionProvider();
    let trx = await trxProvider();
    let simpanToken = await trx("tb_user")
      .where("id_user", user.id_user)
      .update({
        refresh_token: refreshToken,
        updated_at: new Date().getTime(),
      });

    let userLog = await db("tb_user_log").transacting(trx).insert({
      id_user: user.id_user,
      jenis_log: "login",
      ip_address: meta.ip,
      negara: meta.country,
      kode_negara: meta.country_code,
      kota: meta.city,
      benua: meta.continent,
      lintang: meta.latitude,
      bujur: meta.longtitude,
      isp: meta.org,
      asn: meta.asn,
      created_at: new Date().getTime(),
    });
    if (simpanToken && userLog) {
      await trx.commit();
      return { accessToken, refreshToken };
    }
    await trx.rollback();
    return false;
  } catch (error) {
    throw error;
  }
};

module.exports = service;
