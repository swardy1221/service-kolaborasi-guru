const db = require("../../config/database");

const service = async (user, meta) => {
  try {
    const trxProvider = db.transactionProvider();
    const trx = await trxProvider();
    try {
      let createdUser = await trx("tb_user").insert({
        kode_user: user.kodeUser,
        email: user.email,
        password: user.password,
        role: user.role,
        kode_verifikasi: user.kodeVerifikasi,
        created_at: user.createdAt,
        updated_at: user.updatedAt,
      });

      let createdRole;
      switch (user.role) {
        case "siswa":
          createdRole = await db("tb_user_siswa").transacting(trx).insert({
            id_user: createdUser,
            foto_profile: user.fotoProfile,
            created_at: user.createdAt,
            updated_at: user.updatedAt,
          });
          break;
        case "guru":
          createdRole = await db("tb_user_guru").transacting(trx).insert({
            id_user: createdUser,
            nuptk: user.nuptk,
            foto_profile: user.fotoProfile,
            created_at: user.createdAt,
            updated_at: user.updatedAt,
          });
        default:
          break;
      }
      let userLog = await db("tb_user_log").transacting(trx).insert({
        id_user: createdUser,
        jenis_log: "daftar",
        ip_address: meta.ip,
        negara: meta.country,
        kode_negara: meta.country_code,
        kota: meta.city,
        benua: meta.continent,
        lintang: meta.latitude,
        bujur: meta.longtitude,
        isp: meta.org,
        asn: meta.asn,
        created_at: new Date().getTime(),
      });

      if (user.role != "admin") {
        if (createdUser && createdRole && userLog) {
          await trx.commit();
          return createdUser;
        } else {
          await trx.rollback();
          return false;
        }
      } else {
        if (createdUser && userLog) {
          await trx.commit();
          return createdUser;
        } else {
          await trx.rollback();
          return false;
        }
      }
    } catch (error) {
      await trx.rollback();
      throw error;
    }
  } catch (error) {
    throw error;
  }
};

module.exports = service;
