const db = require("../../config/database");

const service = async (id) => {
  try {
    let user = await db("tb_user").where("id_user", id).first();

    if (!user) return false;
    return user;
  } catch (error) {
    throw error;
  }
};

module.exports = service;
