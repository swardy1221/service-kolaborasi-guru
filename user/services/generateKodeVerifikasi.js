const crypto = require("crypto");

const service = async (size = 20) => {
  return crypto.randomBytes(size).toString("hex").slice(0, size);
};

module.exports = service;
