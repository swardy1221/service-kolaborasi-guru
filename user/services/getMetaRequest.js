const iplocate = require("node-iplocate");
const db = require("../../config/database");
const response = require("../../config/responses");

const service = async (ip) => {
  try {
    let details = await iplocate(ip);
    return details;
  } catch (error) {
    console.log(error);
    return response.serverError(error);
  }
};

module.exports = service;
