const nodemailer = require("nodemailer");
require("dotenv").config();
const response = require("../../config/responses");
const transporter = require("../../config/mailer");

//Cek koneksi ke google account
// transporter.verify(function (error, success) {
//   if (error) {
//     console.log(error);
//   } else {
//     console.log("Server is ready to take our messages");
//   }
// });

// const transporter = nodemailer.createTransport({
//   service: "gmail",
//   auth: {
//     user: "swardyantara@gmail.com",
//     pass: "Swardyswat10",
//   },
// });

const service = async (user) => {
  try {
    const mailOptions = {
      from: `"Sistem Kolaborasi Guru" <${process.env.MAIL_UNAME}>`,
      to: user.email,
      subject: "Pendaftaran Akun Sistem Kolaborasi Guru",
      html: `<html><body>
              Klik link di bawah ini untuk melakukan verifikasi: <br/>
              <p><a href='${process.env.APP_URL}/user/${user.kode_user}/verifikasi/${user.kode_verifikasi}'>Verifikasi Akun Saya</a></p>
              Terima kasih
              <br/><br/>
              </body></html>`,
    };

    let status = await transporter.sendMail(mailOptions);
    console.log(status.response)
    if (status instanceof Error) {
      console.log(status)
      return false
    }
    return true;
  } catch (error) {
    throw error;
  }
};

module.exports = service;
