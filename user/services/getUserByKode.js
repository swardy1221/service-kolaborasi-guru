const db = require("../../config/database");

const service = async (kodeUser) => {
  try {
    let user = await db("tb_user").where("kode_user", kodeUser).first();

    if (!user) return false;
    return user;
  } catch (error) {
    throw error;
  }
};

module.exports = service;
