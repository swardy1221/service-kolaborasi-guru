const db = require('../../config/database');

const service = async (idPesan) => {
    try {
        let pesan = await db('tb_user_kirim_pesan').where('id_pesan', idPesan).first();

        if (!pesan) return false;
        return pesan;
    } catch (error) {
        throw error;
    }
}

module.exports = service;