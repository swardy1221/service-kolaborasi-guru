const db = require('../../config/database')
const fs = require('fs').promises;
require("dotenv").config();

const service = async (user, file) => {
    try {
        let role = user.role || "";
        // console.log(file.filename)
        // console.log(user.id_user)
        switch (role) {
            case 'admin':
                return false;
                break;
            case 'guru':
                let oldPath = await db('tb_user_guru').where('id_user', user.idUser).first();
                // console.log(oldPath);

                let path = await db('tb_user_guru').where('id_user', user.idUser).update({
                    foto_profile: `/image/avatar/${file.filename}`
                });
                if (!path) return false

                let updatedPath = await db('tb_user_guru').where('id_user', user.idUser).first();
                // console.log(updatedPath);
                if (!updatedPath) return false;
                let removeOldAvatar = await fs.unlink('public/' + oldPath.foto_profile);
                if (removeOldAvatar instanceof Error) return false
                return updatedPath;
                break;
            default:
                return false
                break
        }
    } catch (error) {
        throw error
    }
}

module.exports = service;
