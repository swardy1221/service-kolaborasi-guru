const db = require('../../config/database');

const service = async (user) => {
    try {
        const trxProvider = db.transactionProvider();
        const trx = await trxProvider();
        try {
            let updateduser = await db('tb_user').where("id_user", user.idUser).transacting(trx).update({ updated_at: user.updatedAt });
            if (!updateduser) {
                await trx.rollback();
                return false;
            }

            let updatedGuru = await db('tb_user_guru').where('id_user', user.idUser).transacting(trx).update({
                nama_lengkap: user.namaLengkap,
                nuptk: user.nuptk,
                nip: user.nip,
                npsn: user.npsn,
                nama_sekolah: user.namaSekolah,
                jenis_kelamin: user.jenisKelamin,
                tempat_lahir: user.tempatLahir,
                tanggal_lahir: user.tanggalLahir,
                bio: user.bio,
                email: user.email,
                no_hp: user.noHp,
                updated_at: user.updatedAt
            })
            if (!updatedGuru) {
                await trx.rollback();
                return false;
            }

            let updateMataPelajaran = await db('tb_guru_mengampu_mata_pelajaran').transacting(trx).insert(user.mapel);
            if (!updateMataPelajaran) {
                await trx.rollback();
                return false;
            }

            let updateRiwayatStudi = await db('tb_riwayat_pendidikan').transacting(trx).insert(user.riwayatPendidikan);
            if (!updateRiwayatStudi) {
                await trx.rollback();
                return false;
            }

            await trx.commit();
            return true;
        } catch (error) {
            throw error
        }
    } catch (error) {
        throw error
    }
}

module.exports = service;