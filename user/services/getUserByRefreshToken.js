const db = require("../../config/database");

const service = async (refreshToken) => {
  try {
    let user = await db("tb_user")
      .select("*")
      .where("refresh_token", refreshToken)
      .first();

    if (!user) return false;
    return user;
  } catch (error) {
    throw error;
  }
};

module.exports = service;
