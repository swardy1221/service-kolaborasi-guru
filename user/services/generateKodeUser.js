const { v4: uuidv4 } = require("uuid");

const service = async () => {
  return uuidv4();
};

module.exports = service;
