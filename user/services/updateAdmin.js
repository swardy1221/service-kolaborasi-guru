const db = require('../../config/database');

const service = async (oldAdmin, newAdmin) => {
    const trxProvider = db.transactionProvider();
    const trx = await trxProvider();
    try {
        let userGuru = await trx('tb_user').where('id_user', newAdmin).update({
            is_admin: 1
        });
        let removeAdmin = await db('tb_user').transacting(trx).where('id_user', oldAdmin).update({
            is_admin: 0
        });

        if (!userGuru || !removeAdmin) {
            await trx.rollback();
            return false
        }
        await trx.commit();
        return true;
    } catch (error) {
        throw error;
    }
}

module.exports = service;