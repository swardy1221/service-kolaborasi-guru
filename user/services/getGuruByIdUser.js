const db = require('../../config/database');

const service = async (idUser) => {
    try {
        let guru = await db('tb_user_guru').where('id_user', idUser).first();
        if (!guru) return false
        return guru
    } catch (error) {
        throw error
    }
}

module.exports = service