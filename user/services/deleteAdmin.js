const db = require('../../config/database');

const service = async (oldAdmin) => {
    const trxProvider = db.transactionProvider();
    const trx = await trxProvider();
    try {
        let userGuru = await trx('tb_user').where('id_user', oldAdmin).update({
            is_admin: 0
        });

        if (!userGuru) {
            await trx.rollback();
            return false
        }
        await trx.commit();
        return true;
    } catch (error) {
        throw error;
    }
}

module.exports = service;