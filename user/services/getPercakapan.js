const db = require('../../config/database');

const service = async (idPengirim, idPenerima) => {
    try {
        let percakapan = await db('tb_user_kirim_pesan').where('id_pengirim', 'in', [idPenerima, idPengirim]).where('id_penerima', 'in', [idPenerima, idPengirim]).orderBy('created_at', 'desc');
        if (!percakapan.length) return false;
        return percakapan;
    } catch (error) {
        throw error;
    }
}

module.exports = service;