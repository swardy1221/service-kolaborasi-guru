const db = require("../../config/database");

const service = async (email) => {
  try {
    let user = await db("tb_user").where("email", email).first();

    if (!user) return false;
    return user;
  } catch (error) {
    throw error;
  }
};

module.exports = service;
