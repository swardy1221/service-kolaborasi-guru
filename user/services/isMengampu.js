const db = require('../../config/database');

const service = async (idUser, idMataPelajaran) => {
    try {
        let queryIdGuru = await db('tb_user_guru').select('id_guru').where('id_user', idUser).first();
        // console.log(queryIdGuru)
        let idMapelDiampu = await db('tb_guru_mengampu_mata_pelajaran').where('id_guru', queryIdGuru.id_guru).select('id_mata_pelajaran');
        // console.log(idMapelDiampu)
        let cek = idMapelDiampu.some(item => item.id_mata_pelajaran == idMataPelajaran);
        // let cek = idMapelDiampu.includes(idMataPelajaran);
        // console.log(cek);
        if (!cek) return false;
        return true;
    } catch (error) {
        throw error;
    }
}

module.exports = service;