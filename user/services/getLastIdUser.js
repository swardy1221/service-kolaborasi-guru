const db = require("../../config/database");

const service = async () => {
  try {
    let id = await db("tb_user")
      .select("id_user")
      .orderBy("id_user", "desc")
      .first();
    return id;
  } catch (error) {
    console.log(error);
    return response.serverError(error);
  }
};

module.exports = service;
