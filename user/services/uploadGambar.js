const multer = require("multer");
const path = require("path");

const uploadGambarService = async (req) => {
  const storage = multer.diskStorage({
    destination: path.join(__dirname + "./../../public/images/"),
    filename: function (req, file, cb) {
      cb(
        null,
        file.fieldname + "-" + Date.now() + path.extname(file.originalname)
      );
    },
  });

  const upload = multer({
    storage: storage,
  }).single("foto_profile");

  upload(req, res, (err) => {
    if (err) throw err;
    return;
  });
};

module.exports = uploadGambarService;
