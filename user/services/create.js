const db = require("../../config/database");
const uploadFoto = require("./uploadGambar");
const bcrypt = require("bcrypt");

const createSiswa = async (req) => {
  const {
    username,
    password,
    role,
    email,
    nuptk,
    nip,
    nisn,
    nama_lengkap,
    jenis_kelamin,
    tempat_lahir,
    tanggal_lahir,
    foto_profile,
    bio,
    riwayat_pendidikan,
  } = req.body;

  const trxProvider = db.transactionProvider();
  const trx = await trxProvider();

  try {
    let encrypted = await bcrypt.hash(password, 10);
    let kode_verifikasi =
      new Date().getTime().toString(36) + Math.random().toString(36).slice(2);
    const createUser = await trx("tb_user").insert({
      username,
      password: encrypted,
      role,
      email,
      kode_verifikasi,
    });

    if (createUser) {
      if (role == "admin") {
        await trx.commit();
      } else if (role == "siswa") {
        //buat akun siswa

        // let alamat_foto = await uploadFoto(req);

        let createSiswa = await trx("tb_siswa").insert({
          id_user: createUser,
          nisn,
          nama_lengkap,
          jenis_kelamin,
          tempat_lahir,
          tanggal_lahir,
          // foto_profile: alamat_foto,
          bio,
        });

        if (createSiswa) {
        }
      } else {
        //buat akun guru
      }

      return createUser;
    } else {
      trx.rollback();
      return "Create User error";
    }
  } catch (error) {
    trx.rollback();
    return error;
  }
};

module.exports = createSiswa;
