const express = require("express");
const router = express.Router();
//Controllers
const {
  // refreshAccessToken,
  auth,
  createUser,
  update,
  updateAvatar,
  updateAdmin,
  addAdmin,
  kirimPesan,
  getPercakapan,
  deleteAdmin,
} = require("./controllers/index");
//Validators
const { userValidationRules } = require("./validators/userValidator");
const { loginValidationRules } = require("./validators/loginValidator");
//Middlewares
const cekCaptcha = require("../middlewares/cekCaptcha");
const clientIp = require("../middlewares/requestIp");
const authUser = require("../middlewares/authUser");
const isVerified = require('../middlewares/isVerified');
const authRole = require('../middlewares/authRole');
const avatarUploader = require('../middlewares/uploader/avatar');
const isOwner = require('../middlewares/mySelfById');

//Route bagi Model User
// /api/v1/user

/**
 * GET Method
 */
router.get("/cek-email", auth.cekEmail);
router.get(
  "/:kodeUser/verifikasi/:kodeVerifikasi",
  clientIp,
  auth.verifikasiUser
);
router.get("/refresh-token", auth.refreshAccessToken);
router.get('/:idPenerima/percakapan', authUser, getPercakapan);

/**
 * POST Method
 */
router.post("/logout", clientIp, auth.logout);
router.post("/", clientIp, cekCaptcha, userValidationRules, createUser);
router.post("/login", clientIp, loginValidationRules, cekCaptcha, isVerified, auth.login);
router.post('/:idUser/admin', authUser, authRole('admin'), addAdmin);
router.post('/:idPenerima/chat', authUser, kirimPesan);
/**
 * PUT Method
 */

router.put("/:idUser", update);
router.put("/:idUser/avatar", authUser, isOwner, avatarUploader.single('avatar'), updateAvatar);
router.put('/:idUser/admin', authUser, authRole('admin'), updateAdmin);

/**
 * DELETE METHOD
 */
router.delete('/:idUser/admin', authUser, authRole('admin'), deleteAdmin);



module.exports = router;
