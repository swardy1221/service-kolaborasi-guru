exports.up = function (knex, Promise) {
    return knex.schema.createTable(
        "tb_user_komentar_paket_soal",
        function (table) {
            table.increments('id_komentar').unsigned().primary();
            table
                .integer("id_user")
                .unsigned()
            table
                .foreign("id_user", "user_komentar_paket_soal")
                .references("id_user")
                .inTable("tb_user")
                .onDelete("CASCADE")
                .onUpdate("CASCADE");
            table.integer("id_paket_soal").unsigned();
            table
                .foreign("id_paket_soal", "komentar_paket_soal")
                .references("id_paket_soal")
                .inTable("tb_paket_soal")
                .onDelete("CASCADE")
                .onUpdate("CASCADE");
            table.text("komentar").notNullable();
            table.bigInteger("created_at").notNullable();
            table.bigInteger("updated_at").nullable();
            table.bigInteger("deleted_at").nullable();
        }
    );
};

exports.down = function (knex) {
    return knex.schema.dropTable("tb_user_komentar_paket_soal");
};
