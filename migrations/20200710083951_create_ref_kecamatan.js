exports.up = function (knex, Promise) {
  return knex.schema.createTable("ref_kecamatan", function (table) {
    table
      .string("id_provinsi", 100)
      .references("id_provinsi")
      .inTable("ref_provinsi")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.string("provinsi", 100).notNullable();
    table
      .string("id_kota", 100)
      .references("id_kota")
      .inTable("ref_kota")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.string("kota", 100).notNullable();
    table.string("id_kecamatan", 100).primary();
    table.string("kecamatan", 100).notNullable();
    table.bigInteger("created_at").notNullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("ref_kecamatan");
};
