exports.up = function (knex, Promise) {
  return knex.schema.createTable("ref_sekolah", function (table) {
    table
      .string("id_provinsi", 100)
      .references("id_provinsi")
      .inTable("ref_provinsi")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.string("provinsi", 100);
    table
      .string("id_kota", 100)
      .references("id_kota")
      .inTable("ref_kota")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.string("kota", 100);
    table
      .string("id_kecamatan", 100)
      .references("id_kecamatan")
      .inTable("ref_kecamatan")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.string("kecamatan", 100);
    table.string("id_sekolah", 100).primary();
    table.string("npsn", 100).notNullable();
    table.string("nama_sekolah", 100).notNullable();
    table.string("bentuk", 100).notNullable();
    table.enu("status", ["N", "S"]).notNullable();
    table.string("alamat").notNullable();
    table.string("lintang").nullable();
    table.string("bujur").nullable();
    table.bigInteger("created_at").notNullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("ref_sekolah");
};
