exports.up = function (knex) {
    return knex.schema.table('tb_perangkat_pembelajaran', table => {
        table.text('catatan_validasi').nullable().after('divalidasi_oleh');
        table.integer('dirujuk_sebanyak').nullable().after('dilihat_sebanyak').defaultTo(0);
    });
};

exports.down = function (knex) {
    return knex.schema.table('tb_perangkat_pembelajaran', table => {
        table.dropColumns('catatan_validasi', 'dirujuk_sebanyak');
    });
};