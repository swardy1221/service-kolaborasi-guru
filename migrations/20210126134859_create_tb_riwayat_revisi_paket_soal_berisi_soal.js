exports.up = function (knex, Promise) {
    return knex.schema.createTable("tb_riwayat_revisi_paket_soal_berisi_soal", function (table) {
        table
            .integer("id_riwayat_revisi_paket_soal")
            .unsigned()
        table
            .foreign('id_riwayat_revisi_paket_soal', 'revisi_paket_berisi_soal')
            .references("id_riwayat_revisi_paket_soal")
            .inTable("tb_riwayat_revisi_paket_soal")
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table
            .integer("id_soal")
            .unsigned()
        table
            .foreign('id_soal', 'soal_dalam_revisi_paket_soal')
            .references("id_soal")
            .inTable("tb_soal")
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table.integer("skor").unsigned().defaultTo(1);
        table.primary(["id_riwayat_revisi_paket_soal", "id_soal"]);
        table.bigInteger('created_at').notNullable();
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable("tb_riwayat_revisi_paket_soal_berisi_soal");
};
