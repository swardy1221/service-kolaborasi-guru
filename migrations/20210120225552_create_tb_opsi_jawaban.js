exports.up = function (knex, Promise) {
    return knex.schema.createTable("tb_opsi_jawaban", function (table) {
        table.increments("id_opsi_jawaban").unsigned().primary();
        table
            .integer("id_soal")
            .unsigned()
        table
            .foreign('id_soal', 'soal_opsi_jawaban')
            .references("id_soal")
            .inTable("tb_soal")
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table.text("deskripsi").notNullable();
        table.enu("tipe_file", ["gambar", "audio", "video", "lainnya"]).nullable();
        table.string("file").nullable();
        table.text("feedback").nullable();
        table.integer("is_kunci").notNullable();
        table.bigInteger("created_at").notNullable();
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable("tb_opsi_jawaban");
};