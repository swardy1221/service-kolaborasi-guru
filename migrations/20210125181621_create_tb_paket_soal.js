exports.up = function (knex, Promise) {
    return knex.schema.createTable("tb_paket_soal", function (table) {
        table.increments("id_paket_soal").unsigned().primary();
        table
            .integer("dibuat_oleh")
            .unsigned()
        table
            .foreign('dibuat_oleh', 'paket_soal_dibuat_oleh')
            .references("id_user")
            .inTable("tb_user")
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table.integer('id_mata_pelajaran')
            .unsigned()
        table
            .foreign('id_mata_pelajaran', 'mata_pelajaran_paket_soal')
            .references('id_mata_pelajaran')
            .inTable('ref_mata_pelajaran')
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table.string('nama_mapel');
        table.boolean("poin_otomatis").nullable(); //default true
        table.integer("poin_maksimal").nullable(); // jika poin otomatis, maka = jumlah soal
        table.string('kode_paket_soal').notNullable();
        table.text("judul").notNullable();
        table.text("deskripsi").notNullable();
        table.integer("jumlah_soal").unsigned().notNullable();
        table.integer("digunakan_sebanyak").unsigned().nullable().defaultTo(0);
        table.integer("jumlah_like").unsigned().nullable().defaultTo(0);
        table.integer("jumlah_dislike").unsigned().nullable().defaultTo(0);
        table.integer("jumlah_komentar").unsigned().nullable().defaultTo(0);
        table.integer("diunduh_sebanyak").unsigned().nullable().defaultTo(0);
        table.integer("dilihat_sebanyak").unsigned().nullable().defaultTo(0);
        table.enu("status_dokumen", ["revisi", "final", "terverifikasi", "ditolak"]).notNullable();
        table.enu("akses", ["terbit", "draft"]).notNullable();
        table
            .integer("divalidasi_oleh")
            .unsigned()
            .nullable();
        table
            .foreign("divalidasi_oleh", "paket_soal_divalidasi_oleh")
            .references("id_user")
            .inTable("tb_user")
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table.text('catatan_validasi').nullable();
        table.bigInteger("created_at").notNullable();
        table.bigInteger("updated_at").notNullable();
        table.bigInteger("deleted_at").nullable();
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable("tb_paket_soal");
};
