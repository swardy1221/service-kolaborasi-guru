exports.up = function (knex, Promise) {
    return knex.schema.createTable("tb_perangkat_pembelajaran", function (table) {
        table.increments("id_perangkat_pembelajaran").unsigned().primary();
        table
            .integer("id_user")
            .unsigned();
        table
            .foreign("id_user", "perangkat_dibuat_oleh")
            .references("id_user")
            .inTable("tb_user")
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table
            .enu("tipe_perangkat_pembelajaran", [
                "prota",
                "prosem",
                "silabus",
                "rpp",
                "lainnya",
            ])
            .notNullable();
        table
            .integer("id_mata_pelajaran")
            .unsigned();
        table
            .foreign("id_mata_pelajaran", "perangkat_milik_mapel")
            .references("id_mata_pelajaran")
            .inTable("ref_mata_pelajaran")
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table.string('nama_mapel').nullable();
        table
            .integer("id_kompetensi_keahlian")
            .unsigned();
        table
            .foreign("id_kompetensi_keahlian", "perangkat_milik_kompetensi_keahlian")
            .references("id_kompetensi_keahlian")
            .inTable("ref_kompetensi_keahlian")
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table.string('kompetensi_keahlian').nullable();
        table.string("judul").notNullable();
        table.text("deskripsi").notNullable();
        table.string("file").nullable();
        table.string('sampul').nullable();
        table.integer("jumlah_like").unsigned().nullable().defaultTo(0);
        table.integer("jumlah_dislike").unsigned().nullable().defaultTo(0);
        table.integer("jumlah_komentar").unsigned().nullable().defaultTo(0);
        table.integer("diunduh_sebanyak").unsigned().nullable().defaultTo(0);
        table.integer("dilihat_sebanyak").unsigned().nullable().defaultTo(0);
        table.enu("status_dokumen", ["revisi1", "revisi2", "revisi3", "final", "terverifikasi", "ditolak"]).notNullable();
        table.enu("akses", ["terbit", "draft"]).notNullable();
        table
            .integer("divalidasi_oleh")
            .unsigned()
            .nullable();
        table
            .foreign("divalidasi_oleh", "perangkat_divalidasi_oleh")
            .references("id_user")
            .inTable("tb_user")
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table.bigInteger("created_at").notNullable();
        table.bigInteger("updated_at").notNullable();
        table.bigInteger("deleted_at").nullable();
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable("tb_perangkat_pembelajaran");
};
