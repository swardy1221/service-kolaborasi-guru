exports.up = function (knex) {
    return knex.schema.table('tb_riwayat_revisi_perangkat_pembelajaran', table => {
        table.string('kode_revisi').notNullable().after('id_perangkat_pembelajaran');
    });
};

exports.down = function (knex) {
    return knex.schema.table('tb_riwayat_revisi_perangkat_pembelajaran', table => {
        table.dropColumn('kode_revisi');
    });
};