exports.up = function (knex) {
    return knex.schema.table('tb_soal', table => {
        table.integer('dilihat_sebanyak').unsigned().defaultTo(0).after('digunakan_sebanyak');
        table.text('catatan_validasi').nullable().after('divalidasi_oleh');
        table.enu('akses', ["terbit", "draft"]).notNullable().defaultTo('draft').after('status');
        // table.text('catatan_validasi').nullable().after('divalidasi_oleh');
        // table.integer('dirujuk_sebanyak').nullable().after('dilihat_sebanyak').defaultTo(0);
    });
};

exports.down = function (knex) {
    return knex.schema.table('tb_soal', table => {
        table.dropColumns('dilihat_sebanyak', 'catatan_validasi', 'akses');
    });
};