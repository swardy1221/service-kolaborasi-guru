exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_user", function (table) {
    table.increments("id_user").unsigned().primary();
    table.string("kode_user", 255).notNullable();
    table.string("email", 255).unique().notNullable();
    table.string("password", 255).notNullable();
    table.enu("role", ["admin", "siswa", "guru"]).notNullable();
    table.string("kode_verifikasi", 255).nullable();
    table.string("kode_reset", 255).nullable();
    table.text("refresh_token").nullable();
    table.bigInteger("verified_at").nullable();
    table.bigInteger("created_at").notNullable();
    table.bigInteger("updated_at").notNullable();
    table.bigInteger("deleted_at").nullable();
  });
};

exports.down = function (knex, Promise) {
  return knex.schema.dropTable("tb_user");
};
