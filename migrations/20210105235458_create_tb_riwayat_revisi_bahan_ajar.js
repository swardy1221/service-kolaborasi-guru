exports.up = function (knex, Promise) {
    return knex.schema.createTable(
        "tb_riwayat_revisi_bahan_ajar",
        function (table) {
            table
                .increments("id_riwayat_revisi_bahan_ajar")
                .unsigned()
                .primary();
            table.integer("id_bahan_ajar").unsigned();
            table
                .foreign("id_bahan_ajar", "id_bahan_foreign_revisi")
                .references("id_bahan_ajar")
                .inTable("tb_bahan_ajar")
                .onDelete("CASCADE")
                .onUpdate("CASCADE");
            table.string("kode_revisi").unique().notNullable();
            table.text("catatan_revisi").notNullable();
            table
                .enu("tipe_bahan_ajar", [
                    "modul",
                    "video",
                    "ppt",
                    "lainnya"
                ])
                .notNullable();
            table
                .integer("id_mata_pelajaran")
                .unsigned();
            table
                .foreign("id_mata_pelajaran", "revisi_bahan_milik_mapel")
                .references("id_mata_pelajaran")
                .inTable("ref_mata_pelajaran")
                .onDelete("CASCADE")
                .onUpdate("CASCADE");
            table.string('nama_mapel').nullable();
            table
                .integer("id_kompetensi_keahlian")
                .unsigned();
            table
                .foreign("id_kompetensi_keahlian", "revisi_bahan_kompetensi_keahlian")
                .references("id_kompetensi_keahlian")
                .inTable("ref_kompetensi_keahlian")
                .onDelete("CASCADE")
                .onUpdate("CASCADE");
            table.string('kompetensi_keahlian').nullable();
            table.string("judul").notNullable();
            table.text("deskripsi").notNullable();
            table.string("file").notNullable();
            table.string('sampul').nullable();
            table.enu("status_dokumen", ["revisi1", "revisi2", "revisi3", "final", "terverifikasi", "ditolak"]).notNullable();
            table.bigInteger("created_at").notNullable();
        }
    );
};

exports.down = function (knex) {
    return knex.schema.dropTable("tb_riwayat_revisi_bahan_ajar");
};
