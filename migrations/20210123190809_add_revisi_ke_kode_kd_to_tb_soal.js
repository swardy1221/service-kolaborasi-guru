exports.up = function (knex) {
    return knex.schema.table('tb_soal', table => {
        table.integer('revisi_ke').unsigned().defaultTo(1).after('kunci_essay');
        table.string('kode_kompetensi_dasar', 10).nullable().after('id_kompetensi_dasar');
        // table.text('catatan_validasi').nullable().after('divalidasi_oleh');
        // table.integer('dirujuk_sebanyak').nullable().after('dilihat_sebanyak').defaultTo(0);
    });
};

exports.down = function (knex) {
    return knex.schema.table('tb_soal', table => {
        table.dropColumns('revisi_ke', 'kode_kompetensi_dasar');
    });
};