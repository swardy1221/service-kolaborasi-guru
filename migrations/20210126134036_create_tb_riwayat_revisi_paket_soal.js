exports.up = function (knex, Promise) {
    return knex.schema.createTable("tb_riwayat_revisi_paket_soal", function (table) {
        table.increments('id_riwayat_revisi_paket_soal').unsigned().primary();
        table
            .integer("id_paket_soal")
            .unsigned();
        table
            .foreign('id_paket_soal', 'riwayat_revisi_paket_soal')
            .references('id_paket_soal')
            .inTable('tb_paket_soal')
            .onDelete('CASCADE')
            .onUpdate('CASCADE')
        table
            .integer("dibuat_oleh")
            .unsigned()
        table
            .foreign('dibuat_oleh', 'riwayat_paket_soal_dibuat_oleh')
            .references("id_user")
            .inTable("tb_user")
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table.integer('id_mata_pelajaran')
            .unsigned()
        table
            .foreign('id_mata_pelajaran', 'mapel_riwayat_paket_soal')
            .references('id_mata_pelajaran')
            .inTable('ref_mata_pelajaran')
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table.string('nama_mapel');
        table.boolean("poin_otomatis").nullable(); //default true
        table.integer("poin_maksimal").nullable(); // jika poin otomatis, maka = jumlah soal
        table.integer('revisi_ke').notNullable();
        table.text("catatan_revisi").notNullable();
        table.text("judul").notNullable();
        table.text("deskripsi").notNullable();
        table.integer("jumlah_soal").unsigned().notNullable();
        table.enu("status_dokumen", ["revisi", "final", "terverifikasi", "ditolak"]).notNullable();
        table.enu("akses", ["terbit", "draft"]).notNullable();
        table.bigInteger("created_at").notNullable();
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable("tb_riwayat_revisi_paket_soal");
};
