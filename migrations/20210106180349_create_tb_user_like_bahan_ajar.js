exports.up = function (knex, Promise) {
    return knex.schema.createTable(
        "tb_user_like_bahan_ajar",
        function (table) {
            table
                .integer("id_user")
                .unsigned();
            table
                .foreign('id_user', 'user_like_bahan')
                .references("id_user")
                .inTable("tb_user")
                .onDelete("CASCADE")
                .onUpdate("CASCADE");
            table.integer("id_bahan_ajar")
                .unsigned();
            table
                .foreign("id_bahan_ajar", "id_bahan_foreign_like")
                .references("id_bahan_ajar")
                .inTable("tb_bahan_ajar")
                .onDelete("CASCADE")
                .onUpdate("CASCADE");
            table.primary(["id_user", "id_bahan_ajar"]);
            table.bigInteger("created_at").notNullable();
        }
    );
};

exports.down = function (knex) {
    return knex.schema.dropTable("tb_user_like_bahan_ajar");
};
