exports.up = function (knex, Promise) {
    return knex.schema.createTable("tb_riwayat_revisi_soal", function (table) {
        table.increments("id_riwayat_revisi_soal").unsigned().primary();
        table
            .integer("id_soal")
            .unsigned()
        table
            .foreign('id_soal', 'riwayat_revisi_soal')
            .references("id_soal")
            .inTable("tb_soal")
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table.integer('id_mata_pelajaran')
            .unsigned()
        table
            .foreign('id_mata_pelajaran', 'mata_pelajaran_revisi_soal')
            .references('id_mata_pelajaran')
            .inTable('ref_mata_pelajaran')
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table.string('nama_mapel');
        table
            .integer("id_kompetensi_dasar")
            .unsigned();
        table
            .foreign('id_kompetensi_dasar', 'kd_revisi_soal')
            .references("id_kompetensi_dasar")
            .inTable("ref_kompetensi_dasar")
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table.string('kode_kompetensi_dasar', 10).nullable();
        table
            .integer("id_kisi_kisi")
            .unsigned()
        table
            .foreign('id_kisi_kisi', 'kisi_kisi_revisi_soal')
            .references("id_kisi_kisi")
            .inTable("tb_kisi_kisi")
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table.string("kode_revisi").unique().notNullable();
        table.text("catatan_revisi").notNullable();
        table
            .enu("tipe_soal", ["pilihan_ganda", "isian_singkat", "benar_salah"])
            .notNullable();
        table.integer("jumlah_jawaban").notNullable().defaultTo(1);
        table.text("teks_soal").notNullable();
        table.enu("tipe_file", ["gambar", "audio", "video", 'dokumen', "lainnya"]).nullable();
        table.string("file").nullable();
        table
            .enu("tingkat_kesulitan", ["c1", "c2", "c3", "c4", "c5", "c6"])
            .notNullable();
        table.string("kunci_essay").nullable();
        table.integer('revisi_ke').unsigned().defaultTo(1);
        table.bigInteger("created_at").notNullable();
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable("tb_riwayat_revisi_soal");
};
