exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_guru_mengampu_mata_pelajaran", function (
    table
  ) {
    table
      .integer("id_guru")
      .unsigned()
      .references("id_guru")
      .inTable("tb_user_guru")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .integer("id_mata_pelajaran")
      .unsigned()
      .references("id_mata_pelajaran")
      .inTable("ref_mata_pelajaran")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.primary(["id_guru", "id_mata_pelajaran"]);
    table.bigInteger("created_at").notNullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_guru_mengampu_mata_pelajaran");
};
