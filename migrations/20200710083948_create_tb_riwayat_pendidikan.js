exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_riwayat_pendidikan", function (table) {
    table.increments("id_riwayat_pendidikan").unsigned().primary();
    table
      .integer("id_guru", 11)
      .unsigned()
      .references("id_guru")
      .inTable("tb_user_guru")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.string("institusi", 100).notNullable();
    table.string("program_studi", 100).notNullable();
    table.string("jenjang", 10).notNullable();
    table.string("gelar_akademik", 20).notNullable();
    table.string("tahun_lulus", 10).notNullable();
    table.bigInteger("created_at").notNullable();
    table.bigInteger("updated_at").notNullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_riwayat_pendidikan");
};
