exports.up = function (knex, Promise) {
    return knex.schema.createTable(
        "tb_referensi",
        function (table) {
            table
                .increments("id_referensi")
                .unsigned()
                .primary();
            table.enu('entitas', ['perangkat_pembelajaran', 'bahan_ajar']).notNullable();
            table.integer('id_entitas').unsigned().notNullable();
            table.integer('id_entitas_referensi').unsigned().notNullable();
            table.text('judul_referensi').notNullable();
            table.bigInteger("created_at").notNullable();
        }
    );
};

exports.down = function (knex) {
    return knex.schema.dropTable("tb_referensi");
};
