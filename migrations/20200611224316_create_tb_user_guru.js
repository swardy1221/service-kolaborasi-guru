exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_user_guru", function (table) {
    table.increments("id_guru").unsigned().primary();
    table
      .integer("id_user", 11)
      .unsigned()
      .references("id_user")
      .inTable("tb_user")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.string("nuptk", 50).unique().notNullable();
    table.string("nip", 50).unique().nullable();
    table.string("nama_lengkap", 255).nullable();
    table.enu("jenis_kelamin", ["laki-laki", "perempuan"]).nullable();
    table.string("tempat_lahir", 100).nullable();
    table.date("tanggal_lahir").nullable();
    table.text("foto_profile").nullable();
    table.text("bio", ["mediumtext"]);
    table.string("npsn").nullable();
    table.string("nama_sekolah").nullable();
    table.bigInteger("created_at").notNullable();
    table.bigInteger("updated_at").notNullable();
    table.bigInteger("deleted_at").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_user_guru");
};
