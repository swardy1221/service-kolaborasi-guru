exports.up = function (knex, Promise) {
    return knex.schema.createTable("tb_paket_soal_berisi_soal", function (table) {
        table
            .integer("id_paket_soal")
            .unsigned()
        table
            .foreign('id_paket_soal', 'paketSoal_berisi_soal')
            .references("id_paket_soal")
            .inTable("tb_paket_soal")
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table
            .integer("id_soal")
            .unsigned()
        table
            .foreign('id_soal', 'soal_dalam_paket_soal')
            .references("id_soal")
            .inTable("tb_soal")
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table.integer("skor").unsigned().defaultTo(1);
        table.primary(["id_paket_soal", "id_soal"]);
        table.bigInteger('created_at').notNullable();
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable("tb_paket_soal_berisi_soal");
};
