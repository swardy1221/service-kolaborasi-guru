exports.up = function (knex) {
    return knex.schema.table('tb_bahan_ajar', table => {
        table.text('catatan_validasi').nullable().after('divalidasi_oleh');
        table.integer('dirujuk_sebanyak').nullable().after('dilihat_sebanyak').defaultTo(0);
    });
};

exports.down = function (knex) {
    return knex.schema.table('tb_bahan_ajar', table => {
        table.dropColumns('catatan_validasi', 'dirujuk_sebanyak');
    });
};