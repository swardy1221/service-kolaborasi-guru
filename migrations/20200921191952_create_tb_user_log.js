exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_user_log", function (table) {
    table
      .integer("id_user")
      .unsigned()
      .references("id_user")
      .inTable("tb_user")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.increments("id_log").unsigned().primary();
    table
      .enu("jenis_log", [
        "login",
        "logout",
        "ubah_sandi",
        "reset_sandi",
        "verifikasi",
        "hapus",
        "daftar",
      ])
      .notNullable();
    table.string("ip_address", 255).notNullable();
    table.string("negara", 255).nullable();
    table.string("kode_negara", 10).nullable();
    table.string("kota", 255).nullable();
    table.string("benua", 50).nullable();
    table.string("lintang", 255).nullable();
    table.string("bujur", 255).nullable();
    table.string("isp").nullable();
    table.string("asn").nullable();
    table.bigInteger("created_at").notNullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_user_log");
};
