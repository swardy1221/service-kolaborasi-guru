exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_user_siswa", function (table) {
    table.increments("id_siswa").unsigned().primary();
    table
      .integer("id_user")
      .unsigned()
      .references("id_user")
      .inTable("tb_user")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.string("nisn", 50).unique().nullable();
    table.string("nama_lengkap", 255).nullable();
    table.enu("jenis_kelamin", ["laki-laki", "perempuan"]).nullable();
    table.string("tempat_lahir", 100).nullable();
    table.date("tanggal_lahir").nullable();
    table.text("foto_profile").nullable();
    table.text("bio", ["mediumtext"]);
    table.string("npsn").nullable();
    table.string("nama_sekolah").nullable();
    table.bigInteger("created_at").notNullable();
    table.bigInteger("updated_at").notNullable();
    table.bigInteger("deleted_at").nullable();
  });
};

exports.down = function (knex, Promise) {
  return knex.schema.dropTable("tb_user_siswa");
};
