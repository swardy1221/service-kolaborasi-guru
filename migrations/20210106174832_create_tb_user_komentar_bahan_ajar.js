exports.up = function (knex, Promise) {
    return knex.schema.createTable(
        "tb_user_komentar_bahan_ajar",
        function (table) {
            table
                .integer("id_user")
                .unsigned()
            table
                .foreign("id_user", "user_komentar_bahan")
                .references("id_user")
                .inTable("tb_user")
                .onDelete("CASCADE")
                .onUpdate("CASCADE");
            table.integer("id_bahan_ajar").unsigned();
            table
                .foreign("id_bahan_ajar", "komentar_bahan")
                .references("id_bahan_ajar")
                .inTable("tb_bahan_ajar")
                .onDelete("CASCADE")
                .onUpdate("CASCADE");
            table.text("komentar").notNullable();
            // table.primary(["id_user", "id_perangkat_pembelajaran"]);
            table.bigInteger("created_at").notNullable();
            table.bigInteger("updated_at").nullable();
            table.bigInteger("deleted_at").nullable();
        }
    );
};

exports.down = function (knex) {
    return knex.schema.dropTable("tb_user_komentar_bahan_ajar");
};
