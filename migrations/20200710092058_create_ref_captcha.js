exports.up = function (knex, Promise) {
  return knex.schema.createTable("ref_captcha", function (table) {
    table.increments("id_captcha").unsigned().primary();
    table.string("pertanyaan", 100).notNullable();
    table.string("jawaban", 100).notNullable();
    table.bigInteger("created_at").notNullable();
    table.bigInteger("updated_at").notNullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("ref_captcha");
};
