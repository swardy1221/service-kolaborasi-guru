exports.up = function (knex, Promise) {
    return knex.schema.createTable("tb_kisi_kisi", function (table) {
        table.increments("id_kisi_kisi").unsigned().primary();
        table
            .integer("id_kompetensi_dasar")
            .unsigned()
        table
            .foreign('id_kompetensi_dasar', 'kd_kisi_kisi')
            .references("id_kompetensi_dasar")
            .inTable("ref_kompetensi_dasar")
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table
            .integer("id_user")
            .unsigned()
        table
            .foreign('id_user', 'user_kisi_kisi')
            .references("id_user")
            .inTable("tb_user")
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table.text("deskripsi").notNullable();
        table.bigInteger("created_at").notNullable();
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable("tb_kisi_kisi");
};
