exports.up = function (knex, Promise) {
    return knex.schema.createTable(
        "tb_riwayat_revisi_perangkat_pembelajaran",
        function (table) {
            table
                .increments("id_riwayat_revisi_perangkat_pembelajaran")
                .unsigned()
                .primary();
            table.integer("id_perangkat_pembelajaran").unsigned();
            table
                .foreign("id_perangkat_pembelajaran", "id_revisi_foreign_revisi")
                .references("id_perangkat_pembelajaran")
                .inTable("tb_perangkat_pembelajaran")
                .onDelete("CASCADE")
                .onUpdate("CASCADE");
            // table.string("kode_revisi").unique().notNullable();
            table.text("catatan_revisi").notNullable();
            table
                .enu("tipe_perangkat_pembelajaran", [
                    "prota",
                    "prosem",
                    "silabus",
                    "rpp",
                    "lainnya"
                ])
                .notNullable();
            table
                .integer("id_mata_pelajaran")
                .unsigned();
            table
                .foreign("id_mata_pelajaran", "revisi_milik_mapel")
                .references("id_mata_pelajaran")
                .inTable("ref_mata_pelajaran")
                .onDelete("CASCADE")
                .onUpdate("CASCADE");
            table.string('nama_mapel').nullable();
            table
                .integer("id_kompetensi_keahlian")
                .unsigned();
            table
                .foreign("id_kompetensi_keahlian", "revisi_milik_kompetensi_keahlian")
                .references("id_kompetensi_keahlian")
                .inTable("ref_kompetensi_keahlian")
                .onDelete("CASCADE")
                .onUpdate("CASCADE");
            table.string('kompetensi_keahlian').nullable();
            table.string("judul").notNullable();
            table.text("deskripsi").notNullable();
            table.string("file").notNullable();
            table.string('sampul').nullable();
            table.enu("status_dokumen", ["revisi1", "revisi2", "revisi3", "revisi_final", "terverifikasi", "verifikasi_ditolak"]).notNullable();
            table.bigInteger("created_at").notNullable();
        }
    );
};

exports.down = function (knex) {
    return knex.schema.dropTable("tb_riwayat_revisi_perangkat_pembelajaran");
};
