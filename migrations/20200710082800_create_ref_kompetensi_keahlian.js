exports.up = function (knex, Promise) {
  return knex.schema.createTable("ref_kompetensi_keahlian", function (table) {
    table
      .integer("id_sk_dikdasmen", 11)
      .unsigned()
      .references("id_sk_dikdasmen")
      .inTable("ref_sk_dikdasmen")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .integer("id_bidang_keahlian", 11)
      .unsigned()
      .references("id_bidang_keahlian")
      .inTable("ref_bidang_keahlian")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.string("bidang_keahlian", 100).notNullable();
    table
      .integer("id_program_keahlian", 11)
      .unsigned()
      .references("id_program_keahlian")
      .inTable("ref_program_keahlian")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.string("program_keahlian", 100).notNullable();
    table.increments("id_kompetensi_keahlian").unsigned().primary();
    table.string("kode_kompetensi_keahlian", 10).notNullable();
    table.string("kompetensi_keahlian", 100).notNullable();
    table.integer("lama_studi").unsigned().notNullable();
    table.bigInteger("created_at").notNullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("ref_kompetensi_keahlian");
};
