exports.up = function (knex, Promise) {
    return knex.schema.createTable(
        "tb_user_komentar_perangkat_pembelajaran",
        function (table) {
            table
                .integer("id_user")
                .unsigned()
            table
                .foreign("id_user", "user_komentar_perangkat")
                .references("id_user")
                .inTable("tb_user")
                .onDelete("CASCADE")
                .onUpdate("CASCADE");
            table.integer("id_perangkat_pembelajaran").unsigned();
            table
                .foreign("id_perangkat_pembelajaran", "komentar_perangkat")
                .references("id_perangkat_pembelajaran")
                .inTable("tb_perangkat_pembelajaran")
                .onDelete("CASCADE")
                .onUpdate("CASCADE");
            table.text("komentar").notNullable();
            // table.primary(["id_user", "id_perangkat_pembelajaran"]);
            table.bigInteger("created_at").notNullable();
            table.bigInteger("updated_at").nullable();
            table.bigInteger("deleted_at").nullable();
        }
    );
};

exports.down = function (knex) {
    return knex.schema.dropTable("tb_user_komentar_perangkat_pembelajaran");
};
