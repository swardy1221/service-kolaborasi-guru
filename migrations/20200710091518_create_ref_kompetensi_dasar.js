exports.up = function (knex, Promise) {
  return knex.schema.createTable("ref_kompetensi_dasar", function (table) {
    table
      .integer("id_mata_pelajaran")
      .unsigned()
      .references("id_mata_pelajaran")
      .inTable("ref_mata_pelajaran")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.string("mata_pelajaran", 100).notNullable();
    table
      .integer("id_kompetensi_inti", 11)
      .unsigned()
      .references("id_kompetensi_inti")
      .inTable("ref_kompetensi_inti")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.string("kode_kompetensi_inti", 10).notNullable();
    table.increments("id_kompetensi_dasar").unsigned().primary();
    table.string("kode_kompetensi_dasar", 10).notNullable();
    table.text("deskripsi").notNullable();
    table.bigInteger("created_at").notNullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("ref_kompetensi_dasar");
};
