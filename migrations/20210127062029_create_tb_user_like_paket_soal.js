exports.up = function (knex, Promise) {
    return knex.schema.createTable(
        "tb_user_like_paket_soal",
        function (table) {
            table
                .integer("id_user")
                .unsigned();
            table
                .foreign('id_user', 'user_like_paket_Soal')
                .references("id_user")
                .inTable("tb_user")
                .onDelete("CASCADE")
                .onUpdate("CASCADE");
            table.integer("id_paket_soal")
                .unsigned();
            table
                .foreign("id_paket_soal", "id_paket_foreign_like")
                .references("id_paket_soal")
                .inTable("tb_paket_soal")
                .onDelete("CASCADE")
                .onUpdate("CASCADE");
            table.primary(["id_user", "id_paket_soal"]);
            table.bigInteger("created_at").notNullable();
        }
    );
};

exports.down = function (knex) {
    return knex.schema.dropTable("tb_user_like_paket_soal");
};
