exports.up = function (knex) {
    return knex.schema.table('tb_perangkat_pembelajaran', table => {
        table.text('slug').notNullable().after('judul');
    });
};

exports.down = function (knex) {
    return knex.schema.table('tb_perangkat_pembelajaran', table => {
        table.dropColumn('slug');
    });
};