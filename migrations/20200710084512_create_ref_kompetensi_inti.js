exports.up = function (knex, Promise) {
  return knex.schema.createTable("ref_kompetensi_inti", function (table) {
    table
      .integer("id_mata_pelajaran", 11)
      .unsigned()
      .references("id_mata_pelajaran")
      .inTable("ref_mata_pelajaran")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.string("mata_pelajaran", 100).notNullable();
    table.increments("id_kompetensi_inti").unsigned().primary();
    table.string("kode_kompetensi_inti", 10).notNullable();
    table.text("deskripsi").notNullable();
    table.bigInteger("created_at").notNullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("ref_kompetensi_inti");
};
