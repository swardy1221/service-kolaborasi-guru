exports.up = function (knex, Promise) {
    return knex.schema.createTable("tb_riwayat_revisi_opsi_jawaban", function (table) {
        table.increments("id_revisi_opsi_jawaban").unsigned().primary();
        table
            .integer("id_riwayat_revisi_soal")
            .unsigned()
        table
            .foreign('id_riwayat_revisi_soal', 'revisi_soal_opsi_jawaban')
            .references("id_riwayat_revisi_soal")
            .inTable("tb_riwayat_revisi_soal")
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table.text("deskripsi").notNullable();
        table.enu("tipe_file", ["gambar", "audio", "video", "lainnya"]).nullable();
        table.string("file").nullable();
        table.text("feedback").nullable();
        table.integer("is_kunci").notNullable();
        table.bigInteger("created_at").notNullable();
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable("tb_riwayat_revisi_opsi_jawaban");
};