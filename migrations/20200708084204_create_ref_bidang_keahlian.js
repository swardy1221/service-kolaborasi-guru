exports.up = function (knex, Promise) {
  return knex.schema.createTable("ref_bidang_keahlian", function (table) {
    table
      .integer("id_sk_dikdasmen", 11)
      .unsigned()
      .references("id_sk_dikdasmen")
      .inTable("ref_sk_dikdasmen")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.increments("id_bidang_keahlian").unsigned().primary();
    table.string("kode_bidang_keahlian", 10).notNullable();
    table.string("bidang_keahlian", 100).notNullable();
    table.bigInteger("created_at").notNullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("ref_bidang_keahlian");
};
