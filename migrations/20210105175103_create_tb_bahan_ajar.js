exports.up = function (knex, Promise) {
    return knex.schema.createTable("tb_bahan_ajar", function (table) {
        table.increments("id_bahan_ajar").unsigned().primary();
        table
            .integer("id_user")
            .unsigned();
        table
            .foreign("id_user", "bahan_dibuat_oleh")
            .references("id_user")
            .inTable("tb_user")
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table
            .enu("tipe_bahan_ajar", [
                "modul",
                "video",
                "ppt",
                "lainnya",
            ])
            .notNullable();
        table
            .integer("id_mata_pelajaran")
            .unsigned();
        table
            .foreign("id_mata_pelajaran", "bahan_milik_mapel")
            .references("id_mata_pelajaran")
            .inTable("ref_mata_pelajaran")
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table.string('nama_mapel').nullable();
        table
            .integer("id_kompetensi_keahlian")
            .unsigned();
        table
            .foreign("id_kompetensi_keahlian", "bahan_milik_kompetensi_keahlian")
            .references("id_kompetensi_keahlian")
            .inTable("ref_kompetensi_keahlian")
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table.string('kompetensi_keahlian').nullable();
        table.text("judul").notNullable();
        table.string("slug").notNullable();
        table.text("deskripsi").notNullable();
        table.string("file").notNullable();
        table.string('sampul').notNullable();
        table.integer("jumlah_like").unsigned().nullable().defaultTo(0);
        table.integer("jumlah_dislike").unsigned().nullable().defaultTo(0);
        table.integer("jumlah_komentar").unsigned().nullable().defaultTo(0);
        table.integer("diunduh_sebanyak").unsigned().nullable().defaultTo(0);
        table.integer("dilihat_sebanyak").unsigned().nullable().defaultTo(0);
        table.enu("status_dokumen", ["revisi1", "revisi2", "revisi3", "revisi_final", "terverifikasi", "ditolak"]).notNullable();
        table.enu("akses", ["terbit", "draft"]).notNullable();
        table
            .integer("divalidasi_oleh")
            .unsigned()
            .nullable();
        table
            .foreign("divalidasi_oleh", "bahan_divalidasi_oleh")
            .references("id_user")
            .inTable("tb_user")
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table.integer('pernah_terbit').unsigned().nullable().defaultTo(0);
        table.bigInteger("created_at").notNullable();
        table.bigInteger("updated_at").notNullable();
        table.bigInteger("deleted_at").nullable();
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable("tb_bahan_ajar");
};
