exports.up = function (knex) {
    return knex.schema.table('tb_user', table => {
        table.integer('is_admin').nullable().unsigned().after('role').defaultTo(0);
    });
};

exports.down = function (knex) {
    return knex.schema.table('tb_user', table => {
        table.dropColumn('is_admin');
    });
};