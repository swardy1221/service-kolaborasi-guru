exports.up = function (knex, Promise) {
  return knex.schema.createTable("ref_provinsi", function (table) {
    table.string("id_provinsi", 100).primary();
    table.string("provinsi", 100).notNullable();
    table.bigInteger("created_at").notNullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("ref_provinsi");
};
