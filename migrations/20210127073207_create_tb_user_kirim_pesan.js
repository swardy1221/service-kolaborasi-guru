exports.up = function (knex, Promise) {
    return knex.schema.createTable(
        "tb_user_kirim_pesan",
        function (table) {
            table.increments('id_pesan').unsigned().primary();
            table
                .integer("id_pengirim")
                .unsigned()
            table
                .foreign("id_pengirim", "pengirim_pesan")
                .references("id_user")
                .inTable("tb_user")
                .onDelete("CASCADE")
                .onUpdate("CASCADE");
            table
                .integer("id_penerima")
                .unsigned()
            table
                .foreign("id_penerima", "penerima_pesan")
                .references("id_user")
                .inTable("tb_user")
                .onDelete("CASCADE")
                .onUpdate("CASCADE");
            table.text("pesan").notNullable();
            table.bigInteger("created_at").notNullable();
            table.bigInteger("updated_at").nullable();
            table.bigInteger("deleted_at").nullable();
        }
    );
};

exports.down = function (knex) {
    return knex.schema.dropTable("tb_user_kirim_pesan");
};
