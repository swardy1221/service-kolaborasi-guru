exports.up = function (knex) {
    return knex.schema.table('tb_referensi', table => {
        // table.dropColumn('id_referensi');
        // table.dropPrimary('id_referensi');
        // table.integer('is_admin').nullable().unsigned().after('role').defaultTo(0);
        table.primary(["id_entitas", "id_entitas_referensi"]);
    });
};

exports.down = function (knex) {
    return knex.schema.table('tb_referensi', table => {
        table.dropPrimary(["id_entitas", "id_entitas_referensi"]);
        table
            .increments("id_referensi")
            .unsigned()
            .primary()
            .first();
    });
};