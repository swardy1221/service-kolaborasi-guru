exports.up = function (knex, Promise) {
    return knex.schema.createTable("tb_soal", function (table) {
        table.increments("id_soal").unsigned().primary();
        table
            .integer("id_user")
            .unsigned();
        table
            .foreign('id_user', 'user_soal')
            .references("id_user")
            .inTable("tb_user")
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table.integer('id_mata_pelajaran')
            .unsigned()
        table
            .foreign('id_mata_pelajaran', 'mata_pelajaran_soal')
            .references('id_mata_pelajaran')
            .inTable('ref_mata_pelajaran')
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table.string('nama_mapel');
        table
            .integer("id_kompetensi_dasar")
            .unsigned();
        table
            .foreign('id_kompetensi_dasar', 'kd_soal')
            .references("id_kompetensi_dasar")
            .inTable("ref_kompetensi_dasar")
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table
            .integer("id_kisi_kisi")
            .unsigned()
        table
            .foreign('id_kisi_kisi', 'kisi_kisi_soal')
            .references("id_kisi_kisi")
            .inTable("tb_kisi_kisi")
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table
            .enu("tipe_soal", ["pilihan_ganda", "isian_singkat", "benar_salah"])
            .nullable();
        table.integer("jumlah_jawaban").nullable().defaultTo(1); //default 1
        table.text("teks_soal").notNullable();
        table.enu("tipe_file", ["gambar", "audio", "video", "dokumen", "lainnya"]).nullable();
        table.string("file").nullable();
        table
            .enu("tingkat_kesulitan", ["c1", "c2", "c3", "c4", "c5", "c6"])
            .nullable();
        table.string("kunci_essay").nullable();
        table.integer("jumlah_like").unsigned().nullable();
        table.integer("jumlah_dislike").unsigned().nullable();
        table.integer("jumlah_komentar").unsigned().nullable();
        table.integer("digunakan_sebanyak").unsigned().nullable();
        table.enu("status", ["final", "ditolak", "terverifikasi", "revisi"]).notNullable();
        table
            .integer("divalidasi_oleh")
            .unsigned()
            .nullable()
        table
            .foreign('divalidasi_oleh', 'soal_divalidasi_oleh')
            .references("id_user")
            .inTable("tb_user")
            .onDelete("CASCADE")
            .onUpdate("CASCADE");
        table.bigInteger("created_at").notNullable();
        table.bigInteger("updated_at").notNullable();
        table.bigInteger("deleted_at").notNullable();
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable("tb_soal");
};
