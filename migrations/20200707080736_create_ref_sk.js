exports.up = function (knex, Promise) {
  return knex.schema.createTable("ref_sk_dikdasmen", function (table) {
    table.increments("id_sk_dikdasmen").unsigned().primary();
    table.string("nomor", 100).notNullable();
    table.date("tanggal", 100).notNullable();
    table.string("tentang").notNullable();
    table.bigInteger("created_at").notNullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("ref_sk_dikdasmen");
};
