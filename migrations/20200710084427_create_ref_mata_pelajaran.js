exports.up = function (knex, Promise) {
  return knex.schema.createTable("ref_mata_pelajaran", function (table) {
    table.increments("id_mata_pelajaran").unsigned().primary();
    table
      .integer("id_bidang_keahlian", 11)
      .unsigned()
      .references("id_bidang_keahlian")
      .inTable("ref_bidang_keahlian")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.string("bidang_keahlian", 100).nullable();
    table
      .integer("id_program_keahlian", 11)
      .unsigned()
      .references("id_program_keahlian")
      .inTable("ref_program_keahlian")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.string("program_keahlian", 100).nullable();
    table
      .integer("id_kompetensi_keahlian", 11)
      .unsigned()
      .references("id_kompetensi_keahlian")
      .inTable("ref_kompetensi_keahlian")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.string("kompetensi_keahlian", 100).nullable();
    table.string("nama_mapel", 100).notNullable();
    table.enu("jenis_mapel", ["A", "B", "C1", "C2", "C3"]).notNullable();
    table.bigInteger("created_at").notNullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("ref_mata_pelajaran");
};
