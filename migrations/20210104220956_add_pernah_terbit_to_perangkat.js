exports.up = function (knex) {
    return knex.schema.table('tb_perangkat_pembelajaran', table => {
        table.integer('pernah_terbit').notNullable().unsigned().after('divalidasi_oleh');
    });
};

exports.down = function (knex) {
    return knex.schema.table('tb_perangkat_pembelajaran', table => {
        table.dropColumn('pernah_terbit');
    });
};