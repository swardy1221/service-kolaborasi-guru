const getKompetensiByProgram = require("../../kompetensi-keahlian/services/getByProgram");

let controller = async (req, res) => {
  try {
    let idProgramKeahlian = req.params.idProgramKeahlian || "";
    let hasil = await getKompetensiByProgram(idProgramKeahlian);
    res.status(hasil.code).send(hasil);
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = controller;
