const programService = require("../services/getDetail");
const getMapel = require("../../mata-pelajaran/services/bidangProgram");

let controller = async (req, res) => {
  try {
    let idProgramKeahlian = req.params.idProgramKeahlian || "";

    let programKeahlian = await programService(idProgramKeahlian);

    let hasil = await getMapel(
      programKeahlian.results.id_bidang_keahlian,
      idProgramKeahlian
    );
    res.status(hasil.code).send(hasil);
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = controller;
