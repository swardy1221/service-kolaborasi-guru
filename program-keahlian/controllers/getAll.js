const getAllService = require("../services/getAll");
const searchService = require("../services/search");
const getByBidang = require("../services/getByBidang");

let controller = async (req, res) => {
  try {
    let { query, idBidangKeahlian } = req.query || "";
    let hasil;

    if (query) {
      hasil = await searchService(query);
    } else if (!idBidangKeahlian) {
      hasil = await getAllService();
    } else {
      hasil = await getByBidang(idBidangKeahlian);
    }
    res.status(hasil.code).send(hasil);
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = controller;
