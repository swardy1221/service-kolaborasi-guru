const db = require("../../config/database");
const response = require("../../config/responses");

const service = async (idProgramKeahlian) => {
  try {
    let programKeahlian = await db("ref_program_keahlian")
      .where("id_program_keahlian", idProgramKeahlian)
      .first();

    if (programKeahlian) {
      return response.success(programKeahlian, "program keahlian");
    } else {
      return response.notFound("program keahlian");
    }
  } catch (error) {
    return response.serverError(error);
  }
};

module.exports = service;
