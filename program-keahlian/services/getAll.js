const db = require("../../config/database");
const response = require("../../config/responses");

const service = async () => {
  try {
    let programKeahlian = await db("ref_program_keahlian").select("*");

    if (programKeahlian.length) {
      return response.success(programKeahlian, "program keahlian");
    } else {
      return response.notFound("program keahlian");
    }
  } catch (error) {
    return response.serverError(error);
  }
};

module.exports = service;
