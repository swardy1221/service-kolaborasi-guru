const express = require("express");
const router = express.Router();
const getAll = require("./controllers/getAll");
const kompetensiKeahlian = require("./controllers/getKompetensiKeahlian");
const mataPelajaran = require("./controllers/getMataPelajaran");

//Route bagi Model Program Kehalian
router.get("/", getAll);
router.get("/:idProgramKeahlian/kompetensi-keahlian", kompetensiKeahlian);
router.get("/:idProgramKeahlian/mata-pelajaran", mataPelajaran);

module.exports = router;
