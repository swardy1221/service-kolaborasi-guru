const db = require('../../config/database');

const service = async (idGuru, mataPelajaran) => {
    try {
        let mataPelajaran = await db('ref_mata_pelajaran')
            .select('id_mata_pelajaran', 'nama_mapel')
            .where('id_guru', idGuru)
            .whereIn('id_mata_pelajaran', mataPelajaran);
    } catch (error) {
        throw error;
    }
}

module.exports = service;