const db = require('../../config/database');

const service = async (idGuru) => {
    try {
        let idMataPelajaranQuery = db('tb_guru_mengampu_mata_pelajaran').select('id_mata_pelajaran').where('id_guru', idGuru);
        // if(!idMataPelajaran) return false;

        let mataPelajaran = await db('ref_mata_pelajaran').select('id_mata_pelajaran', 'nama_mapel').whereIn('id_mata_pelajaran', idMataPelajaranQuery);
        if (!mataPelajaran.length) return false;
        return mataPelajaran
    } catch (error) {
        throw error;
    }
}

module.exports = service;