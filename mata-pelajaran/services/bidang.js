const db = require("../../config/database");
const response = require("../../config/responses");

const service = async (idBidangKeahlian) => {
  try {
    let mataPelajaran = await db("ref_mata_pelajaran")
      .select("*")
      .where("id_bidang_keahlian", 99)
      .orWhere("id_bidang_keahlian", idBidangKeahlian);

    if (mataPelajaran.length) {
      return response.success(mataPelajaran, "mata pelajaran");
    } else {
      return response.notFound("mata pelajaran");
    }
  } catch (error) {
    return response.serverError(error);
  }
};

module.exports = service;
