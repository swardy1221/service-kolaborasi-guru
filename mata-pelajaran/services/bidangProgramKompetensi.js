const db = require("../../config/database");
const response = require("../../config/responses");

const service = async (
  idBidangKeahlian,
  idProgramKeahlian,
  idKompetensiKeahlian
) => {
  try {
    let mataPelajaran = await db("ref_mata_pelajaran")
      .select("*")
      .where({ id_bidang_keahlian: 99 })
      .union(
        db("ref_mata_pelajaran").select("*").where({
          id_bidang_keahlian: idBidangKeahlian,
          id_program_keahlian: 999,
        }),
        db("ref_mata_pelajaran").select("*").where({
          id_bidang_keahlian: idBidangKeahlian,
          id_program_keahlian: idProgramKeahlian,
          id_kompetensi_keahlian: 999,
        }),
        db("ref_mata_pelajaran").select("*").where({
          id_kompetensi_keahlian: idKompetensiKeahlian,
        })
      );

    if (mataPelajaran.length) {
      return response.success(mataPelajaran, "mata pelajaran");
    } else {
      return response.notFound("mata pelajaran");
    }
  } catch (error) {
    return response.serverError(error);
  }
};

module.exports = service;
