const db = require("../../config/database");
const response = require("../../config/responses");

const service = async (
  idBidangKeahlian,
  idProgramKeahlian,
  idKompetensiKeahlian,
  jenisMapel
) => {
  try {
    let mataPelajaran = await db("ref_mata_pelajaran")
      .select("*")
      .where({ id_bidang_keahlian: 99, jenis_mapel: jenisMapel })
      .union(
        db("ref_mata_pelajaran").select("*").where({
          id_bidang_keahlian: idBidangKeahlian,
          jenis_mapel: jenisMapel,
          id_program_keahlian: 999,
        }),
        db("ref_mata_pelajaran").select("*").where({
          id_bidang_keahlian: idBidangKeahlian,
          id_program_keahlian: idProgramKeahlian,
          jenis_mapel: jenisMapel,
          id_kompetensi_keahlian: 999,
        }),
        db("ref_mata_pelajaran").select("*").where({
          id_kompetensi_keahlian: idKompetensiKeahlian,
          jenis_mapel: jenisMapel,
        })
      );

    if (mataPelajaran.length) {
      return response.success(mataPelajaran, "mata pelajaran");
    } else {
      return response.notFound("mata pelajaran");
    }
  } catch (error) {
    return response.serverError(error);
  }
};

module.exports = service;
