const db = require("../../config/database");
const response = require("../../config/responses");

const service = async (idMataPelajaran) => {
  try {
    let kompetensiInti = await db("ref_kompetensi_inti")
      .select("*")
      .where("id_mata_pelajaran", idMataPelajaran);

    if (kompetensiInti.length) {
      return response.success(kompetensiInti, "kompetensi inti");
    } else {
      return response.notFound("kompetensi inti");
    }
  } catch (error) {
    return response.serverError(error);
  }
};

module.exports = service;
