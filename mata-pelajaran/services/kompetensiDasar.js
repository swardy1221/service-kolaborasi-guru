const db = require("../../config/database");
const response = require("../../config/responses");

const service = async (idMataPelajaran) => {
  try {
    let kompetensiDasar = await db("ref_kompetensi_dasar")
      .select("*")
      .where("id_mata_pelajaran", idMataPelajaran);

    if (kompetensiDasar.length) {
      return response.success(kompetensiDasar, "kompetensi dasar");
    } else {
      return response.notFound("kompetensi dasar");
    }
  } catch (error) {
    return response.serverError(error);
  }
};

module.exports = service;
