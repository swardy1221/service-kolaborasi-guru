const db = require("../../config/database");
const response = require("../../config/responses");

const service = async (query) => {
  try {
    let mataPelajaran = await db("ref_mata_pelajaran")
      .select("*")
      .where("nama_mapel", "like", `%${query}%`);

    if (mataPelajaran.length) {
      return response.success(mataPelajaran, "mata pelajaran");
    } else {
      return response.notFound("mata pelajaran");
    }
  } catch (error) {
    return response.serverError(error);
  }
};

module.exports = service;
