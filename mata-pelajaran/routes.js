const express = require("express");
const router = express.Router();
const getAll = require("./controllers/getAll");
const kompetensiInti = require("./controllers/kompetensiInti");
const kompetensiDasar = require("./controllers/kompetensiDasar");
const getByIdGuru = require('./controllers/getByIdGuru');

const { authUser } = require('../middlewares/index');
// const mataPelajaran = require("./controllers/getMataPelajaran");

//Route bagi Model Mata Pelajaran
router.get("/", getAll);
router.get('/my', authUser, getByIdGuru);
router.get("/:idMataPelajaran/kompetensi-inti", kompetensiInti);
router.get("/:idMataPelajaran/kompetensi-dasar", kompetensiDasar);
// router.get("/:idKompetensiKeahlian/mata-pelajaran", mataPelajaran);

module.exports = router;
