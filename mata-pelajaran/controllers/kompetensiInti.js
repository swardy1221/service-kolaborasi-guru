const kiService = require("../services/kompetensiInti");

const controller = async (req, res) => {
  try {
    let idMataPelajaran = req.params.idMataPelajaran || "";
    let hasil = await kiService(idMataPelajaran);
    res.status(hasil.code).send(hasil);
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = controller;
