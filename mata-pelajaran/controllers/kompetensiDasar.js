const kdService = require("../services/kompetensiDasar");

const controller = async (req, res) => {
  try {
    let idMataPelajaran = req.params.idMataPelajaran || "";
    let hasil = await kdService(idMataPelajaran);
    res.status(hasil.code).send(hasil);
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = controller;
