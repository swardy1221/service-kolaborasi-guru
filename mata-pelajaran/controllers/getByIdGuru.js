const response = require('../../config/responses');
const getGuruByIdUser = require('../../user/services/getGuruByIdUser');
const getByIdGuru = require('../services/getByIdGuru');

const controller = async (req, res) => {
    try {
        let guru = await getGuruByIdUser(req.user.idUser);
        if (!guru) return res.status(404).send(response.notFound('user guru tidak ditemukan'));

        let myMataPelajaran = await getByIdGuru(guru.id_guru);
        if (!myMataPelajaran.length) return res.status(404).send(response.notFound('mata pelajaran'));

        return res.status(200).send(response.success(myMataPelajaran, 'mata pelajaran'));
    } catch (error) {
        return res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;