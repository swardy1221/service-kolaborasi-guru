const searchService = require("../services/search");
const bpkjService = require("../services/bidangProgramKompetensiJenis");
const bpkService = require("../services/bidangProgramKompetensi");
const bpjService = require("../services/bidangProgramJenis");
const bpService = require("../services/bidangProgram");
const bjService = require("../services/bidangJenis");
const bService = require("../services/bidang");
const getAll = require('../services/getAll');

let controller = async (req, res) => {
  try {
    let {
      idBidangKeahlian,
      idProgramKeahlian,
      idKompetensiKeahlian,
      jenisMapel,
      query,
    } = req.query || "";
    let hasil;

    if (query) {
      hasil = await searchService(query);
    } else if (idBidangKeahlian) {
      if (idProgramKeahlian) {
        if (idKompetensiKeahlian) {
          if (jenisMapel) {
            // BPKJ
            hasil = await bpkjService(
              idBidangKeahlian,
              idProgramKeahlian,
              idKompetensiKeahlian,
              jenisMapel
            );
          } else {
            // BPK
            hasil = await bpkService(
              idBidangKeahlian,
              idProgramKeahlian,
              idKompetensiKeahlian
            );
          }
        } else {
          if (jenisMapel) {
            //BPJ
            hasil = await bpjService(
              idBidangKeahlian,
              idProgramKeahlian,
              jenisMapel
            );
          } else {
            //BP
            hasil = await bpService(idBidangKeahlian, idProgramKeahlian);
          }
        }
      } else {
        if (jenisMapel) {
          //BJ
          hasil = await bjService(idBidangKeahlian, jenisMapel);
        } else {
          // B
          hasil = await bService(idBidangKeahlian);
        }
      }
    } else {
      hasil = await getAll()
    }
    res.status(hasil.code).send(hasil);
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = controller;
