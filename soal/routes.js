const express = require("express");
const router = express.Router();
// Import Controllers
const {
    search,
    update,
    create,
    validasi,
    softDelete,
    getDetailById,
    getByStatus,
    getPenggunaSoal,
    getRiwayatRevisi,
    getRevisedById,
} = require("./controllers/index");

//Import Middlewares

const {
    authUser,
    authRole,
    uploader,
} = require('../middlewares/index');

//Route bagi Soal

/**
 * GET METHOD
 */
router.get('/', authUser, search)
router.get("/:idSoal", authUser, getDetailById);
router.get('/status/:status', authUser, authRole('admin'), getByStatus)
router.get('/:idSoal/pengguna', authUser, getPenggunaSoal);
router.get('/:idSoal/riwayat-revisi', authUser, getRiwayatRevisi);
router.get('/:idSoal/riwayat-revisi/:idRiwayatRevisi', authUser, getRevisedById);


/**
 * POST METHOD
 */
router.post("/", authUser, uploader.soal.single('file'), create);

/**
 * PUT METHOD
 */
router.put('/:idSoal', authUser, uploader.soal.single('file'), update)
router.put('/:idSoal/validasi', authUser, authRole('admin'), validasi)

/**
 * DELETE METHOD
 */
router.delete('/:idSoal', authUser, softDelete);

module.exports = router;
