const db = require('../../config/database');

const service = async (dataSoal) => {
    const trxProvider = db.transactionProvider();
    const trx = await trxProvider();
    try {
        let createSoal = await trx('tb_soal').insert({
            id_user: dataSoal.idUser,
            id_mata_pelajaran: dataSoal.idMataPelajaran,
            nama_mapel: dataSoal.namaMataPelajaran,
            id_kompetensi_dasar: dataSoal.idKompetensiDasar,
            kode_kompetensi_dasar: dataSoal.kodeKompetensiDasar,
            id_kisi_kisi: dataSoal.idKisiKisi,
            tipe_soal: dataSoal.tipeSoal,
            jumlah_jawaban: dataSoal.jumlahJawaban,
            teks_soal: dataSoal.teksSoal,
            tipe_file: dataSoal.tipeFile,
            file: dataSoal.file,
            tingkat_kesulitan: dataSoal.tingkatKesulitan,
            kunci_essay: dataSoal.kunciEssay,
            status: dataSoal.status,
            created_at: dataSoal.createdAt,
            updated_at: dataSoal.createdAt,
        })

        if (!createSoal) {
            await trx.rollback();
            return false;
        }

        for (const opsi of dataSoal.dataOpsi) {
            let createOpsi = await db('tb_opsi_jawaban').transacting(trx).insert({
                id_soal: createSoal,
                deskripsi: opsi.teksOpsi,
                tipe_file: opsi.tipeFile,
                file: opsi.file,
                feedback: opsi.feedback,
                is_kunci: opsi.isKunci,
                created_at: dataSoal.createdAt
            })

            if (!createOpsi) {
                await trx.rollback();
                return false
            }
        }

        let createRiwayat = await db('tb_riwayat_revisi_soal').transacting(trx).insert({
            id_soal: createSoal,
            id_mata_pelajaran: dataSoal.idMataPelajaran,
            nama_mapel: dataSoal.namaMataPelajaran,
            id_kompetensi_dasar: dataSoal.idKompetensiDasar,
            kode_kompetensi_dasar: dataSoal.kodeKompetensiDasar,
            id_kisi_kisi: dataSoal.idKisiKisi,
            revisi_ke: dataSoal.revisiKe,
            catatan_revisi: dataSoal.catatanRevisi,
            tipe_soal: dataSoal.tipeSoal,
            jumlah_jawaban: dataSoal.jumlahJawaban,
            teks_soal: dataSoal.teksSoal,
            tipe_file: dataSoal.tipeFile,
            file: dataSoal.file,
            tingkat_kesulitan: dataSoal.tingkatKesulitan,
            kunci_essay: dataSoal.kunciEssay,
            created_at: dataSoal.createdAt
        })

        if (!createRiwayat) {
            await trx.rollback();
            return false;
        }

        for (const opsi of dataSoal.dataOpsi) {
            let createOpsi = await db('tb_riwayat_revisi_opsi_jawaban').transacting(trx).insert({
                id_riwayat_revisi_soal: createRiwayat,
                deskripsi: opsi.teksOpsi,
                tipe_file: opsi.tipeFile,
                file: opsi.file,
                feedback: opsi.feedback,
                is_kunci: opsi.isKunci,
                created_at: dataSoal.createdAt
            })

            if (!createOpsi) {
                await trx.rollback();
                return false
            }
        }

        await trx.commit();
        let createdSoal = await db('tb_soal').where('id_soal', createSoal).first();
        return createdSoal;
    } catch (error) {
        throw error;
    }
}

module.exports = service;