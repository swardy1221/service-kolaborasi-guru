const db = require('../../config/database');

const service = async (idRiwayatRevisi) => {
    try {
        let detail = await db('tb_riwayat_revisi_soal').where('id_riwayat_revisi_soal', idRiwayatRevisi).first();

        if (!detail) return false;
        return detail;
    } catch (error) {
        throw error;
    }
}

module.exports = service;