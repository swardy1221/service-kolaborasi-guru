const db = require('../../config/database');

const service = async (idSoal) => {
    try {
        let idPaketSoal = db('tb_paket_soal_berisi_soal').where('id_soal', idSoal).select('id_paket_soal');
        let paketSoal = await db('tb_paket_soal').where('id_paket_soal', 'in', idPaketSoal)
            .select('dibuat_oleh', 'kode_paket_soal', 'created_at');
        // console.log(paketSoal);
        if (!paketSoal.length) false;
        return paketSoal;
    } catch (error) {
        throw error;
    }
}

module.exports = service;