const db = require('../../config/database');

const service = async (idSoal) => {
    const trxProvider = db.transactionProvider();
    const trx = await trxProvider();
    try {
        let deleteSoal = await trx('tb_soal').where('id_soal', idSoal).update({
            deleted_at: new Date().getTime()
        })

        if (!deleteSoal) {
            await trx.rollback();
            return false
        };

        await trx.commit();
        return true
    } catch (error) {
        throw error;
    }
}

module.exports = service;