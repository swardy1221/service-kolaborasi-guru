const db = require('../../config/database');

const service = async (idSoal) => {
    try {
        let riwayatRevisiSoal = await db('tb_riwayat_revisi_soal').where('id_soal', idSoal);
        if (!riwayatRevisiSoal.length) return false;
        return riwayatRevisiSoal;
    } catch (error) {
        throw error;
    }
}

module.exports = service;