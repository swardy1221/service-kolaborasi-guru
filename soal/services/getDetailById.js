const db = require('../../config/database');

const service = async (idSoal) => {
    try {
        let soal = await db('tb_soal').where('id_soal', idSoal).where('deleted_at', null).first();
        if (!soal) return false;
        return soal
    } catch (error) {
        throw error;
    }
}

module.exports = service;