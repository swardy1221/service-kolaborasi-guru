const db = require('../../../config/database');


const service = async (idMataPelajaran, limit = 5) => {
    try {
        let soal = await db('tb_soal').where({
            // akses: 'terbit',
            // deleted_at: null,
            id_mata_pelajaran: idMataPelajaran
        }).limit(limit)
            .orderByRaw('RAND()');
        // let soal = await db('')
        if (!soal.length) return false;
        return soal;
    } catch (error) {
        throw error;
    }
}

module.exports = service;