const db = require('../../config/database');


const service = async (status) => {
    try {
        let soal = await db('tb_soal').where({
            // akses: 'terbit',
            // deleted_at: null,
            status: status
            // id_mata_pelajaran: idMataPelajaran
        });
        if (!soal.length) return false;
        return soal;
    } catch (error) {
        throw error;
    }
}

module.exports = service;