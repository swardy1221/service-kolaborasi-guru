const db = require('../../config/database');

const service = async (idSoal, dataSoal) => {
    const trxProvider = db.transactionProvider();
    const trx = await trxProvider();
    try {
        let validasiSoal = await trx('tb_soal').where('id_soal', idSoal).update({
            status: dataSoal.statusValidasi,
            divalidasi_oleh: dataSoal.idUser,
            catatan_validasi: dataSoal.catatanValidasi
        })
        if (!validasiSoal) {
            await trx.rollback();
            return false;
        }
        await trx.commit();
        return true
    } catch (error) {
        throw error;
    }

}

module.exports = service;