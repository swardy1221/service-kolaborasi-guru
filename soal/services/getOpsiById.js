const db = require('../../config/database');

const service = async (idSoal) => {
    try {
        let opsi = await db('tb_opsi_jawaban').where('id_soal', idSoal).orderByRaw('RAND()');
        if (!opsi) return false
        return opsi
    } catch (error) {
        throw error;
    }
}

module.exports = service;