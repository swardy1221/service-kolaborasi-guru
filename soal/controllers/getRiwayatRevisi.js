const response = require('../../config/responses');
const {
    getDetailById,
    getRiwayatRevisi,
} = require('../services/index');

const controller = async (req, res) => {

    try {
        let { idSoal } = req.params || "";
        let { idUser } = req.user || "";

        let cekSoal = await getDetailById(idSoal);
        if (!cekSoal) return res.status(404).send(response.notFound('butir soal'));
        if (cekSoal.id_user != idUser) return res.status(403).send(response.forbidden('butir soal milik pengguna lain'))
        let riwayatRevisi = await getRiwayatRevisi(idSoal);
        if (!riwayatRevisi.length) return res.status(404).send(response.notFound('riwayat revisi soal'));

        return res.status(200).send(response.success(riwayatRevisi, 'riwayat revisi soal'));
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;