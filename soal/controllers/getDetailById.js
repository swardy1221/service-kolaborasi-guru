const response = require('../../config/responses');
const {
    getDetailById,
    getOpsiById
} = require('../services/index');

const controller = async (req, res) => {
    try {
        let { idSoal } = req.params || "";
        let cekSoal = await getDetailById(idSoal);
        if (!cekSoal) return res.status(404).send(response.notFound('soal'));

        let opsi = await getOpsiById(idSoal);
        if (!opsi) return res.status(400).send(response.badRequest('opsi jawaban tidak ditemukan'));

        return res.status(200).send(response.success({ ...cekSoal, opsi_jawaban: opsi }, 'soal'))
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;