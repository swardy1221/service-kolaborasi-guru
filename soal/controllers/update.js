const response = require('../../config/responses');
const {
    update,
    getOpsiById,
    getDetailById,
} = require('../services/index');

const controller = async (req, res) => {
    try {
        let { idUser } = req.user || "";
        let { idSoal } = req.params || "";
        let cekSoal = await getDetailById(idSoal);
        if (!cekSoal) return res.status(404).send(response.notFound('butir soal'));
        if (idUser != cekSoal.id_user) res.status(403).send(response.forbidden('butir soal milik user lain'));
        if (cekSoal.status == 'terverifikasi') return res.status(403).send(response.forbidden('butir soal sudah divalidasi'));

        let {
            idMataPelajaran,
            namaMataPelajaran,
            idKompetensiDasar,
            kodeKompetensiDasar,
            idKisiKisi,
            tipeSoal,
            jumlahJawaban,
            teksSoal,
            tipeFile,
            file,
            tingkatKesulitan,
            kunciEssay,
            status,
            opsiJawaban,
            feedbackOpsi,
            catatanRevisi,
            indexKunci
        } = req.body || "";

        //Preproses data Opsi jawaban
        let dataOpsi = [];
        let teksOpsi;
        let feedback;
        let isKunci;
        for (let index = 0; index < 5; index++) {
            teksOpsi = opsiJawaban[index];
            feedback = feedbackOpsi[index];
            indexKunci == index ? isKunci = 1 : isKunci = 0;
            dataOpsi.push({
                teksOpsi,
                feedback,
                isKunci
            })
        }

        //Preproses data Soal
        let dataSoal = {
            idSoal,
            idUser,
            idMataPelajaran,
            kodeKompetensiDasar,
            namaMataPelajaran,
            idKompetensiDasar,
            idKisiKisi,
            tipeSoal,
            jumlahJawaban,
            teksSoal,
            tipeFile,
            file,
            tingkatKesulitan,
            kunciEssay,
            status,
            revisiKe: parseInt(cekSoal.revisi_ke) + 1,
            catatanRevisi,
            createdAt: new Date().getTime(),
            dataOpsi
        }

        let updateSoal = await update(dataSoal);
        if (!updateSoal) return res.status(400).send(response.badRequest('gagal mengupdate butir soal'));
        let updatedSoal = await getDetailById(idSoal);
        let opsi = await getOpsiById(idSoal);
        updatedSoal.opsi_jawaban = opsi;

        return res.status(200).send(response.updated(updatedSoal, 'butir soal'));
    } catch (error) {
        return res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;