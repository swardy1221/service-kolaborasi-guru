const response = require('../../config/responses');
const {
    create,
    getOpsiById
} = require('../services/index')

const controller = async (req, res) => {
    try {
        let { idUser } = req.user || "";
        let {
            idMataPelajaran,
            namaMataPelajaran,
            idKompetensiDasar,
            kodeKompetensiDasar,
            idKisiKisi,
            tipeSoal,
            jumlahJawaban,
            teksSoal,
            tipeFile,
            tingkatKesulitan,
            kunciEssay,
            status,
            opsiJawaban,
            feedbackOpsi,
            revisiKe,
            indexKunci
        } = req.body || "";

        //Preproses uploaded file jika ada
        let file;
        if (req.file) {
            // console.log(req.file);
            file = `/soal/${req.file.filename}`;
        }
        // return res.status(200).send(response.success('berhasil upload file'))

        //Preproses data Opsi jawaban
        let dataOpsi = [];
        let teksOpsi;
        let feedback;
        let isKunci;
        for (let index = 0; index < 5; index++) {
            teksOpsi = opsiJawaban[index];
            feedback = feedbackOpsi[index];
            indexKunci == index ? isKunci = 1 : isKunci = 0;
            dataOpsi.push({
                teksOpsi,
                feedback,
                isKunci
            })
        }

        //Preproses data Soal
        let dataSoal = {
            idUser,
            idMataPelajaran,
            kodeKompetensiDasar,
            namaMataPelajaran,
            idKompetensiDasar,
            idKisiKisi,
            tipeSoal,
            jumlahJawaban,
            teksSoal,
            tipeFile,
            file,
            tingkatKesulitan,
            kunciEssay,
            status,
            revisiKe: revisiKe ? revisiKe : 1,
            catatanRevisi: 'Pertama kali diupload',
            createdAt: new Date().getTime(),
            dataOpsi
        }

        let createSoal = await create(dataSoal);
        let opsi = await getOpsiById(createSoal.id_soal);
        createSoal.opsi_jawaban = opsi;
        // opsiJawaban = opsiJawaban.map((item, index) => {
        //     let teksOpsi = item;
        //     let feedback = 
        //     dataOpsi[index].teksOpsi = item;
        //     index == indexKunci ? dataOpsi[index].is_kunci = 1 : dataOpsi[index].is_kunci = 0;
        //     return item
        // });

        // console.log(dataSoal);
        return res.status(200).send(response.success(createSoal, 'soal'))
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }

}

module.exports = controller;