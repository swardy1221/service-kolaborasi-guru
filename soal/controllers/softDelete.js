const response = require('../../config/responses');
const {
    getDetailById,
    softDelete
} = require('../services/index');

const controller = async (req, res) => {
    try {
        let { idSoal } = req.params || "";
        let { idUser } = req.user || "";
        let cekSoal = await getDetailById(idSoal);
        if (!cekSoal) return res.status(404).send(response.notFound('butir soal'));
        if (cekSoal.status == 'terverifikasi') return res.status(403).send(response.forbidden('butir soal sudah divalidasi'));
        if (idUser != cekSoal.id_user) return res.status(403).send(response.forbidden('tidak dapat menghapus butir soal milik guru lain'));
        let deleteSoal = await softDelete(idSoal);
        if (!deleteSoal) return res.status(400).send(response.badRequest('gagal menghapus butir soal'));
        return res.status(200).send(response.deleted('butir soal'));
    } catch (error) {
        return res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;