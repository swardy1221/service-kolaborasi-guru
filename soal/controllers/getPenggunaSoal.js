const response = require('../../config/responses');
const { getPenggunaSoal, getDetailById } = require('../services/index');
const creatorName = require('../../helpers/creatorName');

const controller = async (req, res) => {
    try {
        let { idSoal } = req.params || "";

        let cekSoal = await getDetailById(idSoal);
        if (!cekSoal) return res.status(404).send(response.notFound('butir soal'));
        let paketSoalPengguna = await getPenggunaSoal(idSoal);
        if (!paketSoalPengguna.length) return res.status(404).send(response.notFound('paket soal yg menggunakan soal ini'));

        for (satuPaket of paketSoalPengguna) {
            // console.log(satuPaket.dibuat_oleh);
            let dibuatOleh = await creatorName(satuPaket.dibuat_oleh);
            // console.log(dibuatOleh);
            // if (!dibuatOleh) return res.status(400).send(response.badRequest('user pembuat tidak ditemukan'));
            satuPaket.user_pembuat = dibuatOleh;
        }

        return res.status(200).send(response.success(paketSoalPengguna, 'pengguna soal'));
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;