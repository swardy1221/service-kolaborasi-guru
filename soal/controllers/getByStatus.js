const response = require('../../config/responses');
const { getByStatus, getOpsiById } = require('../services/index');

const controller = async (req, res) => {
    try {
        let { status } = req.params || "";

        let soal = await getByStatus(status);
        if (!soal.length) return res.status(404).send(response.notFound('butir soal'));

        for (satuSoal of soal) {
            let opsi = await getOpsiById(satuSoal.id_soal);
            if (!opsi) return res.status(404).send(response.notFound('opsi jawaban'));
            satuSoal.opsi_jawaban = opsi;
        }
        return res.status(200).send(response.success(soal, 'butir soal'));
    } catch (error) {
        return res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;