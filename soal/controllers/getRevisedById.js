const response = require('../../config/responses');
const {
    getRevisedById,
    getDetailById,
} = require('../services/index');

const controller = async (req, res) => {
    try {
        let { idUser } = req.user || "";
        console.log(idUser)
        let { idRiwayatRevisi, idSoal } = req.params || "";

        let cekSoal = await getDetailById(idSoal);
        if (!cekSoal) return res.status(404).send(response.notFound('butir soal'));
        if (cekSoal.id_user != idUser) return res.status(401).send(response.unauthorized('butir soal milik pengguna lain'));

        let detail = await getRevisedById(idRiwayatRevisi);
        if (!detail) return res.status(404).send(response.notFound('butir soal'));

        return res.status(200).send(response.success(detail, 'butir soal'))
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;