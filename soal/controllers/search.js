const response = require('../../config/responses');
const isMengampu = require('../../user/services/isMengampu');
const {
    search,
    getOpsiById
} = require('../services/index');

const controller = async (req, res) => {
    try {
        let { idUser } = req.user || "";
        let { idMataPelajaran, idKompetensiDasar, level, limit } = req.query || "";
        let cekMengampu = await isMengampu(idUser, idMataPelajaran);

        if (!cekMengampu) return res.status(403).send(response.forbidden('anda tidak mengampu mata pelajaran ini'));
        // console.log(cekMengampu)
        let soal;
        if (idKompetensiDasar) {
            if (level) {
                //KD dan Level
                soal = await search.byKDLevel(idMataPelajaran, idKompetensiDasar, level, limit);
                if (!soal) return res.status(404).send(response.notFound('butir soal'));
                // return res.status(200).send(response.success(soal, 'butir soal'));
            } else {
                //KD saja
                soal = await search.byKompetensiDasar(idMataPelajaran, idKompetensiDasar, limit);
                if (!soal) return res.status(404).send(response.notFound('butir soal'));
            }
        } else {
            if (level) {
                //Level saja
                soal = await search.byLevel(idMataPelajaran, level, limit);
                if (!soal) return res.status(404).send(response.notFound('butir soal'));
            } else {
                //Mapel saja
                soal = await search.byMapel(idMataPelajaran, limit);
                if (!soal) return res.status(404).send(response.notFound('butir soal'));
            }
        }

        for (satuSoal of soal) {
            let opsi = await getOpsiById(satuSoal.id_soal);
            if (!opsi) return res.status(404).send(response.notFound('opsi jawaban'));
            satuSoal.opsi_jawaban = opsi;
        }

        return res.status(200).send(response.success(soal, 'butir soal'));
    } catch (error) {
        return res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;