const response = require('../../config/responses');
const {
    validasi,
    getDetailById
} = require('../services/index');

const controller = async (req, res) => {
    try {
        let { idUser } = req.user || "";
        let { idSoal } = req.params || "";

        let cekSoal = await getDetailById(idSoal);
        if (!cekSoal) return res.status(404).send(response.notFound('butir soal'));
        if (cekSoal.status != 'final') return res.status(403).send(response.forbidden('status belum final'));
        // if (idUser == cekSoal.id_user) return res.status(403).send(response.forbidden('validasi harus dilakukan oleh admin lain'));
        let {
            statusValidasi,
            catatanValidasi
        } = req.body || "";

        let dataSoal = {
            statusValidasi,
            catatanValidasi,
            idUser
        }

        let validasiSoal = await validasi(idSoal, dataSoal);
        if (!validasiSoal) return res.status(400).send(response.badRequest('gagal validasi butir soal'));

        return res.status(200).send(response.customSuccess('berhasil validasi butir soal'));
    } catch (error) {
        return res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;