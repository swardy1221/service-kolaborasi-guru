const express = require("express");
const router = express.Router();
const getAll = require("./controllers/getAll");
const programKeahlian = require("./controllers/getProgramKeahlian");
const kompetensiKeahlian = require("./controllers/getKompetensiKeahlian");
const mataPelajaran = require("./controllers/getMataPelajaran");

//Route bagi Model Bidang Kehalian
router.get("/", getAll);
router.get("/:idBidangKeahlian/program-keahlian", programKeahlian);
router.get("/:idBidangKeahlian/kompetensi-keahlian", kompetensiKeahlian);
router.get("/:idBidangKeahlian/mata-pelajaran", mataPelajaran);

module.exports = router;
