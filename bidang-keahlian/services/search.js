const db = require("../../config/database");
const response = require("../../config/responses");

const service = async () => {
  try {
    let bidangKeahlian = await db("ref_bidang_keahlian")
      .select("*")
      .where("nama_bidang_keahlian", "like", `%${query}%`);

    if (bidangKeahlian.length) {
      return response.success(bidangKeahlian, "bidang keahlian");
    } else {
      return response.notFound("bidang keahlian");
    }
  } catch (error) {
    return response.serverError(error);
  }
};

module.exports = service;
