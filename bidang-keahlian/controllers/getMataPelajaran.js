const mapelService = require("../../mata-pelajaran/services/bidang");

let controller = async (req, res) => {
  try {
    let idBidangKeahlian = req.params.idBidangKeahlian || "";
    let hasil = await mapelService(idBidangKeahlian);
    res.status(hasil.code).send(hasil);
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = controller;
