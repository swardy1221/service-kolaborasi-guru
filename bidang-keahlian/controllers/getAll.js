const getAllService = require("../services/getAll");
const searchService = require("../services/search");

let controller = async (req, res) => {
  try {
    let { query } = req.query || "";
    let hasil;
    if (query) {
      hasil = await searchService(query);
    } else {
      hasil = await getAllService();
    }
    res.status(hasil.code).send(hasil);
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = controller;
