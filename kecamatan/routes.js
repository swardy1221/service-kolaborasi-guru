const express = require("express");
const router = express.Router();
const getSekolah = require("./controllers/getSekolah");

// Route untuk Model Ref-Kota
router.get("/:idKecamatan/sekolah", getSekolah);

module.exports = router;
