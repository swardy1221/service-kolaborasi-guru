const db = require('../../config/database');

const service = async (idKecamatan) => {
    try {
        let dataSekolah = await db('ref_sekolah').where('id_kecamatan', idKecamatan);
        if (!dataSekolah.length) return false
        return dataSekolah;
    } catch (error) {
        throw error;
    }
}

module.exports = service;