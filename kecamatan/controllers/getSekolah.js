const response = require('../../config/responses');
const getSekolah = require('../services/getSekolah');

const controller = async (req, res) => {
    try {
        let { idKecamatan } = req.params || "";
        let dataSekolah = await getSekolah(idKecamatan);
        if (!dataSekolah.length) return res.status(400).send(response.notFound('data sekolah'));

        return res.status(200).send(response.success(dataSekolah, 'data sekolah'));

    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;