const db = require("../../config/database");
const response = require("../../config/responses");

const service = async (idBidangKeahlian) => {
  try {
    let kompetensiKeahlian = await db("ref_kompetensi_keahlian")
      .select("*")
      .where("id_bidang_keahlian", idBidangKeahlian);

    if (kompetensiKeahlian.length) {
      return response.success(kompetensiKeahlian, "kompetensi keahlian");
    } else {
      return response.notFound("kompetensi keahlian");
    }
  } catch (error) {
    return response.serverError(error);
  }
};

module.exports = service;
