const db = require("../../config/database");
const response = require("../../config/responses");

let service = async () => {
  try {
    console.log('service')
    let kompetensiKeahlian = await db("ref_kompetensi_keahlian").select("*");
    console.log(kompetensiKeahlian)
    if (kompetensiKeahlian.length) {
      return response.success(kompetensiKeahlian, "kompetensi keahlian");
    } else {
      return response.notFound("kompetensi keahlian");
    }
  } catch (error) {
    return response.serverError(error);
  }
};

module.exports = service;
