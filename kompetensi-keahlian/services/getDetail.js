const db = require("../../config/database");
const response = require("../../config/responses");

const service = async (idKompetensiKeahlian) => {
  try {
    let kompetensiKeahlian = await db("ref_kompetensi_keahlian")
      .where("id_kompetensi_keahlian", idKompetensiKeahlian)
      .first();

    if (kompetensiKeahlian) {
      return response.success(kompetensiKeahlian, "kompetensi keahlian");
    } else {
      return response.notFound("kompetensi keahlian");
    }
  } catch (error) {
    return response.serverError(error);
  }
};

module.exports = service;
