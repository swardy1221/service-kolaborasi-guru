const db = require("../../config/database");
const response = require("../../config/responses");

let service = async (idBidangKeahlian, lamaStudi) => {
  try {
    let kompetensiKeahlian = await db("ref_kompetensi_keahlian")
      .select("*")
      .where({
        id_bidang_keahlian: idBidangKeahlian,
        lama_studi: lamaStudi,
      });

    if (kompetensiKeahlian.length) {
      return response.success(kompetensiKeahlian, "kompetensi keahlian");
    } else {
      return response.notFound("kompetensi keahlian");
    }
  } catch (error) {
    return response.serverError(error);
  }
};

module.exports = service;
