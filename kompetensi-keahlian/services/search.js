const db = require("../../config/database");
const response = require("../../config/responses");

const service = async (query) => {
  try {
    let kompetensiKeahlian = await db("ref_kompetensi_keahlian")
      .select("*")
      .where("nama_kompetensi_keahlian", "like", `%${query}%`);

    if (kompetensiKeahlian.length) {
      return response.success(kompetensiKeahlian, "kompetensi keahlian");
    } else {
      return response.notFound("kompetensi keahlian");
    }
  } catch (error) {
    return response.serverError(error);
  }
};

module.exports = service;
