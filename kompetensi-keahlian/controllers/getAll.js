const {
  // admin.getById,
  getAll,
  getByBidang,
  getByBidangLamaStudi,
  getByBidangProgram,
  getByBidangProgramLamaStudi,
  getByLamaStudi,
  getByProgram,
  getByProgramLamaStudi,
  search,
  admin,
} = require("../services/index");

let controller = async (req, res) => {
  try {
    console.log('controller')
    let { query, idBidangKeahlian, idProgramKeahlian, lamaStudi } =
      req.query || "";
    let hasil = await getAll();
    console.log(hasil)
    return res.status(hasil.code).send(hasil);
    if (query) {
      hasil = await admin.dashboard.coba(query);
    } else if (!idBidangKeahlian) {
      if (!idProgramKeahlian) {
        if (!lamaStudi) {
          // Get All
          hasil = await getAll();
        } else {
          // L
          hasil = await getByLamaStudi(lamaStudi);
        }
      } else {
        if (!lamaStudi) {
          // P
          hasil = await getByProgram(idProgramKeahlian);
        } else {
          // PL
          hasil = await getByProgramLamaStudi(idProgramKeahlian, lamaStudi);
        }
      }
    } else {
      if (!idProgramKeahlian) {
        if (!lamaStudi) {
          // B
          hasil = await getByBidang(idBidangKeahlian);
        } else {
          // BL
          hasil = await getByBidangLamaStudi(idBidangKeahlian, lamaStudi);
        }
      } else {
        if (!lamaStudi) {
          //BP
          hasil = await getByBidangProgram(idBidangKeahlian, idProgramKeahlian);
        } else {
          // BPL
          hasil = await getByBidangProgramLamaStudi(
            idBidangKeahlian,
            idProgramKeahlian,
            lamaStudi
          );
        }
      }
    }

    res.status(hasil.code).send(hasil);
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = controller;
