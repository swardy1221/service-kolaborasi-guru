const kompetensiService = require("../services/getDetail");
const bpkService = require("../../mata-pelajaran/services/bidangProgramKompetensi");
const { admin } = require("../services/index");
const response = require('../../config/responses');

let controller = async (req, res) => {
  try {
    let idKompetensiKeahlian = req.params.idKompetensiKeahlian || "";

    let kompetensiKeahlian = await admin.detail(idKompetensiKeahlian);
    // if(!kompetensiKeahlian)
    if (!kompetensiKeahlian) return res.status(404).send(response.notFound('mata pelajaran'))
    let hasil = await bpkService(
      kompetensiKeahlian.results.id_bidang_keahlian,
      kompetensiKeahlian.results.id_program_keahlian,
      kompetensiKeahlian.results.id_kompetensi_keahlian
    );
    res.status(hasil.code).send(hasil);
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = controller;
