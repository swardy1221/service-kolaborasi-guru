const express = require("express");
const router = express.Router();
const getAll = require("./controllers/getAll");
const mataPelajaran = require("./controllers/getMataPelajaran");

//Route bagi Model Kompetensi Kehalian
router.get("/", getAll);
router.get("/:idKompetensiKeahlian/mata-pelajaran", mataPelajaran);

module.exports = router;
