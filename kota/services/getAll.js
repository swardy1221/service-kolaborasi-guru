const db = require("../../config/database");
const response = require("../../config/responses");

const service = async () => {
  try {
    let kota = await db("ref_kota").select("*");

    if (kota.length) {
      return response.success(kota, "kota");
    } else {
      return response.notFound("kota");
    }
  } catch (error) {
    return response.serverError(error);
  }
};

module.exports = service;
