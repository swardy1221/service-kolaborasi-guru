const db = require("../../config/database");
const response = require("../../config/responses");

const service = async (idProvinsi) => {
  try {
    let kota = await db("ref_kota")
      .select("*")
      .where("id_provinsi", idProvinsi);

    if (kota.length) {
      return response.success(kota, "kota");
    } else {
      return response.notFound("kota");
    }
  } catch (error) {
    return response.serverError(error);
  }
};

module.exports = service;
