const getAllService = require("../services/getAll");
const searchService = require("../services/search");
const getByProvinsi = require("../services/getByProvinsi");

let controller = async (req, res) => {
  try {
    let { query, idProvinsi } = req.query || "";
    let hasil;

    if (query) {
      hasil = await searchService(query);
    } else if (!idProvinsi) {
      hasil = await getAllService();
    } else {
      hasil = await getByProvinsi(idProvinsi);
    }
    res.status(hasil.code).send(hasil);
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = controller;
