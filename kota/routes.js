const express = require("express");
const router = express.Router();
const getAllController = require("./controllers/getAll");

// Route untuk Model Ref-Kota
router.get("/", getAllController);

module.exports = router;
