"use strict";

exports.success = (data, entitas) => {
  const resolve = {
    code: 200,
    message: `Data ${entitas} ditemukan`,
    results: data,
  };
  return resolve;
};

exports.customSuccess = (pesan) => {
  const resolve = {
    code: 200,
    message: pesan,
  };
  return resolve;
};

exports.deleted = (entitas) => {
  const resolve = {
    code: 204,
    message: `Data ${entitas} berhasil dihapus`,
  };
  return resolve;
};

exports.created = (data, entitas) => {
  const resolve = {
    code: 201,
    message: `Berhasil menambahkan ${entitas}`,
    results: data,
  };
  return resolve;
};

exports.updated = (data, entitas) => {
  const resolve = {
    code: 200,
    message: `Berhasil mengubah ${entitas}`,
    results: data,
  };
  return resolve;
};

exports.badRequest = (pesan) => {
  const resolve = {
    code: 400,
    message: pesan,
  };
  return resolve;
};

exports.unauthorized = (pesan = "Tidak memiliki akses, silahkan login") => {
  const resolve = {
    code: 401,
    message: pesan,
  };
  return resolve;
};

exports.forbidden = (pesan = "Akses dilarang") => {
  const resolve = {
    code: 403,
    message: pesan,
  };
  return resolve;
};

exports.notFound = (entitas) => {
  const resolve = {
    code: 404,
    message: `Data ${entitas} tidak ditemukan`,
  };
  return resolve;
};

exports.duplicate = (message) => {
  const resolve = {
    code: 409,
    message: message,
  }
  return resolve;
}

exports.serverError = (error) => {
  const resolve = {
    code: 500,
    message: "Internal Server Error",
    error: error.message,
  };
  return resolve;
};

// exports.validationError = (error) => {
//   return error;
// };
