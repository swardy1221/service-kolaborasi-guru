require("dotenv").config();
const dev = require("knex")({
  client: "mysql2",
  connection: {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    port: process.env.DB_PORT,
  },
});

const prod = require("knex")({
  client: "mysql2",
  connection: {
    host: "46.17.172.1",
    user: "u143577159_kolaborasi",
    password: "Kolaborasi123",
    database: "u143577159_kolaborasi",
    port: 3306,
  },
});

module.exports = dev;
