const express = require("express");
const router = express.Router();
//Import Controllers
const {
    like,
    search,
    create,
    update,
    penyuka,
    populer,
    validasi,
    cekJudul,
    komentar,
    pengunduh,
    ubahAkses,
    getByTipe,
    unduhById,
    softDelete,
    getByStatus,
    addReferensi,
    getReferensi,
    riwayatRevisi,
    getDetailBySlug,
    getKomentarById,
    riwayatRevisiByKode,
} = require('./controllers/index');

//Import Middlewares
const {
    authUser,
    authRole,
    uploader
} = require('../middlewares/index');

//Route bagi Model Perangkat Pembelajaran
/**
 * GET METHOD
 */
router.get('/', search)
router.get('/:slug', getDetailBySlug);
router.get('/:kategori/popular', populer);
router.get('/my/:tipe', authUser, getByTipe);
router.get('/status/:status', authUser, authRole('admin'), getByStatus);
router.get('/:idPerangkatPembelajaran/unduh', authUser, unduhById);
router.get('/:idPerangkatPembelajaran/penyuka', authUser, penyuka);
router.get('/:idPerangkatPembelajaran/pengunduh', authUser, pengunduh);
router.get('/:idPerangkatPembelajaran/referensi', authUser, getReferensi);
router.get('/:idPerangkatPembelajaran/komentar', authUser, getKomentarById);
router.get('/:idPerangkatPembelajaran/riwayat-perubahan', authUser, riwayatRevisi);
router.get('/:idPerangkatPembelajaran/riwayat-perubahan/:kodeRevisi', authUser, riwayatRevisiByKode);

/**
 * POST METHOD
 */
router.post('/', authUser, authRole('guru'), uploader.perangkatPembelajaran.fields([{ name: 'sampul', maxCount: 1 }, { name: 'file', maxCount: 1 }]), create);
router.post('/cek-judul', authUser, cekJudul);
router.post('/:idPerangkatPembelajaran/like', authUser, like);
router.post('/:idPerangkatPembelajaran/komentar', authUser, komentar);
router.post('/:idPerangkatPembelajaran/referensi', authUser, addReferensi);

/**
 * PUT METHOD
 */
router.put('/:idPerangkatPembelajaran', authUser, authRole('guru'), uploader.perangkatPembelajaran.fields([{ name: 'sampul', maxCount: 1 }, { name: 'file', maxCount: 1 }]), update);
router.put('/:idPerangkatPembelajaran/ubah-akses', authUser, authRole('guru'), ubahAkses);
router.put('/:idPerangkatPembelajaran/validasi', authUser, authRole('admin'), validasi)

/**
 * DELETE METHOD
 */
router.delete('/:idPerangkatPembelajaran', authUser, softDelete);

//Total 17 endpoints
module.exports = router;
