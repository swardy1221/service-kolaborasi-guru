const { getKomentarById, getDetailById } = require('../services/index');
const response = require('../../config/responses');

const controller = async (req, res) => {
    try {
        let { idPerangkatPembelajaran } = req.params || "";
        let cekPerangkat = await getDetailById(idPerangkatPembelajaran);
        if (!cekPerangkat) return res.status(404).send(response.notFound('perangkat pembelajaran'));
        let komentar = await getKomentarById(idPerangkatPembelajaran);
        if (!komentar) return res.status(404).send(response.notFound('komentar'));
        return res.status(200).send(response.success(komentar, 'komentar'));
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;