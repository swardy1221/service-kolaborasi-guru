const response = require('../../config/responses');
const {
    getDetailById,
    validasi
} = require('../services/index');

const controller = async (req, res) => {
    try {
        let { idPerangkatPembelajaran } = req.params || "";
        let { idUser } = req.user || "";
        let { catatanValidasi, statusValidasi } = req.body || "";

        let cekPerangkat = await getDetailById(idPerangkatPembelajaran);
        if (!cekPerangkat) return res.status(404).send(response.notFound('perangkat pembelajaran'));
        if (cekPerangkat.status_dokumen != 'final') return res.status(403).send(response.forbidden('status belum final'));
        let updatePerangkat = await validasi(idPerangkatPembelajaran, idUser, catatanValidasi, statusValidasi);
        if (!updatePerangkat) return res.status(400).send(response.badRequest('gagal memvalidasi perangkat pembelajaran'));

        return res.status(200).send(response.updated(updatePerangkat, 'perangkat pembelajaran'));
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;