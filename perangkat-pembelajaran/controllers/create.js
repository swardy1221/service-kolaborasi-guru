const {
    create
} = require("../services/index");
const slugGenerator = require('../../helpers/slugGenerator');
const response = require("../../config/responses");
const crypto = require('crypto');

const controller = async (req, res) => {
    try {
        let now = new Date().getTime();
        let idUser = req.user.idUser;
        let statusDokumen = req.body.statusDokumen || "revisi1";
        let {
            tipePerangkatPembelajaran,
            idMataPelajaran,
            namaMataPelajaran,
            idKompetensiKeahlian,
            kompetensiKeahlian,
            judul,
            deskripsi,
            akses,
        } = req.body || "";
        let createdAt = now;
        let updatedAt = now;
        let pernahTerbit = akses == 'terbit' ? 1 : 0;
        let slug = await slugGenerator(judul, 5);
        let kodeRevisi = crypto.randomBytes(10).toString('hex');
        let dataPerangkat = {
            idUser,
            tipePerangkatPembelajaran,
            idMataPelajaran,
            namaMataPelajaran,
            idKompetensiKeahlian,
            kompetensiKeahlian,
            judul,
            slug,
            deskripsi,
            akses,
            statusDokumen,
            pernahTerbit,
            kodeRevisi,
            createdAt,
            updatedAt
        };
        if (!req.files['sampul'][0]) dataPerangkat.sampul = `/image/default/${tipePerangkatPembelajaran}.png`;
        dataPerangkat.sampul = `/perangkat-ajar/sampul/${req.files['sampul'][0]['filename']}`;
        if (!req.files['file'][0]) dataPerangkat.file = null;
        dataPerangkat.file = `/perangkat-ajar/file/${req.files['file'][0]['filename']}`;

        let createdPerangkat = await create(dataPerangkat);
        if (!createdPerangkat) return res.status(400).send(response.badRequest('gagal membuat perangkat pembelajaran'));
        return res.status(200).send(response.created(createdPerangkat, 'perangkat pembelajaran'));
    } catch (error) {
        return res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;