const response = require('../../config/responses');
const { getDetailById, pengunduh } = require('../services/index');

const controller = async (req, res) => {
    try {
        let { idPerangkatPembelajaran } = req.params || "";
        let cekPerangkat = await getDetailById(idPerangkatPembelajaran);
        if (!cekPerangkat) return res.status(404).send(response.notFound('perangkat pembelajaran'));
        console.log('controller');
        let listPengunduh = await pengunduh(idPerangkatPembelajaran);
        if (!listPengunduh) return res.status(404).send(response.notFound('pengunduh perangkat pembelajaran'));
        return res.status(200).send(response.success(listPengunduh, 'pengunduh perangkat pembelajaran'));
    } catch (error) {
        return res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;