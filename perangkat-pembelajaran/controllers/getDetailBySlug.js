const { getDetailBySlug, getKomentarById, viewCounter, cekLike } = require('../services/index');
const namaPenulis = require('../../helpers/creatorName');
const response = require('../../config/responses');

const controller = async (req, res) => {
    try {
        let { idUser } = req.user || "";
        let { slug } = req.params || "";
        // if (!counterView) return res.status(400).send(response.badRequest('gagal memuat data'));
        let perangkat = await getDetailBySlug(slug);
        let counterView = await viewCounter(slug);
        if (!perangkat) return res.status(404).send(response.notFound('perangkat pembelajaran'));
        // let statusLike = await cekLike(idUser, perangkat.id_perangkat_pembelajaran);
        // console.log(statusLike);
        // // if (idUser) {
        // //     perangkat.like = statusLike;
        // // }
        // perangkat.like = idUser ? await cekLike(idUser, id_perangkat_pembelajaran) : false;
        let pembuat = await namaPenulis(perangkat.id_user)
        let komentar = await getKomentarById(perangkat.id_perangkat_pembelajaran);
        let results = {
            ...perangkat,
            pembuat,
            komentar,
        }
        return res.status(200).send({
            code: 200,
            message: 'data ditemukan',
            results
        })
    } catch (error) {
        return res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;