const response = require('../../config/responses');
const { softDelete, getDetailById } = require('../services/index');

const controller = async (req, res) => {
    try {
        let { idPerangkatPembelajaran } = req.params || "";
        let cekPerangkat = await getDetailById(idPerangkatPembelajaran);
        if (!cekPerangkat) return res.status(404).send(response.notFound('perangkat pembelajaran'));
        let deletedPerangkat = await softDelete(idPerangkatPembelajaran);
        if (!deletedPerangkat) return res.status(400).send(response.badRequest('gagal menghapus perangkat pembelajaran'));
        return res.status(200).send(response.customSuccess('berhasil menghapus perangkat pembelajaran'));
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;