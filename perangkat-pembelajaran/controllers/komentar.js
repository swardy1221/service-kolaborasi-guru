const { komentar, getDetailById } = require('../services/index');
const response = require('../../config/responses');
const controller = async (req, res) => {
    try {
        let idUser = req.user.idUser || "";
        let { isiKomentar } = req.body || "";
        let { idPerangkatPembelajaran } = req.params || "";

        let cekPerangkat = await getDetailById(idPerangkatPembelajaran);
        if (!cekPerangkat) return res.status(404).send(response.notFound('perangkat pembelajaran'));

        let createdKomentar = await komentar(idUser, idPerangkatPembelajaran, isiKomentar);
        if (!createdKomentar) return res.status(400).send(response.badRequest('gagal menambahkan komentar'));

        return res.status(201).send(response.created(createdKomentar, 'komentar'));
    } catch (error) {
        return res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;