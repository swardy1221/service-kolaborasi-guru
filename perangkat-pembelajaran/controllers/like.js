const { like, getDetailById } = require('../services/index');
const response = require('../../config/responses');

const controller = async (req, res) => {
    try {
        let idUser = req.user.idUser;
        let { idPerangkatPembelajaran } = req.params || "";

        let cekPerangkat = await getDetailById(idPerangkatPembelajaran);
        if (!cekPerangkat) return res.status(404).send(response.notFound('perangkat pembelajaran'));

        let updatedPerangkat = await like(idUser, idPerangkatPembelajaran);
        if (!updatedPerangkat) return res.status(400).send(response.badRequest('gagal like/unlike perangkat pembelajaran'));

        return res.status(200).send(response.customSuccess('berhasil like/unlike perangkat pembelajaran'));
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;