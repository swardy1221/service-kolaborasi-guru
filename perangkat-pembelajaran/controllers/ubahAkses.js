const response = require('../../config/responses');
const {
    getDetailById,
    ubahAkses,
} = require('../services/index');

const controller = async (req, res) => {
    try {
        let { idPerangkatPembelajaran } = req.params || "";

        let cekPerangkat = await getDetailById(idPerangkatPembelajaran);
        if (!cekPerangkat) return res.status(404).send(response.notFound('perangkat pembelajaran'));
        let akses = cekPerangkat.akses == 'draft' ? 'terbit' : 'draft';
        let ubahAksesPerangkatPembelajaran = await ubahAkses(idPerangkatPembelajaran, akses);
        if (!ubahAksesPerangkatPembelajaran) return res.status(400).send(response.badRequest('gagal mengubah akses perangkat pembelajaran'));

        let updatedPerangkatPembelajaran = await getDetailById(idPerangkatPembelajaran);
        return res.status(200).send(response.updated(updatedPerangkatPembelajaran, 'perangkat pembelajaran'));
    } catch (error) {
        return res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;