const response = require('../../config/responses');
const { getDetailById, downloadCounter } = require('../services/index');
const path = require('path')

const controller = async (req, res) => {
    try {
        let { idPerangkatPembelajaran } = req.params || "";
        let idUser = req.user.idUser;

        let cekPerangkat = await getDetailById(idPerangkatPembelajaran);
        if (!cekPerangkat) return res.status(404).send(response.notFound('perangkat pembelajaran'));

        let fullPath = path.join(__baseDir, `public`, cekPerangkat.file);
        // console.log(fullPath);
        // let fileName = perangkat.file.split(`/`)[3];
        // console.log(fileName)
        return res.download(fullPath, async (error) => {
            if (error) return res.status(500).send(response.serverError(error))
            await downloadCounter({ idPerangkatPembelajaran, idUser });
        });
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;