const { getByStatus } = require('../services/index');
const response = require('../../config/responses');

const controller = async (req, res) => {
    try {
        let status = req.params.status || "";
        let perangkatPembelajaran = await getByStatus(status);

        if (!perangkatPembelajaran) return res.status(404).send(response.notFound('perangkat pembelajaran'));
        return res.status(200).send(response.success(perangkatPembelajaran, 'perangkat pembelajaran'));
    } catch (error) {
        res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;