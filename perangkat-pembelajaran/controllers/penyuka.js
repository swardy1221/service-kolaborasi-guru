const response = require('../../config/responses');
const { getDetailById, penyuka } = require('../services/index');

const controller = async (req, res) => {
    try {
        let { idPerangkatPembelajaran } = req.params || "";
        let cekPerangkat = await getDetailById(idPerangkatPembelajaran);
        if (!cekPerangkat) return res.status(404).send(response.notFound('perangkat pembelajaran'));

        let listPenyuka = await penyuka(idPerangkatPembelajaran);
        if (!listPenyuka) return res.status(404).send(response.notFound('penyuka perangkat pembelajaran'));
        return res.status(200).send(response.success(listPenyuka, 'penyuka perangkat pembelajaran'));
    } catch (error) {
        return res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;