const response = require('../../config/responses');
const { ubahAkses, getDetailById, riwayatRevisiById } = require('../services');

const controller = async (req, res) => {
    try {
        let { idPerangkatPembelajaran } = req.params || "";
        let { aksesDokumen } = req.body || "";

        let cekPerangkat = await getDetailById(idPerangkatPembelajaran);
        // console.log(cekPerangkat);
        if (!cekPerangkat) return res.status(404).send(response.notFound('perangkat pembelajaran'));
        if (aksesDokumen == 'draft' && cekPerangkat.divalidasi_oleh != null) return res.status(400).send(response.badRequest('perangkat sudah diverifikasi tidak dapat di draft'));
        if (cekPerangkat.akses == aksesDokumen) return res.status(400).send(response.badRequest('status akses masih sama'));
        let tambah = false;
        if (cekPerangkat.pernah_terbit == 1 && aksesDokumen == 'terbit') {
            console.log('if 1')
            let riwayat = await riwayatRevisiById(idPerangkatPembelajaran, cekPerangkat.status_dokumen);
            if (riwayat.length > 1) {
                console.log('if 2')
                tambah = true;
            }
        }
        console.log(tambah)
        let updatePerangkat = await ubahAkses(cekPerangkat, aksesDokumen, tambah);
        if (!updatePerangkat) return res.status(400).send(response.badRequest('gagal mengubah akses perangkat pembelajaran'));
        return res.status(200).send(response.customSuccess('berhasil mengubah akses perangkat pembelajaran'));
    } catch (error) {
        return res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;