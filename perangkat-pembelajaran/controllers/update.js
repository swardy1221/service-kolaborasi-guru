const {
    update,
    getDetailById
} = require("../services/index");
const slugGenerator = require('../../helpers/slugGenerator');
const response = require("../../config/responses");
const crypto = require('crypto');

const controller = async (req, res) => {
    try {
        let { idPerangkatPembelajaran } = req.params || "";
        let cekPerangkat = await getDetailById(idPerangkatPembelajaran);
        if (!cekPerangkat) return res.status(404).send(response.notFound('perangkat pembelajaran'));
        let now = new Date().getTime();
        let idUser = req.user.idUser;
        let {
            tipePerangkatPembelajaran,
            idMataPelajaran,
            namaMataPelajaran,
            idKompetensiKeahlian,
            kompetensiKeahlian,
            judul,
            deskripsi,
            akses,
            statusDokumen,
            catatanRevisi
        } = req.body || "";
        //Cek status_dokumen harus berubah/berbeda dengan sebelumnya
        if (cekPerangkat.pernah_terbit == 1 && akses == 'terbit' && cekPerangkat.status_dokumen <= statusDokumen) return res.status(400).send(response.badRequest('status dokumen harus berubah'));
        let createdAt = now;
        let updatedAt = now;
        let slug = await slugGenerator(judul, 5);
        let kodeRevisi = crypto.randomBytes(10).toString('hex');
        // console.log(kodeRevisi);
        let dataPerangkat = {
            idUser,
            idPerangkatPembelajaran,
            tipePerangkatPembelajaran,
            idMataPelajaran,
            namaMataPelajaran,
            idKompetensiKeahlian,
            kompetensiKeahlian,
            judul,
            slug,
            deskripsi,
            akses,
            statusDokumen,
            catatanRevisi,
            kodeRevisi,
            createdAt,
            updatedAt
        };
        //Jika belum pernah terbit, dan akses == 'terbit', maka di update agar pernah_terbit = 1
        if (cekPerangkat.pernah_terbit == 0 && akses == 'terbit') {
            dataPerangkat.pernahTerbit = 1;
        }
        if (!req.files['sampul'][0]) dataPerangkat.sampul = `/image/default/${tipePerangkatPembelajaran}.png`;
        dataPerangkat.sampul = `/perangkat-ajar/sampul/${req.files['sampul'][0]['filename']}`;
        if (!req.files['file'][0]) dataPerangkat.file = null;
        dataPerangkat.file = `/perangkat-ajar/file/${req.files['file'][0]['filename']}`;

        let updatedPerangkat = await update(dataPerangkat);
        if (!updatedPerangkat) return res.status(400).send(response.badRequest('gagal mengubah perangkat pembelajaran'));
        return res.status(200).send(response.updated(updatedPerangkat, 'perangkat pembelajaran'));
    } catch (error) {
        return res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;