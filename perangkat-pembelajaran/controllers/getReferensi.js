const response = require('../../config/responses');
const {
    getDetailById,
    getReferensi
} = require('../services/index');

const controller = async (req, res) => {
    try {
        let { idPerangkatPembelajaran } = req.params || "";

        let cekPerangkat = await getDetailById(idPerangkatPembelajaran);
        if (!cekPerangkat) return res.status(404).send(response.notFound('perangkat pembelajaran'));

        let referensi = await getReferensi(idPerangkatPembelajaran);
        if (!referensi.length) return res.status(404).send(response.notFound('referensi'));

        return res.status(200).send(response.success(referensi, 'referensi perangkat pembelajaran'))
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;