const response = require('../../config/responses');
const { riwayatRevisiByKode, getDetailById } = require('../services/index');

const controller = async (req, res) => {
    try {
        let { idPerangkatPembelajaran, kodeRevisi } = req.params || "";
        let cekPerangkat = await getDetailById(idPerangkatPembelajaran);
        if (!cekPerangkat) return res.status(404).send(response.notFound('perangkat pembelajaran'));

        let riwayat = await riwayatRevisiByKode(idPerangkatPembelajaran, kodeRevisi);
        if (!riwayat) return res.status(404).send(response.notFound('riwayat revisi perangkat pembelajaran'));
        return res.status(200).send(response.success(riwayat, 'riwayat revisi perangkat pembelajaran'));
    } catch (error) {
        return res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;