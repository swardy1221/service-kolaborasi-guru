const db = require('../../config/database');

const service = async (idUser, tipe) => {
    try {
        let perangkatPembelajaran = await db('tb_perangkat_pembelajaran').where({ tipe_perangkat_pembelajaran: tipe, id_user: idUser });
        if (!perangkatPembelajaran.length) return false;
        return perangkatPembelajaran;
    } catch (error) {
        throw error
    }
}

module.exports = service;