const db = require('../../config/database');

const service = async (perangkat, akses, tambah) => {
    const trxProvider = db.transactionProvider();
    const trx = await trxProvider();
    try {
        switch (tambah) {
            case true:
                let statusRevisi = perangkat.status_dokumen;
                statusRevisi = statusRevisi == 'revisi3' ? 'final' :
                    'revisi' + (parseInt(statusRevisi[statusRevisi.length - 1]) + 1);
                let updatePerangkatTambah = await trx('tb_perangkat_pembelajaran')
                    .where('id_perangkat_pembelajaran', perangkat.id_perangkat_pembelajaran)
                    .update({
                        akses: akses,
                        status_dokumen: statusRevisi
                    });
                if (!updatePerangkatTambah) {
                    await trx.rollback();
                    return false
                }
                await trx.commit();
                let updatedPerangkatTambah = await db('tb_perangkat_pembelajaran')
                    .where('id_perangkat_pembelajaran', perangkat.id_perangkat_pembelajaran)
                    .first();
                return updatedPerangkatTambah;
            case false:
                let updatePerangkat = await trx('tb_perangkat_pembelajaran')
                    .where('id_perangkat_pembelajaran', perangkat.id_perangkat_pembelajaran)
                    .update({
                        akses: akses
                    });

                if (!updatePerangkat) {
                    await trx.rollback();
                    return false
                }
                await trx.commit();
                let updatedPerangkat = await db('tb_perangkat_pembelajaran')
                    .where('id_perangkat_pembelajaran', perangkat.id_perangkat_pembelajaran)
                    .first();
                return updatedPerangkat;
            default:
                break;
        }
        await trx.rollback();
        return false;
    } catch (error) {
        await trx.rollback();
        throw error;
    }
}

module.exports = service;