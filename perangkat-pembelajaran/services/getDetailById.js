const db = require('../../config/database');

const service = async (idPerangkat) => {
    try {
        let perangkat = await db('tb_perangkat_pembelajaran').where('id_perangkat_pembelajaran', idPerangkat).where('deleted_at', null).first();
        if (!perangkat) return false
        return perangkat;
    } catch (error) {
        throw error;
    }
}

module.exports = service;