const db = require('../../config/database');

const service = async (idPerangkat) => {
    try {
        let listIdQuery = db('tb_user_unduh_perangkat_pembelajaran').select('id_user').where('id_perangkat_pembelajaran', idPerangkat);

        let pengunduh = await db('tb_user')
            .join('tb_user_unduh_perangkat_pembelajaran', 'tb_user.id_user', 'tb_user_unduh_perangkat_pembelajaran.id_user')
            .join('tb_user_guru', 'tb_user.id_user', 'tb_user_guru.id_user')
            .select('tb_user_guru.id_user', 'tb_user_guru.nama_lengkap', 'tb_user_guru.nuptk', 'tb_user_guru.nama_sekolah', 'tb_user_guru.foto_profile', 'tb_user_unduh_perangkat_pembelajaran.created_at')
            .where('tb_user_unduh_perangkat_pembelajaran.id_perangkat_pembelajaran', idPerangkat)
            .whereIn('tb_user.id_user', listIdQuery);
        if (!pengunduh.length) return false;
        return pengunduh;
    } catch (error) {
        throw error;
    }
}
module.exports = service;