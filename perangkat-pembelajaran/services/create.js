const db = require('../../config/database');

const service = async (dataPerangkat) => {
    try {
        const trxProvider = db.transactionProvider();
        const trx = await trxProvider();
        try {
            let createPerangkat = await trx('tb_perangkat_pembelajaran').insert({
                id_user: dataPerangkat.idUser,
                tipe_perangkat_pembelajaran: dataPerangkat.tipePerangkatPembelajaran,
                id_mata_pelajaran: dataPerangkat.idMataPelajaran,
                nama_mapel: dataPerangkat.namaMataPelajaran,
                id_kompetensi_keahlian: dataPerangkat.idKompetensiKeahlian,
                kompetensi_keahlian: dataPerangkat.kompetensiKeahlian,
                judul: dataPerangkat.judul,
                slug: dataPerangkat.slug,
                deskripsi: dataPerangkat.deskripsi,
                akses: dataPerangkat.akses,
                sampul: dataPerangkat.sampul,
                file: dataPerangkat.file,
                status_dokumen: dataPerangkat.statusDokumen,
                pernah_terbit: dataPerangkat.pernahTerbit,
                created_at: dataPerangkat.createdAt,
                updated_at: dataPerangkat.updatedAt
            })

            let createRiwayat = await db('tb_riwayat_revisi_perangkat_pembelajaran').transacting(trx).insert({
                id_perangkat_pembelajaran: createPerangkat,
                catatan_revisi: "Initial Revisi",
                kode_revisi: dataPerangkat.kodeRevisi,
                tipe_perangkat_pembelajaran: dataPerangkat.tipePerangkatPembelajaran,
                id_mata_pelajaran: dataPerangkat.idMataPelajaran,
                nama_mapel: dataPerangkat.namaMataPelajaran,
                id_kompetensi_keahlian: dataPerangkat.idKompetensiKeahlian,
                kompetensi_keahlian: dataPerangkat.kompetensiKeahlian,
                judul: dataPerangkat.judul,
                deskripsi: dataPerangkat.deskripsi,
                sampul: dataPerangkat.sampul,
                file: dataPerangkat.file,
                status_dokumen: dataPerangkat.statusDokumen,
                created_at: dataPerangkat.createdAt
            });

            if (!createPerangkat || !createRiwayat) {
                await trx.rollback()
                return false
            }
            await trx.commit();
            let createdPerangkat = await db('tb_perangkat_pembelajaran').where('id_perangkat_pembelajaran', createPerangkat).first();
            return createdPerangkat;
        } catch (error) {
            await trx.rollback();
            throw error;
        }
    } catch (error) {
        throw error;
    }
}

module.exports = service;