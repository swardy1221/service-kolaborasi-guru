const db = require('../../config/database');

const service = async (idUser, idPerangkatPembelajaran) => {
    try {
        let cekLike = await db('tb_user_like_perangkat_pembelajaran').where({
            id_user: idUser,
            id_perangkat_pembelajaran: idPerangkatPembelajaran
        }).first();

        if (!cekLike) return false;
        return true;
    } catch (error) {
        throw error;
    }
}

module.exports = service;