const db = require('../../../config/database');

const service = async (options) => {
    try {
        const page = parseInt(options.page || 1);
        const limit = parseInt(options.limit || 8);
        const offset = (page - 1) * limit;
        let perangkatPembelajaran = await db('tb_perangkat_pembelajaran').limit(limit)
            .offset(offset);
        if (!perangkatPembelajaran.length) return false;
        return perangkatPembelajaran;
    } catch (error) {
        throw error;
    }
}

module.exports = service;