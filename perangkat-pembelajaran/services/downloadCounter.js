const db = require('../../config/database');

const service = async (data) => {
    const trxProvider = db.transactionProvider();
    const trx = await trxProvider();
    try {
        let logUnduh = await trx('tb_user_unduh_perangkat_pembelajaran').insert({
            id_user: data.idUser,
            id_perangkat_pembelajaran: data.idPerangkatPembelajaran,
            created_at: new Date().getTime()
        })

        // if (!logUnduh) {
        //     await trx.rollback()
        //     return false
        // }

        let counterUnduh = await db('tb_perangkat_pembelajaran').transacting(trx).where('id_perangkat_pembelajaran', data.idPerangkatPembelajaran).increment('diunduh_sebanyak', 1);
        if (!counterUnduh || !logUnduh) {
            await trx.rollback();
            return false
        }

        await trx.commit()
        return true;
    } catch (error) {
        throw error;
    }
}

module.exports = service;