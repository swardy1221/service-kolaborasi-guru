const db = require('../../config/database');

const service = async (idPerangkatPembelajaran, akses) => {
    try {
        let ubahAkses = await db('tb_perangkat_pembelajaran').where('id_perangkat_pembelajaran', idPerangkatPembelajaran).update({
            akses
        });

        if (!ubahAkses) return false;
        return true;
    } catch (error) {
        throw error;
    }
}

module.exports = service;