const db = require('../../config/database');

const service = async (dataPerangkat) => {
    const trxProvider = db.transactionProvider();
    const trx = await trxProvider();
    try {
        let updatePerangkat = dataPerangkat.pernahTerbit ? await trx('tb_perangkat_pembelajaran').where('id_perangkat_pembelajaran', dataPerangkat.idPerangkatPembelajaran).update({
            tipe_perangkat_pembelajaran: dataPerangkat.tipePerangkatPembelajaran,
            id_mata_pelajaran: dataPerangkat.idMataPelajaran,
            nama_mapel: dataPerangkat.namaMataPelajaran,
            id_kompetensi_keahlian: dataPerangkat.idKompetensiKeahlian,
            kompetensi_keahlian: dataPerangkat.kompetensiKeahlian,
            judul: dataPerangkat.judul,
            slug: dataPerangkat.slug,
            deskripsi: dataPerangkat.deskripsi,
            file: dataPerangkat.file,
            sampul: dataPerangkat.sampul,
            status_dokumen: dataPerangkat.statusDokumen,
            akses: dataPerangkat.akses,
            pernah_terbit: dataPerangkat.pernahTerbit
        }) : await trx('tb_perangkat_pembelajaran').where('id_perangkat_pembelajaran', dataPerangkat.idPerangkatPembelajaran).update({
            tipe_perangkat_pembelajaran: dataPerangkat.tipePerangkatPembelajaran,
            id_mata_pelajaran: dataPerangkat.idMataPelajaran,
            nama_mapel: dataPerangkat.namaMataPelajaran,
            id_kompetensi_keahlian: dataPerangkat.idKompetensiKeahlian,
            kompetensi_keahlian: dataPerangkat.kompetensiKeahlian,
            judul: dataPerangkat.judul,
            slug: dataPerangkat.slug,
            deskripsi: dataPerangkat.deskripsi,
            file: dataPerangkat.file,
            sampul: dataPerangkat.sampul,
            status_dokumen: dataPerangkat.statusDokumen,
            akses: dataPerangkat.akses,
        });
        if (!updatePerangkat) {
            await trx.rollback();
            return false;
        }

        let logRevisi = await db('tb_riwayat_revisi_perangkat_pembelajaran').transacting(trx).insert({
            id_perangkat_pembelajaran: dataPerangkat.idPerangkatPembelajaran,
            kode_revisi: dataPerangkat.kodeRevisi,
            catatan_revisi: dataPerangkat.catatanRevisi,
            tipe_perangkat_pembelajaran: dataPerangkat.tipePerangkat,
            id_mata_pelajaran: dataPerangkat.idMataPelajaran,
            nama_mapel: dataPerangkat.namaMataPelajaran,
            id_kompetensi_keahlian: dataPerangkat.idKompetensiKeahlian,
            kompetensi_keahlian: dataPerangkat.kompetensiKeahlian,
            judul: dataPerangkat.judul,
            deskripsi: dataPerangkat.deskripsi,
            file: dataPerangkat.file,
            sampul: dataPerangkat.sampul,
            status_dokumen: dataPerangkat.statusDokumen,
            created_at: dataPerangkat.createdAt,
        });
        if (!logRevisi) {
            await trx.rollback();
            return false;
        }
        await trx.commit()
        let updatedPerangkat = await db('tb_perangkat_pembelajaran').where('id_perangkat_pembelajaran', dataPerangkat.idPerangkatPembelajaran).first();
        return updatedPerangkat;
    } catch (error) {
        throw error;
    }
}

module.exports = service;