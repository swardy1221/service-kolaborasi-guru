const db = require('../../config/database');

const service = async (idUser, idPerangkat, komentar) => {
    const trxProvider = db.transactionProvider();
    const trx = await trxProvider();
    try {
        let createKomentar = await trx('tb_user_komentar_perangkat_pembelajaran').insert({
            id_user: idUser,
            id_perangkat_pembelajaran: idPerangkat,
            komentar,
            created_at: new Date().getTime()
        });
        console.log(createKomentar)
        if (!createKomentar) {
            trx.rollback()
            return false;
        }

        let counterKomentar = await db('tb_perangkat_pembelajaran').transacting(trx).where('id_perangkat_pembelajaran', idPerangkat).increment('jumlah_komentar', 1);
        console.log(counterKomentar)
        if (!counterKomentar) {
            trx.rollback();
            return false;
        }
        await trx.commit()
        let createdKomentar = await db('tb_user_komentar_perangkat_pembelajaran').where({
            id_user: idUser,
            id_perangkat_pembelajaran: idPerangkat,
        }).orderBy('created_at', 'desc').first();
        console.log(createdKomentar)
        return createdKomentar;
    } catch (error) {
        await trx.rollback();
        throw error;
    }
}

module.exports = service;