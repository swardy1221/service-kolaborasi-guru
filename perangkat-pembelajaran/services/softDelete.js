const db = require('../../config/database');

const service = async (idPerangkat) => {
    try {
        let deletePerangkat = await db('tb_perangkat_pembelajaran').where('id_perangkat_pembelajaran', idPerangkat).update({
            deleted_at: new Date().getTime()
        });
        if (!deletePerangkat) return false
        return true
    } catch (error) {
        throw error;
    }
}

module.exports = service;