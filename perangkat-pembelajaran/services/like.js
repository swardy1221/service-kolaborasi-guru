const db = require('../../config/database');

const service = async (idUser, idPerangkat) => {
    try {
        const trxProvider = db.transactionProvider();
        const trx = await trxProvider();
        try {
            let statusLike = await db('tb_user_like_perangkat_pembelajaran').where({
                id_user: idUser,
                id_perangkat_pembelajaran: idPerangkat
            }).first();

            if (statusLike) {
                //Jika ada, maka unline
                let unlikePerangkat = await trx('tb_perangkat_pembelajaran').where('id_perangkat_pembelajaran', idPerangkat).decrement('jumlah_like', 1);
                if (!unlikePerangkat) {
                    await trx.rollback();
                    return false
                }
                let userUnlike = await db('tb_user_like_perangkat_pembelajaran').transacting(trx).where({
                    id_user: idUser,
                    id_perangkat_pembelajaran: idPerangkat
                }).del();
                if (!userUnlike) {
                    await trx.rollback()
                    return false
                }
                await trx.commit()
                return true
            }
            //Jika tidak ada, maka like
            let updatePerangkat = await trx('tb_perangkat_pembelajaran').where('id_perangkat_pembelajaran', idPerangkat).increment('jumlah_like', 1);
            if (!updatePerangkat) {
                await trx.rollback();
                return false
            }
            let userLike = await db('tb_user_like_perangkat_pembelajaran').transacting(trx).insert({
                id_user: idUser,
                id_perangkat_pembelajaran: idPerangkat,
                created_at: new Date().getTime()
            });
            if (!userLike) {
                await trx.rollback()
                return false
            }
            await trx.commit()
            return true
        } catch (error) {
            await trx.rollback();
            throw error;
        }
    } catch (error) {
        throw error;
    }
}

module.exports = service;