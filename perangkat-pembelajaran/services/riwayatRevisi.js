const db = require('../../config/database');

const service = async (idPerangkat) => {
    try {
        let riwayat = await db('tb_riwayat_revisi_perangkat_pembelajaran').where({
            id_perangkat_pembelajaran: idPerangkat
        });
        if (!riwayat) return false
        return riwayat
    } catch (error) {
        throw error
    }
}

module.exports = service;