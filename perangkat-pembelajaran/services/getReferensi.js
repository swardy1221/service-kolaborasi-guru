const db = require('../../config/database');


const service = async (idPerangkat) => {
    try {
        let idPerangkatReferensi = db('tb_referensi').where({
            id_entitas: idPerangkat,
            entitas: 'perangkat_pembelajaran'
        }).select('id_entitas_referensi');
        let referensi = await db('tb_perangkat_pembelajaran').where('id_perangkat_pembelajaran', 'in', idPerangkatReferensi);
        if (!referensi.length) return false;

        return referensi;
    } catch (error) {
        throw error;
    }
}

module.exports = service;