const db = require('../../config/database');

const service = async (slug) => {
    try {
        let perangkat = await db('tb_perangkat_pembelajaran').where('slug', slug).where('deleted_at', null).first();
        if (!perangkat) return false
        return perangkat
    } catch (error) {
        throw error;
    }
}

module.exports = service;