const db = require('../../config/database');

const service = async (status) => {
    try {
        let perangkatPembelajaran = await db('tb_perangkat_pembelajaran').where('status_dokumen', status);
        if (!perangkatPembelajaran.length) return false;
        return perangkatPembelajaran;
    } catch (error) {
        throw error;
    }
}

module.exports = service;