const db = require('../../config/database');

const service = async (idPerangkat) => {
    try {
        let komentar = await db('tb_user_komentar_perangkat_pembelajaran')
            .join('tb_user_guru', 'tb_user_komentar_perangkat_pembelajaran.id_user', '=', 'tb_user_guru.id_user')
            .join('tb_perangkat_pembelajaran', 'tb_user_komentar_perangkat_pembelajaran.id_perangkat_pembelajaran', '=', 'tb_perangkat_pembelajaran.id_perangkat_pembelajaran')
            .select('tb_user_guru.id_user', 'tb_user_guru.foto_profile', 'tb_user_guru.nama_lengkap', 'tb_user_komentar_perangkat_pembelajaran.komentar', 'tb_user_komentar_perangkat_pembelajaran.created_at')
            .where('tb_user_komentar_perangkat_pembelajaran.id_perangkat_pembelajaran', idPerangkat);
        if (!komentar.length) return false;
        return komentar
        // let idPerangkat = await db('tb_perangkat_pembelajaran').where('id_perangkat_pembelajaran', idPerangkat).first();
        // console.log(idPerangkat);
        let listKomentatorQuery = db('tb_user_komentar_perangkat_pembelajaran').select('id_user').where('id_perangkat_pembelajaran', idPerangkat).distinct();
        let listKomentator = await db('tb_user_komentar_perangkat_pembelajaran').select('id_user').where('id_perangkat_pembelajaran', idPerangkat).distinct();
        // listKomentator = listKomentator.filter((item, index) => listKomentator.indexOf(item) !== index)
        console.log(listKomentator);
        let users = await db('tb_user_guru').select('foto_profile', 'nama_lengkap').whereIn('id_user', listKomentatorQuery);
        let isiKomentar = await db('tb_user_komentar_perangkat_pembelajaran').whereIn('id_user', listKomentatorQuery);
        console.log(users);
        console.log(isiKomentar);
        return { ...users, ...isiKomentar }
        if (!perangkat) return false
        return perangkat
    } catch (error) {
        throw error;
    }
}

module.exports = service;