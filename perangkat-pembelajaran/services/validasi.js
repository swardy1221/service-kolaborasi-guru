const db = require('../../config/database');

const service = async (idPerangkat, idUser, catatanValidasi, statusValidasi) => {
    const trxProvider = db.transactionProvider();
    const trx = await trxProvider();
    try {
        let updatePerangkat = await trx('tb_perangkat_pembelajaran').where('id_perangkat_pembelajaran', idPerangkat).update({
            divalidasi_oleh: idUser,
            status_dokumen: statusValidasi,
            catatan_validasi: catatanValidasi,
            updated_at: new Date().getTime(),
        })

        if (!updatePerangkat) {
            await trx.rollback();
            return false
        }

        await trx.commit();
        let updatedPerangkat = await db('tb_perangkat_pembelajaran').where('id_perangkat_pembelajaran', idPerangkat).first();
        return updatedPerangkat;
    } catch (error) {
        await trx.rollback();
        throw error;
    }
}

module.exports = service;