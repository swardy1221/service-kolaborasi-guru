const express = require("express");
const router = express.Router();
const {
    create,
    search,
    getDetailById,
    generateKodePaketSoal,
    ubahAkses,
    update,
    like,
    getDetailByKode,
    getByStatus,
    softDelete,
    validasi,
    getTerbaru,
    tambahKomentar,
    getKomentarById,
    getPenyuka,
    riwayatRevisiByKode,
    revisedById,
} = require('./controllers/index');

const {
    authUser,
    authRole,
    uploader
} = require('../middlewares/index');

/**
 * GET METHOD
 */
router.get('/', search)
router.get('/kode', authUser, generateKodePaketSoal);
router.get('/terbaru', authUser, getTerbaru);
router.get('/status/:status', authUser, getByStatus);
router.get('/:kodePaketSoal', authUser, getDetailByKode);
router.get('/:idPaketSoal/komentar', authUser, getKomentarById);
router.get('/:idPaketSoal/penyuka', authUser, getPenyuka);
router.get('/:kodePaketSoal/riwayat-revisi', authUser, riwayatRevisiByKode);
router.get('/:idPaketSoal/riwayat-revisi/:idRiwayatRevisi', authUser, revisedById);
/**
 * POST METHOD
 */
router.post('/', authUser, uploader.paketSoal.single('sampul'), create);
router.post('/:idPaketSoal/like', authUser, like);
router.post('/:idPaketSoal/komentar', authUser, tambahKomentar);

/**
 * PUT METHOD
 */
router.put('/:idPaketSoal', authUser, uploader.paketSoal.single('sampul'), update);
router.put('/:idPaketSoal/ubah-akses', authUser, ubahAkses);
router.put('/:idPaketSoal/validasi', authUser, authRole('admin'), validasi)

/**
 * DELETE METHOD
 */
router.delete('/:idPaketSoal', authUser, softDelete)

module.exports = router;
