const response = require('../../config/responses');
const {
    getDetailById,
    revisedById,
} = require('../services/index');

const controller = async (req, res) => {
    try {
        let { idPaketSoal, idRiwayatRevisi } = req.params || "";

        let cekPaketSoal = await getDetailById(idPaketSoal);
        if (!cekPaketSoal) return res.status(404).send(response.notFound('paket soal'));

        let revised = await revisedById(idPaketSoal, idRiwayatRevisi);
        if (!revised) return res.status(404).send(response.notFound('riwayat revisi paket soal'));

        return res.status(200).send(response.success(revised, 'riwayat revisi paket soal'));
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;