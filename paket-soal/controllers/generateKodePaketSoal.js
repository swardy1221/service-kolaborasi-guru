const response = require('../../config/responses');
const generateKodePaketSoal = require('../services/generateKodePaketSoal');

const controller = async (req, res) => {
    try {
        let kode = await generateKodePaketSoal();
        return res.status(200).send(response.success(kode, 'kode paket soal'))
        return kode;
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;