const response = require('../../config/responses');
const {
    search
} = require('../services/index');

const controller = async (req, res) => {
    try {
        let { idMataPelajaran } = req.query || "";

        let paketSoal = await search.byMataPelajaran(idMataPelajaran);

        if (!paketSoal.length) return res.status(404).send(response.notFound('paket soal'));
        return res.status(200).send(response.success(paketSoal, 'paket soal'));
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;