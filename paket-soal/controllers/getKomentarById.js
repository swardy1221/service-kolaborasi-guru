const response = require('../../config/responses');
const {
    getKomentarById,
    getDetailById,
} = require('../services/index');
const creatorName = require('../../helpers/creatorName');

const controller = async (req, res) => {
    try {
        let { idPaketSoal } = req.params || "";
        console.log(idPaketSoal);
        let cekPaketSoal = await getDetailById(idPaketSoal);
        if (!cekPaketSoal) return res.status(404).send(response.notFound('paket soal'));

        let komentar = await getKomentarById(idPaketSoal);
        if (!komentar.length) return res.status(404).send(response.notFound('komentar pada paket soal'));

        for (satuKomentar of komentar) {
            let komentator = await creatorName(satuKomentar.id_user);
            satuKomentar.user = komentator;
        }

        return res.status(200).send(response.success(komentar, 'komentar pada paket soal'));
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;