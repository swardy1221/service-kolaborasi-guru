const response = require('../../config/responses');
const {
    create,
    getDetailById,
    getSoalById,
    getDetailByKode,
} = require('../services/index');
const getOpsiSoalByIdSoal = require('../../soal/services/getOpsiById');

const controller = async (req, res) => {
    try {
        let { idUser } = req.user || "";
        let createdAt = new Date().getTime();
        let {
            idMataPelajaran,
            namaMataPelajaran,
            kodePaketSoal,
            judul,
            deskripsi,
            jumlahSoal,
            statusDokumen,
            akses,
            soal,
            skorSoal
        } = req.body || "";

        let cekPaketSoal = await getDetailByKode(kodePaketSoal);
        if (cekPaketSoal) return res.status(409).send(response.duplicate('kode paket soal sudah digunakan'));

        //preproses sampul
        let file;
        if (req.file) {
            // console.log(req.file);
            file = `/paket-soal/sampul/${req.file.filename}`;
        } else {
            file = `/image/default/paket-soal.png`
        }


        //Preproses data soal
        dataSoal = [];
        let idSoal;
        let skor;
        for (let index = 0; index < jumlahSoal; index++) {
            idSoal = soal[index];
            skor = skorSoal ? skorSoal[index] : 1;
            dataSoal.push({
                idSoal,
                skor
            })
        }


        //Preproses data paket soal
        let dataPaketSoal = {
            idUser,
            idMataPelajaran,
            namaMataPelajaran,
            kodePaketSoal,
            sampul: file,
            judul,
            deskripsi,
            jumlahSoal,
            statusDokumen,
            akses,
            revisiKe: 1,
            catatanRevisi: "Pertama kali dibuat",
            createdAt,
            dataSoal
        }

        let createPaketSoal = await create(dataPaketSoal);
        if (!createPaketSoal) return res.status(400).send(response.badRequest('gagal membuat paket soal'));

        let soalByPaketSoal = await getSoalById(createPaketSoal);
        if (!soalByPaketSoal.length) return res.status(404).send(response.notFound('soal'));

        for (satuSoal of soalByPaketSoal) {
            let opsi = await getOpsiSoalByIdSoal(satuSoal.id_soal);
            if (!opsi) return res.status(404).send(response.notFound('opsi jawaban'));
            satuSoal.opsiJawaban = opsi;
        }

        let createdPaketSoal = await getDetailById(createPaketSoal);
        createdPaketSoal.soal = soalByPaketSoal;

        return res.status(201).send(response.created(createdPaketSoal, 'paket soal'));
    } catch (error) {
        return res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;