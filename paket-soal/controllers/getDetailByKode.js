const response = require('../../config/responses');
const {
    getDetailByKode,
    getSoalById,
    cekLike,
    counterDilihat,
} = require('../services/index');
const getOpsiSoalByIdSoal = require('../../soal/services/getOpsiById');

const controller = async (req, res) => {
    try {
        let { idUser } = req.user || "";
        let { kodePaketSoal } = req.params || "";

        let addCounter = await counterDilihat(kodePaketSoal);
        // if (!addCounter) return res.status(400).send(response.badRequest('terjadi kesalahan'));

        let cekPaketSoal = await getDetailByKode(kodePaketSoal);
        if (!cekPaketSoal) return res.status(404).send(response.notFound('paket soal'));

        let soalByPaketSoal = await getSoalById(cekPaketSoal.id_paket_soal);
        if (!soalByPaketSoal.length) return res.status(404).send(response.notFound('soal'));

        for (satuSoal of soalByPaketSoal) {
            let opsi = await getOpsiSoalByIdSoal(satuSoal.id_soal);
            if (!opsi) return res.status(404).send(response.notFound('opsi jawaban'));
            satuSoal.opsiJawaban = opsi;
        }
        cekPaketSoal.like = await cekLike(idUser, cekPaketSoal.id_paket_soal);
        cekPaketSoal.soal = soalByPaketSoal;
        return res.status(200).send(response.success(cekPaketSoal, 'paket soal'));
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;