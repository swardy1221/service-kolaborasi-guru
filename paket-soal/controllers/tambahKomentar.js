const response = require('../../config/responses');
const {
    tambahKomentar,
    getKomentarById,
    getDetailById,
} = require('../services/index');

const controller = async (req, res) => {
    try {
        let { idUser } = req.user || "";
        let { idPaketSoal } = req.params || "";
        let { komentar } = req.body || "";

        let dataKomentar = {
            idUser,
            idPaketSoal,
            komentar,
            createdAt: new Date().getTime()
        }
        let cekPaketSoal = await getDetailById(idPaketSoal);
        if (!cekPaketSoal) return res.status(404).send(response.notFound('paket soal'));
        let tambahKomentarPaketSoal = await tambahKomentar(dataKomentar);
        if (!tambahKomentarPaketSoal) return res.status(400).send(response.badRequest('gagal menambahkan komentar pada paket soal'));

        let createdKomentar = await getKomentarById(idPaketSoal);
        return res.status(201).send(response.created(createdKomentar, 'komentar'));
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;