const response = require('../../config/responses');
const {
    getDetailById,
    validasi,
} = require('../services/index');

const controller = async (req, res) => {
    try {
        let { idUser } = req.user || "";
        let { idPaketSoal } = req.params || "";
        let { statusValidasi, catatanValidasi } = req.body || "";

        let cekPaketSoal = await getDetailById(idPaketSoal);
        if (!cekPaketSoal) return res.status(404).send(response.notFound('paket soal'));
        if (cekPaketSoal.status_dokumen != 'final') return res.status(400).send(response.badRequest('status paket soal belum final'))
        let validasiPaketSoal = await validasi(idUser, idPaketSoal, statusValidasi, catatanValidasi);
        if (!validasiPaketSoal) return res.status(400).send(response.badRequest('validasi paket soal gagal'));

        return res.status(200).send(response.customSuccess('validasi paket soal berhasil'))
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;