const response = require('../../config/responses');
const {
    cekLike,
    like,
    unlike
} = require('../services/index');

const controller = async (req, res) => {
    try {
        let { idUser } = req.user || "";
        let { idPaketSoal } = req.params || "";
        let statusLike = await cekLike(idUser, idPaketSoal);
        console.log(statusLike);
        if (!statusLike) {
            //Like
            console.log('like')
            let likePaketSoal = await like(idUser, idPaketSoal);
            if (!likePaketSoal) return res.status(400).send(response.badRequest('menyukai paket soal gagal'));

            return res.status(200).send(response.customSuccess('menyukai paket soal berhasil'));
        } else {
            console.log('unlike')
            //unlike
            let unlikePaketSoal = await unlike(idUser, idPaketSoal);
            if (!unlikePaketSoal) return res.status(400).send(response.badRequest('batal menyukai paket soal gagal'));

            return res.status(200).send(response.customSuccess('batal menyukai paket soal berhasil'));
        }
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;