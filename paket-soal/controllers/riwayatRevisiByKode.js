const response = require('../../config/responses');
const {
    getDetailByKode,
    riwayatRevisiByKode,
} = require('../services/index');

const controller = async (req, res) => {
    try {
        let { kodePaketSoal } = req.params || "";

        let cekPaketSoal = await getDetailByKode(kodePaketSoal);
        if (!cekPaketSoal) return res.status(404).send(response.notFound('paket soal'));

        let riwayatRevisi = await riwayatRevisiByKode(cekPaketSoal.id_paket_soal);
        if (!riwayatRevisi.length) return res.status(404).send(response.notFound('riwayat revisi paket soal'));

        return res.status(200).send(response.success(riwayatRevisi, 'riwayat revisi paket soal'));
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;