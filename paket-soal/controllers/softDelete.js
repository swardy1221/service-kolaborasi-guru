const response = require('../../config/responses');
const {
    getDetailById,
    softDelete,
} = require('../services/index');

const controller = async (req, res) => {
    try {
        let { idUser } = req.user || "";
        let { idPaketSoal } = req.params || "";
        let cekPaketSoal = await getDetailById(idPaketSoal);
        if (!cekPaketSoal) return res.status(404).send(response.notFound('paket soal'));
        if (cekPaketSoal.dibuat_oleh != idUser) return res.status(403).send(response.forbidden('paket soal dibuat oleh pengguna lain'));
        let deletePaketSoal = await softDelete(idPaketSoal);
        if (!deletePaketSoal) return res.status(400).send(response.badRequest('gagal menghapus paket soal'));

        return res.status(200).send(response.deleted('paket soal'));
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;