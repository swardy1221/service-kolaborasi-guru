const response = require('../../config/responses');
const {
    getTerbaru,
    getSoalById,
} = require('../services/index');
const getOpsiSoalByIdSoal = require('../../soal/services/getOpsiById');

const controller = async (req, res) => {
    try {
        let paketSoal = await getTerbaru();
        if (!paketSoal.length) return res.status(404).send(response.notFound('paket soal terbaru'));

        // for (let satuPaket of paketSoal) {
        //     let soalByPaketSoal = await getSoalById(satuPaket.id_paket_soal);
        //     if (!soalByPaketSoal.length) return res.status(404).send(response.notFound('soal'));

        //     for (satuSoal of soalByPaketSoal) {
        //         let opsi = await getOpsiSoalByIdSoal(satuSoal.id_soal);
        //         if (!opsi) return res.status(404).send(response.notFound('opsi jawaban'));
        //         satuSoal.opsiJawaban = opsi;
        //     }
        //     satuPaket.soal = soalByPaketSoal;
        // }
        return res.status(200).send(response.success(paketSoal, 'paket soal'));
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;