const response = require('../../config/responses');
const {
    update,
    getDetailById,
    getSoalById
} = require('../services/index');
const getOpsiSoalByIdSoal = require('../../soal/services/getOpsiById');

const controller = async (req, res) => {
    try {
        let { idUser } = req.user || "";
        let { idPaketSoal } = req.params || "";
        let createdAt = new Date().getTime();
        let {
            idMataPelajaran,
            namaMataPelajaran,
            kodePaketSoal,
            judul,
            deskripsi,
            jumlahSoal,
            statusDokumen,
            catatanRevisi,
            akses,
            soal,
            skorSoal
        } = req.body || "";

        let cekPaketSoal = await getDetailById(idPaketSoal);
        if (!cekPaketSoal) return res.status(404).send(response.notFound('paket soal'));
        if (idUser != cekPaketSoal.dibuat_oleh) return res.status(403).send(response.forbidden('paket soal milik pengguna lain'));

        let file = req.file ? `/paket-soal/sampul/${req.file.filename}` : null;

        //Preproses data soal
        dataSoal = [];
        let idSoal;
        let skor;
        for (let index = 0; index < jumlahSoal; index++) {
            idSoal = soal[index];
            skor = skorSoal ? skorSoal[index] : 1;
            dataSoal.push({
                idSoal,
                skor
            })
        }

        // console.log(dataSoal)

        //Preproses data paket soal
        let dataPaketSoal = {
            idPaketSoal,
            idUser,
            idMataPelajaran,
            namaMataPelajaran,
            kodePaketSoal,
            sampul: file,
            judul,
            deskripsi,
            jumlahSoal,
            statusDokumen,
            akses,
            revisiKe: parseInt(cekPaketSoal.revisi_ke) + 1,
            catatanRevisi,
            createdAt,
            dataSoal
        }
        if (file) dataPaketSoal.sampul = file;

        let updatePaketSoal = await update(dataPaketSoal);
        if (!updatePaketSoal) return res.status(400).send(response.badRequest('gagal mengubah paket soal'));

        let soalByPaketSoal = await getSoalById(idPaketSoal);
        if (!soalByPaketSoal.length) return res.status(404).send(response.notFound('soal'));

        for (satuSoal of soalByPaketSoal) {
            let opsi = await getOpsiSoalByIdSoal(satuSoal.id_soal);
            if (!opsi) return res.status(404).send(response.notFound('opsi jawaban'));
            satuSoal.opsiJawaban = opsi;
        }

        let updatedPaketSoal = await getDetailById(idPaketSoal);
        updatedPaketSoal.soal = soalByPaketSoal;

        return res.status(200).send(response.updated(updatedPaketSoal, 'paket soal'))
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;