const response = require('../../config/responses');
const {
    getDetailById,
    getPenyuka,
} = require('../services/index');
const creatorName = require('../../helpers/creatorName');

const controller = async (req, res) => {
    try {
        let { idPaketSoal } = req.params || "";
        let cekPaketSoal = await getDetailById(idPaketSoal);
        if (!cekPaketSoal) return res.status(404).send(response.notFound('paket soal'));

        let penyuka = await getPenyuka(idPaketSoal);
        if (!penyuka.length) return res.status(404).send(response.notFound('penyuka paket soal'));

        for (liker of penyuka) {
            let detailUser = await creatorName(liker.id_user);
            liker.penyuka = detailUser;
        }

        return res.status(200).send(response.success(penyuka, 'penyuka paket soal'));
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;