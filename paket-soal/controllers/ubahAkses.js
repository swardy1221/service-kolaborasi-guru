const response = require('../../config/responses');
const {
    getDetailById,
    ubahAkses,
} = require('../services/index');

const controller = async (req, res) => {
    try {
        let { idPaketSoal } = req.params || "";
        let cekPaketSoal = await getDetailById(idPaketSoal);
        if (!cekPaketSoal) return res.status(404).send(response.notFound('paket soal'));
        let akses = cekPaketSoal.akses == 'draft' ? 'terbit' : 'draft';
        let ubahAksesPaketSoal = await ubahAkses(idPaketSoal, akses);
        if (!ubahAksesPaketSoal) return res.status(response.badRequest('gagal mengubah akses paket soal'));

        let updatedPaketSoal = await getDetailById(idPaketSoal);

        return res.status(200).send(response.updated(updatedPaketSoal, 'paket soal'))
    } catch (error) {
        return res.status(500).send(response.serverError(error))
    }
}

module.exports = controller;