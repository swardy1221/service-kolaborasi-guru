const db = require('../../config/database');

const service = async (idUser, idPaketSoal) => {
    try {
        let cekLike = await db('tb_user_like_paket_soal').where({
            id_user: idUser,
            id_paket_soal: idPaketSoal
        }).first();

        if (!cekLike) return false;
        return true;
    } catch (error) {
        throw error;
    }
}

module.exports = service;