const db = require('../../config/database');

const service = async (status) => {
    try {
        let paketSoal = await db('tb_paket_soal').where('status_dokumen', status);
        if (!paketSoal) return false;
        return paketSoal;
    } catch (error) {
        throw error;
    }
}

module.exports = service;