const db = require('../../config/database');

const service = async (idPaketSoal, idRevisi) => {
    try {
        let paketSoal = await db('tb_riwayat_revisi_paket_soal').where('id_riwayat_revisi_paket_soal', idRevisi).where('id_paket_soal', idPaketSoal).first();

        if (!paketSoal) return false;
        return paketSoal;
    } catch (error) {
        throw error;
    }
}

module.exports = service;