const db = require('../../config/database');

const service = async (kodePaketSoal) => {
    try {
        let paketSoal = await db('tb_paket_soal').where('kode_paket_soal', kodePaketSoal).first();
        if (!paketSoal) return false;
        return paketSoal;
    } catch (error) {
        throw error;
    }
}

module.exports = service;