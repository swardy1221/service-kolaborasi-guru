const db = require('../../config/database');

const service = async (dataKomentar) => {
    const trxProvider = db.transactionProvider();
    const trx = await trxProvider();
    try {
        let tambahKomentar = await trx('tb_user_komentar_paket_soal').insert({
            id_user: dataKomentar.idUser,
            id_paket_soal: dataKomentar.idPaketSoal,
            komentar: dataKomentar.komentar,
            created_at: dataKomentar.createdAt,
        });

        if (!tambahKomentar) {
            await trx.rollback();
            return false;
        }

        let counterKomentar = await db('tb_paket_soal').transacting(trx)
            .where('id_paket_soal', dataKomentar.idPaketSoal)
            .increment('jumlah_komentar', 1);
        if (!counterKomentar) {
            await trx.rollback();
            return false;
        }

        await trx.commit();
        return tambahKomentar;
    } catch (error) {
        await trx.rollback();
        throw error;
    }
}

module.exports = service;