const db = require('../../config/database');

const service = async (idUser, idPaketSoal, statusValidasi, catatanValidasi) => {
    try {
        let validasiPaketSoal = await db('tb_paket_soal').where('id_paket_soal', idPaketSoal).update({
            divalidasi_oleh: idUser,
            status_dokumen: statusValidasi,
            catatan_validasi: catatanValidasi,
            updated_at: new Date().getTime()
        })

        if (!validasiPaketSoal) return false;
        return true;
    } catch (error) {
        throw error;
    }
}

module.exports = service;