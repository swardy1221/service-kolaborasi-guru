const db = require('../../config/database');

const service = async (idPaketSoal) => {
    try {
        let paketSoal = await db('tb_paket_soal').where('id_paket_soal', idPaketSoal).where('deleted_at', null).first();;
        if (!paketSoal) return false;
        return paketSoal
    } catch (error) {
        throw error;
    }
}

module.exports = service;