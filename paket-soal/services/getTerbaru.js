const db = require('../../config/database');

const service = async () => {
    try {
        let paketSoalTerbaru = await db('tb_paket_soal').where('deleted_at', null).orderBy('created_at', 'desc').limit(4);
        if (!paketSoalTerbaru.length) return false;
        return paketSoalTerbaru;
    } catch (error) {
        throw error;
    }
}

module.exports = service;