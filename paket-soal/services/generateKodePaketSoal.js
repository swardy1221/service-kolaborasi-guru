const crypto = require('crypto');

const service = async () => {
    try {
        let kode = crypto.randomBytes(3).toString('hex').toUpperCase();
        return kode;
    } catch (error) {
        throw error;
    }
}

module.exports = service;