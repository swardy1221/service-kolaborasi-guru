const db = require('../../config/database');

const service = async (idPaketSoal) => {
    try {
        let komentar = await db('tb_user_komentar_paket_soal').where('id_paket_soal', idPaketSoal).orderBy('created_at', 'asc');
        if (!komentar.length) return false;
        return komentar;
    } catch (error) {
        throw error;
    }
}

module.exports = service;