const db = require('../../../config/database');

const service = async (idMataPelajaran) => {
    try {
        let paketSoal = await db('tb_paket_soal').where('id_mata_pelajaran', idMataPelajaran);

        if (!paketSoal.length) return false;
        return paketSoal;
    } catch (error) {
        throw error;
    }
}

module.exports = service;