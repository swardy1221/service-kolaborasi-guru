const db = require('../../config/database');

const service = async (dataPaketSoal) => {
    const trxProvider = db.transactionProvider();
    const trx = await trxProvider();
    try {
        let updatePaketSoal = await trx('tb_paket_soal').where('id_paket_soal', dataPaketSoal.idPaketSoal).update({
            dibuat_oleh: dataPaketSoal.idUser,
            id_mata_pelajaran: dataPaketSoal.idMataPelajaran,
            nama_mapel: dataPaketSoal.namaMataPelajaran,
            kode_paket_soal: dataPaketSoal.kodePaketSoal,
            sampul: dataPaketSoal.sampul,
            judul: dataPaketSoal.judul,
            deskripsi: dataPaketSoal.deskripsi,
            jumlah_soal: dataPaketSoal.jumlahSoal,
            status_dokumen: dataPaketSoal.statusDokumen,
            akses: dataPaketSoal.akses,
            revisi_ke: dataPaketSoal.revisiKe,
            updated_at: dataPaketSoal.createdAt
        });

        if (!updatePaketSoal) {
            await trx.rollback();
            return false;
        }

        //Delete soal-soal yg ada di paket soal
        let deleteSoalLama = await db('tb_paket_soal_berisi_soal').where('id_paket_soal', dataPaketSoal.idPaketSoal).del();
        console.log(deleteSoalLama)
        // if (!deleteSoalLama) {
        //     await trx.rollback();
        //     return false;
        // }
        //insert soal-soal ke dalam paket soal
        for (const soal of dataPaketSoal.dataSoal) {
            let paketSoalBerisiSoal = await db('tb_paket_soal_berisi_soal').transacting(trx).insert({
                id_paket_soal: dataPaketSoal.idPaketSoal,
                id_soal: soal.idSoal,
                skor: soal.skor,
                created_at: dataPaketSoal.createdAt
            })

            if (!paketSoalBerisiSoal) {
                await trx.rollback();
                return false
            }
        }

        // for (let index = 0; index < soalLama.length; index++) {
        //     let updateSoal = await db('tb_paket_soal_berisi_soal').transacting(trx)
        //         .where('id_paket_soal', dataPaketSoal.idPaketSoal)
        //         .where('id_soal', soalLama[index].id_soal)
        //         .update({
        //             id_soal: dataPaketSoal.dataSoal[index].idSoal
        //         });

        //     if (!updateSoal) {
        //         await trx.rollback();
        //         return false;
        //     }
        // }
        let createRiwayatPaketSoal = await db('tb_riwayat_revisi_paket_soal').transacting(trx).insert({
            id_paket_soal: dataPaketSoal.idPaketSoal,
            dibuat_oleh: dataPaketSoal.idUser,
            id_mata_pelajaran: dataPaketSoal.idMataPelajaran,
            nama_mapel: dataPaketSoal.namaMataPelajaran,
            sampul: dataPaketSoal.sampul,
            judul: dataPaketSoal.judul,
            deskripsi: dataPaketSoal.deskripsi,
            jumlah_soal: dataPaketSoal.jumlahSoal,
            status_dokumen: dataPaketSoal.statusDokumen,
            akses: dataPaketSoal.akses,
            revisi_ke: dataPaketSoal.revisiKe,
            catatan_revisi: dataPaketSoal.catatanRevisi,
            created_at: dataPaketSoal.createdAt,
        })

        if (!createRiwayatPaketSoal) {
            await trx.rollback();
            return false;
        }

        //Insert soal-soal ke dalam riwayat revisi paket soal
        for (const soal of dataPaketSoal.dataSoal) {
            let paketSoalBerisiSoal = await db('tb_riwayat_revisi_paket_soal_berisi_soal').transacting(trx).insert({
                id_riwayat_revisi_paket_soal: createRiwayatPaketSoal,
                id_soal: soal.idSoal,
                skor: soal.skor,
                created_at: dataPaketSoal.createdAt
            })

            if (!paketSoalBerisiSoal) {
                await trx.rollback();
                return false
            }
        }

        await trx.commit();
        return true;
    } catch (error) {
        throw error;
    }
}

module.exports = service;