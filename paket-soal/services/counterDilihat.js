const db = require('../../config/database');

const service = async (kodePaketSoal) => {
    try {
        let counterDilihat = await db('tb_paket_soal').where('kode_paket_soal', kodePaketSoal).increment('dilihat_sebanyak', 1);
        if (!counterDilihat) return false;
        return true;
    } catch (error) {
        throw error;
    }
}

module.exports = service;