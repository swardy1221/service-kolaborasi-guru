const db = require('../../config/database');

const service = async (dataPaketSoal) => {
    const trxProvider = db.transactionProvider();
    const trx = await trxProvider();
    try {
        let createPaketSoal = await trx('tb_paket_soal').insert({
            dibuat_oleh: dataPaketSoal.idUser,
            id_mata_pelajaran: dataPaketSoal.idMataPelajaran,
            nama_mapel: dataPaketSoal.namaMataPelajaran,
            kode_paket_soal: dataPaketSoal.kodePaketSoal,
            sampul: dataPaketSoal.sampul,
            judul: dataPaketSoal.judul,
            deskripsi: dataPaketSoal.deskripsi,
            jumlah_soal: dataPaketSoal.jumlahSoal,
            status_dokumen: dataPaketSoal.statusDokumen,
            akses: dataPaketSoal.akses,
            revisi_ke: dataPaketSoal.revisiKe,
            created_at: dataPaketSoal.createdAt,
            updated_at: dataPaketSoal.createdAt
        })

        if (!createPaketSoal) {
            await trx.rollback();
            return false;
        }

        //insert soal-soal ke dalam paket soal
        //Counter statistik pada soal
        for (const soal of dataPaketSoal.dataSoal) {
            let paketSoalBerisiSoal = await db('tb_paket_soal_berisi_soal').transacting(trx).insert({
                id_paket_soal: createPaketSoal,
                id_soal: soal.idSoal,
                skor: soal.skor,
                created_at: dataPaketSoal.createdAt
            })

            let counterSoal = await db('tb_soal').transacting(trx).increment('digunakan_sebanyak', 1).where('id_soal', soal.idSoal);

            if (!paketSoalBerisiSoal) {
                await trx.rollback();
                return false
            }
        }

        let createRiwayatPaketSoal = await db('tb_riwayat_revisi_paket_soal').transacting(trx).insert({
            id_paket_soal: createPaketSoal,
            dibuat_oleh: dataPaketSoal.idUser,
            id_mata_pelajaran: dataPaketSoal.idMataPelajaran,
            nama_mapel: dataPaketSoal.namaMataPelajaran,
            sampul: dataPaketSoal.sampul,
            judul: dataPaketSoal.judul,
            deskripsi: dataPaketSoal.deskripsi,
            jumlah_soal: dataPaketSoal.jumlahSoal,
            status_dokumen: dataPaketSoal.statusDokumen,
            akses: dataPaketSoal.akses,
            revisi_ke: dataPaketSoal.revisiKe,
            catatan_revisi: dataPaketSoal.catatanRevisi,
            created_at: dataPaketSoal.createdAt,
        })

        if (!createRiwayatPaketSoal) {
            await trx.rollback();
            return false;
        }

        //Insert soal-soal ke dalam riwayat revisi paket soal
        for (const soal of dataPaketSoal.dataSoal) {
            let paketSoalBerisiSoal = await db('tb_riwayat_revisi_paket_soal_berisi_soal').transacting(trx).insert({
                id_riwayat_revisi_paket_soal: createRiwayatPaketSoal,
                id_soal: soal.idSoal,
                skor: soal.skor,
                created_at: dataPaketSoal.createdAt
            })

            if (!paketSoalBerisiSoal) {
                await trx.rollback();
                return false
            }
        }

        await trx.commit();
        return createPaketSoal;
    } catch (error) {
        throw error;
    }
}

module.exports = service;