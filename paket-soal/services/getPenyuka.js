const db = require('../../config/database');

const service = async (idPaketSoal) => {
    try {
        let penyuka = await db('tb_user_like_paket_soal').where('id_paket_soal', idPaketSoal).select('id_user', 'created_at');
        if (!penyuka.length) return false;
        return penyuka;
    } catch (error) {
        throw error;
    }
}

module.exports = service;