const db = require('../../config/database');

const service = async (idPaketSoal) => {
    try {
        let riwayat = await db('tb_riwayat_revisi_paket_soal').where({
            id_paket_soal: idPaketSoal,
        });
        if (!riwayat.length) return false
        return riwayat
    } catch (error) {
        throw error
    }
}

module.exports = service;