const db = require('../../config/database');

const service = async (idUser, idPaketSoal) => {
    const trxProvider = db.transactionProvider();
    const trx = await trxProvider();
    try {
        let updateCounter = await trx('tb_paket_soal').where('id_paket_soal', idPaketSoal).increment('jumlah_like', 1)
        if (!updateCounter) {
            await trx.rollback();
            return false;
        }

        let userLike = await db('tb_user_like_paket_soal').transacting(trx).insert({
            id_user: idUser,
            id_paket_soal: idPaketSoal,
            created_at: new Date().getTime()
        });

        if (!userLike) {
            await trx.rollback();
            return false;
        }

        await trx.commit();
        return true;
    } catch (error) {
        await trx.rollback();
        throw error;
    }
}

module.exports = service;