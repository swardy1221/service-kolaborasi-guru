const db = require('../../config/database');

const service = async (idPaketSoal) => {
    try {
        let deletePaketSoal = await db('tb_paket_soal').where('id_paket_soal', idPaketSoal).update({
            deleted_at: new Date().getTime()
        });

        if (!deletePaketSoal) return false;
        return true;
    } catch (error) {
        throw error;
    }
}

module.exports = service;