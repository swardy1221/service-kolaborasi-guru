const db = require('../../config/database');

const service = async (idPaketSoal) => {
    try {
        let idSoal = db('tb_paket_soal_berisi_soal').where('id_paket_soal', idPaketSoal).select('id_soal');
        let soal = await db('tb_soal').where('id_soal', 'in', idSoal).orderByRaw('RAND()');
        // console.log(soal)
        if (!soal.length) return false;
        return soal
    } catch (error) {
        throw error;
    }
}

module.exports = service;