exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_lampiran", function (table) {
    table.increments("id_lampiran").unsigned().primary();
    table
      .integer("id_tugas")
      .unsigned()
      .nullable()
      .references("id_tugas")
      .inTable("tb_tugas")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .integer("id_diskusi")
      .unsigned()
      .nullable()
      .references("id_diskusi")
      .inTable("tb_diskusi")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.string("nama_file", 100).notNullable();
    table.string("alamat").notNullable();
    table
      .specificType("created_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP()"));
    table
      .specificType("updated_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()"));
    table.specificType("deleted_at", "DATETIME").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_lampiran");
};
