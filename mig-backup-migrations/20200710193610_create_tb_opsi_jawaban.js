exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_opsi_jawaban", function (table) {
    table.increments("id_opsi_jawaban").unsigned().primary();
    table
      .integer("id_soal")
      .unsigned()
      .references("id_soal")
      .inTable("tb_soal")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.string("deskripsi").notNullable();
    table.enu("tipe_file", ["gambar", "audio", "video", "lainnya"]).nullable();
    table.string("file").nullable();
    table.string("feedback").nullable();
    table.boolean("is_kunci").notNullable();
    table
      .specificType("created_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP()"));
    table
      .specificType("updated_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()"));
    table.specificType("deleted_at", "DATETIME").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_opsi_jawaban");
};
