exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_soal", function (table) {
    table.increments("id_soal").unsigned().primary();
    table
      .integer("id_user")
      .unsigned()
      .references("id_user")
      .inTable("tb_user")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .integer("id_kompetensi_dasar")
      .unsigned()
      .references("id_kompetensi_dasar")
      .inTable("ref_kompetensi_dasar")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .integer("id_kisi_kisi")
      .unsigned()
      .references("id_kisi_kisi")
      .inTable("tb_kisi_kisi")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .integer("id_grup")
      .unsigned()
      .references("id_grup")
      .inTable("tb_grup")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .enu("tipe_soal", ["pilihan_ganda", "isian_singkat", "benar_salah"])
      .nullable();
    table.integer("jumlah_jawaban").nullable(); //default 1
    table.string("teks_soal").notNullable();
    table.enu("tipe_file", ["gambar", "audio", "video", "lainnya"]).nullable();
    table.string("file").nullable();
    table
      .enu("tingkat_kesulitan", ["c1", "c2", "c3", "c4", "c5", "c6"])
      .nullable();
    table.string("kunci_essay").nullable();
    table.integer("jumlah_like").unsigned().nullable();
    table.integer("jumlah_dislike").unsigned().nullable();
    table.integer("jumlah_komentar").unsigned().nullable();
    table.integer("digunakan_sebanyak").unsigned().nullable();
    table.enu("status", ["perbaikan", "siap_validasi", "valid"]).notNullable();
    table
      .integer("divalidasi_oleh")
      .unsigned()
      .nullable()
      .references("id_user")
      .inTable("tb_user")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .specificType("created_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP()"));
    table
      .specificType("updated_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()"));
    table.specificType("deleted_at", "DATETIME").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_soal");
};
