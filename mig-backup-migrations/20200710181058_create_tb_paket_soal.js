exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_paket_soal", function (table) {
    table.increments("id_paket_soal").unsigned().primary();
    table
      .integer("dibuat_oleh")
      .unsigned()
      .references("id_user")
      .inTable("tb_user")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .integer("id_grup")
      .unsigned()
      .references("id_grup")
      .inTable("tb_grup")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .integer("id_mata_pelajaran")
      .unsigned()
      .references("id_mata_pelajaran")
      .inTable("tb_mata_pelajaran")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.boolean("poin_otomatis").notNullable(); //default true
    table.integer("poin_maksimal").notNullable(); // jika poin otomatis, maka = jumlah soal
    table.string("deskripsi").notNullable();
    table.integer("jumlah_soal").unsigned().notNullable();
    table.integer("digunakan_sebanyak").unsigned().nullable();
    table.integer("diunduh_sebanyak").unsigned().nullable();
    table
      .specificType("created_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP()"));
    table
      .specificType("updated_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()"));
    table.specificType("deleted_at", "DATETIME").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_paket_soal");
};
