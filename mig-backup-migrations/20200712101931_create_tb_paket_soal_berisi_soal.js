exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_paket_soal_berisi_soal", function (table) {
    table
      .integer("id_paket_soal")
      .unsigned()
      .references("id_paket_soal")
      .inTable("tb_paket_soal")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .integer("id_soal")
      .unsigned()
      .references("id_soal")
      .inTable("tb_soal")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.integer("poin").unsigned();
    table.primary(["id_paket_soal", "id_soal"]);
    table
      .specificType("created_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP()"));
    table
      .specificType("updated_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()"));
    table.specificType("deleted_at", "DATETIME").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_paket_soal_berisi_soal");
};
