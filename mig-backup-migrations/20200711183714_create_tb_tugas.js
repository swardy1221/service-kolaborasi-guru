exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_tugas", function (table) {
    table.increments("id_tugas").unsigned().primary();
    table
      .integer("id_grup")
      .unsigned()
      .references("id_grup")
      .inTable("tb_grup")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .integer("id_folder")
      .unsigned()
      .nullable()
      .references("id_folder")
      .inTable("tb_folder")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.string("judul_tugas", 100).notNullable();
    table.string("deskripsi").notNullable();
    table.specificType("aktif_sampai", "DATETIME");
    table.enu("status", ["aktif", "tidak_aktif"]);
    table
      .specificType("created_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP()"));
    table
      .specificType("updated_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()"));
    table.specificType("deleted_at", "DATETIME").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_tugas");
};
