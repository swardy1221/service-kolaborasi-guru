exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_anggota_grup", function (table) {
    table
      .integer("id_grup")
      .unsigned()
      .references("id_grup")
      .inTable("tb_grup")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .integer("id_user")
      .unsigned()
      .references("id_user")
      .inTable("tb_user")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.boolean("admin").notNullable().defaultTo(false);
    table.primary(["id_grup", "id_user"]);
    table
      .specificType("created_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP()"));
    table
      .specificType("updated_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()"));
    table.specificType("deleted_at", "DATETIME").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_anggota_grup");
};
