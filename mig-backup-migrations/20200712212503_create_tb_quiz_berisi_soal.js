exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_quiz_berisi_soal", function (table) {
    table
      .integer("id_quiz")
      .unsigned()
      .references("id_quiz")
      .inTable("tb_quiz")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .integer("id_soal")
      .unsigned()
      .references("id_soal")
      .inTable("tb_soal")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.integer("poin").unsigned();
    table.primary(["id_quiz", "id_soal"]);
    table
      .specificType("created_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP()"));
    table
      .specificType("updated_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()"));
    table.specificType("deleted_at", "DATETIME").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_quiz_berisi_soal");
};
