exports.up = function (knex, Promise) {
  return knex.schema.createTable(
    "tb_user_like_perangkat_pembelajaran",
    function (table) {
      table
        .integer("id_user")
        .unsigned()
        .references("id_user")
        .inTable("tb_user")
        .onDelete("CASCADE")
        .onUpdate("CASCADE");
      table.integer("id_perangkat_pembelajaran").unsigned();
      table
        .foreign("id_perangkat_pembelajaran", "id_perangkat_foreign_like")
        .references("id_perangkat_pembelajaran")
        .inTable("tb_perangkat_pembelajaran")
        .onDelete("CASCADE")
        .onUpdate("CASCADE");
      table.primary(["id_user", "id_perangkat_pembelajaran"]);
      table
        .specificType("created_at", "DATETIME")
        .notNullable()
        .defaultTo(knex.raw("CURRENT_TIMESTAMP()"));
      table
        .specificType("updated_at", "DATETIME")
        .notNullable()
        .defaultTo(
          knex.raw("CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()")
        );
      table.specificType("deleted_at", "DATETIME").nullable();
    }
  );
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_user_like_perangkat_pembelajaran");
};
