exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_grup", function (table) {
    table.increments("id_grup").unsigned().primary();
    table
      .integer("id_mata_pelajaran")
      .unsigned()
      .references("id_mata_pelajaran")
      .inTable("tb_mata_pelajaran")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .integer("dibuat_oleh")
      .unsigned()
      .references("id_user")
      .inTable("tb_user")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.enu("akses_grup", ["terbuka", "tertutup"]).notNullable();
    table.enu("tipe_grup", ["guru", "kelas"]).notNullable();
    table.string("kode", 100).unique();
    table.string("deskripsi").nullable();
    table.string("foto_grup").nullable();
    table.integer("jumlah_anggota").unsigned().nullable();
    table.integer("jumlah_maks_anggota").unsigned().nullable();
    table.integer("jumlah_soal").unsigned().nullable();
    table.integer("jumlah_bahan_ajar").unsigned().nullable();
    table.integer("jumlah_perangkat_pembelajaran").unsigned().nullable();
    table
      .specificType("created_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP()"));
    table
      .specificType("updated_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()"));
    table.specificType("deleted_at", "DATETIME").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_grup");
};
