exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_user_mengerjakan_tugas", function (table) {
    table
      .integer("id_user")
      .unsigned()
      .references("id_user")
      .inTable("tb_user")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .integer("id_tugas")
      .unsigned()
      .references("id_tugas")
      .inTable("tb_tugas")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .integer("diperiksa_oleh")
      .unsigned()
      .nullable()
      .references("id_user")
      .inTable("tb_user")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.string("file").notNullable();
    table.string("deskripsi").notNullable();
    table.string("feedback").nullable();
    table.integer("nilai").unsigned().nullable();
    table.primary(["id_user", "id_tugas"]);
    table
      .specificType("created_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP()"));
    table
      .specificType("updated_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()"));
    table.specificType("deleted_at", "DATETIME").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_user_mengerjakan_tugas");
};
