exports.up = function (knex, Promise) {
  return knex.schema.createTable(
    "tb_riwayat_revisi_perangkat_pembelajaran",
    function (table) {
      table
        .increments("id_riwayat_revisi_perangkat_pembelajaran")
        .unsigned()
        .primary();
      table.integer("id_perangkat_pembelajaran").unsigned();
      table
        .foreign("id_perangkat_pembelajaran", "id_perangkat_foreign_revisi")
        .references("id_perangkat_pembelajaran")
        .inTable("tb_perangkat_pembelajaran")
        .onDelete("CASCADE")
        .onUpdate("CASCADE");
      table.string("kode_revisi").unique().notNullable();
      table.string("catatan_revisi").notNullable();
      table
        .enu("tipe_perangkat_pembelajaran", [
          "prota",
          "prosem",
          "silabus",
          "rpp",
        ])
        .notNullable();
      table.string("deskripsi").notNullable();
      table.string("file").notNullable();
      table
        .specificType("created_at", "DATETIME")
        .notNullable()
        .defaultTo(knex.raw("CURRENT_TIMESTAMP()"));
      table
        .specificType("updated_at", "DATETIME")
        .notNullable()
        .defaultTo(
          knex.raw("CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()")
        );
      table.specificType("deleted_at", "DATETIME").nullable();
    }
  );
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_riwayat_revisi_perangkat_pembelajaran");
};
