exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_aktivitas_siswa", function (table) {
    table.increments("id_aktivitas_siswa").unsigned().primary();
    table
      .integer("id_grup")
      .unsigned()
      .references("id_grup")
      .inTable("tb_grup")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .integer("id_folder")
      .unsigned()
      .references("id_folder")
      .inTable("tb_folder")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.integer("id_entitas").unsigned().notNullable();
    table.string("nama_entitas").notNullable();
    table
      .specificType("created_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP()"));
    table
      .specificType("updated_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()"));
    table.specificType("deleted_at", "DATETIME").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_aktivitas_siswa");
};
