exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_quiz", function (table) {
    table.increments("id_quiz").unsigned().primary();
    table
      .integer("id_paket_soal")
      .unsigned()
      .nullable()
      .references("id_paket_soal")
      .inTable("tb_paket_soal")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .integer("id_grup")
      .unsigned()
      .references("id_grup")
      .inTable("tb_grup")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .enu("feedback", ["skor", "skor_jawaban", "skor_jawaban_feedback"])
      .notNullable();
    table.boolean("aktif").notNullable().defaultTo(false);
    table.boolean("review_jawaban").notNullable().defaultTo(true); // 1 = bisa cek ke soal sebelumnya, // 0 = tdk bisa
    table.string("nama_quiz").notNullable();
    table.boolean("acak_soal").notNullable().defaultTo(true);
    table.boolean("acak_opsi_jawaban").notNullable().defaultTo(true);
    table.boolean("limit_waktu").notNullable().defaultTo(true);
    table.time("durasi_pengerjaan").nullable();
    table.specificType("aktif_dari", "DATETIME").nullable();
    table.specificType("aktif_sampai", "DATETIME").nullable();
    table.integer("jumlah_pengerjaan").notNullable().defaultTo(1);
    table.boolean("bisa_tunda").notNullable().defaultTo(false);
    table.enu("skala_nilai", ["100", "10", "abc", "a+"]).notNullable();
    table
      .specificType("created_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP()"));
    table
      .specificType("updated_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()"));
    table.specificType("deleted_at", "DATETIME").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_quiz");
};
