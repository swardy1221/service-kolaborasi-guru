exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_riwayat_revisi_bahan_ajar", function (
    table
  ) {
    table.increments("id_riwayat_revisi_bahan_ajar").unsigned().primary();
    table
      .integer("id_bahan_ajar")
      .unsigned()
      .references("id_bahan_ajar")
      .inTable("tb_bahan_ajar")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.string("kode_revisi").unique().notNullable();
    table.string("catatan_revisi").notNullable();
    table
      .enu("tipe_bahan_ajar", ["presentasi", "modul", "video", "audio"])
      .notNullable();
    table.string("deskripsi").notNullable();
    table
      .enu("tipe_file", ["gambar", "dokumen", "audio", "video", "lainnya"])
      .nullable();
    table.string("file").nullable();
    table
      .specificType("created_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP()"));
    table
      .specificType("updated_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()"));
    table.specificType("deleted_at", "DATETIME").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_riwayat_revisi_bahan_ajar");
};
