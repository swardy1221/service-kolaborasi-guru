exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_user_message_user", function (table) {
    table
      .integer("id_user_pengirim")
      .unsigned()
      .references("id_user")
      .inTable("tb_user")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .integer("id_user_penerima")
      .unsigned()
      .references("id_user")
      .inTable("tb_user")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.string("pesan").notNullable();
    table.boolean("dibaca").notNullable().defaultTo(false);
    table.primary(["id_user_pengirim", "id_user_penerima"]);
    table
      .specificType("created_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP()"));
    table
      .specificType("updated_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()"));
    table.specificType("deleted_at", "DATETIME").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_user_message_user");
};
