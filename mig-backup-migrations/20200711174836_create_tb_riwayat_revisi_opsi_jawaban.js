exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_riwayat_revisi_opsi_jawaban", function (
    table
  ) {
    table.increments("id_riwayat_revisi_opsi_jawaban").unsigned().primary();
    table
      .integer("id_riwayat_revisi_soal")
      .unsigned()
      .references("id_riwayat_revisi_soal")
      .inTable("tb_riwayat_revisi_soal")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.enu("tipe_opsi", ["teks", "gambar", "audio", "video"]).notNullable();
    table.string("deskripsi").notNullable();
    table.enu("tipe_file", ["gambar", "audio", "video", "lainnya"]).nullable();
    table.string("file").nullable();
    table.string("feedback").notNullable();
    table.boolean("is_kunci").notNullable().defaultTo(false);
    table
      .specificType("created_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP()"));
    table
      .specificType("updated_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()"));
    table.specificType("deleted_at", "DATETIME").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_riwayat_revisi_opsi_jawaban");
};
