exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_kisi_kisi", function (table) {
    table.increments("id_kisi_kisi").unsigned().primary();
    table
      .integer("id_kompetensi_dasar")
      .unsigned()
      .references("id_kompetensi_dasar")
      .inTable("ref_kompetensi_dasar")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .integer("id_user")
      .unsigned()
      .references("id_user")
      .inTable("tb_user")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.string("deskripsi").notNullable();
    table
      .specificType("created_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP()"));
    table
      .specificType("updated_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()"));
    table.specificType("deleted_at", "DATETIME").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_kisi_kisi");
};
