exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_riwayat_revisi_soal", function (table) {
    table.increments("id_riwayat_revisi_soal").unsigned().primary();
    table.string("kode_revisi").unique().notNullable();
    table
      .integer("id_soal")
      .unsigned()
      .references("id_soal")
      .inTable("tb_soal")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.string("catatan_revisi").notNullable();
    table
      .enu("tipe_soal", ["pilihan_ganda", "isian_singkat", "benar_salah"])
      .notNullable();
    table.integer("jumlah_jawaban").notNullable();
    table.string("teks_soal").notNullable();
    table.enu("tipe_file", ["gambar", "audio", "video", "lainnya"]).nullable();
    table.string("file").nullable();
    table
      .enu("tingkat_kesulitan", ["c1", "c2", "c3", "c4", "c5", "c6"])
      .notNullable();
    table.string("kunci_essay").nullable();
    table
      .specificType("created_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP()"));
    table
      .specificType("updated_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()"));
    table.specificType("deleted_at", "DATETIME").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_riwayat_revisi_soal");
};
