exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_user_mengerjakan_quiz", function (table) {
    table
      .integer("id_user")
      .unsigned()
      .references("id_user")
      .inTable("tb_user")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .integer("id_quiz")
      .unsigned()
      .references("id_quiz")
      .inTable("tb_quiz")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.integer("quiz_ke").unsigned().notNullable().defaultTo(1);
    table.specificType("waktu_mulai", "DATETIME").nullable();
    table.specificType("waktu_selesai", "DATETIME").nullable();
    table
      .enu("status", ["sedang", "sudah", "pause", "belum"])
      .defaultTo("belum");
    table.string("hasil"); //sesuai pengaturan skala pada quiz, alternatif bisa handle didepan
    table.primary(["id_user", "id_quiz"]);
    table
      .specificType("created_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP()"));
    table
      .specificType("updated_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()"));
    table.specificType("deleted_at", "DATETIME").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_user_mengerjakan_quiz");
};
