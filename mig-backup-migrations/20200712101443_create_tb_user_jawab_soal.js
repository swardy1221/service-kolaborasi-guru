exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_user_jawab_soal", function (table) {
    table
      .integer("id_user")
      .unsigned()
      .references("id_user")
      .inTable("tb_user")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .integer("id_soal")
      .unsigned()
      .references("id_soal")
      .inTable("tb_soal")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .integer("id_opsi_jawaban")
      .unsigned()
      .references("id_opsi_jawaban")
      .inTable("tb_opsi_jawaban")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.string("jawaban_essay").nullable();
    table.boolean("benar").notNullable().defaultTo(false);
    table.primary(["id_user", "id_soal"]);
    table
      .specificType("created_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP()"));
    table
      .specificType("updated_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()"));
    table.specificType("deleted_at", "DATETIME").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_user_jawab_soal");
};
