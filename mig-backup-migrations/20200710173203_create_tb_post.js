exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_post", function (table) {
    table.increments("id_post").unsigned().primary();
    table
      .integer("id_user")
      .unsigned()
      .references("id_user")
      .inTable("tb_user")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .integer("id_grup")
      .unsigned()
      .references("id_grup")
      .inTable("tb_grup")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .enu("tipe_akses", ["public", "teman", "pribadi", "grup"])
      .notNullable();
    table.string("deskripsi").nullable();
    table
      .enu("tipe_file", ["gambar", "audio", "video", "dokumen", "lainnya"])
      .nullable();
    table.string("file").nullable();
    table.integer("jumlah_like").unsigned().nullable();
    table.integer("jumlah_komentar").unsigned().nullable();
    table
      .specificType("created_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP()"));
    table
      .specificType("updated_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()"));
    table.specificType("deleted_at", "DATETIME").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_post");
};
