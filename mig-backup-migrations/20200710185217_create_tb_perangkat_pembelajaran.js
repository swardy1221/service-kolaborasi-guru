exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_perangkat_pembelajaran", function (table) {
    table.increments("id_perangkat_pembelajaran").unsigned().primary();
    table
      .integer("id_user")
      .unsigned()
      .references("id_user")
      .inTable("tb_user")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .integer("id_grup")
      .unsigned()
      .references("id_grup")
      .inTable("tb_grup")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .enu("tipe_perangkat_pembelajaran", [
        "prota",
        "prosem",
        "silabus",
        "rpp",
        "lainnya",
      ])
      .notNullable();
    table.string("deskripsi").notNullable();
    table.string("file").nullable();
    table.integer("jumlah_like").unsigned().nullable();
    table.integer("jumlah_dislike").unsigned().nullable();
    table.integer("jumlah_komentar").unsigned().nullable();
    table.integer("diunduh_sebanyak").unsigned().nullable();
    table.enu("status", ["perbaikan", "siap_validasi", "valid"]).notNullable();
    table
      .integer("divalidasi_oleh")
      .unsigned()
      .nullable()
      .references("id_user")
      .inTable("tb_user")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .specificType("created_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP()"));
    table
      .specificType("updated_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()"));
    table.specificType("deleted_at", "DATETIME").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_perangkat_pembelajaran");
};
