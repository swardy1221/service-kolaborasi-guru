exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_objek_pemberitahuan", function (table) {
    table.increments("id_objek_pemberitahuan");
    table
      .integer("id_tipe_entitas_pemberitahuan")
      .unsigned()
      .references("id_tipe_entitas_pemberitahuan")
      .inTable("tb_tipe_entitas_pemberitahuan")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.integer("id_entitas").unsigned().notNullable();
    // table.boolean("pushed").notNullable().defaultTo(false);
    // solusi menggunakan immediate push dengan web socket
    table
      .specificType("created_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP()"));
    table
      .specificType("updated_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()"));
    table.specificType("deleted_at", "DATETIME").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_objek_pemberitahuan");
};
