exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_penerima_pemberitahuan", function (table) {
    table.increments("id_penerima_pemberitahuan");
    table
      .integer("id_user")
      .unsigned()
      .references("id_user")
      .inTable("tb_user")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .integer("id_objek_pemberitahuan")
      .unsigned()
      .references("id_objek_pemberitahuan")
      .inTable("tb_objek_pemberitahuan")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table.string("pesan").notNullable();
    table.string("tautan").notNullable();
    table.boolean("dilihat").notNullable().defaultTo(false);
    table
      .specificType("created_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP()"));
    table
      .specificType("updated_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()"));
    table.specificType("deleted_at", "DATETIME").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_penerima_pemberitahuan");
};
