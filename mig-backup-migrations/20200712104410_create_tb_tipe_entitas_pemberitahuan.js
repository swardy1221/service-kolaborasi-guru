exports.up = function (knex, Promise) {
  return knex.schema.createTable("tb_tipe_entitas_pemberitahuan", function (
    table
  ) {
    table.increments("id_tipe_entitas_pemberitahuan");
    table.string("nama_entitas").notNullable();
    table.string("deskripsi").notNullable();
    table
      .specificType("created_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP()"));
    table
      .specificType("updated_at", "DATETIME")
      .notNullable()
      .defaultTo(knex.raw("CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()"));
    table.specificType("deleted_at", "DATETIME").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tb_tipe_entitas_pemberitahuan");
};
