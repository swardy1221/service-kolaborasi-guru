const express = require("express");
const router = express.Router();

const { create } = require('./controllers/index');
const authUser = require('../middlewares/authUser');


router.post('/', authUser, create);


module.exports = router;