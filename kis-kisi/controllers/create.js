const response = require('../../config/responses');
const { create } = require('../services/index')
const controller = async (req, res) => {
    try {
        let {
            idKompetensiDasar,
            deskripsi
        } = req.body || "";
        let { idUser } = req.user || "";
        let dataKisi = {
            idKompetensiDasar,
            deskripsi,
            idUser
        }
        let createdKisi = await create(dataKisi);
        if (!createdKisi) return res.status(400).send(response.badRequest('gagal menambahkan kisi-kisi'));

        return res.status(201).send(response.customSuccess('berhasil menambahkan kisi-kisi'))
    } catch (error) {
        return res.status(500).send(response.serverError(error));
    }
}

module.exports = controller;