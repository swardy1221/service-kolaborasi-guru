const db = require('../../config/database');

const service = async (dataKisi) => {
    try {
        let createKisi = await db('tb_kisi_kisi').insert({
            id_kompetensi_dasar: dataKisi.idKompetensiDasar,
            id_user: dataKisi.idUser,
            deskripsi: dataKisi.deskripsi,
            created_at: new Date().getTime()
        })

        if (!createKisi) return false;

        return createKisi;
    } catch (error) {
        throw error;
    }
}
module.exports = service;