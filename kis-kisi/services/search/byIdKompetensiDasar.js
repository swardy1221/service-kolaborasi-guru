const db = require('../../../config/database');

const service = async (idKompetensiDasar) => {
    try {
        let kisiKisi = await db('tb_kisi_kisi').where('id_kompetensi_dasar', idKompetensiDasar);

        if (!kisiKisi.length) return false;

        return kisiKisi;
    } catch (error) {
        throw error;
    }
}

module.exports = service;