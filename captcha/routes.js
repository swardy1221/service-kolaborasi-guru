const express = require("express");
const router = express.Router();
const getRandom = require("./controllers/getRandom");
const getCaptchaById = require("./controllers/getCaptchaById");

//Route bagi Model Captcha
// /api/v1/captcha/
router.get("/", getRandom);
router.get("/:idCaptcha", getCaptchaById);

module.exports = router;
