const db = require("../../config/database");

const service = async (idCaptcha, captchaJawaban) => {
  try {
    const captcha = await db("ref_captcha")
      .where("id_captcha", idCaptcha)
      .first();

    if (captcha.jawaban == captchaJawaban) {
      return true;
    } else {
      return false;
    }
  } catch (error) {
    throw error;
  }
};

module.exports = service;
