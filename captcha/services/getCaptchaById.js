const db = require("../../config/database");

const service = async (id) => {
  try {
    let captcha = await db("ref_captcha")
      .select("*")
      .where("id_captcha", id)
      .first();

    if (!captcha) return false;
    return captcha;
  } catch (error) {
    throw error;
  }
};

module.exports = service;
