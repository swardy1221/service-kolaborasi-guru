const db = require("../../config/database");
const response = require("../../config/responses");

let getRandomService = async () => {
  try {
    const chaptchas = await db("ref_captcha").select("*");
    const randomCaptcha =
      chaptchas[Math.floor(Math.random() * chaptchas.length)];
    if (randomCaptcha) {
      return response.success(randomCaptcha, "captcha");
    } else {
      return response.notFound("captcha");
    }
  } catch (error) {
    return response.serverError(error);
  }
};

module.exports = getRandomService;
