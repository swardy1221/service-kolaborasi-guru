const getCaptchaById = require("../services/getCaptchaById");
const response = require("../../config/responses");

const controller = async (req, res) => {
  try {
    let idCaptcha = req.params.idCaptcha || "";
    let captcha = await getCaptchaById(idCaptcha);

    if (!captcha) return res.status(404).send(response.notFound("captcha"));
    return res.status(200).send(response.success(captcha, "captcha"));
  } catch (error) {
    return res.status(500).send(response.serverError(error));
  }
};

module.exports = controller;
