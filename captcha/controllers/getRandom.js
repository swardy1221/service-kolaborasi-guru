const getRandomService = require("../services/getRandom");

const getRandomController = async (req, res) => {
  try {
    let hasil = await getRandomService();
    res.status(hasil.code).send(hasil);
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = getRandomController;
