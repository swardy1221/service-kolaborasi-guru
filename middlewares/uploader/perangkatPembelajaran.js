const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        if (file.fieldname == 'sampul') {
            cb(null, "public/perangkat-ajar/sampul");
        } else {
            cb(null, "public/perangkat-ajar/file");
        }
    },
    filename: function (req, file, cb) {
        if (file.fieldname == 'sampul') {
            cb(null, new Date().getTime() + "-" + req.body.tipePerangkatPembelajaran + path.extname(file.originalname));
        } else {
            cb(null, new Date().getTime() + "-" + req.body.tipePerangkatPembelajaran + "-" + req.body.statusDokumen + path.extname(file.originalname));
        }
    },
});

const uploader = multer({
    storage: storage,
    // fileFilter: fileFilter
});

module.exports = uploader;