const multer = require('multer');
const path = require('path');
const reverseDateString = require('../../helpers/reverseDateString');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, "public/soal");
        // if (file.fieldname == 'sampul') {
        // } else {
        //     cb(null, "public/soal");
        // }
    },
    filename: function (req, file, cb) {
        cb(null, reverseDateString() + "-" + req.user.idUser + "-" + req.body.idMataPelajaran + path.extname(file ? file.originalname : '.jpg'));
        // if (file.fieldname == 'sampul') {
        // } else {
        //     cb(null, reverseDateString() + req.user.idUser + "-" + req.body.tipeBahanAjar + "-" + req.body.statusDokumen + path.extname(file.originalname));
        // }
    },
});

const uploader = multer({
    storage: storage,
    // fileFilter: fileFilter
});

module.exports = uploader;