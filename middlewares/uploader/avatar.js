const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, "public/image/avatar");
    },
    filename: function (req, file, cb) {
        cb(null, new Date().getTime() + "-" + req.user.idUser + "-avatar" + path.extname(file.originalname));
    },
});

const uploader = multer({
    storage: storage,
    // fileFilter: fileFilter
});

const fileFilter = (req, file, cb) => {

    // The function should call `cb` with a boolean
    // to indicate if the file should be accepted
    if (file.size > 2048) return true

    // To reject this file pass `false`, like so:
    cb(null, false)

    // To accept the file pass `true`, like so:
    cb(null, true)

    // You can always pass an error if something goes wrong:
    cb(new Error('I don\'t have a clue!'))

}

module.exports = uploader;