const multer = require('multer');
const path = require('path');
const reverseDateString = require('../../helpers/reverseDateString');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        if (file.fieldname == 'sampul') {
            cb(null, "public/paket-soal/sampul");
        } else {
            cb(null, "public/paket-soal/file");
        }
    },
    filename: function (req, file, cb) {
        if (file.fieldname == 'sampul') {
            cb(null, reverseDateString() + req.user.idUser + "-" + req.body.idMataPelajaran + path.extname(file.originalname));
        } else {
            cb(null, reverseDateString() + req.user.idUser + "-" + req.body.idMataPelajaran + "-" + req.body.statusDokumen + path.extname(file.originalname));
        }
    },
});

const uploader = multer({
    storage: storage,
    // fileFilter: fileFilter
});

module.exports = uploader;