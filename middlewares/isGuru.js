const response = require("../config/responses");
const middleware = async (req, res, next) => {
    if (req.user.role != 'guru') return res.send(response.unauthorized("anda bukan guru"));
    next()
}

module.exports = middleware;