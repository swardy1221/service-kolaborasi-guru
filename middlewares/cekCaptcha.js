const db = require("../config/database");

const service = (req, res, next) => {
  try {
    let { idCaptcha, captchaJawaban } = req.body || "";
    const captcha = db("ref_captcha").where("id_captcha", idCaptcha).first();
    captcha
      .then((captcha) => {
        if (captcha.jawaban != captchaJawaban)
          return res.status(400).send({ code: 400, message: "Captcha Salah!" });
        // console.log("captcha lewat");
        next();
      })
      .catch((error) => {
        throw error;
      });
  } catch (error) {
    throw error;
  }
};

module.exports = service;
