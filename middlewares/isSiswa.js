const response = require("../config/responses");
const middleware = async (req, res, next) => {
    if (req.user.role != 'siswa') return res.send(response.unauthorized("anda bukan siswa"));
    next()
}

module.exports = middleware;