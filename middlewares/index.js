const fs = require("fs");
const path = require("path");

// const folderName = __dirname;
// const foundFiles = {};
/*
let items = fs.readdirSync(folderName, { withFileTypes: true });
items.forEach((item) => {
  if (item.isDirectory()) {
    // Belum mendukung nested folder
    // Selanjutnya dibuat nested dengan separator titik,
    // supaya mengikuti konvensi JS
    console.log(`${item.name} is a folder`);
  } else {
    if (item.name !== "index.js") {
      const namaService = item.name.slice(0, -3);
      const fullPath = path.join(folderName, namaService);
      foundFiles[namaService] = require(fullPath);
    }
  }
});
module.exports = { ...foundFiles };
*/
const findFiles = (folderName, foundFiles = {}) => {
    // async function findFiles(folderName) {
    let items = fs.readdirSync(folderName, { withFileTypes: true });
    // console.log(items);
    items.forEach((item) => {
        if (item.isDirectory()) {
            let foundFolder = (foundFiles[item.name] = {});
            findFiles(`${folderName}/${item.name}`, foundFolder);
        } else {
            if (item.name !== "index.js") {
                const namaService = item.name.slice(0, -3);
                const fullPath = path.join(folderName, namaService);
                foundFiles[namaService] = require(fullPath);
            }
            // console.log(item);
            // const namaService = item.name.slice(0, -3);
            // const fullPath = path.join(folderName, namaService);
            // console.log(fullPath);
            // console.log(service);
            // foundFiles[namaService] = require(fullPath);
            // console.log(`Found file: ${item.name} in folder ${folderName}`);
        }
    });
    // console.log(foundFiles);

    return foundFiles;
};

// main();
// findFiles(__dirname);
const hasil = findFiles(__dirname);
module.exports = { ...hasil };
// console.log(await findFiles(__dirname));
// console.log(router);
