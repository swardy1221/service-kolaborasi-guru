const response = require("../config/responses");

const middleware = async (req, res, next) => {
    // console.log('params: ' + req.params.idUser);
    // console.log('req.user: ' + req.user.id_user);
    let idUser = req.params.idUser || null;
    if (req.user.idUser != idUser) return res.send(response.forbidden("anda tidak berhak atas data ini"));
    next()
}

module.exports = middleware;