const db = require('../config/database');
const response = require('../config/responses');

const middleware = async (req, res, next) => {
    try {
        let isVerified = await db('tb_user').where('email', req.body.email).where('verified_at', '<>', "").first();
        if (!isVerified) return res.send(response.unauthorized('akun belum diverifikasi'));
        next();
    } catch (error) {
        return res.status(500).send(response.serverError(error));
    }
}

module.exports = middleware;