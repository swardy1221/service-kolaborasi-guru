const jwt = require("jsonwebtoken");
const {
  ACCESS_TOKEN_SECRET,
  REFRESH_TOKEN_SECRET,
} = require("../config/authsecret");
const response = require("../config/responses");
const getUserById = require("../user/services/getUserById");

const middleware = (req, res, next) => {
  const authHeader = req.headers["authorization"] || "";
  // console.log(authHeader)
  const token = authHeader && authHeader.split(" ")[1];
  // console.log(token)
  if (token == null) return res.send(response.unauthorized());
  // if (token == null) return res.sendStatus(401);

  jwt.verify(token, ACCESS_TOKEN_SECRET, (err, user) => {
    // if (err) return res.sendStatus(403); //invalid token
    // console.log(err)
    if (err) return res.send(response.unauthorized("token invalid")); //invalid token
    // getUserById(user.idUser).then((user) => {
    //   if (user.refresh_token == null)
    //     return res.send(response.unauthorized("token invalid"));
    //   req.user = user;
    //   // console.log(req.user)
    //   next();
    // });
    req.user = user;
    // console.log(req.user)
    next();
  });
};

module.exports = middleware;
