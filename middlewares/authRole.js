const response = require("../config/responses");
const middleware = role => {
    return (req, res, next) => {
        console.log(req.user)
        switch (role) {
            case 'admin':
                if (req.user.isAdmin != 1) return res.status(401).send(response.unauthorized(`anda bukan ${role}`));
                return next();
            case 'guru':
                if (req.user.role != role) return res.status(401).send(response.unauthorized(`anda bukan ${role}`));
                return next();
            default:
                break;
        }
    }
}

module.exports = middleware;